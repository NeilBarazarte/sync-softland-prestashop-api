class PsLinksmenutopLangsController < ApplicationController
  before_action :set_ps_linksmenutop_lang, only: [:show, :update, :destroy]

  # GET /ps_linksmenutop_langs
  def index
    @ps_linksmenutop_langs = PsLinksmenutopLang.all

    render json: @ps_linksmenutop_langs
  end

  # GET /ps_linksmenutop_langs/1
  def show
    render json: @ps_linksmenutop_lang
  end

  # POST /ps_linksmenutop_langs
  def create
    @ps_linksmenutop_lang = PsLinksmenutopLang.new(ps_linksmenutop_lang_params)

    if @ps_linksmenutop_lang.save
      render json: @ps_linksmenutop_lang, status: :created, location: @ps_linksmenutop_lang
    else
      render json: @ps_linksmenutop_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_linksmenutop_langs/1
  def update
    if @ps_linksmenutop_lang.update(ps_linksmenutop_lang_params)
      render json: @ps_linksmenutop_lang
    else
      render json: @ps_linksmenutop_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_linksmenutop_langs/1
  def destroy
    @ps_linksmenutop_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_linksmenutop_lang
      @ps_linksmenutop_lang = PsLinksmenutopLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_linksmenutop_lang_params
      params.fetch(:ps_linksmenutop_lang, {})
    end
end
