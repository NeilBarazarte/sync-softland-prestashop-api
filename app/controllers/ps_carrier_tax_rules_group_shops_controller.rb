class PsCarrierTaxRulesGroupShopsController < ApplicationController
  before_action :set_ps_carrier_tax_rules_group_shop, only: [:show, :update, :destroy]

  # GET /ps_carrier_tax_rules_group_shops
  def index
    @ps_carrier_tax_rules_group_shops = PsCarrierTaxRulesGroupShop.all

    render json: @ps_carrier_tax_rules_group_shops
  end

  # GET /ps_carrier_tax_rules_group_shops/1
  def show
    render json: @ps_carrier_tax_rules_group_shop
  end

  # POST /ps_carrier_tax_rules_group_shops
  def create
    @ps_carrier_tax_rules_group_shop = PsCarrierTaxRulesGroupShop.new(ps_carrier_tax_rules_group_shop_params)

    if @ps_carrier_tax_rules_group_shop.save
      render json: @ps_carrier_tax_rules_group_shop, status: :created, location: @ps_carrier_tax_rules_group_shop
    else
      render json: @ps_carrier_tax_rules_group_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_carrier_tax_rules_group_shops/1
  def update
    if @ps_carrier_tax_rules_group_shop.update(ps_carrier_tax_rules_group_shop_params)
      render json: @ps_carrier_tax_rules_group_shop
    else
      render json: @ps_carrier_tax_rules_group_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_carrier_tax_rules_group_shops/1
  def destroy
    @ps_carrier_tax_rules_group_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_carrier_tax_rules_group_shop
      @ps_carrier_tax_rules_group_shop = PsCarrierTaxRulesGroupShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_carrier_tax_rules_group_shop_params
      params.fetch(:ps_carrier_tax_rules_group_shop, {})
    end
end
