class PsWarehousesController < ApplicationController
  before_action :set_ps_warehouse, only: [:show, :update, :destroy]

  # GET /ps_warehouses
  def index
    @ps_warehouses = PsWarehouse.all

    render json: @ps_warehouses
  end

  # GET /ps_warehouses/1
  def show
    render json: @ps_warehouse
  end

  # POST /ps_warehouses
  def create
    @ps_warehouse = PsWarehouse.new(ps_warehouse_params)

    if @ps_warehouse.save
      render json: @ps_warehouse, status: :created, location: @ps_warehouse
    else
      render json: @ps_warehouse.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_warehouses/1
  def update
    if @ps_warehouse.update(ps_warehouse_params)
      render json: @ps_warehouse
    else
      render json: @ps_warehouse.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_warehouses/1
  def destroy
    @ps_warehouse.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_warehouse
      @ps_warehouse = PsWarehouse.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_warehouse_params
      params.fetch(:ps_warehouse, {})
    end
end
