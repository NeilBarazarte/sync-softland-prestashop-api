class PsOrderStatesController < ApplicationController
  before_action :set_ps_order_state, only: [:show, :update, :destroy]

  # GET /ps_order_states
  def index
    @ps_order_states = PsOrderState.all

    render json: @ps_order_states
  end

  # GET /ps_order_states/1
  def show
    render json: @ps_order_state
  end

  # POST /ps_order_states
  def create
    @ps_order_state = PsOrderState.new(ps_order_state_params)

    if @ps_order_state.save
      render json: @ps_order_state, status: :created, location: @ps_order_state
    else
      render json: @ps_order_state.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_states/1
  def update
    if @ps_order_state.update(ps_order_state_params)
      render json: @ps_order_state
    else
      render json: @ps_order_state.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_states/1
  def destroy
    @ps_order_state.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_state
      @ps_order_state = PsOrderState.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_state_params
      params.fetch(:ps_order_state, {})
    end
end
