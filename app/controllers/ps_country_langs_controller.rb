class PsCountryLangsController < ApplicationController
  before_action :set_ps_country_lang, only: [:show, :update, :destroy]

  # GET /ps_country_langs
  def index
    @ps_country_langs = PsCountryLang.all

    render json: @ps_country_langs
  end

  # GET /ps_country_langs/1
  def show
    render json: @ps_country_lang
  end

  # POST /ps_country_langs
  def create
    @ps_country_lang = PsCountryLang.new(ps_country_lang_params)

    if @ps_country_lang.save
      render json: @ps_country_lang, status: :created, location: @ps_country_lang
    else
      render json: @ps_country_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_country_langs/1
  def update
    if @ps_country_lang.update(ps_country_lang_params)
      render json: @ps_country_lang
    else
      render json: @ps_country_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_country_langs/1
  def destroy
    @ps_country_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_country_lang
      @ps_country_lang = PsCountryLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_country_lang_params
      params.fetch(:ps_country_lang, {})
    end
end
