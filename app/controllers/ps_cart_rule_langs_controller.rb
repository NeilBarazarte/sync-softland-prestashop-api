class PsCartRuleLangsController < ApplicationController
  before_action :set_ps_cart_rule_lang, only: [:show, :update, :destroy]

  # GET /ps_cart_rule_langs
  def index
    @ps_cart_rule_langs = PsCartRuleLang.all

    render json: @ps_cart_rule_langs
  end

  # GET /ps_cart_rule_langs/1
  def show
    render json: @ps_cart_rule_lang
  end

  # POST /ps_cart_rule_langs
  def create
    @ps_cart_rule_lang = PsCartRuleLang.new(ps_cart_rule_lang_params)

    if @ps_cart_rule_lang.save
      render json: @ps_cart_rule_lang, status: :created, location: @ps_cart_rule_lang
    else
      render json: @ps_cart_rule_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_rule_langs/1
  def update
    if @ps_cart_rule_lang.update(ps_cart_rule_lang_params)
      render json: @ps_cart_rule_lang
    else
      render json: @ps_cart_rule_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_rule_langs/1
  def destroy
    @ps_cart_rule_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_rule_lang
      @ps_cart_rule_lang = PsCartRuleLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_rule_lang_params
      params.fetch(:ps_cart_rule_lang, {})
    end
end
