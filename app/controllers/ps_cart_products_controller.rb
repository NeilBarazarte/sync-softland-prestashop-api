class PsCartProductsController < ApplicationController
  before_action :set_ps_cart_product, only: [:show, :update, :destroy]

  # GET /ps_cart_products
  def index
    @ps_cart_products = PsCartProduct.all

    render json: @ps_cart_products
  end

  # GET /ps_cart_products/1
  def show
    render json: @ps_cart_product
  end

  # POST /ps_cart_products
  def create
    @ps_cart_product = PsCartProduct.new(ps_cart_product_params)

    if @ps_cart_product.save
      render json: @ps_cart_product, status: :created, location: @ps_cart_product
    else
      render json: @ps_cart_product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_products/1
  def update
    if @ps_cart_product.update(ps_cart_product_params)
      render json: @ps_cart_product
    else
      render json: @ps_cart_product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_products/1
  def destroy
    @ps_cart_product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_product
      @ps_cart_product = PsCartProduct.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_product_params
      params.fetch(:ps_cart_product, {})
    end
end
