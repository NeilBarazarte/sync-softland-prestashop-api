class PsReassurancesController < ApplicationController
  before_action :set_ps_reassurance, only: [:show, :update, :destroy]

  # GET /ps_reassurances
  def index
    @ps_reassurances = PsReassurance.all

    render json: @ps_reassurances
  end

  # GET /ps_reassurances/1
  def show
    render json: @ps_reassurance
  end

  # POST /ps_reassurances
  def create
    @ps_reassurance = PsReassurance.new(ps_reassurance_params)

    if @ps_reassurance.save
      render json: @ps_reassurance, status: :created, location: @ps_reassurance
    else
      render json: @ps_reassurance.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_reassurances/1
  def update
    if @ps_reassurance.update(ps_reassurance_params)
      render json: @ps_reassurance
    else
      render json: @ps_reassurance.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_reassurances/1
  def destroy
    @ps_reassurance.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_reassurance
      @ps_reassurance = PsReassurance.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_reassurance_params
      params.fetch(:ps_reassurance, {})
    end
end
