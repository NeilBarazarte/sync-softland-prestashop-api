class PsOrderReturnStatesController < ApplicationController
  before_action :set_ps_order_return_state, only: [:show, :update, :destroy]

  # GET /ps_order_return_states
  def index
    @ps_order_return_states = PsOrderReturnState.all

    render json: @ps_order_return_states
  end

  # GET /ps_order_return_states/1
  def show
    render json: @ps_order_return_state
  end

  # POST /ps_order_return_states
  def create
    @ps_order_return_state = PsOrderReturnState.new(ps_order_return_state_params)

    if @ps_order_return_state.save
      render json: @ps_order_return_state, status: :created, location: @ps_order_return_state
    else
      render json: @ps_order_return_state.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_return_states/1
  def update
    if @ps_order_return_state.update(ps_order_return_state_params)
      render json: @ps_order_return_state
    else
      render json: @ps_order_return_state.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_return_states/1
  def destroy
    @ps_order_return_state.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_return_state
      @ps_order_return_state = PsOrderReturnState.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_return_state_params
      params.fetch(:ps_order_return_state, {})
    end
end
