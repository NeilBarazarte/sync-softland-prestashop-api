class PsTabLangsController < ApplicationController
  before_action :set_ps_tab_lang, only: [:show, :update, :destroy]

  # GET /ps_tab_langs
  def index
    @ps_tab_langs = PsTabLang.all

    render json: @ps_tab_langs
  end

  # GET /ps_tab_langs/1
  def show
    render json: @ps_tab_lang
  end

  # POST /ps_tab_langs
  def create
    @ps_tab_lang = PsTabLang.new(ps_tab_lang_params)

    if @ps_tab_lang.save
      render json: @ps_tab_lang, status: :created, location: @ps_tab_lang
    else
      render json: @ps_tab_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tab_langs/1
  def update
    if @ps_tab_lang.update(ps_tab_lang_params)
      render json: @ps_tab_lang
    else
      render json: @ps_tab_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tab_langs/1
  def destroy
    @ps_tab_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tab_lang
      @ps_tab_lang = PsTabLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tab_lang_params
      params.fetch(:ps_tab_lang, {})
    end
end
