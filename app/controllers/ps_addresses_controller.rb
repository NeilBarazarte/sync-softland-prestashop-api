class PsAddressesController < ApplicationController
  before_action :set_ps_address, only: [:show, :update, :destroy]

  # GET /ps_addresses
  def index
    @ps_addresses = PsAddress.all

    render json: @ps_addresses
  end

  # GET /ps_addresses/1
  def show
    render json: @ps_address
  end

  # POST /ps_addresses
  def create
    @ps_address = PsAddress.new(ps_address_params)

    if @ps_address.save
      render json: @ps_address, status: :created, location: @ps_address
    else
      render json: @ps_address.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_addresses/1
  def update
    if @ps_address.update(ps_address_params)
      render json: @ps_address
    else
      render json: @ps_address.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_addresses/1
  def destroy
    @ps_address.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_address
      @ps_address = PsAddress.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_address_params
      params.fetch(:ps_address, {})
    end
end
