class PsModulesController < ApplicationController
  before_action :set_ps_module, only: [:show, :update, :destroy]

  # GET /ps_modules
  def index
    @ps_modules = PsModule.all

    render json: @ps_modules
  end

  # GET /ps_modules/1
  def show
    render json: @ps_module
  end

  # POST /ps_modules
  def create
    @ps_module = PsModule.new(ps_module_params)

    if @ps_module.save
      render json: @ps_module, status: :created, location: @ps_module
    else
      render json: @ps_module.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_modules/1
  def update
    if @ps_module.update(ps_module_params)
      render json: @ps_module
    else
      render json: @ps_module.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_modules/1
  def destroy
    @ps_module.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_module
      @ps_module = PsModule.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_module_params
      params.fetch(:ps_module, {})
    end
end
