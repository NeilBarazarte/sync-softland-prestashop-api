class PsDeliveriesController < ApplicationController
  before_action :set_ps_delivery, only: [:show, :update, :destroy]

  # GET /ps_deliveries
  def index
    @ps_deliveries = PsDelivery.all

    render json: @ps_deliveries
  end

  # GET /ps_deliveries/1
  def show
    render json: @ps_delivery
  end

  # POST /ps_deliveries
  def create
    @ps_delivery = PsDelivery.new(ps_delivery_params)

    if @ps_delivery.save
      render json: @ps_delivery, status: :created, location: @ps_delivery
    else
      render json: @ps_delivery.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_deliveries/1
  def update
    if @ps_delivery.update(ps_delivery_params)
      render json: @ps_delivery
    else
      render json: @ps_delivery.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_deliveries/1
  def destroy
    @ps_delivery.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_delivery
      @ps_delivery = PsDelivery.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_delivery_params
      params.fetch(:ps_delivery, {})
    end
end
