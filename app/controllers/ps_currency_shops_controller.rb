class PsCurrencyShopsController < ApplicationController
  before_action :set_ps_currency_shop, only: [:show, :update, :destroy]

  # GET /ps_currency_shops
  def index
    @ps_currency_shops = PsCurrencyShop.all

    render json: @ps_currency_shops
  end

  # GET /ps_currency_shops/1
  def show
    render json: @ps_currency_shop
  end

  # POST /ps_currency_shops
  def create
    @ps_currency_shop = PsCurrencyShop.new(ps_currency_shop_params)

    if @ps_currency_shop.save
      render json: @ps_currency_shop, status: :created, location: @ps_currency_shop
    else
      render json: @ps_currency_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_currency_shops/1
  def update
    if @ps_currency_shop.update(ps_currency_shop_params)
      render json: @ps_currency_shop
    else
      render json: @ps_currency_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_currency_shops/1
  def destroy
    @ps_currency_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_currency_shop
      @ps_currency_shop = PsCurrencyShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_currency_shop_params
      params.fetch(:ps_currency_shop, {})
    end
end
