class PsCustomizationsController < ApplicationController
  before_action :set_ps_customization, only: [:show, :update, :destroy]

  # GET /ps_customizations
  def index
    @ps_customizations = PsCustomization.all

    render json: @ps_customizations
  end

  # GET /ps_customizations/1
  def show
    render json: @ps_customization
  end

  # POST /ps_customizations
  def create
    @ps_customization = PsCustomization.new(ps_customization_params)

    if @ps_customization.save
      render json: @ps_customization, status: :created, location: @ps_customization
    else
      render json: @ps_customization.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_customizations/1
  def update
    if @ps_customization.update(ps_customization_params)
      render json: @ps_customization
    else
      render json: @ps_customization.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_customizations/1
  def destroy
    @ps_customization.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_customization
      @ps_customization = PsCustomization.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_customization_params
      params.fetch(:ps_customization, {})
    end
end
