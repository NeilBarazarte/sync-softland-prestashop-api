class PsCmsCategoriesController < ApplicationController
  before_action :set_ps_cms_category, only: [:show, :update, :destroy]

  # GET /ps_cms_categories
  def index
    @ps_cms_categories = PsCmsCategory.all

    render json: @ps_cms_categories
  end

  # GET /ps_cms_categories/1
  def show
    render json: @ps_cms_category
  end

  # POST /ps_cms_categories
  def create
    @ps_cms_category = PsCmsCategory.new(ps_cms_category_params)

    if @ps_cms_category.save
      render json: @ps_cms_category, status: :created, location: @ps_cms_category
    else
      render json: @ps_cms_category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cms_categories/1
  def update
    if @ps_cms_category.update(ps_cms_category_params)
      render json: @ps_cms_category
    else
      render json: @ps_cms_category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cms_categories/1
  def destroy
    @ps_cms_category.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cms_category
      @ps_cms_category = PsCmsCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cms_category_params
      params.fetch(:ps_cms_category, {})
    end
end
