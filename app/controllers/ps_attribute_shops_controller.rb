class PsAttributeShopsController < ApplicationController
  before_action :set_ps_attribute_shop, only: [:show, :update, :destroy]

  # GET /ps_attribute_shops
  def index
    @ps_attribute_shops = PsAttributeShop.all

    render json: @ps_attribute_shops
  end

  # GET /ps_attribute_shops/1
  def show
    render json: @ps_attribute_shop
  end

  # POST /ps_attribute_shops
  def create
    @ps_attribute_shop = PsAttributeShop.new(ps_attribute_shop_params)

    if @ps_attribute_shop.save
      render json: @ps_attribute_shop, status: :created, location: @ps_attribute_shop
    else
      render json: @ps_attribute_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_attribute_shops/1
  def update
    if @ps_attribute_shop.update(ps_attribute_shop_params)
      render json: @ps_attribute_shop
    else
      render json: @ps_attribute_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_attribute_shops/1
  def destroy
    @ps_attribute_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_attribute_shop
      @ps_attribute_shop = PsAttributeShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_attribute_shop_params
      params.fetch(:ps_attribute_shop, {})
    end
end
