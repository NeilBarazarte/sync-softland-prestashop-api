class PsModuleAccessesController < ApplicationController
  before_action :set_ps_module_access, only: [:show, :update, :destroy]

  # GET /ps_module_accesses
  def index
    @ps_module_accesses = PsModuleAccess.all

    render json: @ps_module_accesses
  end

  # GET /ps_module_accesses/1
  def show
    render json: @ps_module_access
  end

  # POST /ps_module_accesses
  def create
    @ps_module_access = PsModuleAccess.new(ps_module_access_params)

    if @ps_module_access.save
      render json: @ps_module_access, status: :created, location: @ps_module_access
    else
      render json: @ps_module_access.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_module_accesses/1
  def update
    if @ps_module_access.update(ps_module_access_params)
      render json: @ps_module_access
    else
      render json: @ps_module_access.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_module_accesses/1
  def destroy
    @ps_module_access.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_module_access
      @ps_module_access = PsModuleAccess.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_module_access_params
      params.fetch(:ps_module_access, {})
    end
end
