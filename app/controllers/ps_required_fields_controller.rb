class PsRequiredFieldsController < ApplicationController
  before_action :set_ps_required_field, only: [:show, :update, :destroy]

  # GET /ps_required_fields
  def index
    @ps_required_fields = PsRequiredField.all

    render json: @ps_required_fields
  end

  # GET /ps_required_fields/1
  def show
    render json: @ps_required_field
  end

  # POST /ps_required_fields
  def create
    @ps_required_field = PsRequiredField.new(ps_required_field_params)

    if @ps_required_field.save
      render json: @ps_required_field, status: :created, location: @ps_required_field
    else
      render json: @ps_required_field.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_required_fields/1
  def update
    if @ps_required_field.update(ps_required_field_params)
      render json: @ps_required_field
    else
      render json: @ps_required_field.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_required_fields/1
  def destroy
    @ps_required_field.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_required_field
      @ps_required_field = PsRequiredField.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_required_field_params
      params.fetch(:ps_required_field, {})
    end
end
