class PsOperatingSystemsController < ApplicationController
  before_action :set_ps_operating_system, only: [:show, :update, :destroy]

  # GET /ps_operating_systems
  def index
    @ps_operating_systems = PsOperatingSystem.all

    render json: @ps_operating_systems
  end

  # GET /ps_operating_systems/1
  def show
    render json: @ps_operating_system
  end

  # POST /ps_operating_systems
  def create
    @ps_operating_system = PsOperatingSystem.new(ps_operating_system_params)

    if @ps_operating_system.save
      render json: @ps_operating_system, status: :created, location: @ps_operating_system
    else
      render json: @ps_operating_system.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_operating_systems/1
  def update
    if @ps_operating_system.update(ps_operating_system_params)
      render json: @ps_operating_system
    else
      render json: @ps_operating_system.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_operating_systems/1
  def destroy
    @ps_operating_system.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_operating_system
      @ps_operating_system = PsOperatingSystem.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_operating_system_params
      params.fetch(:ps_operating_system, {})
    end
end
