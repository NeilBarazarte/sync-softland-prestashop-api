class PsHookModuleExceptionsController < ApplicationController
  before_action :set_ps_hook_module_exception, only: [:show, :update, :destroy]

  # GET /ps_hook_module_exceptions
  def index
    @ps_hook_module_exceptions = PsHookModuleException.all

    render json: @ps_hook_module_exceptions
  end

  # GET /ps_hook_module_exceptions/1
  def show
    render json: @ps_hook_module_exception
  end

  # POST /ps_hook_module_exceptions
  def create
    @ps_hook_module_exception = PsHookModuleException.new(ps_hook_module_exception_params)

    if @ps_hook_module_exception.save
      render json: @ps_hook_module_exception, status: :created, location: @ps_hook_module_exception
    else
      render json: @ps_hook_module_exception.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_hook_module_exceptions/1
  def update
    if @ps_hook_module_exception.update(ps_hook_module_exception_params)
      render json: @ps_hook_module_exception
    else
      render json: @ps_hook_module_exception.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_hook_module_exceptions/1
  def destroy
    @ps_hook_module_exception.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_hook_module_exception
      @ps_hook_module_exception = PsHookModuleException.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_hook_module_exception_params
      params.fetch(:ps_hook_module_exception, {})
    end
end
