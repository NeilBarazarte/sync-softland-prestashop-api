class PsImageTypesController < ApplicationController
  before_action :set_ps_image_type, only: [:show, :update, :destroy]

  # GET /ps_image_types
  def index
    @ps_image_types = PsImageType.all

    render json: @ps_image_types
  end

  # GET /ps_image_types/1
  def show
    render json: @ps_image_type
  end

  # POST /ps_image_types
  def create
    @ps_image_type = PsImageType.new(ps_image_type_params)

    if @ps_image_type.save
      render json: @ps_image_type, status: :created, location: @ps_image_type
    else
      render json: @ps_image_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_image_types/1
  def update
    if @ps_image_type.update(ps_image_type_params)
      render json: @ps_image_type
    else
      render json: @ps_image_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_image_types/1
  def destroy
    @ps_image_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_image_type
      @ps_image_type = PsImageType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_image_type_params
      params.fetch(:ps_image_type, {})
    end
end
