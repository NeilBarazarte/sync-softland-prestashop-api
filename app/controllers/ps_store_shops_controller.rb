class PsStoreShopsController < ApplicationController
  before_action :set_ps_store_shop, only: [:show, :update, :destroy]

  # GET /ps_store_shops
  def index
    @ps_store_shops = PsStoreShop.all

    render json: @ps_store_shops
  end

  # GET /ps_store_shops/1
  def show
    render json: @ps_store_shop
  end

  # POST /ps_store_shops
  def create
    @ps_store_shop = PsStoreShop.new(ps_store_shop_params)

    if @ps_store_shop.save
      render json: @ps_store_shop, status: :created, location: @ps_store_shop
    else
      render json: @ps_store_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_store_shops/1
  def update
    if @ps_store_shop.update(ps_store_shop_params)
      render json: @ps_store_shop
    else
      render json: @ps_store_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_store_shops/1
  def destroy
    @ps_store_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_store_shop
      @ps_store_shop = PsStoreShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_store_shop_params
      params.fetch(:ps_store_shop, {})
    end
end
