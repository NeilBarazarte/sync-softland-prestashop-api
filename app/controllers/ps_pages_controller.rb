class PsPagesController < ApplicationController
  before_action :set_ps_page, only: [:show, :update, :destroy]

  # GET /ps_pages
  def index
    @ps_pages = PsPage.all

    render json: @ps_pages
  end

  # GET /ps_pages/1
  def show
    render json: @ps_page
  end

  # POST /ps_pages
  def create
    @ps_page = PsPage.new(ps_page_params)

    if @ps_page.save
      render json: @ps_page, status: :created, location: @ps_page
    else
      render json: @ps_page.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_pages/1
  def update
    if @ps_page.update(ps_page_params)
      render json: @ps_page
    else
      render json: @ps_page.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_pages/1
  def destroy
    @ps_page.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_page
      @ps_page = PsPage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_page_params
      params.fetch(:ps_page, {})
    end
end
