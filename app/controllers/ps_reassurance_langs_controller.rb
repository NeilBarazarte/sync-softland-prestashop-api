class PsReassuranceLangsController < ApplicationController
  before_action :set_ps_reassurance_lang, only: [:show, :update, :destroy]

  # GET /ps_reassurance_langs
  def index
    @ps_reassurance_langs = PsReassuranceLang.all

    render json: @ps_reassurance_langs
  end

  # GET /ps_reassurance_langs/1
  def show
    render json: @ps_reassurance_lang
  end

  # POST /ps_reassurance_langs
  def create
    @ps_reassurance_lang = PsReassuranceLang.new(ps_reassurance_lang_params)

    if @ps_reassurance_lang.save
      render json: @ps_reassurance_lang, status: :created, location: @ps_reassurance_lang
    else
      render json: @ps_reassurance_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_reassurance_langs/1
  def update
    if @ps_reassurance_lang.update(ps_reassurance_lang_params)
      render json: @ps_reassurance_lang
    else
      render json: @ps_reassurance_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_reassurance_langs/1
  def destroy
    @ps_reassurance_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_reassurance_lang
      @ps_reassurance_lang = PsReassuranceLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_reassurance_lang_params
      params.fetch(:ps_reassurance_lang, {})
    end
end
