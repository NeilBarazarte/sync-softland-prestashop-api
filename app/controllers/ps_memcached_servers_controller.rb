class PsMemcachedServersController < ApplicationController
  before_action :set_ps_memcached_server, only: [:show, :update, :destroy]

  # GET /ps_memcached_servers
  def index
    @ps_memcached_servers = PsMemcachedServer.all

    render json: @ps_memcached_servers
  end

  # GET /ps_memcached_servers/1
  def show
    render json: @ps_memcached_server
  end

  # POST /ps_memcached_servers
  def create
    @ps_memcached_server = PsMemcachedServer.new(ps_memcached_server_params)

    if @ps_memcached_server.save
      render json: @ps_memcached_server, status: :created, location: @ps_memcached_server
    else
      render json: @ps_memcached_server.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_memcached_servers/1
  def update
    if @ps_memcached_server.update(ps_memcached_server_params)
      render json: @ps_memcached_server
    else
      render json: @ps_memcached_server.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_memcached_servers/1
  def destroy
    @ps_memcached_server.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_memcached_server
      @ps_memcached_server = PsMemcachedServer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_memcached_server_params
      params.fetch(:ps_memcached_server, {})
    end
end
