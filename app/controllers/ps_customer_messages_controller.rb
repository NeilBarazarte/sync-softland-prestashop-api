class PsCustomerMessagesController < ApplicationController
  before_action :set_ps_customer_message, only: [:show, :update, :destroy]

  # GET /ps_customer_messages
  def index
    @ps_customer_messages = PsCustomerMessage.all

    render json: @ps_customer_messages
  end

  # GET /ps_customer_messages/1
  def show
    render json: @ps_customer_message
  end

  # POST /ps_customer_messages
  def create
    @ps_customer_message = PsCustomerMessage.new(ps_customer_message_params)

    if @ps_customer_message.save
      render json: @ps_customer_message, status: :created, location: @ps_customer_message
    else
      render json: @ps_customer_message.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_customer_messages/1
  def update
    if @ps_customer_message.update(ps_customer_message_params)
      render json: @ps_customer_message
    else
      render json: @ps_customer_message.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_customer_messages/1
  def destroy
    @ps_customer_message.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_customer_message
      @ps_customer_message = PsCustomerMessage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_customer_message_params
      params.fetch(:ps_customer_message, {})
    end
end
