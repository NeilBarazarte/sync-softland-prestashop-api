class PsGroupReductionsController < ApplicationController
  before_action :set_ps_group_reduction, only: [:show, :update, :destroy]

  # GET /ps_group_reductions
  def index
    @ps_group_reductions = PsGroupReduction.all

    render json: @ps_group_reductions
  end

  # GET /ps_group_reductions/1
  def show
    render json: @ps_group_reduction
  end

  # POST /ps_group_reductions
  def create
    @ps_group_reduction = PsGroupReduction.new(ps_group_reduction_params)

    if @ps_group_reduction.save
      render json: @ps_group_reduction, status: :created, location: @ps_group_reduction
    else
      render json: @ps_group_reduction.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_group_reductions/1
  def update
    if @ps_group_reduction.update(ps_group_reduction_params)
      render json: @ps_group_reduction
    else
      render json: @ps_group_reduction.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_group_reductions/1
  def destroy
    @ps_group_reduction.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_group_reduction
      @ps_group_reduction = PsGroupReduction.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_group_reduction_params
      params.fetch(:ps_group_reduction, {})
    end
end
