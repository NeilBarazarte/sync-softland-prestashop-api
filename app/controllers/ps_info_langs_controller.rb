class PsInfoLangsController < ApplicationController
  before_action :set_ps_info_lang, only: [:show, :update, :destroy]

  # GET /ps_info_langs
  def index
    @ps_info_langs = PsInfoLang.all

    render json: @ps_info_langs
  end

  # GET /ps_info_langs/1
  def show
    render json: @ps_info_lang
  end

  # POST /ps_info_langs
  def create
    @ps_info_lang = PsInfoLang.new(ps_info_lang_params)

    if @ps_info_lang.save
      render json: @ps_info_lang, status: :created, location: @ps_info_lang
    else
      render json: @ps_info_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_info_langs/1
  def update
    if @ps_info_lang.update(ps_info_lang_params)
      render json: @ps_info_lang
    else
      render json: @ps_info_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_info_langs/1
  def destroy
    @ps_info_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_info_lang
      @ps_info_lang = PsInfoLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_info_lang_params
      params.fetch(:ps_info_lang, {})
    end
end
