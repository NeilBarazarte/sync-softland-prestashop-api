class PsOrderInvoiceTaxesController < ApplicationController
  before_action :set_ps_order_invoice_tax, only: [:show, :update, :destroy]

  # GET /ps_order_invoice_taxes
  def index
    @ps_order_invoice_taxes = PsOrderInvoiceTax.all

    render json: @ps_order_invoice_taxes
  end

  # GET /ps_order_invoice_taxes/1
  def show
    render json: @ps_order_invoice_tax
  end

  # POST /ps_order_invoice_taxes
  def create
    @ps_order_invoice_tax = PsOrderInvoiceTax.new(ps_order_invoice_tax_params)

    if @ps_order_invoice_tax.save
      render json: @ps_order_invoice_tax, status: :created, location: @ps_order_invoice_tax
    else
      render json: @ps_order_invoice_tax.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_invoice_taxes/1
  def update
    if @ps_order_invoice_tax.update(ps_order_invoice_tax_params)
      render json: @ps_order_invoice_tax
    else
      render json: @ps_order_invoice_tax.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_invoice_taxes/1
  def destroy
    @ps_order_invoice_tax.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_invoice_tax
      @ps_order_invoice_tax = PsOrderInvoiceTax.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_invoice_tax_params
      params.fetch(:ps_order_invoice_tax, {})
    end
end
