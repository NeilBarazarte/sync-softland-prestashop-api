class PsAttachmentLangsController < ApplicationController
  before_action :set_ps_attachment_lang, only: [:show, :update, :destroy]

  # GET /ps_attachment_langs
  def index
    @ps_attachment_langs = PsAttachmentLang.all

    render json: @ps_attachment_langs
  end

  # GET /ps_attachment_langs/1
  def show
    render json: @ps_attachment_lang
  end

  # POST /ps_attachment_langs
  def create
    @ps_attachment_lang = PsAttachmentLang.new(ps_attachment_lang_params)

    if @ps_attachment_lang.save
      render json: @ps_attachment_lang, status: :created, location: @ps_attachment_lang
    else
      render json: @ps_attachment_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_attachment_langs/1
  def update
    if @ps_attachment_lang.update(ps_attachment_lang_params)
      render json: @ps_attachment_lang
    else
      render json: @ps_attachment_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_attachment_langs/1
  def destroy
    @ps_attachment_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_attachment_lang
      @ps_attachment_lang = PsAttachmentLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_attachment_lang_params
      params.fetch(:ps_attachment_lang, {})
    end
end
