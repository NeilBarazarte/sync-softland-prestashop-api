class PsCarrierGroupsController < ApplicationController
  before_action :set_ps_carrier_group, only: [:show, :update, :destroy]

  # GET /ps_carrier_groups
  def index
    @ps_carrier_groups = PsCarrierGroup.all

    render json: @ps_carrier_groups
  end

  # GET /ps_carrier_groups/1
  def show
    render json: @ps_carrier_group
  end

  # POST /ps_carrier_groups
  def create
    @ps_carrier_group = PsCarrierGroup.new(ps_carrier_group_params)

    if @ps_carrier_group.save
      render json: @ps_carrier_group, status: :created, location: @ps_carrier_group
    else
      render json: @ps_carrier_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_carrier_groups/1
  def update
    if @ps_carrier_group.update(ps_carrier_group_params)
      render json: @ps_carrier_group
    else
      render json: @ps_carrier_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_carrier_groups/1
  def destroy
    @ps_carrier_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_carrier_group
      @ps_carrier_group = PsCarrierGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_carrier_group_params
      params.fetch(:ps_carrier_group, {})
    end
end
