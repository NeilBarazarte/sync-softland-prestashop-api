class PsStockMvtsController < ApplicationController
  before_action :set_ps_stock_mvt, only: [:show, :update, :destroy]

  # GET /ps_stock_mvts
  def index
    @ps_stock_mvts = PsStockMvt.all

    render json: @ps_stock_mvts
  end

  # GET /ps_stock_mvts/1
  def show
    render json: @ps_stock_mvt
  end

  # POST /ps_stock_mvts
  def create
    @ps_stock_mvt = PsStockMvt.new(ps_stock_mvt_params)

    if @ps_stock_mvt.save
      render json: @ps_stock_mvt, status: :created, location: @ps_stock_mvt
    else
      render json: @ps_stock_mvt.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_stock_mvts/1
  def update
    if @ps_stock_mvt.update(ps_stock_mvt_params)
      render json: @ps_stock_mvt
    else
      render json: @ps_stock_mvt.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_stock_mvts/1
  def destroy
    @ps_stock_mvt.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_stock_mvt
      @ps_stock_mvt = PsStockMvt.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_stock_mvt_params
      params.fetch(:ps_stock_mvt, {})
    end
end
