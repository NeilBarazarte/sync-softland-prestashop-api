class PsImportMatchesController < ApplicationController
  before_action :set_ps_import_match, only: [:show, :update, :destroy]

  # GET /ps_import_matches
  def index
    @ps_import_matches = PsImportMatch.all

    render json: @ps_import_matches
  end

  # GET /ps_import_matches/1
  def show
    render json: @ps_import_match
  end

  # POST /ps_import_matches
  def create
    @ps_import_match = PsImportMatch.new(ps_import_match_params)

    if @ps_import_match.save
      render json: @ps_import_match, status: :created, location: @ps_import_match
    else
      render json: @ps_import_match.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_import_matches/1
  def update
    if @ps_import_match.update(ps_import_match_params)
      render json: @ps_import_match
    else
      render json: @ps_import_match.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_import_matches/1
  def destroy
    @ps_import_match.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_import_match
      @ps_import_match = PsImportMatch.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_import_match_params
      params.fetch(:ps_import_match, {})
    end
end
