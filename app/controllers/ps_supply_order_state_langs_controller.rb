class PsSupplyOrderStateLangsController < ApplicationController
  before_action :set_ps_supply_order_state_lang, only: [:show, :update, :destroy]

  # GET /ps_supply_order_state_langs
  def index
    @ps_supply_order_state_langs = PsSupplyOrderStateLang.all

    render json: @ps_supply_order_state_langs
  end

  # GET /ps_supply_order_state_langs/1
  def show
    render json: @ps_supply_order_state_lang
  end

  # POST /ps_supply_order_state_langs
  def create
    @ps_supply_order_state_lang = PsSupplyOrderStateLang.new(ps_supply_order_state_lang_params)

    if @ps_supply_order_state_lang.save
      render json: @ps_supply_order_state_lang, status: :created, location: @ps_supply_order_state_lang
    else
      render json: @ps_supply_order_state_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_supply_order_state_langs/1
  def update
    if @ps_supply_order_state_lang.update(ps_supply_order_state_lang_params)
      render json: @ps_supply_order_state_lang
    else
      render json: @ps_supply_order_state_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_supply_order_state_langs/1
  def destroy
    @ps_supply_order_state_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_supply_order_state_lang
      @ps_supply_order_state_lang = PsSupplyOrderStateLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_supply_order_state_lang_params
      params.fetch(:ps_supply_order_state_lang, {})
    end
end
