class PsSmartyLastFlushesController < ApplicationController
  before_action :set_ps_smarty_last_flush, only: [:show, :update, :destroy]

  # GET /ps_smarty_last_flushes
  def index
    @ps_smarty_last_flushes = PsSmartyLastFlush.all

    render json: @ps_smarty_last_flushes
  end

  # GET /ps_smarty_last_flushes/1
  def show
    render json: @ps_smarty_last_flush
  end

  # POST /ps_smarty_last_flushes
  def create
    @ps_smarty_last_flush = PsSmartyLastFlush.new(ps_smarty_last_flush_params)

    if @ps_smarty_last_flush.save
      render json: @ps_smarty_last_flush, status: :created, location: @ps_smarty_last_flush
    else
      render json: @ps_smarty_last_flush.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_smarty_last_flushes/1
  def update
    if @ps_smarty_last_flush.update(ps_smarty_last_flush_params)
      render json: @ps_smarty_last_flush
    else
      render json: @ps_smarty_last_flush.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_smarty_last_flushes/1
  def destroy
    @ps_smarty_last_flush.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_smarty_last_flush
      @ps_smarty_last_flush = PsSmartyLastFlush.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_smarty_last_flush_params
      params.fetch(:ps_smarty_last_flush, {})
    end
end
