class PsTaxesController < ApplicationController
  before_action :set_ps_tax, only: [:show, :update, :destroy]

  # GET /ps_taxes
  def index
    @ps_taxes = PsTax.all

    render json: @ps_taxes
  end

  # GET /ps_taxes/1
  def show
    render json: @ps_tax
  end

  # POST /ps_taxes
  def create
    @ps_tax = PsTax.new(ps_tax_params)

    if @ps_tax.save
      render json: @ps_tax, status: :created, location: @ps_tax
    else
      render json: @ps_tax.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_taxes/1
  def update
    if @ps_tax.update(ps_tax_params)
      render json: @ps_tax
    else
      render json: @ps_tax.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_taxes/1
  def destroy
    @ps_tax.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tax
      @ps_tax = PsTax.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tax_params
      params.fetch(:ps_tax, {})
    end
end
