class PsProductSuppliersController < ApplicationController
  before_action :set_ps_product_supplier, only: [:show, :update, :destroy]

  # GET /ps_product_suppliers
  def index
    @ps_product_suppliers = PsProductSupplier.all

    render json: @ps_product_suppliers
  end

  # GET /ps_product_suppliers/1
  def show
    render json: @ps_product_supplier
  end

  # POST /ps_product_suppliers
  def create
    @ps_product_supplier = PsProductSupplier.new(ps_product_supplier_params)

    if @ps_product_supplier.save
      render json: @ps_product_supplier, status: :created, location: @ps_product_supplier
    else
      render json: @ps_product_supplier.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_suppliers/1
  def update
    if @ps_product_supplier.update(ps_product_supplier_params)
      render json: @ps_product_supplier
    else
      render json: @ps_product_supplier.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_suppliers/1
  def destroy
    @ps_product_supplier.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_supplier
      @ps_product_supplier = PsProductSupplier.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_supplier_params
      params.fetch(:ps_product_supplier, {})
    end
end
