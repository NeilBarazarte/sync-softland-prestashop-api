class PsTranslationsController < ApplicationController
  before_action :set_ps_translation, only: [:show, :update, :destroy]

  # GET /ps_translations
  def index
    @ps_translations = PsTranslation.all

    render json: @ps_translations
  end

  # GET /ps_translations/1
  def show
    render json: @ps_translation
  end

  # POST /ps_translations
  def create
    @ps_translation = PsTranslation.new(ps_translation_params)

    if @ps_translation.save
      render json: @ps_translation, status: :created, location: @ps_translation
    else
      render json: @ps_translation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_translations/1
  def update
    if @ps_translation.update(ps_translation_params)
      render json: @ps_translation
    else
      render json: @ps_translation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_translations/1
  def destroy
    @ps_translation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_translation
      @ps_translation = PsTranslation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_translation_params
      params.fetch(:ps_translation, {})
    end
end
