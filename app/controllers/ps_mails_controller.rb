class PsMailsController < ApplicationController
  before_action :set_ps_mail, only: [:show, :update, :destroy]

  # GET /ps_mails
  def index
    @ps_mails = PsMail.all

    render json: @ps_mails
  end

  # GET /ps_mails/1
  def show
    render json: @ps_mail
  end

  # POST /ps_mails
  def create
    @ps_mail = PsMail.new(ps_mail_params)

    if @ps_mail.save
      render json: @ps_mail, status: :created, location: @ps_mail
    else
      render json: @ps_mail.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_mails/1
  def update
    if @ps_mail.update(ps_mail_params)
      render json: @ps_mail
    else
      render json: @ps_mail.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_mails/1
  def destroy
    @ps_mail.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_mail
      @ps_mail = PsMail.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_mail_params
      params.fetch(:ps_mail, {})
    end
end
