class PsOrdersController < ApplicationController
  before_action :set_ps_order, only: [:show, :update, :destroy]

  # GET /ps_orders
  def index
    @ps_orders = PsOrder.all

    render json: @ps_orders
  end

  # GET /ps_orders/1
  def show
    render json: @ps_order
  end

  # POST /ps_orders
  def create
    @ps_order = PsOrder.new(ps_order_params)

    if @ps_order.save
      render json: @ps_order, status: :created, location: @ps_order
    else
      render json: @ps_order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_orders/1
  def update
    if @ps_order.update(ps_order_params)
      render json: @ps_order
    else
      render json: @ps_order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_orders/1
  def destroy
    @ps_order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order
      @ps_order = PsOrder.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_params
      params.fetch(:ps_order, {})
    end
end
