class PsRequestSqlsController < ApplicationController
  before_action :set_ps_request_sql, only: [:show, :update, :destroy]

  # GET /ps_request_sqls
  def index
    @ps_request_sqls = PsRequestSql.all

    render json: @ps_request_sqls
  end

  # GET /ps_request_sqls/1
  def show
    render json: @ps_request_sql
  end

  # POST /ps_request_sqls
  def create
    @ps_request_sql = PsRequestSql.new(ps_request_sql_params)

    if @ps_request_sql.save
      render json: @ps_request_sql, status: :created, location: @ps_request_sql
    else
      render json: @ps_request_sql.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_request_sqls/1
  def update
    if @ps_request_sql.update(ps_request_sql_params)
      render json: @ps_request_sql
    else
      render json: @ps_request_sql.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_request_sqls/1
  def destroy
    @ps_request_sql.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_request_sql
      @ps_request_sql = PsRequestSql.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_request_sql_params
      params.fetch(:ps_request_sql, {})
    end
end
