class PsWarehouseProductLocationsController < ApplicationController
  before_action :set_ps_warehouse_product_location, only: [:show, :update, :destroy]

  # GET /ps_warehouse_product_locations
  def index
    @ps_warehouse_product_locations = PsWarehouseProductLocation.all

    render json: @ps_warehouse_product_locations
  end

  # GET /ps_warehouse_product_locations/1
  def show
    render json: @ps_warehouse_product_location
  end

  # POST /ps_warehouse_product_locations
  def create
    @ps_warehouse_product_location = PsWarehouseProductLocation.new(ps_warehouse_product_location_params)

    if @ps_warehouse_product_location.save
      render json: @ps_warehouse_product_location, status: :created, location: @ps_warehouse_product_location
    else
      render json: @ps_warehouse_product_location.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_warehouse_product_locations/1
  def update
    if @ps_warehouse_product_location.update(ps_warehouse_product_location_params)
      render json: @ps_warehouse_product_location
    else
      render json: @ps_warehouse_product_location.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_warehouse_product_locations/1
  def destroy
    @ps_warehouse_product_location.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_warehouse_product_location
      @ps_warehouse_product_location = PsWarehouseProductLocation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_warehouse_product_location_params
      params.fetch(:ps_warehouse_product_location, {})
    end
end
