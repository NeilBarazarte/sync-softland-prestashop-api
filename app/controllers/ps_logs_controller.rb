class PsLogsController < ApplicationController
  before_action :set_ps_log, only: [:show, :update, :destroy]

  # GET /ps_logs
  def index
    @ps_logs = PsLog.all

    render json: @ps_logs
  end

  # GET /ps_logs/1
  def show
    render json: @ps_log
  end

  # POST /ps_logs
  def create
    @ps_log = PsLog.new(ps_log_params)

    if @ps_log.save
      render json: @ps_log, status: :created, location: @ps_log
    else
      render json: @ps_log.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_logs/1
  def update
    if @ps_log.update(ps_log_params)
      render json: @ps_log
    else
      render json: @ps_log.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_logs/1
  def destroy
    @ps_log.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_log
      @ps_log = PsLog.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_log_params
      params.fetch(:ps_log, {})
    end
end
