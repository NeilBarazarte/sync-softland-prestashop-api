class PsWarehouseShopsController < ApplicationController
  before_action :set_ps_warehouse_shop, only: [:show, :update, :destroy]

  # GET /ps_warehouse_shops
  def index
    @ps_warehouse_shops = PsWarehouseShop.all

    render json: @ps_warehouse_shops
  end

  # GET /ps_warehouse_shops/1
  def show
    render json: @ps_warehouse_shop
  end

  # POST /ps_warehouse_shops
  def create
    @ps_warehouse_shop = PsWarehouseShop.new(ps_warehouse_shop_params)

    if @ps_warehouse_shop.save
      render json: @ps_warehouse_shop, status: :created, location: @ps_warehouse_shop
    else
      render json: @ps_warehouse_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_warehouse_shops/1
  def update
    if @ps_warehouse_shop.update(ps_warehouse_shop_params)
      render json: @ps_warehouse_shop
    else
      render json: @ps_warehouse_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_warehouse_shops/1
  def destroy
    @ps_warehouse_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_warehouse_shop
      @ps_warehouse_shop = PsWarehouseShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_warehouse_shop_params
      params.fetch(:ps_warehouse_shop, {})
    end
end
