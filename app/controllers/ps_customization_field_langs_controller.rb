class PsCustomizationFieldLangsController < ApplicationController
  before_action :set_ps_customization_field_lang, only: [:show, :update, :destroy]

  # GET /ps_customization_field_langs
  def index
    @ps_customization_field_langs = PsCustomizationFieldLang.all

    render json: @ps_customization_field_langs
  end

  # GET /ps_customization_field_langs/1
  def show
    render json: @ps_customization_field_lang
  end

  # POST /ps_customization_field_langs
  def create
    @ps_customization_field_lang = PsCustomizationFieldLang.new(ps_customization_field_lang_params)

    if @ps_customization_field_lang.save
      render json: @ps_customization_field_lang, status: :created, location: @ps_customization_field_lang
    else
      render json: @ps_customization_field_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_customization_field_langs/1
  def update
    if @ps_customization_field_lang.update(ps_customization_field_lang_params)
      render json: @ps_customization_field_lang
    else
      render json: @ps_customization_field_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_customization_field_langs/1
  def destroy
    @ps_customization_field_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_customization_field_lang
      @ps_customization_field_lang = PsCustomizationFieldLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_customization_field_lang_params
      params.fetch(:ps_customization_field_lang, {})
    end
end
