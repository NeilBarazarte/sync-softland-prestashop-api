class PsReferrersController < ApplicationController
  before_action :set_ps_referrer, only: [:show, :update, :destroy]

  # GET /ps_referrers
  def index
    @ps_referrers = PsReferrer.all

    render json: @ps_referrers
  end

  # GET /ps_referrers/1
  def show
    render json: @ps_referrer
  end

  # POST /ps_referrers
  def create
    @ps_referrer = PsReferrer.new(ps_referrer_params)

    if @ps_referrer.save
      render json: @ps_referrer, status: :created, location: @ps_referrer
    else
      render json: @ps_referrer.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_referrers/1
  def update
    if @ps_referrer.update(ps_referrer_params)
      render json: @ps_referrer
    else
      render json: @ps_referrer.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_referrers/1
  def destroy
    @ps_referrer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_referrer
      @ps_referrer = PsReferrer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_referrer_params
      params.fetch(:ps_referrer, {})
    end
end
