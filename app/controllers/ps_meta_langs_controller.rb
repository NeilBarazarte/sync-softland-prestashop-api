class PsMetaLangsController < ApplicationController
  before_action :set_ps_meta_lang, only: [:show, :update, :destroy]

  # GET /ps_meta_langs
  def index
    @ps_meta_langs = PsMetaLang.all

    render json: @ps_meta_langs
  end

  # GET /ps_meta_langs/1
  def show
    render json: @ps_meta_lang
  end

  # POST /ps_meta_langs
  def create
    @ps_meta_lang = PsMetaLang.new(ps_meta_lang_params)

    if @ps_meta_lang.save
      render json: @ps_meta_lang, status: :created, location: @ps_meta_lang
    else
      render json: @ps_meta_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_meta_langs/1
  def update
    if @ps_meta_lang.update(ps_meta_lang_params)
      render json: @ps_meta_lang
    else
      render json: @ps_meta_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_meta_langs/1
  def destroy
    @ps_meta_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_meta_lang
      @ps_meta_lang = PsMetaLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_meta_lang_params
      params.fetch(:ps_meta_lang, {})
    end
end
