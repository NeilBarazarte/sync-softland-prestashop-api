class PsAliasesController < ApplicationController
  before_action :set_ps_alias, only: [:show, :update, :destroy]

  # GET /ps_aliases
  def index
    @ps_aliases = PsAlias.all

    render json: @ps_aliases
  end

  # GET /ps_aliases/1
  def show
    render json: @ps_alias
  end

  # POST /ps_aliases
  def create
    @ps_alias = PsAlias.new(ps_alias_params)

    if @ps_alias.save
      render json: @ps_alias, status: :created, location: @ps_alias
    else
      render json: @ps_alias.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_aliases/1
  def update
    if @ps_alias.update(ps_alias_params)
      render json: @ps_alias
    else
      render json: @ps_alias.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_aliases/1
  def destroy
    @ps_alias.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_alias
      @ps_alias = PsAlias.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_alias_params
      params.fetch(:ps_alias, {})
    end
end
