class PsTabModulePreferencesController < ApplicationController
  before_action :set_ps_tab_module_preference, only: [:show, :update, :destroy]

  # GET /ps_tab_module_preferences
  def index
    @ps_tab_module_preferences = PsTabModulePreference.all

    render json: @ps_tab_module_preferences
  end

  # GET /ps_tab_module_preferences/1
  def show
    render json: @ps_tab_module_preference
  end

  # POST /ps_tab_module_preferences
  def create
    @ps_tab_module_preference = PsTabModulePreference.new(ps_tab_module_preference_params)

    if @ps_tab_module_preference.save
      render json: @ps_tab_module_preference, status: :created, location: @ps_tab_module_preference
    else
      render json: @ps_tab_module_preference.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tab_module_preferences/1
  def update
    if @ps_tab_module_preference.update(ps_tab_module_preference_params)
      render json: @ps_tab_module_preference
    else
      render json: @ps_tab_module_preference.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tab_module_preferences/1
  def destroy
    @ps_tab_module_preference.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tab_module_preference
      @ps_tab_module_preference = PsTabModulePreference.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tab_module_preference_params
      params.fetch(:ps_tab_module_preference, {})
    end
end
