class PsProductAttachmentsController < ApplicationController
  before_action :set_ps_product_attachment, only: [:show, :update, :destroy]

  # GET /ps_product_attachments
  def index
    @ps_product_attachments = PsProductAttachment.all

    render json: @ps_product_attachments
  end

  # GET /ps_product_attachments/1
  def show
    render json: @ps_product_attachment
  end

  # POST /ps_product_attachments
  def create
    @ps_product_attachment = PsProductAttachment.new(ps_product_attachment_params)

    if @ps_product_attachment.save
      render json: @ps_product_attachment, status: :created, location: @ps_product_attachment
    else
      render json: @ps_product_attachment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_attachments/1
  def update
    if @ps_product_attachment.update(ps_product_attachment_params)
      render json: @ps_product_attachment
    else
      render json: @ps_product_attachment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_attachments/1
  def destroy
    @ps_product_attachment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_attachment
      @ps_product_attachment = PsProductAttachment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_attachment_params
      params.fetch(:ps_product_attachment, {})
    end
end
