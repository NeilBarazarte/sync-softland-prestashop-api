class PsEmailsubscriptionsController < ApplicationController
  before_action :set_ps_emailsubscription, only: [:show, :update, :destroy]

  # GET /ps_emailsubscriptions
  def index
    @ps_emailsubscriptions = PsEmailsubscription.all

    render json: @ps_emailsubscriptions
  end

  # GET /ps_emailsubscriptions/1
  def show
    render json: @ps_emailsubscription
  end

  # POST /ps_emailsubscriptions
  def create
    @ps_emailsubscription = PsEmailsubscription.new(ps_emailsubscription_params)

    if @ps_emailsubscription.save
      render json: @ps_emailsubscription, status: :created, location: @ps_emailsubscription
    else
      render json: @ps_emailsubscription.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_emailsubscriptions/1
  def update
    if @ps_emailsubscription.update(ps_emailsubscription_params)
      render json: @ps_emailsubscription
    else
      render json: @ps_emailsubscription.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_emailsubscriptions/1
  def destroy
    @ps_emailsubscription.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_emailsubscription
      @ps_emailsubscription = PsEmailsubscription.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_emailsubscription_params
      params.fetch(:ps_emailsubscription, {})
    end
end
