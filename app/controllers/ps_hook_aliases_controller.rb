class PsHookAliasesController < ApplicationController
  before_action :set_ps_hook_alias, only: [:show, :update, :destroy]

  # GET /ps_hook_aliases
  def index
    @ps_hook_aliases = PsHookAlias.all

    render json: @ps_hook_aliases
  end

  # GET /ps_hook_aliases/1
  def show
    render json: @ps_hook_alias
  end

  # POST /ps_hook_aliases
  def create
    @ps_hook_alias = PsHookAlias.new(ps_hook_alias_params)

    if @ps_hook_alias.save
      render json: @ps_hook_alias, status: :created, location: @ps_hook_alias
    else
      render json: @ps_hook_alias.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_hook_aliases/1
  def update
    if @ps_hook_alias.update(ps_hook_alias_params)
      render json: @ps_hook_alias
    else
      render json: @ps_hook_alias.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_hook_aliases/1
  def destroy
    @ps_hook_alias.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_hook_alias
      @ps_hook_alias = PsHookAlias.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_hook_alias_params
      params.fetch(:ps_hook_alias, {})
    end
end
