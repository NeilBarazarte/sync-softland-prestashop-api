class PsModuleHistoriesController < ApplicationController
  before_action :set_ps_module_history, only: [:show, :update, :destroy]

  # GET /ps_module_histories
  def index
    @ps_module_histories = PsModuleHistory.all

    render json: @ps_module_histories
  end

  # GET /ps_module_histories/1
  def show
    render json: @ps_module_history
  end

  # POST /ps_module_histories
  def create
    @ps_module_history = PsModuleHistory.new(ps_module_history_params)

    if @ps_module_history.save
      render json: @ps_module_history, status: :created, location: @ps_module_history
    else
      render json: @ps_module_history.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_module_histories/1
  def update
    if @ps_module_history.update(ps_module_history_params)
      render json: @ps_module_history
    else
      render json: @ps_module_history.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_module_histories/1
  def destroy
    @ps_module_history.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_module_history
      @ps_module_history = PsModuleHistory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_module_history_params
      params.fetch(:ps_module_history, {})
    end
end
