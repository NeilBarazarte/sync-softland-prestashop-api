class PsLayeredFiltersController < ApplicationController
  before_action :set_ps_layered_filter, only: [:show, :update, :destroy]

  # GET /ps_layered_filters
  def index
    @ps_layered_filters = PsLayeredFilter.all

    render json: @ps_layered_filters
  end

  # GET /ps_layered_filters/1
  def show
    render json: @ps_layered_filter
  end

  # POST /ps_layered_filters
  def create
    @ps_layered_filter = PsLayeredFilter.new(ps_layered_filter_params)

    if @ps_layered_filter.save
      render json: @ps_layered_filter, status: :created, location: @ps_layered_filter
    else
      render json: @ps_layered_filter.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_layered_filters/1
  def update
    if @ps_layered_filter.update(ps_layered_filter_params)
      render json: @ps_layered_filter
    else
      render json: @ps_layered_filter.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_layered_filters/1
  def destroy
    @ps_layered_filter.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_layered_filter
      @ps_layered_filter = PsLayeredFilter.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_layered_filter_params
      params.fetch(:ps_layered_filter, {})
    end
end
