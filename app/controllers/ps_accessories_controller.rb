class PsAccessoriesController < ApplicationController
  before_action :set_ps_accessory, only: [:show, :update, :destroy]

  # GET /ps_accessories
  def index
    @ps_accessories = PsAccessory.all

    render json: @ps_accessories
  end

  # GET /ps_accessories/1
  def show
    render json: @ps_accessory
  end

  # POST /ps_accessories
  def create
    @ps_accessory = PsAccessory.new(ps_accessory_params)

    if @ps_accessory.save
      render json: @ps_accessory, status: :created, location: @ps_accessory
    else
      render json: @ps_accessory.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_accessories/1
  def update
    if @ps_accessory.update(ps_accessory_params)
      render json: @ps_accessory
    else
      render json: @ps_accessory.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_accessories/1
  def destroy
    @ps_accessory.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_accessory
      @ps_accessory = PsAccessory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_accessory_params
      params.fetch(:ps_accessory, {})
    end
end
