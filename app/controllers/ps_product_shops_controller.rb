class PsProductShopsController < ApplicationController
  before_action :set_ps_product_shop, only: [:show, :update, :destroy]

  # GET /ps_product_shops
  def index
    @ps_product_shops = PsProductShop.all

    render json: @ps_product_shops
  end

  # GET /ps_product_shops/1
  def show
    render json: @ps_product_shop
  end

  # POST /ps_product_shops
  def create
    @ps_product_shop = PsProductShop.new(ps_product_shop_params)

    if @ps_product_shop.save
      render json: @ps_product_shop, status: :created, location: @ps_product_shop
    else
      render json: @ps_product_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_shops/1
  def update
    if @ps_product_shop.update(ps_product_shop_params)
      render json: @ps_product_shop
    else
      render json: @ps_product_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_shops/1
  def destroy
    @ps_product_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_shop
      @ps_product_shop = PsProductShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_shop_params
      params.fetch(:ps_product_shop, {})
    end
end
