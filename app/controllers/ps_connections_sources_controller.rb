class PsConnectionsSourcesController < ApplicationController
  before_action :set_ps_connections_source, only: [:show, :update, :destroy]

  # GET /ps_connections_sources
  def index
    @ps_connections_sources = PsConnectionsSource.all

    render json: @ps_connections_sources
  end

  # GET /ps_connections_sources/1
  def show
    render json: @ps_connections_source
  end

  # POST /ps_connections_sources
  def create
    @ps_connections_source = PsConnectionsSource.new(ps_connections_source_params)

    if @ps_connections_source.save
      render json: @ps_connections_source, status: :created, location: @ps_connections_source
    else
      render json: @ps_connections_source.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_connections_sources/1
  def update
    if @ps_connections_source.update(ps_connections_source_params)
      render json: @ps_connections_source
    else
      render json: @ps_connections_source.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_connections_sources/1
  def destroy
    @ps_connections_source.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_connections_source
      @ps_connections_source = PsConnectionsSource.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_connections_source_params
      params.fetch(:ps_connections_source, {})
    end
end
