class PsConditionAdvicesController < ApplicationController
  before_action :set_ps_condition_advice, only: [:show, :update, :destroy]

  # GET /ps_condition_advices
  def index
    @ps_condition_advices = PsConditionAdvice.all

    render json: @ps_condition_advices
  end

  # GET /ps_condition_advices/1
  def show
    render json: @ps_condition_advice
  end

  # POST /ps_condition_advices
  def create
    @ps_condition_advice = PsConditionAdvice.new(ps_condition_advice_params)

    if @ps_condition_advice.save
      render json: @ps_condition_advice, status: :created, location: @ps_condition_advice
    else
      render json: @ps_condition_advice.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_condition_advices/1
  def update
    if @ps_condition_advice.update(ps_condition_advice_params)
      render json: @ps_condition_advice
    else
      render json: @ps_condition_advice.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_condition_advices/1
  def destroy
    @ps_condition_advice.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_condition_advice
      @ps_condition_advice = PsConditionAdvice.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_condition_advice_params
      params.fetch(:ps_condition_advice, {})
    end
end
