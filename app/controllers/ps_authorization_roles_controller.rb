class PsAuthorizationRolesController < ApplicationController
  before_action :set_ps_authorization_role, only: [:show, :update, :destroy]

  # GET /ps_authorization_roles
  def index
    @ps_authorization_roles = PsAuthorizationRole.all

    render json: @ps_authorization_roles
  end

  # GET /ps_authorization_roles/1
  def show
    render json: @ps_authorization_role
  end

  # POST /ps_authorization_roles
  def create
    @ps_authorization_role = PsAuthorizationRole.new(ps_authorization_role_params)

    if @ps_authorization_role.save
      render json: @ps_authorization_role, status: :created, location: @ps_authorization_role
    else
      render json: @ps_authorization_role.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_authorization_roles/1
  def update
    if @ps_authorization_role.update(ps_authorization_role_params)
      render json: @ps_authorization_role
    else
      render json: @ps_authorization_role.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_authorization_roles/1
  def destroy
    @ps_authorization_role.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_authorization_role
      @ps_authorization_role = PsAuthorizationRole.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_authorization_role_params
      params.fetch(:ps_authorization_role, {})
    end
end
