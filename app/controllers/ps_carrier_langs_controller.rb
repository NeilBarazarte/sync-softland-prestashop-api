class PsCarrierLangsController < ApplicationController
  before_action :set_ps_carrier_lang, only: [:show, :update, :destroy]

  # GET /ps_carrier_langs
  def index
    @ps_carrier_langs = PsCarrierLang.all

    render json: @ps_carrier_langs
  end

  # GET /ps_carrier_langs/1
  def show
    render json: @ps_carrier_lang
  end

  # POST /ps_carrier_langs
  def create
    @ps_carrier_lang = PsCarrierLang.new(ps_carrier_lang_params)

    if @ps_carrier_lang.save
      render json: @ps_carrier_lang, status: :created, location: @ps_carrier_lang
    else
      render json: @ps_carrier_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_carrier_langs/1
  def update
    if @ps_carrier_lang.update(ps_carrier_lang_params)
      render json: @ps_carrier_lang
    else
      render json: @ps_carrier_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_carrier_langs/1
  def destroy
    @ps_carrier_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_carrier_lang
      @ps_carrier_lang = PsCarrierLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_carrier_lang_params
      params.fetch(:ps_carrier_lang, {})
    end
end
