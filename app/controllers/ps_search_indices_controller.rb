class PsSearchIndicesController < ApplicationController
  before_action :set_ps_search_index, only: [:show, :update, :destroy]

  # GET /ps_search_indices
  def index
    @ps_search_indices = PsSearchIndex.all

    render json: @ps_search_indices
  end

  # GET /ps_search_indices/1
  def show
    render json: @ps_search_index
  end

  # POST /ps_search_indices
  def create
    @ps_search_index = PsSearchIndex.new(ps_search_index_params)

    if @ps_search_index.save
      render json: @ps_search_index, status: :created, location: @ps_search_index
    else
      render json: @ps_search_index.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_search_indices/1
  def update
    if @ps_search_index.update(ps_search_index_params)
      render json: @ps_search_index
    else
      render json: @ps_search_index.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_search_indices/1
  def destroy
    @ps_search_index.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_search_index
      @ps_search_index = PsSearchIndex.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_search_index_params
      params.fetch(:ps_search_index, {})
    end
end
