class PsCountryShopsController < ApplicationController
  before_action :set_ps_country_shop, only: [:show, :update, :destroy]

  # GET /ps_country_shops
  def index
    @ps_country_shops = PsCountryShop.all

    render json: @ps_country_shops
  end

  # GET /ps_country_shops/1
  def show
    render json: @ps_country_shop
  end

  # POST /ps_country_shops
  def create
    @ps_country_shop = PsCountryShop.new(ps_country_shop_params)

    if @ps_country_shop.save
      render json: @ps_country_shop, status: :created, location: @ps_country_shop
    else
      render json: @ps_country_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_country_shops/1
  def update
    if @ps_country_shop.update(ps_country_shop_params)
      render json: @ps_country_shop
    else
      render json: @ps_country_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_country_shops/1
  def destroy
    @ps_country_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_country_shop
      @ps_country_shop = PsCountryShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_country_shop_params
      params.fetch(:ps_country_shop, {})
    end
end
