class PsCartsController < ApplicationController
  before_action :set_ps_cart, only: [:show, :update, :destroy]

  # GET /ps_carts
  def index
    @ps_carts = PsCart.all

    render json: @ps_carts
  end

  # GET /ps_carts/1
  def show
    render json: @ps_cart
  end

  # POST /ps_carts
  def create
    @ps_cart = PsCart.new(ps_cart_params)

    if @ps_cart.save
      render json: @ps_cart, status: :created, location: @ps_cart
    else
      render json: @ps_cart.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_carts/1
  def update
    if @ps_cart.update(ps_cart_params)
      render json: @ps_cart
    else
      render json: @ps_cart.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_carts/1
  def destroy
    @ps_cart.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart
      @ps_cart = PsCart.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_params
      params.fetch(:ps_cart, {})
    end
end
