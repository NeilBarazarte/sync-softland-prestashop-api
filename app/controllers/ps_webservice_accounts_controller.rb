class PsWebserviceAccountsController < ApplicationController
  before_action :set_ps_webservice_account, only: [:show, :update, :destroy]

  # GET /ps_webservice_accounts
  def index
    @ps_webservice_accounts = PsWebserviceAccount.all

    render json: @ps_webservice_accounts
  end

  # GET /ps_webservice_accounts/1
  def show
    render json: @ps_webservice_account
  end

  # POST /ps_webservice_accounts
  def create
    @ps_webservice_account = PsWebserviceAccount.new(ps_webservice_account_params)

    if @ps_webservice_account.save
      render json: @ps_webservice_account, status: :created, location: @ps_webservice_account
    else
      render json: @ps_webservice_account.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_webservice_accounts/1
  def update
    if @ps_webservice_account.update(ps_webservice_account_params)
      render json: @ps_webservice_account
    else
      render json: @ps_webservice_account.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_webservice_accounts/1
  def destroy
    @ps_webservice_account.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_webservice_account
      @ps_webservice_account = PsWebserviceAccount.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_webservice_account_params
      params.fetch(:ps_webservice_account, {})
    end
end
