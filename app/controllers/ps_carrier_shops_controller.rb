class PsCarrierShopsController < ApplicationController
  before_action :set_ps_carrier_shop, only: [:show, :update, :destroy]

  # GET /ps_carrier_shops
  def index
    @ps_carrier_shops = PsCarrierShop.all

    render json: @ps_carrier_shops
  end

  # GET /ps_carrier_shops/1
  def show
    render json: @ps_carrier_shop
  end

  # POST /ps_carrier_shops
  def create
    @ps_carrier_shop = PsCarrierShop.new(ps_carrier_shop_params)

    if @ps_carrier_shop.save
      render json: @ps_carrier_shop, status: :created, location: @ps_carrier_shop
    else
      render json: @ps_carrier_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_carrier_shops/1
  def update
    if @ps_carrier_shop.update(ps_carrier_shop_params)
      render json: @ps_carrier_shop
    else
      render json: @ps_carrier_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_carrier_shops/1
  def destroy
    @ps_carrier_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_carrier_shop
      @ps_carrier_shop = PsCarrierShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_carrier_shop_params
      params.fetch(:ps_carrier_shop, {})
    end
end
