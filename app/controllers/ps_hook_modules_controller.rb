class PsHookModulesController < ApplicationController
  before_action :set_ps_hook_module, only: [:show, :update, :destroy]

  # GET /ps_hook_modules
  def index
    @ps_hook_modules = PsHookModule.all

    render json: @ps_hook_modules
  end

  # GET /ps_hook_modules/1
  def show
    render json: @ps_hook_module
  end

  # POST /ps_hook_modules
  def create
    @ps_hook_module = PsHookModule.new(ps_hook_module_params)

    if @ps_hook_module.save
      render json: @ps_hook_module, status: :created, location: @ps_hook_module
    else
      render json: @ps_hook_module.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_hook_modules/1
  def update
    if @ps_hook_module.update(ps_hook_module_params)
      render json: @ps_hook_module
    else
      render json: @ps_hook_module.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_hook_modules/1
  def destroy
    @ps_hook_module.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_hook_module
      @ps_hook_module = PsHookModule.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_hook_module_params
      params.fetch(:ps_hook_module, {})
    end
end
