class PsShopUrlsController < ApplicationController
  before_action :set_ps_shop_url, only: [:show, :update, :destroy]

  # GET /ps_shop_urls
  def index
    @ps_shop_urls = PsShopUrl.all

    render json: @ps_shop_urls
  end

  # GET /ps_shop_urls/1
  def show
    render json: @ps_shop_url
  end

  # POST /ps_shop_urls
  def create
    @ps_shop_url = PsShopUrl.new(ps_shop_url_params)

    if @ps_shop_url.save
      render json: @ps_shop_url, status: :created, location: @ps_shop_url
    else
      render json: @ps_shop_url.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_shop_urls/1
  def update
    if @ps_shop_url.update(ps_shop_url_params)
      render json: @ps_shop_url
    else
      render json: @ps_shop_url.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_shop_urls/1
  def destroy
    @ps_shop_url.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_shop_url
      @ps_shop_url = PsShopUrl.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_shop_url_params
      params.fetch(:ps_shop_url, {})
    end
end
