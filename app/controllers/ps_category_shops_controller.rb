class PsCategoryShopsController < ApplicationController
  before_action :set_ps_category_shop, only: [:show, :update, :destroy]

  # GET /ps_category_shops
  def index
    @ps_category_shops = PsCategoryShop.all

    render json: @ps_category_shops
  end

  # GET /ps_category_shops/1
  def show
    render json: @ps_category_shop
  end

  # POST /ps_category_shops
  def create
    @ps_category_shop = PsCategoryShop.new(ps_category_shop_params)

    if @ps_category_shop.save
      render json: @ps_category_shop, status: :created, location: @ps_category_shop
    else
      render json: @ps_category_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_category_shops/1
  def update
    if @ps_category_shop.update(ps_category_shop_params)
      render json: @ps_category_shop
    else
      render json: @ps_category_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_category_shops/1
  def destroy
    @ps_category_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_category_shop
      @ps_category_shop = PsCategoryShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_category_shop_params
      params.fetch(:ps_category_shop, {})
    end
end
