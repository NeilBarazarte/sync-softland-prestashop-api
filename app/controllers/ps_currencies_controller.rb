class PsCurrenciesController < ApplicationController
  before_action :set_ps_currency, only: [:show, :update, :destroy]

  # GET /ps_currencies
  def index
    @ps_currencies = PsCurrency.all

    render json: @ps_currencies
  end

  # GET /ps_currencies/1
  def show
    render json: @ps_currency
  end

  # POST /ps_currencies
  def create
    @ps_currency = PsCurrency.new(ps_currency_params)

    if @ps_currency.save
      render json: @ps_currency, status: :created, location: @ps_currency
    else
      render json: @ps_currency.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_currencies/1
  def update
    if @ps_currency.update(ps_currency_params)
      render json: @ps_currency
    else
      render json: @ps_currency.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_currencies/1
  def destroy
    @ps_currency.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_currency
      @ps_currency = PsCurrency.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_currency_params
      params.fetch(:ps_currency, {})
    end
end
