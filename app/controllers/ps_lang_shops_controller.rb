class PsLangShopsController < ApplicationController
  before_action :set_ps_lang_shop, only: [:show, :update, :destroy]

  # GET /ps_lang_shops
  def index
    @ps_lang_shops = PsLangShop.all

    render json: @ps_lang_shops
  end

  # GET /ps_lang_shops/1
  def show
    render json: @ps_lang_shop
  end

  # POST /ps_lang_shops
  def create
    @ps_lang_shop = PsLangShop.new(ps_lang_shop_params)

    if @ps_lang_shop.save
      render json: @ps_lang_shop, status: :created, location: @ps_lang_shop
    else
      render json: @ps_lang_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_lang_shops/1
  def update
    if @ps_lang_shop.update(ps_lang_shop_params)
      render json: @ps_lang_shop
    else
      render json: @ps_lang_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_lang_shops/1
  def destroy
    @ps_lang_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_lang_shop
      @ps_lang_shop = PsLangShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_lang_shop_params
      params.fetch(:ps_lang_shop, {})
    end
end
