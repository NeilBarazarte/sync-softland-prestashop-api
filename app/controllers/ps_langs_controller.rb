class PsLangsController < ApplicationController
  before_action :set_ps_lang, only: [:show, :update, :destroy]

  # GET /ps_langs
  def index
    @ps_langs = PsLang.all

    render json: @ps_langs
  end

  # GET /ps_langs/1
  def show
    render json: @ps_lang
  end

  # POST /ps_langs
  def create
    @ps_lang = PsLang.new(ps_lang_params)

    if @ps_lang.save
      render json: @ps_lang, status: :created, location: @ps_lang
    else
      render json: @ps_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_langs/1
  def update
    if @ps_lang.update(ps_lang_params)
      render json: @ps_lang
    else
      render json: @ps_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_langs/1
  def destroy
    @ps_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_lang
      @ps_lang = PsLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_lang_params
      params.fetch(:ps_lang, {})
    end
end
