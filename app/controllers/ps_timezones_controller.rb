class PsTimezonesController < ApplicationController
  before_action :set_ps_timezone, only: [:show, :update, :destroy]

  # GET /ps_timezones
  def index
    @ps_timezones = PsTimezone.all

    render json: @ps_timezones
  end

  # GET /ps_timezones/1
  def show
    render json: @ps_timezone
  end

  # POST /ps_timezones
  def create
    @ps_timezone = PsTimezone.new(ps_timezone_params)

    if @ps_timezone.save
      render json: @ps_timezone, status: :created, location: @ps_timezone
    else
      render json: @ps_timezone.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_timezones/1
  def update
    if @ps_timezone.update(ps_timezone_params)
      render json: @ps_timezone
    else
      render json: @ps_timezone.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_timezones/1
  def destroy
    @ps_timezone.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_timezone
      @ps_timezone = PsTimezone.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_timezone_params
      params.fetch(:ps_timezone, {})
    end
end
