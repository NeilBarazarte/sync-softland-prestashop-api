class PsCustomerThreadsController < ApplicationController
  before_action :set_ps_customer_thread, only: [:show, :update, :destroy]

  # GET /ps_customer_threads
  def index
    @ps_customer_threads = PsCustomerThread.all

    render json: @ps_customer_threads
  end

  # GET /ps_customer_threads/1
  def show
    render json: @ps_customer_thread
  end

  # POST /ps_customer_threads
  def create
    @ps_customer_thread = PsCustomerThread.new(ps_customer_thread_params)

    if @ps_customer_thread.save
      render json: @ps_customer_thread, status: :created, location: @ps_customer_thread
    else
      render json: @ps_customer_thread.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_customer_threads/1
  def update
    if @ps_customer_thread.update(ps_customer_thread_params)
      render json: @ps_customer_thread
    else
      render json: @ps_customer_thread.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_customer_threads/1
  def destroy
    @ps_customer_thread.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_customer_thread
      @ps_customer_thread = PsCustomerThread.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_customer_thread_params
      params.fetch(:ps_customer_thread, {})
    end
end
