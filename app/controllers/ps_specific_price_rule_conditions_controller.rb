class PsSpecificPriceRuleConditionsController < ApplicationController
  before_action :set_ps_specific_price_rule_condition, only: [:show, :update, :destroy]

  # GET /ps_specific_price_rule_conditions
  def index
    @ps_specific_price_rule_conditions = PsSpecificPriceRuleCondition.all

    render json: @ps_specific_price_rule_conditions
  end

  # GET /ps_specific_price_rule_conditions/1
  def show
    render json: @ps_specific_price_rule_condition
  end

  # POST /ps_specific_price_rule_conditions
  def create
    @ps_specific_price_rule_condition = PsSpecificPriceRuleCondition.new(ps_specific_price_rule_condition_params)

    if @ps_specific_price_rule_condition.save
      render json: @ps_specific_price_rule_condition, status: :created, location: @ps_specific_price_rule_condition
    else
      render json: @ps_specific_price_rule_condition.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_specific_price_rule_conditions/1
  def update
    if @ps_specific_price_rule_condition.update(ps_specific_price_rule_condition_params)
      render json: @ps_specific_price_rule_condition
    else
      render json: @ps_specific_price_rule_condition.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_specific_price_rule_conditions/1
  def destroy
    @ps_specific_price_rule_condition.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_specific_price_rule_condition
      @ps_specific_price_rule_condition = PsSpecificPriceRuleCondition.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_specific_price_rule_condition_params
      params.fetch(:ps_specific_price_rule_condition, {})
    end
end
