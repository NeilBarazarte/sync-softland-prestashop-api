class PsFeaturesController < ApplicationController
  before_action :set_ps_feature, only: [:show, :update, :destroy]

  # GET /ps_features
  def index
    @ps_features = PsFeature.all

    render json: @ps_features
  end

  # GET /ps_features/1
  def show
    render json: @ps_feature
  end

  # POST /ps_features
  def create
    @ps_feature = PsFeature.new(ps_feature_params)

    if @ps_feature.save
      render json: @ps_feature, status: :created, location: @ps_feature
    else
      render json: @ps_feature.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_features/1
  def update
    if @ps_feature.update(ps_feature_params)
      render json: @ps_feature
    else
      render json: @ps_feature.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_features/1
  def destroy
    @ps_feature.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_feature
      @ps_feature = PsFeature.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_feature_params
      params.fetch(:ps_feature, {})
    end
end
