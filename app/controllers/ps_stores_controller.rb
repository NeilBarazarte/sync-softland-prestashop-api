class PsStoresController < ApplicationController
  before_action :set_ps_store, only: [:show, :update, :destroy]

  # GET /ps_stores
  def index
    @ps_stores = PsStore.all

    render json: @ps_stores
  end

  # GET /ps_stores/1
  def show
    render json: @ps_store
  end

  # POST /ps_stores
  def create
    @ps_store = PsStore.new(ps_store_params)

    if @ps_store.save
      render json: @ps_store, status: :created, location: @ps_store
    else
      render json: @ps_store.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_stores/1
  def update
    if @ps_store.update(ps_store_params)
      render json: @ps_store
    else
      render json: @ps_store.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_stores/1
  def destroy
    @ps_store.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_store
      @ps_store = PsStore.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_store_params
      params.fetch(:ps_store, {})
    end
end
