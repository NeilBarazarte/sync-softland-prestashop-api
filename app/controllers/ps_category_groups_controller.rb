class PsCategoryGroupsController < ApplicationController
  before_action :set_ps_category_group, only: [:show, :update, :destroy]

  # GET /ps_category_groups
  def index
    @ps_category_groups = PsCategoryGroup.all

    render json: @ps_category_groups
  end

  # GET /ps_category_groups/1
  def show
    render json: @ps_category_group
  end

  # POST /ps_category_groups
  def create
    @ps_category_group = PsCategoryGroup.new(ps_category_group_params)

    if @ps_category_group.save
      render json: @ps_category_group, status: :created, location: @ps_category_group
    else
      render json: @ps_category_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_category_groups/1
  def update
    if @ps_category_group.update(ps_category_group_params)
      render json: @ps_category_group
    else
      render json: @ps_category_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_category_groups/1
  def destroy
    @ps_category_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_category_group
      @ps_category_group = PsCategoryGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_category_group_params
      params.fetch(:ps_category_group, {})
    end
end
