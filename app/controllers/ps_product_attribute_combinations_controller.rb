class PsProductAttributeCombinationsController < ApplicationController
  before_action :set_ps_product_attribute_combination, only: [:show, :update, :destroy]

  # GET /ps_product_attribute_combinations
  def index
    @ps_product_attribute_combinations = PsProductAttributeCombination.all

    render json: @ps_product_attribute_combinations
  end

  # GET /ps_product_attribute_combinations/1
  def show
    render json: @ps_product_attribute_combination
  end

  # POST /ps_product_attribute_combinations
  def create
    @ps_product_attribute_combination = PsProductAttributeCombination.new(ps_product_attribute_combination_params)

    if @ps_product_attribute_combination.save
      render json: @ps_product_attribute_combination, status: :created, location: @ps_product_attribute_combination
    else
      render json: @ps_product_attribute_combination.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_attribute_combinations/1
  def update
    if @ps_product_attribute_combination.update(ps_product_attribute_combination_params)
      render json: @ps_product_attribute_combination
    else
      render json: @ps_product_attribute_combination.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_attribute_combinations/1
  def destroy
    @ps_product_attribute_combination.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_attribute_combination
      @ps_product_attribute_combination = PsProductAttributeCombination.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_attribute_combination_params
      params.fetch(:ps_product_attribute_combination, {})
    end
end
