class PsBadgesController < ApplicationController
  before_action :set_ps_badge, only: [:show, :update, :destroy]

  # GET /ps_badges
  def index
    @ps_badges = PsBadge.all

    render json: @ps_badges
  end

  # GET /ps_badges/1
  def show
    render json: @ps_badge
  end

  # POST /ps_badges
  def create
    @ps_badge = PsBadge.new(ps_badge_params)

    if @ps_badge.save
      render json: @ps_badge, status: :created, location: @ps_badge
    else
      render json: @ps_badge.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_badges/1
  def update
    if @ps_badge.update(ps_badge_params)
      render json: @ps_badge
    else
      render json: @ps_badge.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_badges/1
  def destroy
    @ps_badge.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_badge
      @ps_badge = PsBadge.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_badge_params
      params.fetch(:ps_badge, {})
    end
end
