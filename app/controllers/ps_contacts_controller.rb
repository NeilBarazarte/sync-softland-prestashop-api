class PsContactsController < ApplicationController
  before_action :set_ps_contact, only: [:show, :update, :destroy]

  # GET /ps_contacts
  def index
    @ps_contacts = PsContact.all

    render json: @ps_contacts
  end

  # GET /ps_contacts/1
  def show
    render json: @ps_contact
  end

  # POST /ps_contacts
  def create
    @ps_contact = PsContact.new(ps_contact_params)

    if @ps_contact.save
      render json: @ps_contact, status: :created, location: @ps_contact
    else
      render json: @ps_contact.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_contacts/1
  def update
    if @ps_contact.update(ps_contact_params)
      render json: @ps_contact
    else
      render json: @ps_contact.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_contacts/1
  def destroy
    @ps_contact.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_contact
      @ps_contact = PsContact.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_contact_params
      params.fetch(:ps_contact, {})
    end
end
