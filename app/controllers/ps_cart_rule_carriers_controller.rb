class PsCartRuleCarriersController < ApplicationController
  before_action :set_ps_cart_rule_carrier, only: [:show, :update, :destroy]

  # GET /ps_cart_rule_carriers
  def index
    @ps_cart_rule_carriers = PsCartRuleCarrier.all

    render json: @ps_cart_rule_carriers
  end

  # GET /ps_cart_rule_carriers/1
  def show
    render json: @ps_cart_rule_carrier
  end

  # POST /ps_cart_rule_carriers
  def create
    @ps_cart_rule_carrier = PsCartRuleCarrier.new(ps_cart_rule_carrier_params)

    if @ps_cart_rule_carrier.save
      render json: @ps_cart_rule_carrier, status: :created, location: @ps_cart_rule_carrier
    else
      render json: @ps_cart_rule_carrier.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_rule_carriers/1
  def update
    if @ps_cart_rule_carrier.update(ps_cart_rule_carrier_params)
      render json: @ps_cart_rule_carrier
    else
      render json: @ps_cart_rule_carrier.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_rule_carriers/1
  def destroy
    @ps_cart_rule_carrier.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_rule_carrier
      @ps_cart_rule_carrier = PsCartRuleCarrier.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_rule_carrier_params
      params.fetch(:ps_cart_rule_carrier, {})
    end
end
