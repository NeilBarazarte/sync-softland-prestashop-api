class PsCustomersController < ApplicationController
  before_action :set_ps_customer, only: [:show, :update, :destroy]

  # GET /ps_customers
  def index
    @ps_customers = PsCustomer.all

    render json: @ps_customers
  end

  # GET /ps_customers/1
  def show
    render json: @ps_customer
  end

  # POST /ps_customers
  def create
    @ps_customer = PsCustomer.new(ps_customer_params)

    if @ps_customer.save
      render json: @ps_customer, status: :created, location: @ps_customer
    else
      render json: @ps_customer.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_customers/1
  def update
    if @ps_customer.update(ps_customer_params)
      render json: @ps_customer
    else
      render json: @ps_customer.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_customers/1
  def destroy
    @ps_customer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_customer
      @ps_customer = PsCustomer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_customer_params
      params.fetch(:ps_customer, {})
    end
end
