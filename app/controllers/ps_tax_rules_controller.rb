class PsTaxRulesController < ApplicationController
  before_action :set_ps_tax_rule, only: [:show, :update, :destroy]

  # GET /ps_tax_rules
  def index
    @ps_tax_rules = PsTaxRule.all

    render json: @ps_tax_rules
  end

  # GET /ps_tax_rules/1
  def show
    render json: @ps_tax_rule
  end

  # POST /ps_tax_rules
  def create
    @ps_tax_rule = PsTaxRule.new(ps_tax_rule_params)

    if @ps_tax_rule.save
      render json: @ps_tax_rule, status: :created, location: @ps_tax_rule
    else
      render json: @ps_tax_rule.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tax_rules/1
  def update
    if @ps_tax_rule.update(ps_tax_rule_params)
      render json: @ps_tax_rule
    else
      render json: @ps_tax_rule.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tax_rules/1
  def destroy
    @ps_tax_rule.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tax_rule
      @ps_tax_rule = PsTaxRule.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tax_rule_params
      params.fetch(:ps_tax_rule, {})
    end
end
