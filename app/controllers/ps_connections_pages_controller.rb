class PsConnectionsPagesController < ApplicationController
  before_action :set_ps_connections_page, only: [:show, :update, :destroy]

  # GET /ps_connections_pages
  def index
    @ps_connections_pages = PsConnectionsPage.all

    render json: @ps_connections_pages
  end

  # GET /ps_connections_pages/1
  def show
    render json: @ps_connections_page
  end

  # POST /ps_connections_pages
  def create
    @ps_connections_page = PsConnectionsPage.new(ps_connections_page_params)

    if @ps_connections_page.save
      render json: @ps_connections_page, status: :created, location: @ps_connections_page
    else
      render json: @ps_connections_page.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_connections_pages/1
  def update
    if @ps_connections_page.update(ps_connections_page_params)
      render json: @ps_connections_page
    else
      render json: @ps_connections_page.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_connections_pages/1
  def destroy
    @ps_connections_page.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_connections_page
      @ps_connections_page = PsConnectionsPage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_connections_page_params
      params.fetch(:ps_connections_page, {})
    end
end
