class PsOrderSlipsController < ApplicationController
  before_action :set_ps_order_slip, only: [:show, :update, :destroy]

  # GET /ps_order_slips
  def index
    @ps_order_slips = PsOrderSlip.all

    render json: @ps_order_slips
  end

  # GET /ps_order_slips/1
  def show
    render json: @ps_order_slip
  end

  # POST /ps_order_slips
  def create
    @ps_order_slip = PsOrderSlip.new(ps_order_slip_params)

    if @ps_order_slip.save
      render json: @ps_order_slip, status: :created, location: @ps_order_slip
    else
      render json: @ps_order_slip.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_slips/1
  def update
    if @ps_order_slip.update(ps_order_slip_params)
      render json: @ps_order_slip
    else
      render json: @ps_order_slip.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_slips/1
  def destroy
    @ps_order_slip.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_slip
      @ps_order_slip = PsOrderSlip.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_slip_params
      params.fetch(:ps_order_slip, {})
    end
end
