class PsProfileLangsController < ApplicationController
  before_action :set_ps_profile_lang, only: [:show, :update, :destroy]

  # GET /ps_profile_langs
  def index
    @ps_profile_langs = PsProfileLang.all

    render json: @ps_profile_langs
  end

  # GET /ps_profile_langs/1
  def show
    render json: @ps_profile_lang
  end

  # POST /ps_profile_langs
  def create
    @ps_profile_lang = PsProfileLang.new(ps_profile_lang_params)

    if @ps_profile_lang.save
      render json: @ps_profile_lang, status: :created, location: @ps_profile_lang
    else
      render json: @ps_profile_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_profile_langs/1
  def update
    if @ps_profile_lang.update(ps_profile_lang_params)
      render json: @ps_profile_lang
    else
      render json: @ps_profile_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_profile_langs/1
  def destroy
    @ps_profile_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_profile_lang
      @ps_profile_lang = PsProfileLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_profile_lang_params
      params.fetch(:ps_profile_lang, {})
    end
end
