class PsLayeredPriceIndicesController < ApplicationController
  before_action :set_ps_layered_price_index, only: [:show, :update, :destroy]

  # GET /ps_layered_price_indices
  def index
    @ps_layered_price_indices = PsLayeredPriceIndex.all

    render json: @ps_layered_price_indices
  end

  # GET /ps_layered_price_indices/1
  def show
    render json: @ps_layered_price_index
  end

  # POST /ps_layered_price_indices
  def create
    @ps_layered_price_index = PsLayeredPriceIndex.new(ps_layered_price_index_params)

    if @ps_layered_price_index.save
      render json: @ps_layered_price_index, status: :created, location: @ps_layered_price_index
    else
      render json: @ps_layered_price_index.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_layered_price_indices/1
  def update
    if @ps_layered_price_index.update(ps_layered_price_index_params)
      render json: @ps_layered_price_index
    else
      render json: @ps_layered_price_index.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_layered_price_indices/1
  def destroy
    @ps_layered_price_index.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_layered_price_index
      @ps_layered_price_index = PsLayeredPriceIndex.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_layered_price_index_params
      params.fetch(:ps_layered_price_index, {})
    end
end
