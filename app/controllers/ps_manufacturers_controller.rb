class PsManufacturersController < ApplicationController
  before_action :set_ps_manufacturer, only: [:show, :update, :destroy]

  # GET /ps_manufacturers
  def index
    @ps_manufacturers = PsManufacturer.all

    render json: @ps_manufacturers
  end

  # GET /ps_manufacturers/1
  def show
    render json: @ps_manufacturer
  end

  # POST /ps_manufacturers
  def create
    @ps_manufacturer = PsManufacturer.new(ps_manufacturer_params)

    if @ps_manufacturer.save
      render json: @ps_manufacturer, status: :created, location: @ps_manufacturer
    else
      render json: @ps_manufacturer.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_manufacturers/1
  def update
    if @ps_manufacturer.update(ps_manufacturer_params)
      render json: @ps_manufacturer
    else
      render json: @ps_manufacturer.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_manufacturers/1
  def destroy
    @ps_manufacturer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_manufacturer
      @ps_manufacturer = PsManufacturer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_manufacturer_params
      params.fetch(:ps_manufacturer, {})
    end
end
