class PsOrderCarriersController < ApplicationController
  before_action :set_ps_order_carrier, only: [:show, :update, :destroy]

  # GET /ps_order_carriers
  def index
    @ps_order_carriers = PsOrderCarrier.all

    render json: @ps_order_carriers
  end

  # GET /ps_order_carriers/1
  def show
    render json: @ps_order_carrier
  end

  # POST /ps_order_carriers
  def create
    @ps_order_carrier = PsOrderCarrier.new(ps_order_carrier_params)

    if @ps_order_carrier.save
      render json: @ps_order_carrier, status: :created, location: @ps_order_carrier
    else
      render json: @ps_order_carrier.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_carriers/1
  def update
    if @ps_order_carrier.update(ps_order_carrier_params)
      render json: @ps_order_carrier
    else
      render json: @ps_order_carrier.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_carriers/1
  def destroy
    @ps_order_carrier.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_carrier
      @ps_order_carrier = PsOrderCarrier.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_carrier_params
      params.fetch(:ps_order_carrier, {})
    end
end
