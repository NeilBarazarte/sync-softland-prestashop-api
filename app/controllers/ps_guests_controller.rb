class PsGuestsController < ApplicationController
  before_action :set_ps_guest, only: [:show, :update, :destroy]

  # GET /ps_guests
  def index
    @ps_guests = PsGuest.all

    render json: @ps_guests
  end

  # GET /ps_guests/1
  def show
    render json: @ps_guest
  end

  # POST /ps_guests
  def create
    @ps_guest = PsGuest.new(ps_guest_params)

    if @ps_guest.save
      render json: @ps_guest, status: :created, location: @ps_guest
    else
      render json: @ps_guest.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_guests/1
  def update
    if @ps_guest.update(ps_guest_params)
      render json: @ps_guest
    else
      render json: @ps_guest.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_guests/1
  def destroy
    @ps_guest.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_guest
      @ps_guest = PsGuest.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_guest_params
      params.fetch(:ps_guest, {})
    end
end
