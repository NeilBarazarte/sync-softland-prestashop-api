class PsOrderMessageLangsController < ApplicationController
  before_action :set_ps_order_message_lang, only: [:show, :update, :destroy]

  # GET /ps_order_message_langs
  def index
    @ps_order_message_langs = PsOrderMessageLang.all

    render json: @ps_order_message_langs
  end

  # GET /ps_order_message_langs/1
  def show
    render json: @ps_order_message_lang
  end

  # POST /ps_order_message_langs
  def create
    @ps_order_message_lang = PsOrderMessageLang.new(ps_order_message_lang_params)

    if @ps_order_message_lang.save
      render json: @ps_order_message_lang, status: :created, location: @ps_order_message_lang
    else
      render json: @ps_order_message_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_message_langs/1
  def update
    if @ps_order_message_lang.update(ps_order_message_lang_params)
      render json: @ps_order_message_lang
    else
      render json: @ps_order_message_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_message_langs/1
  def destroy
    @ps_order_message_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_message_lang
      @ps_order_message_lang = PsOrderMessageLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_message_lang_params
      params.fetch(:ps_order_message_lang, {})
    end
end
