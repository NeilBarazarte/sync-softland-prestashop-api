class PsDateRangesController < ApplicationController
  before_action :set_ps_date_range, only: [:show, :update, :destroy]

  # GET /ps_date_ranges
  def index
    @ps_date_ranges = PsDateRange.all

    render json: @ps_date_ranges
  end

  # GET /ps_date_ranges/1
  def show
    render json: @ps_date_range
  end

  # POST /ps_date_ranges
  def create
    @ps_date_range = PsDateRange.new(ps_date_range_params)

    if @ps_date_range.save
      render json: @ps_date_range, status: :created, location: @ps_date_range
    else
      render json: @ps_date_range.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_date_ranges/1
  def update
    if @ps_date_range.update(ps_date_range_params)
      render json: @ps_date_range
    else
      render json: @ps_date_range.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_date_ranges/1
  def destroy
    @ps_date_range.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_date_range
      @ps_date_range = PsDateRange.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_date_range_params
      params.fetch(:ps_date_range, {})
    end
end
