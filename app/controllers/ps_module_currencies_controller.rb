class PsModuleCurrenciesController < ApplicationController
  before_action :set_ps_module_currency, only: [:show, :update, :destroy]

  # GET /ps_module_currencies
  def index
    @ps_module_currencies = PsModuleCurrency.all

    render json: @ps_module_currencies
  end

  # GET /ps_module_currencies/1
  def show
    render json: @ps_module_currency
  end

  # POST /ps_module_currencies
  def create
    @ps_module_currency = PsModuleCurrency.new(ps_module_currency_params)

    if @ps_module_currency.save
      render json: @ps_module_currency, status: :created, location: @ps_module_currency
    else
      render json: @ps_module_currency.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_module_currencies/1
  def update
    if @ps_module_currency.update(ps_module_currency_params)
      render json: @ps_module_currency
    else
      render json: @ps_module_currency.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_module_currencies/1
  def destroy
    @ps_module_currency.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_module_currency
      @ps_module_currency = PsModuleCurrency.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_module_currency_params
      params.fetch(:ps_module_currency, {})
    end
end
