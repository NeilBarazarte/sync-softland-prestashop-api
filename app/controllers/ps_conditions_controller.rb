class PsConditionsController < ApplicationController
  before_action :set_ps_condition, only: [:show, :update, :destroy]

  # GET /ps_conditions
  def index
    @ps_conditions = PsCondition.all

    render json: @ps_conditions
  end

  # GET /ps_conditions/1
  def show
    render json: @ps_condition
  end

  # POST /ps_conditions
  def create
    @ps_condition = PsCondition.new(ps_condition_params)

    if @ps_condition.save
      render json: @ps_condition, status: :created, location: @ps_condition
    else
      render json: @ps_condition.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_conditions/1
  def update
    if @ps_condition.update(ps_condition_params)
      render json: @ps_condition
    else
      render json: @ps_condition.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_conditions/1
  def destroy
    @ps_condition.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_condition
      @ps_condition = PsCondition.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_condition_params
      params.fetch(:ps_condition, {})
    end
end
