class PsEmployeeShopsController < ApplicationController
  before_action :set_ps_employee_shop, only: [:show, :update, :destroy]

  # GET /ps_employee_shops
  def index
    @ps_employee_shops = PsEmployeeShop.all

    render json: @ps_employee_shops
  end

  # GET /ps_employee_shops/1
  def show
    render json: @ps_employee_shop
  end

  # POST /ps_employee_shops
  def create
    @ps_employee_shop = PsEmployeeShop.new(ps_employee_shop_params)

    if @ps_employee_shop.save
      render json: @ps_employee_shop, status: :created, location: @ps_employee_shop
    else
      render json: @ps_employee_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_employee_shops/1
  def update
    if @ps_employee_shop.update(ps_employee_shop_params)
      render json: @ps_employee_shop
    else
      render json: @ps_employee_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_employee_shops/1
  def destroy
    @ps_employee_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_employee_shop
      @ps_employee_shop = PsEmployeeShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_employee_shop_params
      params.fetch(:ps_employee_shop, {})
    end
end
