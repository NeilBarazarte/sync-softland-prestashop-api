class PsWarehouseCarriersController < ApplicationController
  before_action :set_ps_warehouse_carrier, only: [:show, :update, :destroy]

  # GET /ps_warehouse_carriers
  def index
    @ps_warehouse_carriers = PsWarehouseCarrier.all

    render json: @ps_warehouse_carriers
  end

  # GET /ps_warehouse_carriers/1
  def show
    render json: @ps_warehouse_carrier
  end

  # POST /ps_warehouse_carriers
  def create
    @ps_warehouse_carrier = PsWarehouseCarrier.new(ps_warehouse_carrier_params)

    if @ps_warehouse_carrier.save
      render json: @ps_warehouse_carrier, status: :created, location: @ps_warehouse_carrier
    else
      render json: @ps_warehouse_carrier.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_warehouse_carriers/1
  def update
    if @ps_warehouse_carrier.update(ps_warehouse_carrier_params)
      render json: @ps_warehouse_carrier
    else
      render json: @ps_warehouse_carrier.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_warehouse_carriers/1
  def destroy
    @ps_warehouse_carrier.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_warehouse_carrier
      @ps_warehouse_carrier = PsWarehouseCarrier.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_warehouse_carrier_params
      params.fetch(:ps_warehouse_carrier, {})
    end
end
