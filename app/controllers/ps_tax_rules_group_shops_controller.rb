class PsTaxRulesGroupShopsController < ApplicationController
  before_action :set_ps_tax_rules_group_shop, only: [:show, :update, :destroy]

  # GET /ps_tax_rules_group_shops
  def index
    @ps_tax_rules_group_shops = PsTaxRulesGroupShop.all

    render json: @ps_tax_rules_group_shops
  end

  # GET /ps_tax_rules_group_shops/1
  def show
    render json: @ps_tax_rules_group_shop
  end

  # POST /ps_tax_rules_group_shops
  def create
    @ps_tax_rules_group_shop = PsTaxRulesGroupShop.new(ps_tax_rules_group_shop_params)

    if @ps_tax_rules_group_shop.save
      render json: @ps_tax_rules_group_shop, status: :created, location: @ps_tax_rules_group_shop
    else
      render json: @ps_tax_rules_group_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tax_rules_group_shops/1
  def update
    if @ps_tax_rules_group_shop.update(ps_tax_rules_group_shop_params)
      render json: @ps_tax_rules_group_shop
    else
      render json: @ps_tax_rules_group_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tax_rules_group_shops/1
  def destroy
    @ps_tax_rules_group_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tax_rules_group_shop
      @ps_tax_rules_group_shop = PsTaxRulesGroupShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tax_rules_group_shop_params
      params.fetch(:ps_tax_rules_group_shop, {})
    end
end
