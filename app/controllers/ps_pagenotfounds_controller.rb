class PsPagenotfoundsController < ApplicationController
  before_action :set_ps_pagenotfound, only: [:show, :update, :destroy]

  # GET /ps_pagenotfounds
  def index
    @ps_pagenotfounds = PsPagenotfound.all

    render json: @ps_pagenotfounds
  end

  # GET /ps_pagenotfounds/1
  def show
    render json: @ps_pagenotfound
  end

  # POST /ps_pagenotfounds
  def create
    @ps_pagenotfound = PsPagenotfound.new(ps_pagenotfound_params)

    if @ps_pagenotfound.save
      render json: @ps_pagenotfound, status: :created, location: @ps_pagenotfound
    else
      render json: @ps_pagenotfound.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_pagenotfounds/1
  def update
    if @ps_pagenotfound.update(ps_pagenotfound_params)
      render json: @ps_pagenotfound
    else
      render json: @ps_pagenotfound.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_pagenotfounds/1
  def destroy
    @ps_pagenotfound.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_pagenotfound
      @ps_pagenotfound = PsPagenotfound.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_pagenotfound_params
      params.fetch(:ps_pagenotfound, {})
    end
end
