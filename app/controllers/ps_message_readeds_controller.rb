class PsMessageReadedsController < ApplicationController
  before_action :set_ps_message_readed, only: [:show, :update, :destroy]

  # GET /ps_message_readeds
  def index
    @ps_message_readeds = PsMessageReaded.all

    render json: @ps_message_readeds
  end

  # GET /ps_message_readeds/1
  def show
    render json: @ps_message_readed
  end

  # POST /ps_message_readeds
  def create
    @ps_message_readed = PsMessageReaded.new(ps_message_readed_params)

    if @ps_message_readed.save
      render json: @ps_message_readed, status: :created, location: @ps_message_readed
    else
      render json: @ps_message_readed.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_message_readeds/1
  def update
    if @ps_message_readed.update(ps_message_readed_params)
      render json: @ps_message_readed
    else
      render json: @ps_message_readed.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_message_readeds/1
  def destroy
    @ps_message_readed.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_message_readed
      @ps_message_readed = PsMessageReaded.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_message_readed_params
      params.fetch(:ps_message_readed, {})
    end
end
