class PsLayeredIndexableAttributeGroupsController < ApplicationController
  before_action :set_ps_layered_indexable_attribute_group, only: [:show, :update, :destroy]

  # GET /ps_layered_indexable_attribute_groups
  def index
    @ps_layered_indexable_attribute_groups = PsLayeredIndexableAttributeGroup.all

    render json: @ps_layered_indexable_attribute_groups
  end

  # GET /ps_layered_indexable_attribute_groups/1
  def show
    render json: @ps_layered_indexable_attribute_group
  end

  # POST /ps_layered_indexable_attribute_groups
  def create
    @ps_layered_indexable_attribute_group = PsLayeredIndexableAttributeGroup.new(ps_layered_indexable_attribute_group_params)

    if @ps_layered_indexable_attribute_group.save
      render json: @ps_layered_indexable_attribute_group, status: :created, location: @ps_layered_indexable_attribute_group
    else
      render json: @ps_layered_indexable_attribute_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_layered_indexable_attribute_groups/1
  def update
    if @ps_layered_indexable_attribute_group.update(ps_layered_indexable_attribute_group_params)
      render json: @ps_layered_indexable_attribute_group
    else
      render json: @ps_layered_indexable_attribute_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_layered_indexable_attribute_groups/1
  def destroy
    @ps_layered_indexable_attribute_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_layered_indexable_attribute_group
      @ps_layered_indexable_attribute_group = PsLayeredIndexableAttributeGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_layered_indexable_attribute_group_params
      params.fetch(:ps_layered_indexable_attribute_group, {})
    end
end
