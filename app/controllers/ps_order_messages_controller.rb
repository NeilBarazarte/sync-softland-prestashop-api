class PsOrderMessagesController < ApplicationController
  before_action :set_ps_order_message, only: [:show, :update, :destroy]

  # GET /ps_order_messages
  def index
    @ps_order_messages = PsOrderMessage.all

    render json: @ps_order_messages
  end

  # GET /ps_order_messages/1
  def show
    render json: @ps_order_message
  end

  # POST /ps_order_messages
  def create
    @ps_order_message = PsOrderMessage.new(ps_order_message_params)

    if @ps_order_message.save
      render json: @ps_order_message, status: :created, location: @ps_order_message
    else
      render json: @ps_order_message.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_messages/1
  def update
    if @ps_order_message.update(ps_order_message_params)
      render json: @ps_order_message
    else
      render json: @ps_order_message.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_messages/1
  def destroy
    @ps_order_message.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_message
      @ps_order_message = PsOrderMessage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_message_params
      params.fetch(:ps_order_message, {})
    end
end
