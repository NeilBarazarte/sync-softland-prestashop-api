class PsCartRuleProductRuleGroupsController < ApplicationController
  before_action :set_ps_cart_rule_product_rule_group, only: [:show, :update, :destroy]

  # GET /ps_cart_rule_product_rule_groups
  def index
    @ps_cart_rule_product_rule_groups = PsCartRuleProductRuleGroup.all

    render json: @ps_cart_rule_product_rule_groups
  end

  # GET /ps_cart_rule_product_rule_groups/1
  def show
    render json: @ps_cart_rule_product_rule_group
  end

  # POST /ps_cart_rule_product_rule_groups
  def create
    @ps_cart_rule_product_rule_group = PsCartRuleProductRuleGroup.new(ps_cart_rule_product_rule_group_params)

    if @ps_cart_rule_product_rule_group.save
      render json: @ps_cart_rule_product_rule_group, status: :created, location: @ps_cart_rule_product_rule_group
    else
      render json: @ps_cart_rule_product_rule_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_rule_product_rule_groups/1
  def update
    if @ps_cart_rule_product_rule_group.update(ps_cart_rule_product_rule_group_params)
      render json: @ps_cart_rule_product_rule_group
    else
      render json: @ps_cart_rule_product_rule_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_rule_product_rule_groups/1
  def destroy
    @ps_cart_rule_product_rule_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_rule_product_rule_group
      @ps_cart_rule_product_rule_group = PsCartRuleProductRuleGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_rule_product_rule_group_params
      params.fetch(:ps_cart_rule_product_rule_group, {})
    end
end
