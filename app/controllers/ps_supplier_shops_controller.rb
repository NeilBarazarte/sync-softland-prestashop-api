class PsSupplierShopsController < ApplicationController
  before_action :set_ps_supplier_shop, only: [:show, :update, :destroy]

  # GET /ps_supplier_shops
  def index
    @ps_supplier_shops = PsSupplierShop.all

    render json: @ps_supplier_shops
  end

  # GET /ps_supplier_shops/1
  def show
    render json: @ps_supplier_shop
  end

  # POST /ps_supplier_shops
  def create
    @ps_supplier_shop = PsSupplierShop.new(ps_supplier_shop_params)

    if @ps_supplier_shop.save
      render json: @ps_supplier_shop, status: :created, location: @ps_supplier_shop
    else
      render json: @ps_supplier_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_supplier_shops/1
  def update
    if @ps_supplier_shop.update(ps_supplier_shop_params)
      render json: @ps_supplier_shop
    else
      render json: @ps_supplier_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_supplier_shops/1
  def destroy
    @ps_supplier_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_supplier_shop
      @ps_supplier_shop = PsSupplierShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_supplier_shop_params
      params.fetch(:ps_supplier_shop, {})
    end
end
