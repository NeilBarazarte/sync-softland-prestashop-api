class PsSpecificPricePrioritiesController < ApplicationController
  before_action :set_ps_specific_price_priority, only: [:show, :update, :destroy]

  # GET /ps_specific_price_priorities
  def index
    @ps_specific_price_priorities = PsSpecificPricePriority.all

    render json: @ps_specific_price_priorities
  end

  # GET /ps_specific_price_priorities/1
  def show
    render json: @ps_specific_price_priority
  end

  # POST /ps_specific_price_priorities
  def create
    @ps_specific_price_priority = PsSpecificPricePriority.new(ps_specific_price_priority_params)

    if @ps_specific_price_priority.save
      render json: @ps_specific_price_priority, status: :created, location: @ps_specific_price_priority
    else
      render json: @ps_specific_price_priority.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_specific_price_priorities/1
  def update
    if @ps_specific_price_priority.update(ps_specific_price_priority_params)
      render json: @ps_specific_price_priority
    else
      render json: @ps_specific_price_priority.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_specific_price_priorities/1
  def destroy
    @ps_specific_price_priority.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_specific_price_priority
      @ps_specific_price_priority = PsSpecificPricePriority.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_specific_price_priority_params
      params.fetch(:ps_specific_price_priority, {})
    end
end
