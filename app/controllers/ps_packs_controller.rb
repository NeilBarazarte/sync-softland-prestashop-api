class PsPacksController < ApplicationController
  before_action :set_ps_pack, only: [:show, :update, :destroy]

  # GET /ps_packs
  def index
    @ps_packs = PsPack.all

    render json: @ps_packs
  end

  # GET /ps_packs/1
  def show
    render json: @ps_pack
  end

  # POST /ps_packs
  def create
    @ps_pack = PsPack.new(ps_pack_params)

    if @ps_pack.save
      render json: @ps_pack, status: :created, location: @ps_pack
    else
      render json: @ps_pack.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_packs/1
  def update
    if @ps_pack.update(ps_pack_params)
      render json: @ps_pack
    else
      render json: @ps_pack.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_packs/1
  def destroy
    @ps_pack.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_pack
      @ps_pack = PsPack.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_pack_params
      params.fetch(:ps_pack, {})
    end
end
