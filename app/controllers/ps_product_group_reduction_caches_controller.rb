class PsProductGroupReductionCachesController < ApplicationController
  before_action :set_ps_product_group_reduction_cache, only: [:show, :update, :destroy]

  # GET /ps_product_group_reduction_caches
  def index
    @ps_product_group_reduction_caches = PsProductGroupReductionCache.all

    render json: @ps_product_group_reduction_caches
  end

  # GET /ps_product_group_reduction_caches/1
  def show
    render json: @ps_product_group_reduction_cache
  end

  # POST /ps_product_group_reduction_caches
  def create
    @ps_product_group_reduction_cache = PsProductGroupReductionCache.new(ps_product_group_reduction_cache_params)

    if @ps_product_group_reduction_cache.save
      render json: @ps_product_group_reduction_cache, status: :created, location: @ps_product_group_reduction_cache
    else
      render json: @ps_product_group_reduction_cache.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_group_reduction_caches/1
  def update
    if @ps_product_group_reduction_cache.update(ps_product_group_reduction_cache_params)
      render json: @ps_product_group_reduction_cache
    else
      render json: @ps_product_group_reduction_cache.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_group_reduction_caches/1
  def destroy
    @ps_product_group_reduction_cache.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_group_reduction_cache
      @ps_product_group_reduction_cache = PsProductGroupReductionCache.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_group_reduction_cache_params
      params.fetch(:ps_product_group_reduction_cache, {})
    end
end
