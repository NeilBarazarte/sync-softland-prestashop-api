class PsImagesController < ApplicationController
  before_action :set_ps_image, only: [:show, :update, :destroy]

  # GET /ps_images
  def index
    @ps_images = PsImage.all

    render json: @ps_images
  end

  # GET /ps_images/1
  def show
    render json: @ps_image
  end

  # POST /ps_images
  def create
    @ps_image = PsImage.new(ps_image_params)

    if @ps_image.save
      render json: @ps_image, status: :created, location: @ps_image
    else
      render json: @ps_image.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_images/1
  def update
    if @ps_image.update(ps_image_params)
      render json: @ps_image
    else
      render json: @ps_image.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_images/1
  def destroy
    @ps_image.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_image
      @ps_image = PsImage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_image_params
      params.fetch(:ps_image, {})
    end
end
