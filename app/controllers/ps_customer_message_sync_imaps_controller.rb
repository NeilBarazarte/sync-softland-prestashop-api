class PsCustomerMessageSyncImapsController < ApplicationController
  before_action :set_ps_customer_message_sync_imap, only: [:show, :update, :destroy]

  # GET /ps_customer_message_sync_imaps
  def index
    @ps_customer_message_sync_imaps = PsCustomerMessageSyncImap.all

    render json: @ps_customer_message_sync_imaps
  end

  # GET /ps_customer_message_sync_imaps/1
  def show
    render json: @ps_customer_message_sync_imap
  end

  # POST /ps_customer_message_sync_imaps
  def create
    @ps_customer_message_sync_imap = PsCustomerMessageSyncImap.new(ps_customer_message_sync_imap_params)

    if @ps_customer_message_sync_imap.save
      render json: @ps_customer_message_sync_imap, status: :created, location: @ps_customer_message_sync_imap
    else
      render json: @ps_customer_message_sync_imap.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_customer_message_sync_imaps/1
  def update
    if @ps_customer_message_sync_imap.update(ps_customer_message_sync_imap_params)
      render json: @ps_customer_message_sync_imap
    else
      render json: @ps_customer_message_sync_imap.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_customer_message_sync_imaps/1
  def destroy
    @ps_customer_message_sync_imap.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_customer_message_sync_imap
      @ps_customer_message_sync_imap = PsCustomerMessageSyncImap.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_customer_message_sync_imap_params
      params.fetch(:ps_customer_message_sync_imap, {})
    end
end
