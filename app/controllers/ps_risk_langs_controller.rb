class PsRiskLangsController < ApplicationController
  before_action :set_ps_risk_lang, only: [:show, :update, :destroy]

  # GET /ps_risk_langs
  def index
    @ps_risk_langs = PsRiskLang.all

    render json: @ps_risk_langs
  end

  # GET /ps_risk_langs/1
  def show
    render json: @ps_risk_lang
  end

  # POST /ps_risk_langs
  def create
    @ps_risk_lang = PsRiskLang.new(ps_risk_lang_params)

    if @ps_risk_lang.save
      render json: @ps_risk_lang, status: :created, location: @ps_risk_lang
    else
      render json: @ps_risk_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_risk_langs/1
  def update
    if @ps_risk_lang.update(ps_risk_lang_params)
      render json: @ps_risk_lang
    else
      render json: @ps_risk_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_risk_langs/1
  def destroy
    @ps_risk_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_risk_lang
      @ps_risk_lang = PsRiskLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_risk_lang_params
      params.fetch(:ps_risk_lang, {})
    end
end
