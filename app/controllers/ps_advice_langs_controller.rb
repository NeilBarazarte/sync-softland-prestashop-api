class PsAdviceLangsController < ApplicationController
  before_action :set_ps_advice_lang, only: [:show, :update, :destroy]

  # GET /ps_advice_langs
  def index
    @ps_advice_langs = PsAdviceLang.all

    render json: @ps_advice_langs
  end

  # GET /ps_advice_langs/1
  def show
    render json: @ps_advice_lang
  end

  # POST /ps_advice_langs
  def create
    @ps_advice_lang = PsAdviceLang.new(ps_advice_lang_params)

    if @ps_advice_lang.save
      render json: @ps_advice_lang, status: :created, location: @ps_advice_lang
    else
      render json: @ps_advice_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_advice_langs/1
  def update
    if @ps_advice_lang.update(ps_advice_lang_params)
      render json: @ps_advice_lang
    else
      render json: @ps_advice_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_advice_langs/1
  def destroy
    @ps_advice_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_advice_lang
      @ps_advice_lang = PsAdviceLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_advice_lang_params
      params.fetch(:ps_advice_lang, {})
    end
end
