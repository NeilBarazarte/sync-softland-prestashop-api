class PsInfoShopsController < ApplicationController
  before_action :set_ps_info_shop, only: [:show, :update, :destroy]

  # GET /ps_info_shops
  def index
    @ps_info_shops = PsInfoShop.all

    render json: @ps_info_shops
  end

  # GET /ps_info_shops/1
  def show
    render json: @ps_info_shop
  end

  # POST /ps_info_shops
  def create
    @ps_info_shop = PsInfoShop.new(ps_info_shop_params)

    if @ps_info_shop.save
      render json: @ps_info_shop, status: :created, location: @ps_info_shop
    else
      render json: @ps_info_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_info_shops/1
  def update
    if @ps_info_shop.update(ps_info_shop_params)
      render json: @ps_info_shop
    else
      render json: @ps_info_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_info_shops/1
  def destroy
    @ps_info_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_info_shop
      @ps_info_shop = PsInfoShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_info_shop_params
      params.fetch(:ps_info_shop, {})
    end
end
