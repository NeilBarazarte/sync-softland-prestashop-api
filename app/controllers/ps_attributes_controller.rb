class PsAttributesController < ApplicationController
  before_action :set_ps_attribute, only: [:show, :update, :destroy]

  # GET /ps_attributes
  def index
    @ps_attributes = PsAttribute.all

    render json: @ps_attributes
  end

  # GET /ps_attributes/1
  def show
    render json: @ps_attribute
  end

  # POST /ps_attributes
  def create
    @ps_attribute = PsAttribute.new(ps_attribute_params)

    if @ps_attribute.save
      render json: @ps_attribute, status: :created, location: @ps_attribute
    else
      render json: @ps_attribute.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_attributes/1
  def update
    if @ps_attribute.update(ps_attribute_params)
      render json: @ps_attribute
    else
      render json: @ps_attribute.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_attributes/1
  def destroy
    @ps_attribute.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_attribute
      @ps_attribute = PsAttribute.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_attribute_params
      params.fetch(:ps_attribute, {})
    end
end
