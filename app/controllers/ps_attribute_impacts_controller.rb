class PsAttributeImpactsController < ApplicationController
  before_action :set_ps_attribute_impact, only: [:show, :update, :destroy]

  # GET /ps_attribute_impacts
  def index
    @ps_attribute_impacts = PsAttributeImpact.all

    render json: @ps_attribute_impacts
  end

  # GET /ps_attribute_impacts/1
  def show
    render json: @ps_attribute_impact
  end

  # POST /ps_attribute_impacts
  def create
    @ps_attribute_impact = PsAttributeImpact.new(ps_attribute_impact_params)

    if @ps_attribute_impact.save
      render json: @ps_attribute_impact, status: :created, location: @ps_attribute_impact
    else
      render json: @ps_attribute_impact.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_attribute_impacts/1
  def update
    if @ps_attribute_impact.update(ps_attribute_impact_params)
      render json: @ps_attribute_impact
    else
      render json: @ps_attribute_impact.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_attribute_impacts/1
  def destroy
    @ps_attribute_impact.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_attribute_impact
      @ps_attribute_impact = PsAttributeImpact.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_attribute_impact_params
      params.fetch(:ps_attribute_impact, {})
    end
end
