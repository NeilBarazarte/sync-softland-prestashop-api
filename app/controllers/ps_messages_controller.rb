class PsMessagesController < ApplicationController
  before_action :set_ps_message, only: [:show, :update, :destroy]

  # GET /ps_messages
  def index
    @ps_messages = PsMessage.all

    render json: @ps_messages
  end

  # GET /ps_messages/1
  def show
    render json: @ps_message
  end

  # POST /ps_messages
  def create
    @ps_message = PsMessage.new(ps_message_params)

    if @ps_message.save
      render json: @ps_message, status: :created, location: @ps_message
    else
      render json: @ps_message.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_messages/1
  def update
    if @ps_message.update(ps_message_params)
      render json: @ps_message
    else
      render json: @ps_message.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_messages/1
  def destroy
    @ps_message.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_message
      @ps_message = PsMessage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_message_params
      params.fetch(:ps_message, {})
    end
end
