class PsShopsController < ApplicationController
  before_action :set_ps_shop, only: [:show, :update, :destroy]

  # GET /ps_shops
  def index
    @ps_shops = PsShop.all

    render json: @ps_shops
  end

  # GET /ps_shops/1
  def show
    render json: @ps_shop
  end

  # POST /ps_shops
  def create
    @ps_shop = PsShop.new(ps_shop_params)

    if @ps_shop.save
      render json: @ps_shop, status: :created, location: @ps_shop
    else
      render json: @ps_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_shops/1
  def update
    if @ps_shop.update(ps_shop_params)
      render json: @ps_shop
    else
      render json: @ps_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_shops/1
  def destroy
    @ps_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_shop
      @ps_shop = PsShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_shop_params
      params.fetch(:ps_shop, {})
    end
end
