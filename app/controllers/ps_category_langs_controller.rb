class PsCategoryLangsController < ApplicationController
  before_action :set_ps_category_lang, only: [:show, :update, :destroy]

  # GET /ps_category_langs
  def index
    @ps_category_langs = PsCategoryLang.all

    render json: @ps_category_langs
  end

  # GET /ps_category_langs/1
  def show
    render json: @ps_category_lang
  end

  # POST /ps_category_langs
  def create
    @ps_category_lang = PsCategoryLang.new(ps_category_lang_params)

    if @ps_category_lang.save
      render json: @ps_category_lang, status: :created, location: @ps_category_lang
    else
      render json: @ps_category_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_category_langs/1
  def update
    if @ps_category_lang.update(ps_category_lang_params)
      render json: @ps_category_lang
    else
      render json: @ps_category_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_category_langs/1
  def destroy
    @ps_category_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_category_lang
      @ps_category_lang = PsCategoryLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_category_lang_params
      params.fetch(:ps_category_lang, {})
    end
end
