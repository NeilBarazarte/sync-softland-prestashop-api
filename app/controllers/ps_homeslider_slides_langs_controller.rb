class PsHomesliderSlidesLangsController < ApplicationController
  before_action :set_ps_homeslider_slides_lang, only: [:show, :update, :destroy]

  # GET /ps_homeslider_slides_langs
  def index
    @ps_homeslider_slides_langs = PsHomesliderSlidesLang.all

    render json: @ps_homeslider_slides_langs
  end

  # GET /ps_homeslider_slides_langs/1
  def show
    render json: @ps_homeslider_slides_lang
  end

  # POST /ps_homeslider_slides_langs
  def create
    @ps_homeslider_slides_lang = PsHomesliderSlidesLang.new(ps_homeslider_slides_lang_params)

    if @ps_homeslider_slides_lang.save
      render json: @ps_homeslider_slides_lang, status: :created, location: @ps_homeslider_slides_lang
    else
      render json: @ps_homeslider_slides_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_homeslider_slides_langs/1
  def update
    if @ps_homeslider_slides_lang.update(ps_homeslider_slides_lang_params)
      render json: @ps_homeslider_slides_lang
    else
      render json: @ps_homeslider_slides_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_homeslider_slides_langs/1
  def destroy
    @ps_homeslider_slides_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_homeslider_slides_lang
      @ps_homeslider_slides_lang = PsHomesliderSlidesLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_homeslider_slides_lang_params
      params.fetch(:ps_homeslider_slides_lang, {})
    end
end
