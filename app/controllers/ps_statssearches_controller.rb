class PsStatssearchesController < ApplicationController
  before_action :set_ps_statssearch, only: [:show, :update, :destroy]

  # GET /ps_statssearches
  def index
    @ps_statssearches = PsStatssearch.all

    render json: @ps_statssearches
  end

  # GET /ps_statssearches/1
  def show
    render json: @ps_statssearch
  end

  # POST /ps_statssearches
  def create
    @ps_statssearch = PsStatssearch.new(ps_statssearch_params)

    if @ps_statssearch.save
      render json: @ps_statssearch, status: :created, location: @ps_statssearch
    else
      render json: @ps_statssearch.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_statssearches/1
  def update
    if @ps_statssearch.update(ps_statssearch_params)
      render json: @ps_statssearch
    else
      render json: @ps_statssearch.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_statssearches/1
  def destroy
    @ps_statssearch.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_statssearch
      @ps_statssearch = PsStatssearch.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_statssearch_params
      params.fetch(:ps_statssearch, {})
    end
end
