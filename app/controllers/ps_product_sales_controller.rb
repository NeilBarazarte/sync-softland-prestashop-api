class PsProductSalesController < ApplicationController
  before_action :set_ps_product_sale, only: [:show, :update, :destroy]

  # GET /ps_product_sales
  def index
    @ps_product_sales = PsProductSale.all

    render json: @ps_product_sales
  end

  # GET /ps_product_sales/1
  def show
    render json: @ps_product_sale
  end

  # POST /ps_product_sales
  def create
    @ps_product_sale = PsProductSale.new(ps_product_sale_params)

    if @ps_product_sale.save
      render json: @ps_product_sale, status: :created, location: @ps_product_sale
    else
      render json: @ps_product_sale.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_sales/1
  def update
    if @ps_product_sale.update(ps_product_sale_params)
      render json: @ps_product_sale
    else
      render json: @ps_product_sale.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_sales/1
  def destroy
    @ps_product_sale.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_sale
      @ps_product_sale = PsProductSale.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_sale_params
      params.fetch(:ps_product_sale, {})
    end
end
