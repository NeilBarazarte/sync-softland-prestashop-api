class PsInfosController < ApplicationController
  before_action :set_ps_info, only: [:show, :update, :destroy]

  # GET /ps_infos
  def index
    @ps_infos = PsInfo.all

    render json: @ps_infos
  end

  # GET /ps_infos/1
  def show
    render json: @ps_info
  end

  # POST /ps_infos
  def create
    @ps_info = PsInfo.new(ps_info_params)

    if @ps_info.save
      render json: @ps_info, status: :created, location: @ps_info
    else
      render json: @ps_info.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_infos/1
  def update
    if @ps_info.update(ps_info_params)
      render json: @ps_info
    else
      render json: @ps_info.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_infos/1
  def destroy
    @ps_info.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_info
      @ps_info = PsInfo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_info_params
      params.fetch(:ps_info, {})
    end
end
