class PsOrderInvoicesController < ApplicationController
  before_action :set_ps_order_invoice, only: [:show, :update, :destroy]

  # GET /ps_order_invoices
  def index
    @ps_order_invoices = PsOrderInvoice.all

    render json: @ps_order_invoices
  end

  # GET /ps_order_invoices/1
  def show
    render json: @ps_order_invoice
  end

  # POST /ps_order_invoices
  def create
    @ps_order_invoice = PsOrderInvoice.new(ps_order_invoice_params)

    if @ps_order_invoice.save
      render json: @ps_order_invoice, status: :created, location: @ps_order_invoice
    else
      render json: @ps_order_invoice.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_invoices/1
  def update
    if @ps_order_invoice.update(ps_order_invoice_params)
      render json: @ps_order_invoice
    else
      render json: @ps_order_invoice.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_invoices/1
  def destroy
    @ps_order_invoice.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_invoice
      @ps_order_invoice = PsOrderInvoice.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_invoice_params
      params.fetch(:ps_order_invoice, {})
    end
end
