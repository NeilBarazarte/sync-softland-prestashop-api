class PsPageTypesController < ApplicationController
  before_action :set_ps_page_type, only: [:show, :update, :destroy]

  # GET /ps_page_types
  def index
    @ps_page_types = PsPageType.all

    render json: @ps_page_types
  end

  # GET /ps_page_types/1
  def show
    render json: @ps_page_type
  end

  # POST /ps_page_types
  def create
    @ps_page_type = PsPageType.new(ps_page_type_params)

    if @ps_page_type.save
      render json: @ps_page_type, status: :created, location: @ps_page_type
    else
      render json: @ps_page_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_page_types/1
  def update
    if @ps_page_type.update(ps_page_type_params)
      render json: @ps_page_type
    else
      render json: @ps_page_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_page_types/1
  def destroy
    @ps_page_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_page_type
      @ps_page_type = PsPageType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_page_type_params
      params.fetch(:ps_page_type, {})
    end
end
