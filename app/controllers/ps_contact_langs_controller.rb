class PsContactLangsController < ApplicationController
  before_action :set_ps_contact_lang, only: [:show, :update, :destroy]

  # GET /ps_contact_langs
  def index
    @ps_contact_langs = PsContactLang.all

    render json: @ps_contact_langs
  end

  # GET /ps_contact_langs/1
  def show
    render json: @ps_contact_lang
  end

  # POST /ps_contact_langs
  def create
    @ps_contact_lang = PsContactLang.new(ps_contact_lang_params)

    if @ps_contact_lang.save
      render json: @ps_contact_lang, status: :created, location: @ps_contact_lang
    else
      render json: @ps_contact_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_contact_langs/1
  def update
    if @ps_contact_lang.update(ps_contact_lang_params)
      render json: @ps_contact_lang
    else
      render json: @ps_contact_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_contact_langs/1
  def destroy
    @ps_contact_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_contact_lang
      @ps_contact_lang = PsContactLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_contact_lang_params
      params.fetch(:ps_contact_lang, {})
    end
end
