class PsGroupsController < ApplicationController
  before_action :set_ps_group, only: [:show, :update, :destroy]

  # GET /ps_groups
  def index
    @ps_groups = PsGroup.all

    render json: @ps_groups
  end

  # GET /ps_groups/1
  def show
    render json: @ps_group
  end

  # POST /ps_groups
  def create
    @ps_group = PsGroup.new(ps_group_params)

    if @ps_group.save
      render json: @ps_group, status: :created, location: @ps_group
    else
      render json: @ps_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_groups/1
  def update
    if @ps_group.update(ps_group_params)
      render json: @ps_group
    else
      render json: @ps_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_groups/1
  def destroy
    @ps_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_group
      @ps_group = PsGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_group_params
      params.fetch(:ps_group, {})
    end
end
