class PsCartRuleCountriesController < ApplicationController
  before_action :set_ps_cart_rule_country, only: [:show, :update, :destroy]

  # GET /ps_cart_rule_countries
  def index
    @ps_cart_rule_countries = PsCartRuleCountry.all

    render json: @ps_cart_rule_countries
  end

  # GET /ps_cart_rule_countries/1
  def show
    render json: @ps_cart_rule_country
  end

  # POST /ps_cart_rule_countries
  def create
    @ps_cart_rule_country = PsCartRuleCountry.new(ps_cart_rule_country_params)

    if @ps_cart_rule_country.save
      render json: @ps_cart_rule_country, status: :created, location: @ps_cart_rule_country
    else
      render json: @ps_cart_rule_country.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_rule_countries/1
  def update
    if @ps_cart_rule_country.update(ps_cart_rule_country_params)
      render json: @ps_cart_rule_country
    else
      render json: @ps_cart_rule_country.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_rule_countries/1
  def destroy
    @ps_cart_rule_country.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_rule_country
      @ps_cart_rule_country = PsCartRuleCountry.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_rule_country_params
      params.fetch(:ps_cart_rule_country, {})
    end
end
