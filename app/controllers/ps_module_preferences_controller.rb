class PsModulePreferencesController < ApplicationController
  before_action :set_ps_module_preference, only: [:show, :update, :destroy]

  # GET /ps_module_preferences
  def index
    @ps_module_preferences = PsModulePreference.all

    render json: @ps_module_preferences
  end

  # GET /ps_module_preferences/1
  def show
    render json: @ps_module_preference
  end

  # POST /ps_module_preferences
  def create
    @ps_module_preference = PsModulePreference.new(ps_module_preference_params)

    if @ps_module_preference.save
      render json: @ps_module_preference, status: :created, location: @ps_module_preference
    else
      render json: @ps_module_preference.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_module_preferences/1
  def update
    if @ps_module_preference.update(ps_module_preference_params)
      render json: @ps_module_preference
    else
      render json: @ps_module_preference.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_module_preferences/1
  def destroy
    @ps_module_preference.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_module_preference
      @ps_module_preference = PsModulePreference.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_module_preference_params
      params.fetch(:ps_module_preference, {})
    end
end
