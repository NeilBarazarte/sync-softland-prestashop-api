class PsStatesController < ApplicationController
  before_action :set_ps_state, only: [:show, :update, :destroy]

  # GET /ps_states
  def index
    @ps_states = PsState.all

    render json: @ps_states
  end

  # GET /ps_states/1
  def show
    render json: @ps_state
  end

  # POST /ps_states
  def create
    @ps_state = PsState.new(ps_state_params)

    if @ps_state.save
      render json: @ps_state, status: :created, location: @ps_state
    else
      render json: @ps_state.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_states/1
  def update
    if @ps_state.update(ps_state_params)
      render json: @ps_state
    else
      render json: @ps_state.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_states/1
  def destroy
    @ps_state.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_state
      @ps_state = PsState.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_state_params
      params.fetch(:ps_state, {})
    end
end
