class PsSupplyOrderStatesController < ApplicationController
  before_action :set_ps_supply_order_state, only: [:show, :update, :destroy]

  # GET /ps_supply_order_states
  def index
    @ps_supply_order_states = PsSupplyOrderState.all

    render json: @ps_supply_order_states
  end

  # GET /ps_supply_order_states/1
  def show
    render json: @ps_supply_order_state
  end

  # POST /ps_supply_order_states
  def create
    @ps_supply_order_state = PsSupplyOrderState.new(ps_supply_order_state_params)

    if @ps_supply_order_state.save
      render json: @ps_supply_order_state, status: :created, location: @ps_supply_order_state
    else
      render json: @ps_supply_order_state.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_supply_order_states/1
  def update
    if @ps_supply_order_state.update(ps_supply_order_state_params)
      render json: @ps_supply_order_state
    else
      render json: @ps_supply_order_state.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_supply_order_states/1
  def destroy
    @ps_supply_order_state.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_supply_order_state
      @ps_supply_order_state = PsSupplyOrderState.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_supply_order_state_params
      params.fetch(:ps_supply_order_state, {})
    end
end
