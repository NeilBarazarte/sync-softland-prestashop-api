class PsSupplyOrderDetailsController < ApplicationController
  before_action :set_ps_supply_order_detail, only: [:show, :update, :destroy]

  # GET /ps_supply_order_details
  def index
    @ps_supply_order_details = PsSupplyOrderDetail.all

    render json: @ps_supply_order_details
  end

  # GET /ps_supply_order_details/1
  def show
    render json: @ps_supply_order_detail
  end

  # POST /ps_supply_order_details
  def create
    @ps_supply_order_detail = PsSupplyOrderDetail.new(ps_supply_order_detail_params)

    if @ps_supply_order_detail.save
      render json: @ps_supply_order_detail, status: :created, location: @ps_supply_order_detail
    else
      render json: @ps_supply_order_detail.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_supply_order_details/1
  def update
    if @ps_supply_order_detail.update(ps_supply_order_detail_params)
      render json: @ps_supply_order_detail
    else
      render json: @ps_supply_order_detail.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_supply_order_details/1
  def destroy
    @ps_supply_order_detail.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_supply_order_detail
      @ps_supply_order_detail = PsSupplyOrderDetail.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_supply_order_detail_params
      params.fetch(:ps_supply_order_detail, {})
    end
end
