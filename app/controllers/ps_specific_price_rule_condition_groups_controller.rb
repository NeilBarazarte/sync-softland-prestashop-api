class PsSpecificPriceRuleConditionGroupsController < ApplicationController
  before_action :set_ps_specific_price_rule_condition_group, only: [:show, :update, :destroy]

  # GET /ps_specific_price_rule_condition_groups
  def index
    @ps_specific_price_rule_condition_groups = PsSpecificPriceRuleConditionGroup.all

    render json: @ps_specific_price_rule_condition_groups
  end

  # GET /ps_specific_price_rule_condition_groups/1
  def show
    render json: @ps_specific_price_rule_condition_group
  end

  # POST /ps_specific_price_rule_condition_groups
  def create
    @ps_specific_price_rule_condition_group = PsSpecificPriceRuleConditionGroup.new(ps_specific_price_rule_condition_group_params)

    if @ps_specific_price_rule_condition_group.save
      render json: @ps_specific_price_rule_condition_group, status: :created, location: @ps_specific_price_rule_condition_group
    else
      render json: @ps_specific_price_rule_condition_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_specific_price_rule_condition_groups/1
  def update
    if @ps_specific_price_rule_condition_group.update(ps_specific_price_rule_condition_group_params)
      render json: @ps_specific_price_rule_condition_group
    else
      render json: @ps_specific_price_rule_condition_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_specific_price_rule_condition_groups/1
  def destroy
    @ps_specific_price_rule_condition_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_specific_price_rule_condition_group
      @ps_specific_price_rule_condition_group = PsSpecificPriceRuleConditionGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_specific_price_rule_condition_group_params
      params.fetch(:ps_specific_price_rule_condition_group, {})
    end
end
