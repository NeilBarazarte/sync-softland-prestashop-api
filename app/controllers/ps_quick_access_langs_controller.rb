class PsQuickAccessLangsController < ApplicationController
  before_action :set_ps_quick_access_lang, only: [:show, :update, :destroy]

  # GET /ps_quick_access_langs
  def index
    @ps_quick_access_langs = PsQuickAccessLang.all

    render json: @ps_quick_access_langs
  end

  # GET /ps_quick_access_langs/1
  def show
    render json: @ps_quick_access_lang
  end

  # POST /ps_quick_access_langs
  def create
    @ps_quick_access_lang = PsQuickAccessLang.new(ps_quick_access_lang_params)

    if @ps_quick_access_lang.save
      render json: @ps_quick_access_lang, status: :created, location: @ps_quick_access_lang
    else
      render json: @ps_quick_access_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_quick_access_langs/1
  def update
    if @ps_quick_access_lang.update(ps_quick_access_lang_params)
      render json: @ps_quick_access_lang
    else
      render json: @ps_quick_access_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_quick_access_langs/1
  def destroy
    @ps_quick_access_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_quick_access_lang
      @ps_quick_access_lang = PsQuickAccessLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_quick_access_lang_params
      params.fetch(:ps_quick_access_lang, {})
    end
end
