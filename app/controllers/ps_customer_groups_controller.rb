class PsCustomerGroupsController < ApplicationController
  before_action :set_ps_customer_group, only: [:show, :update, :destroy]

  # GET /ps_customer_groups
  def index
    @ps_customer_groups = PsCustomerGroup.all

    render json: @ps_customer_groups
  end

  # GET /ps_customer_groups/1
  def show
    render json: @ps_customer_group
  end

  # POST /ps_customer_groups
  def create
    @ps_customer_group = PsCustomerGroup.new(ps_customer_group_params)

    if @ps_customer_group.save
      render json: @ps_customer_group, status: :created, location: @ps_customer_group
    else
      render json: @ps_customer_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_customer_groups/1
  def update
    if @ps_customer_group.update(ps_customer_group_params)
      render json: @ps_customer_group
    else
      render json: @ps_customer_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_customer_groups/1
  def destroy
    @ps_customer_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_customer_group
      @ps_customer_group = PsCustomerGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_customer_group_params
      params.fetch(:ps_customer_group, {})
    end
end
