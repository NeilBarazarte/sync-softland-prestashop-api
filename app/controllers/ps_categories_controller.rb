class PsCategoriesController < ApplicationController
  before_action :set_ps_category, only: [:show, :update, :destroy]

  # GET /ps_categories
  def index
    @ps_categories = PsCategory.all

    render json: @ps_categories
  end

  # GET /ps_categories/1
  def show
    render json: @ps_category
  end

  # POST /ps_categories
  def create
    @ps_category = PsCategory.new(ps_category_params)

    if @ps_category.save
      render json: @ps_category, status: :created, location: @ps_category
    else
      render json: @ps_category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_categories/1
  def update
    if @ps_category.update(ps_category_params)
      render json: @ps_category
    else
      render json: @ps_category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_categories/1
  def destroy
    @ps_category.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_category
      @ps_category = PsCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_category_params
      params.fetch(:ps_category, {})
    end
end
