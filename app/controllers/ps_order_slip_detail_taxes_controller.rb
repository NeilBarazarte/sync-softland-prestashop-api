class PsOrderSlipDetailTaxesController < ApplicationController
  before_action :set_ps_order_slip_detail_tax, only: [:show, :update, :destroy]

  # GET /ps_order_slip_detail_taxes
  def index
    @ps_order_slip_detail_taxes = PsOrderSlipDetailTax.all

    render json: @ps_order_slip_detail_taxes
  end

  # GET /ps_order_slip_detail_taxes/1
  def show
    render json: @ps_order_slip_detail_tax
  end

  # POST /ps_order_slip_detail_taxes
  def create
    @ps_order_slip_detail_tax = PsOrderSlipDetailTax.new(ps_order_slip_detail_tax_params)

    if @ps_order_slip_detail_tax.save
      render json: @ps_order_slip_detail_tax, status: :created, location: @ps_order_slip_detail_tax
    else
      render json: @ps_order_slip_detail_tax.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_slip_detail_taxes/1
  def update
    if @ps_order_slip_detail_tax.update(ps_order_slip_detail_tax_params)
      render json: @ps_order_slip_detail_tax
    else
      render json: @ps_order_slip_detail_tax.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_slip_detail_taxes/1
  def destroy
    @ps_order_slip_detail_tax.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_slip_detail_tax
      @ps_order_slip_detail_tax = PsOrderSlipDetailTax.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_slip_detail_tax_params
      params.fetch(:ps_order_slip_detail_tax, {})
    end
end
