class PsTaxLangsController < ApplicationController
  before_action :set_ps_tax_lang, only: [:show, :update, :destroy]

  # GET /ps_tax_langs
  def index
    @ps_tax_langs = PsTaxLang.all

    render json: @ps_tax_langs
  end

  # GET /ps_tax_langs/1
  def show
    render json: @ps_tax_lang
  end

  # POST /ps_tax_langs
  def create
    @ps_tax_lang = PsTaxLang.new(ps_tax_lang_params)

    if @ps_tax_lang.save
      render json: @ps_tax_lang, status: :created, location: @ps_tax_lang
    else
      render json: @ps_tax_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tax_langs/1
  def update
    if @ps_tax_lang.update(ps_tax_lang_params)
      render json: @ps_tax_lang
    else
      render json: @ps_tax_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tax_langs/1
  def destroy
    @ps_tax_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tax_lang
      @ps_tax_lang = PsTaxLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tax_lang_params
      params.fetch(:ps_tax_lang, {})
    end
end
