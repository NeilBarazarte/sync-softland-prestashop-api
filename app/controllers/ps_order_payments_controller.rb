class PsOrderPaymentsController < ApplicationController
  before_action :set_ps_order_payment, only: [:show, :update, :destroy]

  # GET /ps_order_payments
  def index
    @ps_order_payments = PsOrderPayment.all

    render json: @ps_order_payments
  end

  # GET /ps_order_payments/1
  def show
    render json: @ps_order_payment
  end

  # POST /ps_order_payments
  def create
    @ps_order_payment = PsOrderPayment.new(ps_order_payment_params)

    if @ps_order_payment.save
      render json: @ps_order_payment, status: :created, location: @ps_order_payment
    else
      render json: @ps_order_payment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_payments/1
  def update
    if @ps_order_payment.update(ps_order_payment_params)
      render json: @ps_order_payment
    else
      render json: @ps_order_payment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_payments/1
  def destroy
    @ps_order_payment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_payment
      @ps_order_payment = PsOrderPayment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_payment_params
      params.fetch(:ps_order_payment, {})
    end
end
