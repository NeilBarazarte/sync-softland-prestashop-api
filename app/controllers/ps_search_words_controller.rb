class PsSearchWordsController < ApplicationController
  before_action :set_ps_search_word, only: [:show, :update, :destroy]

  # GET /ps_search_words
  def index
    @ps_search_words = PsSearchWord.all

    render json: @ps_search_words
  end

  # GET /ps_search_words/1
  def show
    render json: @ps_search_word
  end

  # POST /ps_search_words
  def create
    @ps_search_word = PsSearchWord.new(ps_search_word_params)

    if @ps_search_word.save
      render json: @ps_search_word, status: :created, location: @ps_search_word
    else
      render json: @ps_search_word.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_search_words/1
  def update
    if @ps_search_word.update(ps_search_word_params)
      render json: @ps_search_word
    else
      render json: @ps_search_word.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_search_words/1
  def destroy
    @ps_search_word.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_search_word
      @ps_search_word = PsSearchWord.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_search_word_params
      params.fetch(:ps_search_word, {})
    end
end
