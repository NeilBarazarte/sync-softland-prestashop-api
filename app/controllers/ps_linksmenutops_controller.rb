class PsLinksmenutopsController < ApplicationController
  before_action :set_ps_linksmenutop, only: [:show, :update, :destroy]

  # GET /ps_linksmenutops
  def index
    @ps_linksmenutops = PsLinksmenutop.all

    render json: @ps_linksmenutops
  end

  # GET /ps_linksmenutops/1
  def show
    render json: @ps_linksmenutop
  end

  # POST /ps_linksmenutops
  def create
    @ps_linksmenutop = PsLinksmenutop.new(ps_linksmenutop_params)

    if @ps_linksmenutop.save
      render json: @ps_linksmenutop, status: :created, location: @ps_linksmenutop
    else
      render json: @ps_linksmenutop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_linksmenutops/1
  def update
    if @ps_linksmenutop.update(ps_linksmenutop_params)
      render json: @ps_linksmenutop
    else
      render json: @ps_linksmenutop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_linksmenutops/1
  def destroy
    @ps_linksmenutop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_linksmenutop
      @ps_linksmenutop = PsLinksmenutop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_linksmenutop_params
      params.fetch(:ps_linksmenutop, {})
    end
end
