class PsProductAttributesController < ApplicationController
  before_action :set_ps_product_attribute, only: [:show, :update, :destroy]

  # GET /ps_product_attributes
  def index
    @ps_product_attributes = PsProductAttribute.all

    render json: @ps_product_attributes
  end

  # GET /ps_product_attributes/1
  def show
    render json: @ps_product_attribute
  end

  # POST /ps_product_attributes
  def create
    @ps_product_attribute = PsProductAttribute.new(ps_product_attribute_params)

    if @ps_product_attribute.save
      render json: @ps_product_attribute, status: :created, location: @ps_product_attribute
    else
      render json: @ps_product_attribute.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_attributes/1
  def update
    if @ps_product_attribute.update(ps_product_attribute_params)
      render json: @ps_product_attribute
    else
      render json: @ps_product_attribute.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_attributes/1
  def destroy
    @ps_product_attribute.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_attribute
      @ps_product_attribute = PsProductAttribute.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_attribute_params
      params.fetch(:ps_product_attribute, {})
    end
end
