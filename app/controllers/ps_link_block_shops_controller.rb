class PsLinkBlockShopsController < ApplicationController
  before_action :set_ps_link_block_shop, only: [:show, :update, :destroy]

  # GET /ps_link_block_shops
  def index
    @ps_link_block_shops = PsLinkBlockShop.all

    render json: @ps_link_block_shops
  end

  # GET /ps_link_block_shops/1
  def show
    render json: @ps_link_block_shop
  end

  # POST /ps_link_block_shops
  def create
    @ps_link_block_shop = PsLinkBlockShop.new(ps_link_block_shop_params)

    if @ps_link_block_shop.save
      render json: @ps_link_block_shop, status: :created, location: @ps_link_block_shop
    else
      render json: @ps_link_block_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_link_block_shops/1
  def update
    if @ps_link_block_shop.update(ps_link_block_shop_params)
      render json: @ps_link_block_shop
    else
      render json: @ps_link_block_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_link_block_shops/1
  def destroy
    @ps_link_block_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_link_block_shop
      @ps_link_block_shop = PsLinkBlockShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_link_block_shop_params
      params.fetch(:ps_link_block_shop, {})
    end
end
