class PsOrderReturnsController < ApplicationController
  before_action :set_ps_order_return, only: [:show, :update, :destroy]

  # GET /ps_order_returns
  def index
    @ps_order_returns = PsOrderReturn.all

    render json: @ps_order_returns
  end

  # GET /ps_order_returns/1
  def show
    render json: @ps_order_return
  end

  # POST /ps_order_returns
  def create
    @ps_order_return = PsOrderReturn.new(ps_order_return_params)

    if @ps_order_return.save
      render json: @ps_order_return, status: :created, location: @ps_order_return
    else
      render json: @ps_order_return.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_returns/1
  def update
    if @ps_order_return.update(ps_order_return_params)
      render json: @ps_order_return
    else
      render json: @ps_order_return.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_returns/1
  def destroy
    @ps_order_return.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_return
      @ps_order_return = PsOrderReturn.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_return_params
      params.fetch(:ps_order_return, {})
    end
end
