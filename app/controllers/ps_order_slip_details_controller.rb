class PsOrderSlipDetailsController < ApplicationController
  before_action :set_ps_order_slip_detail, only: [:show, :update, :destroy]

  # GET /ps_order_slip_details
  def index
    @ps_order_slip_details = PsOrderSlipDetail.all

    render json: @ps_order_slip_details
  end

  # GET /ps_order_slip_details/1
  def show
    render json: @ps_order_slip_detail
  end

  # POST /ps_order_slip_details
  def create
    @ps_order_slip_detail = PsOrderSlipDetail.new(ps_order_slip_detail_params)

    if @ps_order_slip_detail.save
      render json: @ps_order_slip_detail, status: :created, location: @ps_order_slip_detail
    else
      render json: @ps_order_slip_detail.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_slip_details/1
  def update
    if @ps_order_slip_detail.update(ps_order_slip_detail_params)
      render json: @ps_order_slip_detail
    else
      render json: @ps_order_slip_detail.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_slip_details/1
  def destroy
    @ps_order_slip_detail.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_slip_detail
      @ps_order_slip_detail = PsOrderSlipDetail.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_slip_detail_params
      params.fetch(:ps_order_slip_detail, {})
    end
end
