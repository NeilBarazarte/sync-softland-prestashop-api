class PsLayeredFilterShopsController < ApplicationController
  before_action :set_ps_layered_filter_shop, only: [:show, :update, :destroy]

  # GET /ps_layered_filter_shops
  def index
    @ps_layered_filter_shops = PsLayeredFilterShop.all

    render json: @ps_layered_filter_shops
  end

  # GET /ps_layered_filter_shops/1
  def show
    render json: @ps_layered_filter_shop
  end

  # POST /ps_layered_filter_shops
  def create
    @ps_layered_filter_shop = PsLayeredFilterShop.new(ps_layered_filter_shop_params)

    if @ps_layered_filter_shop.save
      render json: @ps_layered_filter_shop, status: :created, location: @ps_layered_filter_shop
    else
      render json: @ps_layered_filter_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_layered_filter_shops/1
  def update
    if @ps_layered_filter_shop.update(ps_layered_filter_shop_params)
      render json: @ps_layered_filter_shop
    else
      render json: @ps_layered_filter_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_layered_filter_shops/1
  def destroy
    @ps_layered_filter_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_layered_filter_shop
      @ps_layered_filter_shop = PsLayeredFilterShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_layered_filter_shop_params
      params.fetch(:ps_layered_filter_shop, {})
    end
end
