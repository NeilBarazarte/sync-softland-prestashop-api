class PsAdminFiltersController < ApplicationController
  before_action :set_ps_admin_filter, only: [:show, :update, :destroy]

  # GET /ps_admin_filters
  def index
    @ps_admin_filters = PsAdminFilter.all

    render json: @ps_admin_filters
  end

  # GET /ps_admin_filters/1
  def show
    render json: @ps_admin_filter
  end

  # POST /ps_admin_filters
  def create
    @ps_admin_filter = PsAdminFilter.new(ps_admin_filter_params)

    if @ps_admin_filter.save
      render json: @ps_admin_filter, status: :created, location: @ps_admin_filter
    else
      render json: @ps_admin_filter.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_admin_filters/1
  def update
    if @ps_admin_filter.update(ps_admin_filter_params)
      render json: @ps_admin_filter
    else
      render json: @ps_admin_filter.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_admin_filters/1
  def destroy
    @ps_admin_filter.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_admin_filter
      @ps_admin_filter = PsAdminFilter.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_admin_filter_params
      params.fetch(:ps_admin_filter, {})
    end
end
