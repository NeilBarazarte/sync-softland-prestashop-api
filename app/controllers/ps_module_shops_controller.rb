class PsModuleShopsController < ApplicationController
  before_action :set_ps_module_shop, only: [:show, :update, :destroy]

  # GET /ps_module_shops
  def index
    @ps_module_shops = PsModuleShop.all

    render json: @ps_module_shops
  end

  # GET /ps_module_shops/1
  def show
    render json: @ps_module_shop
  end

  # POST /ps_module_shops
  def create
    @ps_module_shop = PsModuleShop.new(ps_module_shop_params)

    if @ps_module_shop.save
      render json: @ps_module_shop, status: :created, location: @ps_module_shop
    else
      render json: @ps_module_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_module_shops/1
  def update
    if @ps_module_shop.update(ps_module_shop_params)
      render json: @ps_module_shop
    else
      render json: @ps_module_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_module_shops/1
  def destroy
    @ps_module_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_module_shop
      @ps_module_shop = PsModuleShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_module_shop_params
      params.fetch(:ps_module_shop, {})
    end
end
