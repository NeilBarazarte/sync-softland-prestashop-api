class PsHooksController < ApplicationController
  before_action :set_ps_hook, only: [:show, :update, :destroy]

  # GET /ps_hooks
  def index
    @ps_hooks = PsHook.all

    render json: @ps_hooks
  end

  # GET /ps_hooks/1
  def show
    render json: @ps_hook
  end

  # POST /ps_hooks
  def create
    @ps_hook = PsHook.new(ps_hook_params)

    if @ps_hook.save
      render json: @ps_hook, status: :created, location: @ps_hook
    else
      render json: @ps_hook.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_hooks/1
  def update
    if @ps_hook.update(ps_hook_params)
      render json: @ps_hook
    else
      render json: @ps_hook.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_hooks/1
  def destroy
    @ps_hook.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_hook
      @ps_hook = PsHook.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_hook_params
      params.fetch(:ps_hook, {})
    end
end
