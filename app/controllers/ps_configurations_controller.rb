class PsConfigurationsController < ApplicationController
  before_action :set_ps_configuration, only: [:show, :update, :destroy]

  # GET /ps_configurations
  def index
    @ps_configurations = PsConfiguration.all

    render json: @ps_configurations
  end

  # GET /ps_configurations/1
  def show
    render json: @ps_configuration
  end

  # POST /ps_configurations
  def create
    @ps_configuration = PsConfiguration.new(ps_configuration_params)

    if @ps_configuration.save
      render json: @ps_configuration, status: :created, location: @ps_configuration
    else
      render json: @ps_configuration.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_configurations/1
  def update
    if @ps_configuration.update(ps_configuration_params)
      render json: @ps_configuration
    else
      render json: @ps_configuration.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_configurations/1
  def destroy
    @ps_configuration.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_configuration
      @ps_configuration = PsConfiguration.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_configuration_params
      params.fetch(:ps_configuration, {})
    end
end
