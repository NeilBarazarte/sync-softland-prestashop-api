class PsCustomizedDataController < ApplicationController
  before_action :set_ps_customized_datum, only: [:show, :update, :destroy]

  # GET /ps_customized_data
  def index
    @ps_customized_data = PsCustomizedDatum.all

    render json: @ps_customized_data
  end

  # GET /ps_customized_data/1
  def show
    render json: @ps_customized_datum
  end

  # POST /ps_customized_data
  def create
    @ps_customized_datum = PsCustomizedDatum.new(ps_customized_datum_params)

    if @ps_customized_datum.save
      render json: @ps_customized_datum, status: :created, location: @ps_customized_datum
    else
      render json: @ps_customized_datum.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_customized_data/1
  def update
    if @ps_customized_datum.update(ps_customized_datum_params)
      render json: @ps_customized_datum
    else
      render json: @ps_customized_datum.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_customized_data/1
  def destroy
    @ps_customized_datum.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_customized_datum
      @ps_customized_datum = PsCustomizedDatum.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_customized_datum_params
      params.fetch(:ps_customized_datum, {})
    end
end
