class PsContactShopsController < ApplicationController
  before_action :set_ps_contact_shop, only: [:show, :update, :destroy]

  # GET /ps_contact_shops
  def index
    @ps_contact_shops = PsContactShop.all

    render json: @ps_contact_shops
  end

  # GET /ps_contact_shops/1
  def show
    render json: @ps_contact_shop
  end

  # POST /ps_contact_shops
  def create
    @ps_contact_shop = PsContactShop.new(ps_contact_shop_params)

    if @ps_contact_shop.save
      render json: @ps_contact_shop, status: :created, location: @ps_contact_shop
    else
      render json: @ps_contact_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_contact_shops/1
  def update
    if @ps_contact_shop.update(ps_contact_shop_params)
      render json: @ps_contact_shop
    else
      render json: @ps_contact_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_contact_shops/1
  def destroy
    @ps_contact_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_contact_shop
      @ps_contact_shop = PsContactShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_contact_shop_params
      params.fetch(:ps_contact_shop, {})
    end
end
