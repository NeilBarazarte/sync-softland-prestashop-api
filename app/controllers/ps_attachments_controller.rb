class PsAttachmentsController < ApplicationController
  before_action :set_ps_attachment, only: [:show, :update, :destroy]

  # GET /ps_attachments
  def index
    @ps_attachments = PsAttachment.all

    render json: @ps_attachments
  end

  # GET /ps_attachments/1
  def show
    render json: @ps_attachment
  end

  # POST /ps_attachments
  def create
    @ps_attachment = PsAttachment.new(ps_attachment_params)

    if @ps_attachment.save
      render json: @ps_attachment, status: :created, location: @ps_attachment
    else
      render json: @ps_attachment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_attachments/1
  def update
    if @ps_attachment.update(ps_attachment_params)
      render json: @ps_attachment
    else
      render json: @ps_attachment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_attachments/1
  def destroy
    @ps_attachment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_attachment
      @ps_attachment = PsAttachment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_attachment_params
      params.fetch(:ps_attachment, {})
    end
end
