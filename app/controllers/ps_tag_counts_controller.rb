class PsTagCountsController < ApplicationController
  before_action :set_ps_tag_count, only: [:show, :update, :destroy]

  # GET /ps_tag_counts
  def index
    @ps_tag_counts = PsTagCount.all

    render json: @ps_tag_counts
  end

  # GET /ps_tag_counts/1
  def show
    render json: @ps_tag_count
  end

  # POST /ps_tag_counts
  def create
    @ps_tag_count = PsTagCount.new(ps_tag_count_params)

    if @ps_tag_count.save
      render json: @ps_tag_count, status: :created, location: @ps_tag_count
    else
      render json: @ps_tag_count.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tag_counts/1
  def update
    if @ps_tag_count.update(ps_tag_count_params)
      render json: @ps_tag_count
    else
      render json: @ps_tag_count.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tag_counts/1
  def destroy
    @ps_tag_count.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tag_count
      @ps_tag_count = PsTagCount.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tag_count_params
      params.fetch(:ps_tag_count, {})
    end
end
