class PsAttributeGroupLangsController < ApplicationController
  before_action :set_ps_attribute_group_lang, only: [:show, :update, :destroy]

  # GET /ps_attribute_group_langs
  def index
    @ps_attribute_group_langs = PsAttributeGroupLang.all

    render json: @ps_attribute_group_langs
  end

  # GET /ps_attribute_group_langs/1
  def show
    render json: @ps_attribute_group_lang
  end

  # POST /ps_attribute_group_langs
  def create
    @ps_attribute_group_lang = PsAttributeGroupLang.new(ps_attribute_group_lang_params)

    if @ps_attribute_group_lang.save
      render json: @ps_attribute_group_lang, status: :created, location: @ps_attribute_group_lang
    else
      render json: @ps_attribute_group_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_attribute_group_langs/1
  def update
    if @ps_attribute_group_lang.update(ps_attribute_group_lang_params)
      render json: @ps_attribute_group_lang
    else
      render json: @ps_attribute_group_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_attribute_group_langs/1
  def destroy
    @ps_attribute_group_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_attribute_group_lang
      @ps_attribute_group_lang = PsAttributeGroupLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_attribute_group_lang_params
      params.fetch(:ps_attribute_group_lang, {})
    end
end
