class PsSpecificPriceRulesController < ApplicationController
  before_action :set_ps_specific_price_rule, only: [:show, :update, :destroy]

  # GET /ps_specific_price_rules
  def index
    @ps_specific_price_rules = PsSpecificPriceRule.all

    render json: @ps_specific_price_rules
  end

  # GET /ps_specific_price_rules/1
  def show
    render json: @ps_specific_price_rule
  end

  # POST /ps_specific_price_rules
  def create
    @ps_specific_price_rule = PsSpecificPriceRule.new(ps_specific_price_rule_params)

    if @ps_specific_price_rule.save
      render json: @ps_specific_price_rule, status: :created, location: @ps_specific_price_rule
    else
      render json: @ps_specific_price_rule.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_specific_price_rules/1
  def update
    if @ps_specific_price_rule.update(ps_specific_price_rule_params)
      render json: @ps_specific_price_rule
    else
      render json: @ps_specific_price_rule.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_specific_price_rules/1
  def destroy
    @ps_specific_price_rule.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_specific_price_rule
      @ps_specific_price_rule = PsSpecificPriceRule.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_specific_price_rule_params
      params.fetch(:ps_specific_price_rule, {})
    end
end
