class PsStockAvailablesController < ApplicationController
  before_action :set_ps_stock_available, only: [:show, :update, :destroy]

  # GET /ps_stock_availables
  def index
    @ps_stock_availables = PsStockAvailable.all

    render json: @ps_stock_availables
  end

  # GET /ps_stock_availables/1
  def show
    render json: @ps_stock_available
  end

  # POST /ps_stock_availables
  def create
    @ps_stock_available = PsStockAvailable.new(ps_stock_available_params)

    if @ps_stock_available.save
      render json: @ps_stock_available, status: :created, location: @ps_stock_available
    else
      render json: @ps_stock_available.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_stock_availables/1
  def update
    if @ps_stock_available.update(ps_stock_available_params)
      render json: @ps_stock_available
    else
      render json: @ps_stock_available.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_stock_availables/1
  def destroy
    @ps_stock_available.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_stock_available
      @ps_stock_available = PsStockAvailable.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_stock_available_params
      params.fetch(:ps_stock_available, {})
    end
end
