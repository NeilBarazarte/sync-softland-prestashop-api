class PsSearchEnginesController < ApplicationController
  before_action :set_ps_search_engine, only: [:show, :update, :destroy]

  # GET /ps_search_engines
  def index
    @ps_search_engines = PsSearchEngine.all

    render json: @ps_search_engines
  end

  # GET /ps_search_engines/1
  def show
    render json: @ps_search_engine
  end

  # POST /ps_search_engines
  def create
    @ps_search_engine = PsSearchEngine.new(ps_search_engine_params)

    if @ps_search_engine.save
      render json: @ps_search_engine, status: :created, location: @ps_search_engine
    else
      render json: @ps_search_engine.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_search_engines/1
  def update
    if @ps_search_engine.update(ps_search_engine_params)
      render json: @ps_search_engine
    else
      render json: @ps_search_engine.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_search_engines/1
  def destroy
    @ps_search_engine.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_search_engine
      @ps_search_engine = PsSearchEngine.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_search_engine_params
      params.fetch(:ps_search_engine, {})
    end
end
