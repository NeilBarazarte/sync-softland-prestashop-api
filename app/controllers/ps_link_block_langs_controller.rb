class PsLinkBlockLangsController < ApplicationController
  before_action :set_ps_link_block_lang, only: [:show, :update, :destroy]

  # GET /ps_link_block_langs
  def index
    @ps_link_block_langs = PsLinkBlockLang.all

    render json: @ps_link_block_langs
  end

  # GET /ps_link_block_langs/1
  def show
    render json: @ps_link_block_lang
  end

  # POST /ps_link_block_langs
  def create
    @ps_link_block_lang = PsLinkBlockLang.new(ps_link_block_lang_params)

    if @ps_link_block_lang.save
      render json: @ps_link_block_lang, status: :created, location: @ps_link_block_lang
    else
      render json: @ps_link_block_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_link_block_langs/1
  def update
    if @ps_link_block_lang.update(ps_link_block_lang_params)
      render json: @ps_link_block_lang
    else
      render json: @ps_link_block_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_link_block_langs/1
  def destroy
    @ps_link_block_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_link_block_lang
      @ps_link_block_lang = PsLinkBlockLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_link_block_lang_params
      params.fetch(:ps_link_block_lang, {})
    end
end
