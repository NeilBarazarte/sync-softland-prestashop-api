class PsProductTagsController < ApplicationController
  before_action :set_ps_product_tag, only: [:show, :update, :destroy]

  # GET /ps_product_tags
  def index
    @ps_product_tags = PsProductTag.all

    render json: @ps_product_tags
  end

  # GET /ps_product_tags/1
  def show
    render json: @ps_product_tag
  end

  # POST /ps_product_tags
  def create
    @ps_product_tag = PsProductTag.new(ps_product_tag_params)

    if @ps_product_tag.save
      render json: @ps_product_tag, status: :created, location: @ps_product_tag
    else
      render json: @ps_product_tag.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_tags/1
  def update
    if @ps_product_tag.update(ps_product_tag_params)
      render json: @ps_product_tag
    else
      render json: @ps_product_tag.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_tags/1
  def destroy
    @ps_product_tag.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_tag
      @ps_product_tag = PsProductTag.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_tag_params
      params.fetch(:ps_product_tag, {})
    end
end
