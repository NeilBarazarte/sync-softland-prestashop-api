class PsLayeredCategoriesController < ApplicationController
  before_action :set_ps_layered_category, only: [:show, :update, :destroy]

  # GET /ps_layered_categories
  def index
    @ps_layered_categories = PsLayeredCategory.all

    render json: @ps_layered_categories
  end

  # GET /ps_layered_categories/1
  def show
    render json: @ps_layered_category
  end

  # POST /ps_layered_categories
  def create
    @ps_layered_category = PsLayeredCategory.new(ps_layered_category_params)

    if @ps_layered_category.save
      render json: @ps_layered_category, status: :created, location: @ps_layered_category
    else
      render json: @ps_layered_category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_layered_categories/1
  def update
    if @ps_layered_category.update(ps_layered_category_params)
      render json: @ps_layered_category
    else
      render json: @ps_layered_category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_layered_categories/1
  def destroy
    @ps_layered_category.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_layered_category
      @ps_layered_category = PsLayeredCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_layered_category_params
      params.fetch(:ps_layered_category, {})
    end
end
