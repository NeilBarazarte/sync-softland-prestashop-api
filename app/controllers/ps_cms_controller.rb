class PsCmsController < ApplicationController
  before_action :set_ps_cm, only: [:show, :update, :destroy]

  # GET /ps_cms
  def index
    @ps_cms = PsCm.all

    render json: @ps_cms
  end

  # GET /ps_cms/1
  def show
    render json: @ps_cm
  end

  # POST /ps_cms
  def create
    @ps_cm = PsCm.new(ps_cm_params)

    if @ps_cm.save
      render json: @ps_cm, status: :created, location: @ps_cm
    else
      render json: @ps_cm.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cms/1
  def update
    if @ps_cm.update(ps_cm_params)
      render json: @ps_cm
    else
      render json: @ps_cm.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cms/1
  def destroy
    @ps_cm.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cm
      @ps_cm = PsCm.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cm_params
      params.fetch(:ps_cm, {})
    end
end
