class PsFeatureValuesController < ApplicationController
  before_action :set_ps_feature_value, only: [:show, :update, :destroy]

  # GET /ps_feature_values
  def index
    @ps_feature_values = PsFeatureValue.all

    render json: @ps_feature_values
  end

  # GET /ps_feature_values/1
  def show
    render json: @ps_feature_value
  end

  # POST /ps_feature_values
  def create
    @ps_feature_value = PsFeatureValue.new(ps_feature_value_params)

    if @ps_feature_value.save
      render json: @ps_feature_value, status: :created, location: @ps_feature_value
    else
      render json: @ps_feature_value.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_feature_values/1
  def update
    if @ps_feature_value.update(ps_feature_value_params)
      render json: @ps_feature_value
    else
      render json: @ps_feature_value.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_feature_values/1
  def destroy
    @ps_feature_value.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_feature_value
      @ps_feature_value = PsFeatureValue.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_feature_value_params
      params.fetch(:ps_feature_value, {})
    end
end
