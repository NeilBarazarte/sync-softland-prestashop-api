class PsWebserviceAccountShopsController < ApplicationController
  before_action :set_ps_webservice_account_shop, only: [:show, :update, :destroy]

  # GET /ps_webservice_account_shops
  def index
    @ps_webservice_account_shops = PsWebserviceAccountShop.all

    render json: @ps_webservice_account_shops
  end

  # GET /ps_webservice_account_shops/1
  def show
    render json: @ps_webservice_account_shop
  end

  # POST /ps_webservice_account_shops
  def create
    @ps_webservice_account_shop = PsWebserviceAccountShop.new(ps_webservice_account_shop_params)

    if @ps_webservice_account_shop.save
      render json: @ps_webservice_account_shop, status: :created, location: @ps_webservice_account_shop
    else
      render json: @ps_webservice_account_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_webservice_account_shops/1
  def update
    if @ps_webservice_account_shop.update(ps_webservice_account_shop_params)
      render json: @ps_webservice_account_shop
    else
      render json: @ps_webservice_account_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_webservice_account_shops/1
  def destroy
    @ps_webservice_account_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_webservice_account_shop
      @ps_webservice_account_shop = PsWebserviceAccountShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_webservice_account_shop_params
      params.fetch(:ps_webservice_account_shop, {})
    end
end
