class PsConfigurationKpiLangsController < ApplicationController
  before_action :set_ps_configuration_kpi_lang, only: [:show, :update, :destroy]

  # GET /ps_configuration_kpi_langs
  def index
    @ps_configuration_kpi_langs = PsConfigurationKpiLang.all

    render json: @ps_configuration_kpi_langs
  end

  # GET /ps_configuration_kpi_langs/1
  def show
    render json: @ps_configuration_kpi_lang
  end

  # POST /ps_configuration_kpi_langs
  def create
    @ps_configuration_kpi_lang = PsConfigurationKpiLang.new(ps_configuration_kpi_lang_params)

    if @ps_configuration_kpi_lang.save
      render json: @ps_configuration_kpi_lang, status: :created, location: @ps_configuration_kpi_lang
    else
      render json: @ps_configuration_kpi_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_configuration_kpi_langs/1
  def update
    if @ps_configuration_kpi_lang.update(ps_configuration_kpi_lang_params)
      render json: @ps_configuration_kpi_lang
    else
      render json: @ps_configuration_kpi_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_configuration_kpi_langs/1
  def destroy
    @ps_configuration_kpi_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_configuration_kpi_lang
      @ps_configuration_kpi_lang = PsConfigurationKpiLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_configuration_kpi_lang_params
      params.fetch(:ps_configuration_kpi_lang, {})
    end
end
