class PsConfigurationKpisController < ApplicationController
  before_action :set_ps_configuration_kpi, only: [:show, :update, :destroy]

  # GET /ps_configuration_kpis
  def index
    @ps_configuration_kpis = PsConfigurationKpi.all

    render json: @ps_configuration_kpis
  end

  # GET /ps_configuration_kpis/1
  def show
    render json: @ps_configuration_kpi
  end

  # POST /ps_configuration_kpis
  def create
    @ps_configuration_kpi = PsConfigurationKpi.new(ps_configuration_kpi_params)

    if @ps_configuration_kpi.save
      render json: @ps_configuration_kpi, status: :created, location: @ps_configuration_kpi
    else
      render json: @ps_configuration_kpi.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_configuration_kpis/1
  def update
    if @ps_configuration_kpi.update(ps_configuration_kpi_params)
      render json: @ps_configuration_kpi
    else
      render json: @ps_configuration_kpi.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_configuration_kpis/1
  def destroy
    @ps_configuration_kpi.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_configuration_kpi
      @ps_configuration_kpi = PsConfigurationKpi.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_configuration_kpi_params
      params.fetch(:ps_configuration_kpi, {})
    end
end
