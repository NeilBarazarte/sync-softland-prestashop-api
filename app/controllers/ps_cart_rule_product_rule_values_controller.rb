class PsCartRuleProductRuleValuesController < ApplicationController
  before_action :set_ps_cart_rule_product_rule_value, only: [:show, :update, :destroy]

  # GET /ps_cart_rule_product_rule_values
  def index
    @ps_cart_rule_product_rule_values = PsCartRuleProductRuleValue.all

    render json: @ps_cart_rule_product_rule_values
  end

  # GET /ps_cart_rule_product_rule_values/1
  def show
    render json: @ps_cart_rule_product_rule_value
  end

  # POST /ps_cart_rule_product_rule_values
  def create
    @ps_cart_rule_product_rule_value = PsCartRuleProductRuleValue.new(ps_cart_rule_product_rule_value_params)

    if @ps_cart_rule_product_rule_value.save
      render json: @ps_cart_rule_product_rule_value, status: :created, location: @ps_cart_rule_product_rule_value
    else
      render json: @ps_cart_rule_product_rule_value.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_rule_product_rule_values/1
  def update
    if @ps_cart_rule_product_rule_value.update(ps_cart_rule_product_rule_value_params)
      render json: @ps_cart_rule_product_rule_value
    else
      render json: @ps_cart_rule_product_rule_value.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_rule_product_rule_values/1
  def destroy
    @ps_cart_rule_product_rule_value.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_rule_product_rule_value
      @ps_cart_rule_product_rule_value = PsCartRuleProductRuleValue.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_rule_product_rule_value_params
      params.fetch(:ps_cart_rule_product_rule_value, {})
    end
end
