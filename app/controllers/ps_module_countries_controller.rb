class PsModuleCountriesController < ApplicationController
  before_action :set_ps_module_country, only: [:show, :update, :destroy]

  # GET /ps_module_countries
  def index
    @ps_module_countries = PsModuleCountry.all

    render json: @ps_module_countries
  end

  # GET /ps_module_countries/1
  def show
    render json: @ps_module_country
  end

  # POST /ps_module_countries
  def create
    @ps_module_country = PsModuleCountry.new(ps_module_country_params)

    if @ps_module_country.save
      render json: @ps_module_country, status: :created, location: @ps_module_country
    else
      render json: @ps_module_country.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_module_countries/1
  def update
    if @ps_module_country.update(ps_module_country_params)
      render json: @ps_module_country
    else
      render json: @ps_module_country.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_module_countries/1
  def destroy
    @ps_module_country.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_module_country
      @ps_module_country = PsModuleCountry.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_module_country_params
      params.fetch(:ps_module_country, {})
    end
end
