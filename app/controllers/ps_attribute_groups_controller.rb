class PsAttributeGroupsController < ApplicationController
  before_action :set_ps_attribute_group, only: [:show, :update, :destroy]

  # GET /ps_attribute_groups
  def index
    @ps_attribute_groups = PsAttributeGroup.all

    render json: @ps_attribute_groups
  end

  # GET /ps_attribute_groups/1
  def show
    render json: @ps_attribute_group
  end

  # POST /ps_attribute_groups
  def create
    @ps_attribute_group = PsAttributeGroup.new(ps_attribute_group_params)

    if @ps_attribute_group.save
      render json: @ps_attribute_group, status: :created, location: @ps_attribute_group
    else
      render json: @ps_attribute_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_attribute_groups/1
  def update
    if @ps_attribute_group.update(ps_attribute_group_params)
      render json: @ps_attribute_group
    else
      render json: @ps_attribute_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_attribute_groups/1
  def destroy
    @ps_attribute_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_attribute_group
      @ps_attribute_group = PsAttributeGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_attribute_group_params
      params.fetch(:ps_attribute_group, {})
    end
end
