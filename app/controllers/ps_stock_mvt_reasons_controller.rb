class PsStockMvtReasonsController < ApplicationController
  before_action :set_ps_stock_mvt_reason, only: [:show, :update, :destroy]

  # GET /ps_stock_mvt_reasons
  def index
    @ps_stock_mvt_reasons = PsStockMvtReason.all

    render json: @ps_stock_mvt_reasons
  end

  # GET /ps_stock_mvt_reasons/1
  def show
    render json: @ps_stock_mvt_reason
  end

  # POST /ps_stock_mvt_reasons
  def create
    @ps_stock_mvt_reason = PsStockMvtReason.new(ps_stock_mvt_reason_params)

    if @ps_stock_mvt_reason.save
      render json: @ps_stock_mvt_reason, status: :created, location: @ps_stock_mvt_reason
    else
      render json: @ps_stock_mvt_reason.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_stock_mvt_reasons/1
  def update
    if @ps_stock_mvt_reason.update(ps_stock_mvt_reason_params)
      render json: @ps_stock_mvt_reason
    else
      render json: @ps_stock_mvt_reason.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_stock_mvt_reasons/1
  def destroy
    @ps_stock_mvt_reason.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_stock_mvt_reason
      @ps_stock_mvt_reason = PsStockMvtReason.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_stock_mvt_reason_params
      params.fetch(:ps_stock_mvt_reason, {})
    end
end
