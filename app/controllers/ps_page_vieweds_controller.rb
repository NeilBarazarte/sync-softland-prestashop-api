class PsPageViewedsController < ApplicationController
  before_action :set_ps_page_viewed, only: [:show, :update, :destroy]

  # GET /ps_page_vieweds
  def index
    @ps_page_vieweds = PsPageViewed.all

    render json: @ps_page_vieweds
  end

  # GET /ps_page_vieweds/1
  def show
    render json: @ps_page_viewed
  end

  # POST /ps_page_vieweds
  def create
    @ps_page_viewed = PsPageViewed.new(ps_page_viewed_params)

    if @ps_page_viewed.save
      render json: @ps_page_viewed, status: :created, location: @ps_page_viewed
    else
      render json: @ps_page_viewed.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_page_vieweds/1
  def update
    if @ps_page_viewed.update(ps_page_viewed_params)
      render json: @ps_page_viewed
    else
      render json: @ps_page_viewed.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_page_vieweds/1
  def destroy
    @ps_page_viewed.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_page_viewed
      @ps_page_viewed = PsPageViewed.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_page_viewed_params
      params.fetch(:ps_page_viewed, {})
    end
end
