class PsProductAttributeImagesController < ApplicationController
  before_action :set_ps_product_attribute_image, only: [:show, :update, :destroy]

  # GET /ps_product_attribute_images
  def index
    @ps_product_attribute_images = PsProductAttributeImage.all

    render json: @ps_product_attribute_images
  end

  # GET /ps_product_attribute_images/1
  def show
    render json: @ps_product_attribute_image
  end

  # POST /ps_product_attribute_images
  def create
    @ps_product_attribute_image = PsProductAttributeImage.new(ps_product_attribute_image_params)

    if @ps_product_attribute_image.save
      render json: @ps_product_attribute_image, status: :created, location: @ps_product_attribute_image
    else
      render json: @ps_product_attribute_image.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_attribute_images/1
  def update
    if @ps_product_attribute_image.update(ps_product_attribute_image_params)
      render json: @ps_product_attribute_image
    else
      render json: @ps_product_attribute_image.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_attribute_images/1
  def destroy
    @ps_product_attribute_image.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_attribute_image
      @ps_product_attribute_image = PsProductAttributeImage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_attribute_image_params
      params.fetch(:ps_product_attribute_image, {})
    end
end
