class PsSupplyOrdersController < ApplicationController
  before_action :set_ps_supply_order, only: [:show, :update, :destroy]

  # GET /ps_supply_orders
  def index
    @ps_supply_orders = PsSupplyOrder.all

    render json: @ps_supply_orders
  end

  # GET /ps_supply_orders/1
  def show
    render json: @ps_supply_order
  end

  # POST /ps_supply_orders
  def create
    @ps_supply_order = PsSupplyOrder.new(ps_supply_order_params)

    if @ps_supply_order.save
      render json: @ps_supply_order, status: :created, location: @ps_supply_order
    else
      render json: @ps_supply_order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_supply_orders/1
  def update
    if @ps_supply_order.update(ps_supply_order_params)
      render json: @ps_supply_order
    else
      render json: @ps_supply_order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_supply_orders/1
  def destroy
    @ps_supply_order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_supply_order
      @ps_supply_order = PsSupplyOrder.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_supply_order_params
      params.fetch(:ps_supply_order, {})
    end
end
