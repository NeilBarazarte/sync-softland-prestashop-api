class PsModuleGroupsController < ApplicationController
  before_action :set_ps_module_group, only: [:show, :update, :destroy]

  # GET /ps_module_groups
  def index
    @ps_module_groups = PsModuleGroup.all

    render json: @ps_module_groups
  end

  # GET /ps_module_groups/1
  def show
    render json: @ps_module_group
  end

  # POST /ps_module_groups
  def create
    @ps_module_group = PsModuleGroup.new(ps_module_group_params)

    if @ps_module_group.save
      render json: @ps_module_group, status: :created, location: @ps_module_group
    else
      render json: @ps_module_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_module_groups/1
  def update
    if @ps_module_group.update(ps_module_group_params)
      render json: @ps_module_group
    else
      render json: @ps_module_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_module_groups/1
  def destroy
    @ps_module_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_module_group
      @ps_module_group = PsModuleGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_module_group_params
      params.fetch(:ps_module_group, {})
    end
end
