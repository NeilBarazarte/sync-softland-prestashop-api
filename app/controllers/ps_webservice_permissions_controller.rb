class PsWebservicePermissionsController < ApplicationController
  before_action :set_ps_webservice_permission, only: [:show, :update, :destroy]

  # GET /ps_webservice_permissions
  def index
    @ps_webservice_permissions = PsWebservicePermission.all

    render json: @ps_webservice_permissions
  end

  # GET /ps_webservice_permissions/1
  def show
    render json: @ps_webservice_permission
  end

  # POST /ps_webservice_permissions
  def create
    @ps_webservice_permission = PsWebservicePermission.new(ps_webservice_permission_params)

    if @ps_webservice_permission.save
      render json: @ps_webservice_permission, status: :created, location: @ps_webservice_permission
    else
      render json: @ps_webservice_permission.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_webservice_permissions/1
  def update
    if @ps_webservice_permission.update(ps_webservice_permission_params)
      render json: @ps_webservice_permission
    else
      render json: @ps_webservice_permission.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_webservice_permissions/1
  def destroy
    @ps_webservice_permission.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_webservice_permission
      @ps_webservice_permission = PsWebservicePermission.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_webservice_permission_params
      params.fetch(:ps_webservice_permission, {})
    end
end
