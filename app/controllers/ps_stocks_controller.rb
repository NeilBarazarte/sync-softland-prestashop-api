class PsStocksController < ApplicationController
  before_action :set_ps_stock, only: [:show, :update, :destroy]

  # GET /ps_stocks
  def index
    @ps_stocks = PsStock.all

    render json: @ps_stocks
  end

  # GET /ps_stocks/1
  def show
    render json: @ps_stock
  end

  # POST /ps_stocks
  def create
    @ps_stock = PsStock.new(ps_stock_params)

    if @ps_stock.save
      render json: @ps_stock, status: :created, location: @ps_stock
    else
      render json: @ps_stock.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_stocks/1
  def update
    if @ps_stock.update(ps_stock_params)
      render json: @ps_stock
    else
      render json: @ps_stock.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_stocks/1
  def destroy
    @ps_stock.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_stock
      @ps_stock = PsStock.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_stock_params
      params.fetch(:ps_stock, {})
    end
end
