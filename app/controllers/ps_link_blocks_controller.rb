class PsLinkBlocksController < ApplicationController
  before_action :set_ps_link_block, only: [:show, :update, :destroy]

  # GET /ps_link_blocks
  def index
    @ps_link_blocks = PsLinkBlock.all

    render json: @ps_link_blocks
  end

  # GET /ps_link_blocks/1
  def show
    render json: @ps_link_block
  end

  # POST /ps_link_blocks
  def create
    @ps_link_block = PsLinkBlock.new(ps_link_block_params)

    if @ps_link_block.save
      render json: @ps_link_block, status: :created, location: @ps_link_block
    else
      render json: @ps_link_block.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_link_blocks/1
  def update
    if @ps_link_block.update(ps_link_block_params)
      render json: @ps_link_block
    else
      render json: @ps_link_block.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_link_blocks/1
  def destroy
    @ps_link_block.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_link_block
      @ps_link_block = PsLinkBlock.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_link_block_params
      params.fetch(:ps_link_block, {})
    end
end
