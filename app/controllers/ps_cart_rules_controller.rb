class PsCartRulesController < ApplicationController
  before_action :set_ps_cart_rule, only: [:show, :update, :destroy]

  # GET /ps_cart_rules
  def index
    @ps_cart_rules = PsCartRule.all

    render json: @ps_cart_rules
  end

  # GET /ps_cart_rules/1
  def show
    render json: @ps_cart_rule
  end

  # POST /ps_cart_rules
  def create
    @ps_cart_rule = PsCartRule.new(ps_cart_rule_params)

    if @ps_cart_rule.save
      render json: @ps_cart_rule, status: :created, location: @ps_cart_rule
    else
      render json: @ps_cart_rule.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_rules/1
  def update
    if @ps_cart_rule.update(ps_cart_rule_params)
      render json: @ps_cart_rule
    else
      render json: @ps_cart_rule.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_rules/1
  def destroy
    @ps_cart_rule.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_rule
      @ps_cart_rule = PsCartRule.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_rule_params
      params.fetch(:ps_cart_rule, {})
    end
end
