class PsCarriersController < ApplicationController
  before_action :set_ps_carrier, only: [:show, :update, :destroy]

  # GET /ps_carriers
  def index
    @ps_carriers = PsCarrier.all

    render json: @ps_carriers
  end

  # GET /ps_carriers/1
  def show
    render json: @ps_carrier
  end

  # POST /ps_carriers
  def create
    @ps_carrier = PsCarrier.new(ps_carrier_params)

    if @ps_carrier.save
      render json: @ps_carrier, status: :created, location: @ps_carrier
    else
      render json: @ps_carrier.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_carriers/1
  def update
    if @ps_carrier.update(ps_carrier_params)
      render json: @ps_carrier
    else
      render json: @ps_carrier.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_carriers/1
  def destroy
    @ps_carrier.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_carrier
      @ps_carrier = PsCarrier.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_carrier_params
      params.fetch(:ps_carrier, {})
    end
end
