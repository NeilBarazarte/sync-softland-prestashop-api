class PsHomeslidersController < ApplicationController
  before_action :set_ps_homeslider, only: [:show, :update, :destroy]

  # GET /ps_homesliders
  def index
    @ps_homesliders = PsHomeslider.all

    render json: @ps_homesliders
  end

  # GET /ps_homesliders/1
  def show
    render json: @ps_homeslider
  end

  # POST /ps_homesliders
  def create
    @ps_homeslider = PsHomeslider.new(ps_homeslider_params)

    if @ps_homeslider.save
      render json: @ps_homeslider, status: :created, location: @ps_homeslider
    else
      render json: @ps_homeslider.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_homesliders/1
  def update
    if @ps_homeslider.update(ps_homeslider_params)
      render json: @ps_homeslider
    else
      render json: @ps_homeslider.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_homesliders/1
  def destroy
    @ps_homeslider.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_homeslider
      @ps_homeslider = PsHomeslider.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_homeslider_params
      params.fetch(:ps_homeslider, {})
    end
end
