class PsSupplyOrderHistoriesController < ApplicationController
  before_action :set_ps_supply_order_history, only: [:show, :update, :destroy]

  # GET /ps_supply_order_histories
  def index
    @ps_supply_order_histories = PsSupplyOrderHistory.all

    render json: @ps_supply_order_histories
  end

  # GET /ps_supply_order_histories/1
  def show
    render json: @ps_supply_order_history
  end

  # POST /ps_supply_order_histories
  def create
    @ps_supply_order_history = PsSupplyOrderHistory.new(ps_supply_order_history_params)

    if @ps_supply_order_history.save
      render json: @ps_supply_order_history, status: :created, location: @ps_supply_order_history
    else
      render json: @ps_supply_order_history.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_supply_order_histories/1
  def update
    if @ps_supply_order_history.update(ps_supply_order_history_params)
      render json: @ps_supply_order_history
    else
      render json: @ps_supply_order_history.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_supply_order_histories/1
  def destroy
    @ps_supply_order_history.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_supply_order_history
      @ps_supply_order_history = PsSupplyOrderHistory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_supply_order_history_params
      params.fetch(:ps_supply_order_history, {})
    end
end
