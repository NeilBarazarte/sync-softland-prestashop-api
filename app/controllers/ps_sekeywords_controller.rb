class PsSekeywordsController < ApplicationController
  before_action :set_ps_sekeyword, only: [:show, :update, :destroy]

  # GET /ps_sekeywords
  def index
    @ps_sekeywords = PsSekeyword.all

    render json: @ps_sekeywords
  end

  # GET /ps_sekeywords/1
  def show
    render json: @ps_sekeyword
  end

  # POST /ps_sekeywords
  def create
    @ps_sekeyword = PsSekeyword.new(ps_sekeyword_params)

    if @ps_sekeyword.save
      render json: @ps_sekeyword, status: :created, location: @ps_sekeyword
    else
      render json: @ps_sekeyword.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_sekeywords/1
  def update
    if @ps_sekeyword.update(ps_sekeyword_params)
      render json: @ps_sekeyword
    else
      render json: @ps_sekeyword.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_sekeywords/1
  def destroy
    @ps_sekeyword.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_sekeyword
      @ps_sekeyword = PsSekeyword.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_sekeyword_params
      params.fetch(:ps_sekeyword, {})
    end
end
