class PsCategoryProductsController < ApplicationController
  before_action :set_ps_category_product, only: [:show, :update, :destroy]

  # GET /ps_category_products
  def index
    @ps_category_products = PsCategoryProduct.all

    render json: @ps_category_products
  end

  # GET /ps_category_products/1
  def show
    render json: @ps_category_product
  end

  # POST /ps_category_products
  def create
    @ps_category_product = PsCategoryProduct.new(ps_category_product_params)

    if @ps_category_product.save
      render json: @ps_category_product, status: :created, location: @ps_category_product
    else
      render json: @ps_category_product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_category_products/1
  def update
    if @ps_category_product.update(ps_category_product_params)
      render json: @ps_category_product
    else
      render json: @ps_category_product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_category_products/1
  def destroy
    @ps_category_product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_category_product
      @ps_category_product = PsCategoryProduct.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_category_product_params
      params.fetch(:ps_category_product, {})
    end
end
