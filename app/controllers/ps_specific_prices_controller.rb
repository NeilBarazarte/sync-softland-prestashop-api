class PsSpecificPricesController < ApplicationController
  before_action :set_ps_specific_price, only: [:show, :update, :destroy]

  # GET /ps_specific_prices
  def index
    @ps_specific_prices = PsSpecificPrice.all

    render json: @ps_specific_prices
  end

  # GET /ps_specific_prices/1
  def show
    render json: @ps_specific_price
  end

  # POST /ps_specific_prices
  def create
    @ps_specific_price = PsSpecificPrice.new(ps_specific_price_params)

    if @ps_specific_price.save
      render json: @ps_specific_price, status: :created, location: @ps_specific_price
    else
      render json: @ps_specific_price.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_specific_prices/1
  def update
    if @ps_specific_price.update(ps_specific_price_params)
      render json: @ps_specific_price
    else
      render json: @ps_specific_price.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_specific_prices/1
  def destroy
    @ps_specific_price.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_specific_price
      @ps_specific_price = PsSpecificPrice.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_specific_price_params
      params.fetch(:ps_specific_price, {})
    end
end
