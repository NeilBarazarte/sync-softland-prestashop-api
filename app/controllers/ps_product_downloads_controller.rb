class PsProductDownloadsController < ApplicationController
  before_action :set_ps_product_download, only: [:show, :update, :destroy]

  # GET /ps_product_downloads
  def index
    @ps_product_downloads = PsProductDownload.all

    render json: @ps_product_downloads
  end

  # GET /ps_product_downloads/1
  def show
    render json: @ps_product_download
  end

  # POST /ps_product_downloads
  def create
    @ps_product_download = PsProductDownload.new(ps_product_download_params)

    if @ps_product_download.save
      render json: @ps_product_download, status: :created, location: @ps_product_download
    else
      render json: @ps_product_download.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_downloads/1
  def update
    if @ps_product_download.update(ps_product_download_params)
      render json: @ps_product_download
    else
      render json: @ps_product_download.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_downloads/1
  def destroy
    @ps_product_download.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_download
      @ps_product_download = PsProductDownload.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_download_params
      params.fetch(:ps_product_download, {})
    end
end
