class PsMetaController < ApplicationController
  before_action :set_ps_metum, only: [:show, :update, :destroy]

  # GET /ps_meta
  def index
    @ps_meta = PsMetum.all

    render json: @ps_meta
  end

  # GET /ps_meta/1
  def show
    render json: @ps_metum
  end

  # POST /ps_meta
  def create
    @ps_metum = PsMetum.new(ps_metum_params)

    if @ps_metum.save
      render json: @ps_metum, status: :created, location: @ps_metum
    else
      render json: @ps_metum.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_meta/1
  def update
    if @ps_metum.update(ps_metum_params)
      render json: @ps_metum
    else
      render json: @ps_metum.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_meta/1
  def destroy
    @ps_metum.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_metum
      @ps_metum = PsMetum.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_metum_params
      params.fetch(:ps_metum, {})
    end
end
