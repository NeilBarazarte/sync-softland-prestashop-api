class PsRisksController < ApplicationController
  before_action :set_ps_risk, only: [:show, :update, :destroy]

  # GET /ps_risks
  def index
    @ps_risks = PsRisk.all

    render json: @ps_risks
  end

  # GET /ps_risks/1
  def show
    render json: @ps_risk
  end

  # POST /ps_risks
  def create
    @ps_risk = PsRisk.new(ps_risk_params)

    if @ps_risk.save
      render json: @ps_risk, status: :created, location: @ps_risk
    else
      render json: @ps_risk.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_risks/1
  def update
    if @ps_risk.update(ps_risk_params)
      render json: @ps_risk
    else
      render json: @ps_risk.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_risks/1
  def destroy
    @ps_risk.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_risk
      @ps_risk = PsRisk.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_risk_params
      params.fetch(:ps_risk, {})
    end
end
