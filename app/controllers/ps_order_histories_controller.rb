class PsOrderHistoriesController < ApplicationController
  before_action :set_ps_order_history, only: [:show, :update, :destroy]

  # GET /ps_order_histories
  def index
    @ps_order_histories = PsOrderHistory.all

    render json: @ps_order_histories
  end

  # GET /ps_order_histories/1
  def show
    render json: @ps_order_history
  end

  # POST /ps_order_histories
  def create
    @ps_order_history = PsOrderHistory.new(ps_order_history_params)

    if @ps_order_history.save
      render json: @ps_order_history, status: :created, location: @ps_order_history
    else
      render json: @ps_order_history.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_histories/1
  def update
    if @ps_order_history.update(ps_order_history_params)
      render json: @ps_order_history
    else
      render json: @ps_order_history.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_histories/1
  def destroy
    @ps_order_history.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_history
      @ps_order_history = PsOrderHistory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_history_params
      params.fetch(:ps_order_history, {})
    end
end
