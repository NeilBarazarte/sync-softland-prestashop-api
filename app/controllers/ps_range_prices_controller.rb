class PsRangePricesController < ApplicationController
  before_action :set_ps_range_price, only: [:show, :update, :destroy]

  # GET /ps_range_prices
  def index
    @ps_range_prices = PsRangePrice.all

    render json: @ps_range_prices
  end

  # GET /ps_range_prices/1
  def show
    render json: @ps_range_price
  end

  # POST /ps_range_prices
  def create
    @ps_range_price = PsRangePrice.new(ps_range_price_params)

    if @ps_range_price.save
      render json: @ps_range_price, status: :created, location: @ps_range_price
    else
      render json: @ps_range_price.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_range_prices/1
  def update
    if @ps_range_price.update(ps_range_price_params)
      render json: @ps_range_price
    else
      render json: @ps_range_price.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_range_prices/1
  def destroy
    @ps_range_price.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_range_price
      @ps_range_price = PsRangePrice.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_range_price_params
      params.fetch(:ps_range_price, {})
    end
end
