class PsSupplierLangsController < ApplicationController
  before_action :set_ps_supplier_lang, only: [:show, :update, :destroy]

  # GET /ps_supplier_langs
  def index
    @ps_supplier_langs = PsSupplierLang.all

    render json: @ps_supplier_langs
  end

  # GET /ps_supplier_langs/1
  def show
    render json: @ps_supplier_lang
  end

  # POST /ps_supplier_langs
  def create
    @ps_supplier_lang = PsSupplierLang.new(ps_supplier_lang_params)

    if @ps_supplier_lang.save
      render json: @ps_supplier_lang, status: :created, location: @ps_supplier_lang
    else
      render json: @ps_supplier_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_supplier_langs/1
  def update
    if @ps_supplier_lang.update(ps_supplier_lang_params)
      render json: @ps_supplier_lang
    else
      render json: @ps_supplier_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_supplier_langs/1
  def destroy
    @ps_supplier_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_supplier_lang
      @ps_supplier_lang = PsSupplierLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_supplier_lang_params
      params.fetch(:ps_supplier_lang, {})
    end
end
