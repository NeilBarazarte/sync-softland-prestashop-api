class PsFeatureShopsController < ApplicationController
  before_action :set_ps_feature_shop, only: [:show, :update, :destroy]

  # GET /ps_feature_shops
  def index
    @ps_feature_shops = PsFeatureShop.all

    render json: @ps_feature_shops
  end

  # GET /ps_feature_shops/1
  def show
    render json: @ps_feature_shop
  end

  # POST /ps_feature_shops
  def create
    @ps_feature_shop = PsFeatureShop.new(ps_feature_shop_params)

    if @ps_feature_shop.save
      render json: @ps_feature_shop, status: :created, location: @ps_feature_shop
    else
      render json: @ps_feature_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_feature_shops/1
  def update
    if @ps_feature_shop.update(ps_feature_shop_params)
      render json: @ps_feature_shop
    else
      render json: @ps_feature_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_feature_shops/1
  def destroy
    @ps_feature_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_feature_shop
      @ps_feature_shop = PsFeatureShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_feature_shop_params
      params.fetch(:ps_feature_shop, {})
    end
end
