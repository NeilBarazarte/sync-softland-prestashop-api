class PsCmsLangsController < ApplicationController
  before_action :set_ps_cms_lang, only: [:show, :update, :destroy]

  # GET /ps_cms_langs
  def index
    @ps_cms_langs = PsCmsLang.all

    render json: @ps_cms_langs
  end

  # GET /ps_cms_langs/1
  def show
    render json: @ps_cms_lang
  end

  # POST /ps_cms_langs
  def create
    @ps_cms_lang = PsCmsLang.new(ps_cms_lang_params)

    if @ps_cms_lang.save
      render json: @ps_cms_lang, status: :created, location: @ps_cms_lang
    else
      render json: @ps_cms_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cms_langs/1
  def update
    if @ps_cms_lang.update(ps_cms_lang_params)
      render json: @ps_cms_lang
    else
      render json: @ps_cms_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cms_langs/1
  def destroy
    @ps_cms_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cms_lang
      @ps_cms_lang = PsCmsLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cms_lang_params
      params.fetch(:ps_cms_lang, {})
    end
end
