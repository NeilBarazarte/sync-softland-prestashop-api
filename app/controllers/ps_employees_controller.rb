class PsEmployeesController < ApplicationController
  before_action :set_ps_employee, only: [:show, :update, :destroy]

  # GET /ps_employees
  def index
    @ps_employees = PsEmployee.all

    render json: @ps_employees
  end

  # GET /ps_employees/1
  def show
    render json: @ps_employee
  end

  # POST /ps_employees
  def create
    @ps_employee = PsEmployee.new(ps_employee_params)

    if @ps_employee.save
      render json: @ps_employee, status: :created, location: @ps_employee
    else
      render json: @ps_employee.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_employees/1
  def update
    if @ps_employee.update(ps_employee_params)
      render json: @ps_employee
    else
      render json: @ps_employee.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_employees/1
  def destroy
    @ps_employee.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_employee
      @ps_employee = PsEmployee.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_employee_params
      params.fetch(:ps_employee, {})
    end
end
