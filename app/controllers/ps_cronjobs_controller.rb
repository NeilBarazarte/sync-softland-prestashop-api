class PsCronjobsController < ApplicationController
  before_action :set_ps_cronjob, only: [:show, :update, :destroy]

  # GET /ps_cronjobs
  def index
    @ps_cronjobs = PsCronjob.all

    render json: @ps_cronjobs
  end

  # GET /ps_cronjobs/1
  def show
    render json: @ps_cronjob
  end

  # POST /ps_cronjobs
  def create
    @ps_cronjob = PsCronjob.new(ps_cronjob_params)

    if @ps_cronjob.save
      render json: @ps_cronjob, status: :created, location: @ps_cronjob
    else
      render json: @ps_cronjob.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cronjobs/1
  def update
    if @ps_cronjob.update(ps_cronjob_params)
      render json: @ps_cronjob
    else
      render json: @ps_cronjob.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cronjobs/1
  def destroy
    @ps_cronjob.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cronjob
      @ps_cronjob = PsCronjob.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cronjob_params
      params.fetch(:ps_cronjob, {})
    end
end
