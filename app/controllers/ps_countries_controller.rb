class PsCountriesController < ApplicationController
  before_action :set_ps_country, only: [:show, :update, :destroy]

  # GET /ps_countries
  def index
    @ps_countries = PsCountry.all

    render json: @ps_countries
  end

  # GET /ps_countries/1
  def show
    render json: @ps_country
  end

  # POST /ps_countries
  def create
    @ps_country = PsCountry.new(ps_country_params)

    if @ps_country.save
      render json: @ps_country, status: :created, location: @ps_country
    else
      render json: @ps_country.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_countries/1
  def update
    if @ps_country.update(ps_country_params)
      render json: @ps_country
    else
      render json: @ps_country.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_countries/1
  def destroy
    @ps_country.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_country
      @ps_country = PsCountry.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_country_params
      params.fetch(:ps_country, {})
    end
end
