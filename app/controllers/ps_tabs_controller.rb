class PsTabsController < ApplicationController
  before_action :set_ps_tab, only: [:show, :update, :destroy]

  # GET /ps_tabs
  def index
    @ps_tabs = PsTab.all

    render json: @ps_tabs
  end

  # GET /ps_tabs/1
  def show
    render json: @ps_tab
  end

  # POST /ps_tabs
  def create
    @ps_tab = PsTab.new(ps_tab_params)

    if @ps_tab.save
      render json: @ps_tab, status: :created, location: @ps_tab
    else
      render json: @ps_tab.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tabs/1
  def update
    if @ps_tab.update(ps_tab_params)
      render json: @ps_tab
    else
      render json: @ps_tab.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tabs/1
  def destroy
    @ps_tab.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tab
      @ps_tab = PsTab.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tab_params
      params.fetch(:ps_tab, {})
    end
end
