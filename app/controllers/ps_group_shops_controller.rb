class PsGroupShopsController < ApplicationController
  before_action :set_ps_group_shop, only: [:show, :update, :destroy]

  # GET /ps_group_shops
  def index
    @ps_group_shops = PsGroupShop.all

    render json: @ps_group_shops
  end

  # GET /ps_group_shops/1
  def show
    render json: @ps_group_shop
  end

  # POST /ps_group_shops
  def create
    @ps_group_shop = PsGroupShop.new(ps_group_shop_params)

    if @ps_group_shop.save
      render json: @ps_group_shop, status: :created, location: @ps_group_shop
    else
      render json: @ps_group_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_group_shops/1
  def update
    if @ps_group_shop.update(ps_group_shop_params)
      render json: @ps_group_shop
    else
      render json: @ps_group_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_group_shops/1
  def destroy
    @ps_group_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_group_shop
      @ps_group_shop = PsGroupShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_group_shop_params
      params.fetch(:ps_group_shop, {})
    end
end
