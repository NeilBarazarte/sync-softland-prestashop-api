class PsCarrierZonesController < ApplicationController
  before_action :set_ps_carrier_zone, only: [:show, :update, :destroy]

  # GET /ps_carrier_zones
  def index
    @ps_carrier_zones = PsCarrierZone.all

    render json: @ps_carrier_zones
  end

  # GET /ps_carrier_zones/1
  def show
    render json: @ps_carrier_zone
  end

  # POST /ps_carrier_zones
  def create
    @ps_carrier_zone = PsCarrierZone.new(ps_carrier_zone_params)

    if @ps_carrier_zone.save
      render json: @ps_carrier_zone, status: :created, location: @ps_carrier_zone
    else
      render json: @ps_carrier_zone.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_carrier_zones/1
  def update
    if @ps_carrier_zone.update(ps_carrier_zone_params)
      render json: @ps_carrier_zone
    else
      render json: @ps_carrier_zone.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_carrier_zones/1
  def destroy
    @ps_carrier_zone.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_carrier_zone
      @ps_carrier_zone = PsCarrierZone.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_carrier_zone_params
      params.fetch(:ps_carrier_zone, {})
    end
end
