class PsAccessesController < ApplicationController
  before_action :set_ps_access, only: [:show, :update, :destroy]

  # GET /ps_accesses
  def index
    @ps_accesses = PsAccess.all

    render json: @ps_accesses
  end

  # GET /ps_accesses/1
  def show
    render json: @ps_access
  end

  # POST /ps_accesses
  def create
    @ps_access = PsAccess.new(ps_access_params)

    if @ps_access.save
      render json: @ps_access, status: :created, location: @ps_access
    else
      render json: @ps_access.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_accesses/1
  def update
    if @ps_access.update(ps_access_params)
      render json: @ps_access
    else
      render json: @ps_access.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_accesses/1
  def destroy
    @ps_access.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_access
      @ps_access = PsAccess.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_access_params
      params.fetch(:ps_access, {})
    end
end
