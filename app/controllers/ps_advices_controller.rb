class PsAdvicesController < ApplicationController
  before_action :set_ps_advice, only: [:show, :update, :destroy]

  # GET /ps_advices
  def index
    @ps_advices = PsAdvice.all

    render json: @ps_advices
  end

  # GET /ps_advices/1
  def show
    render json: @ps_advice
  end

  # POST /ps_advices
  def create
    @ps_advice = PsAdvice.new(ps_advice_params)

    if @ps_advice.save
      render json: @ps_advice, status: :created, location: @ps_advice
    else
      render json: @ps_advice.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_advices/1
  def update
    if @ps_advice.update(ps_advice_params)
      render json: @ps_advice
    else
      render json: @ps_advice.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_advices/1
  def destroy
    @ps_advice.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_advice
      @ps_advice = PsAdvice.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_advice_params
      params.fetch(:ps_advice, {})
    end
end
