class PsTagsController < ApplicationController
  before_action :set_ps_tag, only: [:show, :update, :destroy]

  # GET /ps_tags
  def index
    @ps_tags = PsTag.all

    render json: @ps_tags
  end

  # GET /ps_tags/1
  def show
    render json: @ps_tag
  end

  # POST /ps_tags
  def create
    @ps_tag = PsTag.new(ps_tag_params)

    if @ps_tag.save
      render json: @ps_tag, status: :created, location: @ps_tag
    else
      render json: @ps_tag.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tags/1
  def update
    if @ps_tag.update(ps_tag_params)
      render json: @ps_tag
    else
      render json: @ps_tag.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tags/1
  def destroy
    @ps_tag.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tag
      @ps_tag = PsTag.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tag_params
      params.fetch(:ps_tag, {})
    end
end
