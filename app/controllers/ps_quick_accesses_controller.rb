class PsQuickAccessesController < ApplicationController
  before_action :set_ps_quick_access, only: [:show, :update, :destroy]

  # GET /ps_quick_accesses
  def index
    @ps_quick_accesses = PsQuickAccess.all

    render json: @ps_quick_accesses
  end

  # GET /ps_quick_accesses/1
  def show
    render json: @ps_quick_access
  end

  # POST /ps_quick_accesses
  def create
    @ps_quick_access = PsQuickAccess.new(ps_quick_access_params)

    if @ps_quick_access.save
      render json: @ps_quick_access, status: :created, location: @ps_quick_access
    else
      render json: @ps_quick_access.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_quick_accesses/1
  def update
    if @ps_quick_access.update(ps_quick_access_params)
      render json: @ps_quick_access
    else
      render json: @ps_quick_access.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_quick_accesses/1
  def destroy
    @ps_quick_access.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_quick_access
      @ps_quick_access = PsQuickAccess.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_quick_access_params
      params.fetch(:ps_quick_access, {})
    end
end
