class PsCartRuleProductRulesController < ApplicationController
  before_action :set_ps_cart_rule_product_rule, only: [:show, :update, :destroy]

  # GET /ps_cart_rule_product_rules
  def index
    @ps_cart_rule_product_rules = PsCartRuleProductRule.all

    render json: @ps_cart_rule_product_rules
  end

  # GET /ps_cart_rule_product_rules/1
  def show
    render json: @ps_cart_rule_product_rule
  end

  # POST /ps_cart_rule_product_rules
  def create
    @ps_cart_rule_product_rule = PsCartRuleProductRule.new(ps_cart_rule_product_rule_params)

    if @ps_cart_rule_product_rule.save
      render json: @ps_cart_rule_product_rule, status: :created, location: @ps_cart_rule_product_rule
    else
      render json: @ps_cart_rule_product_rule.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_rule_product_rules/1
  def update
    if @ps_cart_rule_product_rule.update(ps_cart_rule_product_rule_params)
      render json: @ps_cart_rule_product_rule
    else
      render json: @ps_cart_rule_product_rule.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_rule_product_rules/1
  def destroy
    @ps_cart_rule_product_rule.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_rule_product_rule
      @ps_cart_rule_product_rule = PsCartRuleProductRule.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_rule_product_rule_params
      params.fetch(:ps_cart_rule_product_rule, {})
    end
end
