class PsCmsRolesController < ApplicationController
  before_action :set_ps_cms_role, only: [:show, :update, :destroy]

  # GET /ps_cms_roles
  def index
    @ps_cms_roles = PsCmsRole.all

    render json: @ps_cms_roles
  end

  # GET /ps_cms_roles/1
  def show
    render json: @ps_cms_role
  end

  # POST /ps_cms_roles
  def create
    @ps_cms_role = PsCmsRole.new(ps_cms_role_params)

    if @ps_cms_role.save
      render json: @ps_cms_role, status: :created, location: @ps_cms_role
    else
      render json: @ps_cms_role.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cms_roles/1
  def update
    if @ps_cms_role.update(ps_cms_role_params)
      render json: @ps_cms_role
    else
      render json: @ps_cms_role.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cms_roles/1
  def destroy
    @ps_cms_role.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cms_role
      @ps_cms_role = PsCmsRole.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cms_role_params
      params.fetch(:ps_cms_role, {})
    end
end
