class PsAttributeGroupShopsController < ApplicationController
  before_action :set_ps_attribute_group_shop, only: [:show, :update, :destroy]

  # GET /ps_attribute_group_shops
  def index
    @ps_attribute_group_shops = PsAttributeGroupShop.all

    render json: @ps_attribute_group_shops
  end

  # GET /ps_attribute_group_shops/1
  def show
    render json: @ps_attribute_group_shop
  end

  # POST /ps_attribute_group_shops
  def create
    @ps_attribute_group_shop = PsAttributeGroupShop.new(ps_attribute_group_shop_params)

    if @ps_attribute_group_shop.save
      render json: @ps_attribute_group_shop, status: :created, location: @ps_attribute_group_shop
    else
      render json: @ps_attribute_group_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_attribute_group_shops/1
  def update
    if @ps_attribute_group_shop.update(ps_attribute_group_shop_params)
      render json: @ps_attribute_group_shop
    else
      render json: @ps_attribute_group_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_attribute_group_shops/1
  def destroy
    @ps_attribute_group_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_attribute_group_shop
      @ps_attribute_group_shop = PsAttributeGroupShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_attribute_group_shop_params
      params.fetch(:ps_attribute_group_shop, {})
    end
end
