class PsModuleCarriersController < ApplicationController
  before_action :set_ps_module_carrier, only: [:show, :update, :destroy]

  # GET /ps_module_carriers
  def index
    @ps_module_carriers = PsModuleCarrier.all

    render json: @ps_module_carriers
  end

  # GET /ps_module_carriers/1
  def show
    render json: @ps_module_carrier
  end

  # POST /ps_module_carriers
  def create
    @ps_module_carrier = PsModuleCarrier.new(ps_module_carrier_params)

    if @ps_module_carrier.save
      render json: @ps_module_carrier, status: :created, location: @ps_module_carrier
    else
      render json: @ps_module_carrier.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_module_carriers/1
  def update
    if @ps_module_carrier.update(ps_module_carrier_params)
      render json: @ps_module_carrier
    else
      render json: @ps_module_carrier.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_module_carriers/1
  def destroy
    @ps_module_carrier.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_module_carrier
      @ps_module_carrier = PsModuleCarrier.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_module_carrier_params
      params.fetch(:ps_module_carrier, {})
    end
end
