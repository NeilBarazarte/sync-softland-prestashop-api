class PsImageLangsController < ApplicationController
  before_action :set_ps_image_lang, only: [:show, :update, :destroy]

  # GET /ps_image_langs
  def index
    @ps_image_langs = PsImageLang.all

    render json: @ps_image_langs
  end

  # GET /ps_image_langs/1
  def show
    render json: @ps_image_lang
  end

  # POST /ps_image_langs
  def create
    @ps_image_lang = PsImageLang.new(ps_image_lang_params)

    if @ps_image_lang.save
      render json: @ps_image_lang, status: :created, location: @ps_image_lang
    else
      render json: @ps_image_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_image_langs/1
  def update
    if @ps_image_lang.update(ps_image_lang_params)
      render json: @ps_image_lang
    else
      render json: @ps_image_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_image_langs/1
  def destroy
    @ps_image_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_image_lang
      @ps_image_lang = PsImageLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_image_lang_params
      params.fetch(:ps_image_lang, {})
    end
end
