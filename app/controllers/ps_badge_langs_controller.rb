class PsBadgeLangsController < ApplicationController
  before_action :set_ps_badge_lang, only: [:show, :update, :destroy]

  # GET /ps_badge_langs
  def index
    @ps_badge_langs = PsBadgeLang.all

    render json: @ps_badge_langs
  end

  # GET /ps_badge_langs/1
  def show
    render json: @ps_badge_lang
  end

  # POST /ps_badge_langs
  def create
    @ps_badge_lang = PsBadgeLang.new(ps_badge_lang_params)

    if @ps_badge_lang.save
      render json: @ps_badge_lang, status: :created, location: @ps_badge_lang
    else
      render json: @ps_badge_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_badge_langs/1
  def update
    if @ps_badge_lang.update(ps_badge_lang_params)
      render json: @ps_badge_lang
    else
      render json: @ps_badge_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_badge_langs/1
  def destroy
    @ps_badge_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_badge_lang
      @ps_badge_lang = PsBadgeLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_badge_lang_params
      params.fetch(:ps_badge_lang, {})
    end
end
