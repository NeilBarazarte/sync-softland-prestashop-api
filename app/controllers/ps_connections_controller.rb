class PsConnectionsController < ApplicationController
  before_action :set_ps_connection, only: [:show, :update, :destroy]

  # GET /ps_connections
  def index
    @ps_connections = PsConnection.all

    render json: @ps_connections
  end

  # GET /ps_connections/1
  def show
    render json: @ps_connection
  end

  # POST /ps_connections
  def create
    @ps_connection = PsConnection.new(ps_connection_params)

    if @ps_connection.save
      render json: @ps_connection, status: :created, location: @ps_connection
    else
      render json: @ps_connection.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_connections/1
  def update
    if @ps_connection.update(ps_connection_params)
      render json: @ps_connection
    else
      render json: @ps_connection.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_connections/1
  def destroy
    @ps_connection.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_connection
      @ps_connection = PsConnection.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_connection_params
      params.fetch(:ps_connection, {})
    end
end
