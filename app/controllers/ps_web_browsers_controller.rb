class PsWebBrowsersController < ApplicationController
  before_action :set_ps_web_browser, only: [:show, :update, :destroy]

  # GET /ps_web_browsers
  def index
    @ps_web_browsers = PsWebBrowser.all

    render json: @ps_web_browsers
  end

  # GET /ps_web_browsers/1
  def show
    render json: @ps_web_browser
  end

  # POST /ps_web_browsers
  def create
    @ps_web_browser = PsWebBrowser.new(ps_web_browser_params)

    if @ps_web_browser.save
      render json: @ps_web_browser, status: :created, location: @ps_web_browser
    else
      render json: @ps_web_browser.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_web_browsers/1
  def update
    if @ps_web_browser.update(ps_web_browser_params)
      render json: @ps_web_browser
    else
      render json: @ps_web_browser.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_web_browsers/1
  def destroy
    @ps_web_browser.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_web_browser
      @ps_web_browser = PsWebBrowser.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_web_browser_params
      params.fetch(:ps_web_browser, {})
    end
end
