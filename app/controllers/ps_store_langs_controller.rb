class PsStoreLangsController < ApplicationController
  before_action :set_ps_store_lang, only: [:show, :update, :destroy]

  # GET /ps_store_langs
  def index
    @ps_store_langs = PsStoreLang.all

    render json: @ps_store_langs
  end

  # GET /ps_store_langs/1
  def show
    render json: @ps_store_lang
  end

  # POST /ps_store_langs
  def create
    @ps_store_lang = PsStoreLang.new(ps_store_lang_params)

    if @ps_store_lang.save
      render json: @ps_store_lang, status: :created, location: @ps_store_lang
    else
      render json: @ps_store_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_store_langs/1
  def update
    if @ps_store_lang.update(ps_store_lang_params)
      render json: @ps_store_lang
    else
      render json: @ps_store_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_store_langs/1
  def destroy
    @ps_store_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_store_lang
      @ps_store_lang = PsStoreLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_store_lang_params
      params.fetch(:ps_store_lang, {})
    end
end
