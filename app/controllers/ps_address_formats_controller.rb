class PsAddressFormatsController < ApplicationController
  before_action :set_ps_address_format, only: [:show, :update, :destroy]

  # GET /ps_address_formats
  def index
    @ps_address_formats = PsAddressFormat.all

    render json: @ps_address_formats
  end

  # GET /ps_address_formats/1
  def show
    render json: @ps_address_format
  end

  # POST /ps_address_formats
  def create
    @ps_address_format = PsAddressFormat.new(ps_address_format_params)

    if @ps_address_format.save
      render json: @ps_address_format, status: :created, location: @ps_address_format
    else
      render json: @ps_address_format.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_address_formats/1
  def update
    if @ps_address_format.update(ps_address_format_params)
      render json: @ps_address_format
    else
      render json: @ps_address_format.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_address_formats/1
  def destroy
    @ps_address_format.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_address_format
      @ps_address_format = PsAddressFormat.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_address_format_params
      params.fetch(:ps_address_format, {})
    end
end
