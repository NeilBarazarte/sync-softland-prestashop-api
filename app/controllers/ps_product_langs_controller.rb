class PsProductLangsController < ApplicationController
  before_action :set_ps_product_lang, only: [:show, :update, :destroy]

  # GET /ps_product_langs
  def index
    @ps_product_langs = PsProductLang.all

    render json: @ps_product_langs
  end

  # GET /ps_product_langs/1
  def show
    render json: @ps_product_lang
  end

  # POST /ps_product_langs
  def create
    @ps_product_lang = PsProductLang.new(ps_product_lang_params)

    if @ps_product_lang.save
      render json: @ps_product_lang, status: :created, location: @ps_product_lang
    else
      render json: @ps_product_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_langs/1
  def update
    if @ps_product_lang.update(ps_product_lang_params)
      render json: @ps_product_lang
    else
      render json: @ps_product_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_langs/1
  def destroy
    @ps_product_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_lang
      @ps_product_lang = PsProductLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_lang_params
      params.fetch(:ps_product_lang, {})
    end
end
