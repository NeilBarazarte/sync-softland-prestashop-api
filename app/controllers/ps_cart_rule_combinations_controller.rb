class PsCartRuleCombinationsController < ApplicationController
  before_action :set_ps_cart_rule_combination, only: [:show, :update, :destroy]

  # GET /ps_cart_rule_combinations
  def index
    @ps_cart_rule_combinations = PsCartRuleCombination.all

    render json: @ps_cart_rule_combinations
  end

  # GET /ps_cart_rule_combinations/1
  def show
    render json: @ps_cart_rule_combination
  end

  # POST /ps_cart_rule_combinations
  def create
    @ps_cart_rule_combination = PsCartRuleCombination.new(ps_cart_rule_combination_params)

    if @ps_cart_rule_combination.save
      render json: @ps_cart_rule_combination, status: :created, location: @ps_cart_rule_combination
    else
      render json: @ps_cart_rule_combination.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_rule_combinations/1
  def update
    if @ps_cart_rule_combination.update(ps_cart_rule_combination_params)
      render json: @ps_cart_rule_combination
    else
      render json: @ps_cart_rule_combination.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_rule_combinations/1
  def destroy
    @ps_cart_rule_combination.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_rule_combination
      @ps_cart_rule_combination = PsCartRuleCombination.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_rule_combination_params
      params.fetch(:ps_cart_rule_combination, {})
    end
end
