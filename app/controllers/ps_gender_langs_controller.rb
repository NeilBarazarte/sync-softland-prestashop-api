class PsGenderLangsController < ApplicationController
  before_action :set_ps_gender_lang, only: [:show, :update, :destroy]

  # GET /ps_gender_langs
  def index
    @ps_gender_langs = PsGenderLang.all

    render json: @ps_gender_langs
  end

  # GET /ps_gender_langs/1
  def show
    render json: @ps_gender_lang
  end

  # POST /ps_gender_langs
  def create
    @ps_gender_lang = PsGenderLang.new(ps_gender_lang_params)

    if @ps_gender_lang.save
      render json: @ps_gender_lang, status: :created, location: @ps_gender_lang
    else
      render json: @ps_gender_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_gender_langs/1
  def update
    if @ps_gender_lang.update(ps_gender_lang_params)
      render json: @ps_gender_lang
    else
      render json: @ps_gender_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_gender_langs/1
  def destroy
    @ps_gender_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_gender_lang
      @ps_gender_lang = PsGenderLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_gender_lang_params
      params.fetch(:ps_gender_lang, {})
    end
end
