class PsShopGroupsController < ApplicationController
  before_action :set_ps_shop_group, only: [:show, :update, :destroy]

  # GET /ps_shop_groups
  def index
    @ps_shop_groups = PsShopGroup.all

    render json: @ps_shop_groups
  end

  # GET /ps_shop_groups/1
  def show
    render json: @ps_shop_group
  end

  # POST /ps_shop_groups
  def create
    @ps_shop_group = PsShopGroup.new(ps_shop_group_params)

    if @ps_shop_group.save
      render json: @ps_shop_group, status: :created, location: @ps_shop_group
    else
      render json: @ps_shop_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_shop_groups/1
  def update
    if @ps_shop_group.update(ps_shop_group_params)
      render json: @ps_shop_group
    else
      render json: @ps_shop_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_shop_groups/1
  def destroy
    @ps_shop_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_shop_group
      @ps_shop_group = PsShopGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_shop_group_params
      params.fetch(:ps_shop_group, {})
    end
end
