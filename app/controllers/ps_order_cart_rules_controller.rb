class PsOrderCartRulesController < ApplicationController
  before_action :set_ps_order_cart_rule, only: [:show, :update, :destroy]

  # GET /ps_order_cart_rules
  def index
    @ps_order_cart_rules = PsOrderCartRule.all

    render json: @ps_order_cart_rules
  end

  # GET /ps_order_cart_rules/1
  def show
    render json: @ps_order_cart_rule
  end

  # POST /ps_order_cart_rules
  def create
    @ps_order_cart_rule = PsOrderCartRule.new(ps_order_cart_rule_params)

    if @ps_order_cart_rule.save
      render json: @ps_order_cart_rule, status: :created, location: @ps_order_cart_rule
    else
      render json: @ps_order_cart_rule.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_cart_rules/1
  def update
    if @ps_order_cart_rule.update(ps_order_cart_rule_params)
      render json: @ps_order_cart_rule
    else
      render json: @ps_order_cart_rule.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_cart_rules/1
  def destroy
    @ps_order_cart_rule.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_cart_rule
      @ps_order_cart_rule = PsOrderCartRule.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_cart_rule_params
      params.fetch(:ps_order_cart_rule, {})
    end
end
