class PsFeatureLangsController < ApplicationController
  before_action :set_ps_feature_lang, only: [:show, :update, :destroy]

  # GET /ps_feature_langs
  def index
    @ps_feature_langs = PsFeatureLang.all

    render json: @ps_feature_langs
  end

  # GET /ps_feature_langs/1
  def show
    render json: @ps_feature_lang
  end

  # POST /ps_feature_langs
  def create
    @ps_feature_lang = PsFeatureLang.new(ps_feature_lang_params)

    if @ps_feature_lang.save
      render json: @ps_feature_lang, status: :created, location: @ps_feature_lang
    else
      render json: @ps_feature_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_feature_langs/1
  def update
    if @ps_feature_lang.update(ps_feature_lang_params)
      render json: @ps_feature_lang
    else
      render json: @ps_feature_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_feature_langs/1
  def destroy
    @ps_feature_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_feature_lang
      @ps_feature_lang = PsFeatureLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_feature_lang_params
      params.fetch(:ps_feature_lang, {})
    end
end
