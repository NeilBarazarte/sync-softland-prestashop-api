class PsOrderStateLangsController < ApplicationController
  before_action :set_ps_order_state_lang, only: [:show, :update, :destroy]

  # GET /ps_order_state_langs
  def index
    @ps_order_state_langs = PsOrderStateLang.all

    render json: @ps_order_state_langs
  end

  # GET /ps_order_state_langs/1
  def show
    render json: @ps_order_state_lang
  end

  # POST /ps_order_state_langs
  def create
    @ps_order_state_lang = PsOrderStateLang.new(ps_order_state_lang_params)

    if @ps_order_state_lang.save
      render json: @ps_order_state_lang, status: :created, location: @ps_order_state_lang
    else
      render json: @ps_order_state_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_order_state_langs/1
  def update
    if @ps_order_state_lang.update(ps_order_state_lang_params)
      render json: @ps_order_state_lang
    else
      render json: @ps_order_state_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_order_state_langs/1
  def destroy
    @ps_order_state_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_order_state_lang
      @ps_order_state_lang = PsOrderStateLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_order_state_lang_params
      params.fetch(:ps_order_state_lang, {})
    end
end
