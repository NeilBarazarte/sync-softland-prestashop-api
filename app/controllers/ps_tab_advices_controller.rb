class PsTabAdvicesController < ApplicationController
  before_action :set_ps_tab_advice, only: [:show, :update, :destroy]

  # GET /ps_tab_advices
  def index
    @ps_tab_advices = PsTabAdvice.all

    render json: @ps_tab_advices
  end

  # GET /ps_tab_advices/1
  def show
    render json: @ps_tab_advice
  end

  # POST /ps_tab_advices
  def create
    @ps_tab_advice = PsTabAdvice.new(ps_tab_advice_params)

    if @ps_tab_advice.save
      render json: @ps_tab_advice, status: :created, location: @ps_tab_advice
    else
      render json: @ps_tab_advice.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tab_advices/1
  def update
    if @ps_tab_advice.update(ps_tab_advice_params)
      render json: @ps_tab_advice
    else
      render json: @ps_tab_advice.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tab_advices/1
  def destroy
    @ps_tab_advice.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tab_advice
      @ps_tab_advice = PsTabAdvice.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tab_advice_params
      params.fetch(:ps_tab_advice, {})
    end
end
