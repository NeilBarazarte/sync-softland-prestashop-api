class PsCmsShopsController < ApplicationController
  before_action :set_ps_cms_shop, only: [:show, :update, :destroy]

  # GET /ps_cms_shops
  def index
    @ps_cms_shops = PsCmsShop.all

    render json: @ps_cms_shops
  end

  # GET /ps_cms_shops/1
  def show
    render json: @ps_cms_shop
  end

  # POST /ps_cms_shops
  def create
    @ps_cms_shop = PsCmsShop.new(ps_cms_shop_params)

    if @ps_cms_shop.save
      render json: @ps_cms_shop, status: :created, location: @ps_cms_shop
    else
      render json: @ps_cms_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cms_shops/1
  def update
    if @ps_cms_shop.update(ps_cms_shop_params)
      render json: @ps_cms_shop
    else
      render json: @ps_cms_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cms_shops/1
  def destroy
    @ps_cms_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cms_shop
      @ps_cms_shop = PsCmsShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cms_shop_params
      params.fetch(:ps_cms_shop, {})
    end
end
