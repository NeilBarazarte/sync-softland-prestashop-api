class PsLayeredIndexableFeatureValueLangValuesController < ApplicationController
  before_action :set_ps_layered_indexable_feature_value_lang_value, only: [:show, :update, :destroy]

  # GET /ps_layered_indexable_feature_value_lang_values
  def index
    @ps_layered_indexable_feature_value_lang_values = PsLayeredIndexableFeatureValueLangValue.all

    render json: @ps_layered_indexable_feature_value_lang_values
  end

  # GET /ps_layered_indexable_feature_value_lang_values/1
  def show
    render json: @ps_layered_indexable_feature_value_lang_value
  end

  # POST /ps_layered_indexable_feature_value_lang_values
  def create
    @ps_layered_indexable_feature_value_lang_value = PsLayeredIndexableFeatureValueLangValue.new(ps_layered_indexable_feature_value_lang_value_params)

    if @ps_layered_indexable_feature_value_lang_value.save
      render json: @ps_layered_indexable_feature_value_lang_value, status: :created, location: @ps_layered_indexable_feature_value_lang_value
    else
      render json: @ps_layered_indexable_feature_value_lang_value.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_layered_indexable_feature_value_lang_values/1
  def update
    if @ps_layered_indexable_feature_value_lang_value.update(ps_layered_indexable_feature_value_lang_value_params)
      render json: @ps_layered_indexable_feature_value_lang_value
    else
      render json: @ps_layered_indexable_feature_value_lang_value.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_layered_indexable_feature_value_lang_values/1
  def destroy
    @ps_layered_indexable_feature_value_lang_value.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_layered_indexable_feature_value_lang_value
      @ps_layered_indexable_feature_value_lang_value = PsLayeredIndexableFeatureValueLangValue.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_layered_indexable_feature_value_lang_value_params
      params.fetch(:ps_layered_indexable_feature_value_lang_value, {})
    end
end
