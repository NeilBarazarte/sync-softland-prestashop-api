class PsStockMvtReasonLangsController < ApplicationController
  before_action :set_ps_stock_mvt_reason_lang, only: [:show, :update, :destroy]

  # GET /ps_stock_mvt_reason_langs
  def index
    @ps_stock_mvt_reason_langs = PsStockMvtReasonLang.all

    render json: @ps_stock_mvt_reason_langs
  end

  # GET /ps_stock_mvt_reason_langs/1
  def show
    render json: @ps_stock_mvt_reason_lang
  end

  # POST /ps_stock_mvt_reason_langs
  def create
    @ps_stock_mvt_reason_lang = PsStockMvtReasonLang.new(ps_stock_mvt_reason_lang_params)

    if @ps_stock_mvt_reason_lang.save
      render json: @ps_stock_mvt_reason_lang, status: :created, location: @ps_stock_mvt_reason_lang
    else
      render json: @ps_stock_mvt_reason_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_stock_mvt_reason_langs/1
  def update
    if @ps_stock_mvt_reason_lang.update(ps_stock_mvt_reason_lang_params)
      render json: @ps_stock_mvt_reason_lang
    else
      render json: @ps_stock_mvt_reason_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_stock_mvt_reason_langs/1
  def destroy
    @ps_stock_mvt_reason_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_stock_mvt_reason_lang
      @ps_stock_mvt_reason_lang = PsStockMvtReasonLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_stock_mvt_reason_lang_params
      params.fetch(:ps_stock_mvt_reason_lang, {})
    end
end
