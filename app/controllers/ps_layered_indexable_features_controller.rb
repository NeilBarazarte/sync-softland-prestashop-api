class PsLayeredIndexableFeaturesController < ApplicationController
  before_action :set_ps_layered_indexable_feature, only: [:show, :update, :destroy]

  # GET /ps_layered_indexable_features
  def index
    @ps_layered_indexable_features = PsLayeredIndexableFeature.all

    render json: @ps_layered_indexable_features
  end

  # GET /ps_layered_indexable_features/1
  def show
    render json: @ps_layered_indexable_feature
  end

  # POST /ps_layered_indexable_features
  def create
    @ps_layered_indexable_feature = PsLayeredIndexableFeature.new(ps_layered_indexable_feature_params)

    if @ps_layered_indexable_feature.save
      render json: @ps_layered_indexable_feature, status: :created, location: @ps_layered_indexable_feature
    else
      render json: @ps_layered_indexable_feature.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_layered_indexable_features/1
  def update
    if @ps_layered_indexable_feature.update(ps_layered_indexable_feature_params)
      render json: @ps_layered_indexable_feature
    else
      render json: @ps_layered_indexable_feature.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_layered_indexable_features/1
  def destroy
    @ps_layered_indexable_feature.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_layered_indexable_feature
      @ps_layered_indexable_feature = PsLayeredIndexableFeature.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_layered_indexable_feature_params
      params.fetch(:ps_layered_indexable_feature, {})
    end
end
