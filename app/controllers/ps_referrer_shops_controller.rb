class PsReferrerShopsController < ApplicationController
  before_action :set_ps_referrer_shop, only: [:show, :update, :destroy]

  # GET /ps_referrer_shops
  def index
    @ps_referrer_shops = PsReferrerShop.all

    render json: @ps_referrer_shops
  end

  # GET /ps_referrer_shops/1
  def show
    render json: @ps_referrer_shop
  end

  # POST /ps_referrer_shops
  def create
    @ps_referrer_shop = PsReferrerShop.new(ps_referrer_shop_params)

    if @ps_referrer_shop.save
      render json: @ps_referrer_shop, status: :created, location: @ps_referrer_shop
    else
      render json: @ps_referrer_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_referrer_shops/1
  def update
    if @ps_referrer_shop.update(ps_referrer_shop_params)
      render json: @ps_referrer_shop
    else
      render json: @ps_referrer_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_referrer_shops/1
  def destroy
    @ps_referrer_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_referrer_shop
      @ps_referrer_shop = PsReferrerShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_referrer_shop_params
      params.fetch(:ps_referrer_shop, {})
    end
end
