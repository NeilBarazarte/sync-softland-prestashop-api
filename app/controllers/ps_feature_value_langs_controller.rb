class PsFeatureValueLangsController < ApplicationController
  before_action :set_ps_feature_value_lang, only: [:show, :update, :destroy]

  # GET /ps_feature_value_langs
  def index
    @ps_feature_value_langs = PsFeatureValueLang.all

    render json: @ps_feature_value_langs
  end

  # GET /ps_feature_value_langs/1
  def show
    render json: @ps_feature_value_lang
  end

  # POST /ps_feature_value_langs
  def create
    @ps_feature_value_lang = PsFeatureValueLang.new(ps_feature_value_lang_params)

    if @ps_feature_value_lang.save
      render json: @ps_feature_value_lang, status: :created, location: @ps_feature_value_lang
    else
      render json: @ps_feature_value_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_feature_value_langs/1
  def update
    if @ps_feature_value_lang.update(ps_feature_value_lang_params)
      render json: @ps_feature_value_lang
    else
      render json: @ps_feature_value_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_feature_value_langs/1
  def destroy
    @ps_feature_value_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_feature_value_lang
      @ps_feature_value_lang = PsFeatureValueLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_feature_value_lang_params
      params.fetch(:ps_feature_value_lang, {})
    end
end
