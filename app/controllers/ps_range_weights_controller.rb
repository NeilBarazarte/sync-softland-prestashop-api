class PsRangeWeightsController < ApplicationController
  before_action :set_ps_range_weight, only: [:show, :update, :destroy]

  # GET /ps_range_weights
  def index
    @ps_range_weights = PsRangeWeight.all

    render json: @ps_range_weights
  end

  # GET /ps_range_weights/1
  def show
    render json: @ps_range_weight
  end

  # POST /ps_range_weights
  def create
    @ps_range_weight = PsRangeWeight.new(ps_range_weight_params)

    if @ps_range_weight.save
      render json: @ps_range_weight, status: :created, location: @ps_range_weight
    else
      render json: @ps_range_weight.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_range_weights/1
  def update
    if @ps_range_weight.update(ps_range_weight_params)
      render json: @ps_range_weight
    else
      render json: @ps_range_weight.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_range_weights/1
  def destroy
    @ps_range_weight.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_range_weight
      @ps_range_weight = PsRangeWeight.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_range_weight_params
      params.fetch(:ps_range_weight, {})
    end
end
