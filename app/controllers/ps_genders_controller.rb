class PsGendersController < ApplicationController
  before_action :set_ps_gender, only: [:show, :update, :destroy]

  # GET /ps_genders
  def index
    @ps_genders = PsGender.all

    render json: @ps_genders
  end

  # GET /ps_genders/1
  def show
    render json: @ps_gender
  end

  # POST /ps_genders
  def create
    @ps_gender = PsGender.new(ps_gender_params)

    if @ps_gender.save
      render json: @ps_gender, status: :created, location: @ps_gender
    else
      render json: @ps_gender.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_genders/1
  def update
    if @ps_gender.update(ps_gender_params)
      render json: @ps_gender
    else
      render json: @ps_gender.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_genders/1
  def destroy
    @ps_gender.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_gender
      @ps_gender = PsGender.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_gender_params
      params.fetch(:ps_gender, {})
    end
end
