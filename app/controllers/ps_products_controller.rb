class PsProductsController < ApplicationController
  before_action :set_ps_product, only: [:show, :update, :destroy]

  # GET /ps_products
  def index
    @ps_products = PsProduct.all

    render json: @ps_products

  end

  # GET /ps_products/1
  def show
    render json: @ps_product
  end

  # POST /ps_products
  def create
    @ps_product = PsProduct.new(ps_product_params)

    if @ps_product.save
      render json: @ps_product, status: :created, location: @ps_product
    else
      render json: @ps_product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_products/1
  def update
    if @ps_product.update(ps_product_params)
      render json: @ps_product
    else
      render json: @ps_product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_products/1
  def destroy
    @ps_product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product
      @ps_product = PsProduct.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_params
      #params.fetch(:ps_product, {})
      params.require(:ps_product).permit(:price)
    end
end
