class PsTaxRulesGroupsController < ApplicationController
  before_action :set_ps_tax_rules_group, only: [:show, :update, :destroy]

  # GET /ps_tax_rules_groups
  def index
    @ps_tax_rules_groups = PsTaxRulesGroup.all

    render json: @ps_tax_rules_groups
  end

  # GET /ps_tax_rules_groups/1
  def show
    render json: @ps_tax_rules_group
  end

  # POST /ps_tax_rules_groups
  def create
    @ps_tax_rules_group = PsTaxRulesGroup.new(ps_tax_rules_group_params)

    if @ps_tax_rules_group.save
      render json: @ps_tax_rules_group, status: :created, location: @ps_tax_rules_group
    else
      render json: @ps_tax_rules_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_tax_rules_groups/1
  def update
    if @ps_tax_rules_group.update(ps_tax_rules_group_params)
      render json: @ps_tax_rules_group
    else
      render json: @ps_tax_rules_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_tax_rules_groups/1
  def destroy
    @ps_tax_rules_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_tax_rules_group
      @ps_tax_rules_group = PsTaxRulesGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_tax_rules_group_params
      params.fetch(:ps_tax_rules_group, {})
    end
end
