class PsZonesController < ApplicationController
  before_action :set_ps_zone, only: [:show, :update, :destroy]

  # GET /ps_zones
  def index
    @ps_zones = PsZone.all

    render json: @ps_zones
  end

  # GET /ps_zones/1
  def show
    render json: @ps_zone
  end

  # POST /ps_zones
  def create
    @ps_zone = PsZone.new(ps_zone_params)

    if @ps_zone.save
      render json: @ps_zone, status: :created, location: @ps_zone
    else
      render json: @ps_zone.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_zones/1
  def update
    if @ps_zone.update(ps_zone_params)
      render json: @ps_zone
    else
      render json: @ps_zone.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_zones/1
  def destroy
    @ps_zone.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_zone
      @ps_zone = PsZone.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_zone_params
      params.fetch(:ps_zone, {})
    end
end
