class PsFeatureProductsController < ApplicationController
  before_action :set_ps_feature_product, only: [:show, :update, :destroy]

  # GET /ps_feature_products
  def index
    @ps_feature_products = PsFeatureProduct.all

    render json: @ps_feature_products
  end

  # GET /ps_feature_products/1
  def show
    render json: @ps_feature_product
  end

  # POST /ps_feature_products
  def create
    @ps_feature_product = PsFeatureProduct.new(ps_feature_product_params)

    if @ps_feature_product.save
      render json: @ps_feature_product, status: :created, location: @ps_feature_product
    else
      render json: @ps_feature_product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_feature_products/1
  def update
    if @ps_feature_product.update(ps_feature_product_params)
      render json: @ps_feature_product
    else
      render json: @ps_feature_product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_feature_products/1
  def destroy
    @ps_feature_product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_feature_product
      @ps_feature_product = PsFeatureProduct.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_feature_product_params
      params.fetch(:ps_feature_product, {})
    end
end
