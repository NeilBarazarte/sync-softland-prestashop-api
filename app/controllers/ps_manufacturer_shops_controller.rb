class PsManufacturerShopsController < ApplicationController
  before_action :set_ps_manufacturer_shop, only: [:show, :update, :destroy]

  # GET /ps_manufacturer_shops
  def index
    @ps_manufacturer_shops = PsManufacturerShop.all

    render json: @ps_manufacturer_shops
  end

  # GET /ps_manufacturer_shops/1
  def show
    render json: @ps_manufacturer_shop
  end

  # POST /ps_manufacturer_shops
  def create
    @ps_manufacturer_shop = PsManufacturerShop.new(ps_manufacturer_shop_params)

    if @ps_manufacturer_shop.save
      render json: @ps_manufacturer_shop, status: :created, location: @ps_manufacturer_shop
    else
      render json: @ps_manufacturer_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_manufacturer_shops/1
  def update
    if @ps_manufacturer_shop.update(ps_manufacturer_shop_params)
      render json: @ps_manufacturer_shop
    else
      render json: @ps_manufacturer_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_manufacturer_shops/1
  def destroy
    @ps_manufacturer_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_manufacturer_shop
      @ps_manufacturer_shop = PsManufacturerShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_manufacturer_shop_params
      params.fetch(:ps_manufacturer_shop, {})
    end
end
