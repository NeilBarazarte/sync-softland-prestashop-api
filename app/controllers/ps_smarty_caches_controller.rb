class PsSmartyCachesController < ApplicationController
  before_action :set_ps_smarty_cache, only: [:show, :update, :destroy]

  # GET /ps_smarty_caches
  def index
    @ps_smarty_caches = PsSmartyCache.all

    render json: @ps_smarty_caches
  end

  # GET /ps_smarty_caches/1
  def show
    render json: @ps_smarty_cache
  end

  # POST /ps_smarty_caches
  def create
    @ps_smarty_cache = PsSmartyCache.new(ps_smarty_cache_params)

    if @ps_smarty_cache.save
      render json: @ps_smarty_cache, status: :created, location: @ps_smarty_cache
    else
      render json: @ps_smarty_cache.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_smarty_caches/1
  def update
    if @ps_smarty_cache.update(ps_smarty_cache_params)
      render json: @ps_smarty_cache
    else
      render json: @ps_smarty_cache.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_smarty_caches/1
  def destroy
    @ps_smarty_cache.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_smarty_cache
      @ps_smarty_cache = PsSmartyCache.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_smarty_cache_params
      params.fetch(:ps_smarty_cache, {})
    end
end
