class PsImageShopsController < ApplicationController
  before_action :set_ps_image_shop, only: [:show, :update, :destroy]

  # GET /ps_image_shops
  def index
    @ps_image_shops = PsImageShop.all

    render json: @ps_image_shops
  end

  # GET /ps_image_shops/1
  def show
    render json: @ps_image_shop
  end

  # POST /ps_image_shops
  def create
    @ps_image_shop = PsImageShop.new(ps_image_shop_params)

    if @ps_image_shop.save
      render json: @ps_image_shop, status: :created, location: @ps_image_shop
    else
      render json: @ps_image_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_image_shops/1
  def update
    if @ps_image_shop.update(ps_image_shop_params)
      render json: @ps_image_shop
    else
      render json: @ps_image_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_image_shops/1
  def destroy
    @ps_image_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_image_shop
      @ps_image_shop = PsImageShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_image_shop_params
      params.fetch(:ps_image_shop, {})
    end
end
