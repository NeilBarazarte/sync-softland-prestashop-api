class PsReferrerCachesController < ApplicationController
  before_action :set_ps_referrer_cache, only: [:show, :update, :destroy]

  # GET /ps_referrer_caches
  def index
    @ps_referrer_caches = PsReferrerCache.all

    render json: @ps_referrer_caches
  end

  # GET /ps_referrer_caches/1
  def show
    render json: @ps_referrer_cache
  end

  # POST /ps_referrer_caches
  def create
    @ps_referrer_cache = PsReferrerCache.new(ps_referrer_cache_params)

    if @ps_referrer_cache.save
      render json: @ps_referrer_cache, status: :created, location: @ps_referrer_cache
    else
      render json: @ps_referrer_cache.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_referrer_caches/1
  def update
    if @ps_referrer_cache.update(ps_referrer_cache_params)
      render json: @ps_referrer_cache
    else
      render json: @ps_referrer_cache.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_referrer_caches/1
  def destroy
    @ps_referrer_cache.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_referrer_cache
      @ps_referrer_cache = PsReferrerCache.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_referrer_cache_params
      params.fetch(:ps_referrer_cache, {})
    end
end
