class PsCartRuleShopsController < ApplicationController
  before_action :set_ps_cart_rule_shop, only: [:show, :update, :destroy]

  # GET /ps_cart_rule_shops
  def index
    @ps_cart_rule_shops = PsCartRuleShop.all

    render json: @ps_cart_rule_shops
  end

  # GET /ps_cart_rule_shops/1
  def show
    render json: @ps_cart_rule_shop
  end

  # POST /ps_cart_rule_shops
  def create
    @ps_cart_rule_shop = PsCartRuleShop.new(ps_cart_rule_shop_params)

    if @ps_cart_rule_shop.save
      render json: @ps_cart_rule_shop, status: :created, location: @ps_cart_rule_shop
    else
      render json: @ps_cart_rule_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_cart_rule_shops/1
  def update
    if @ps_cart_rule_shop.update(ps_cart_rule_shop_params)
      render json: @ps_cart_rule_shop
    else
      render json: @ps_cart_rule_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_cart_rule_shops/1
  def destroy
    @ps_cart_rule_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_cart_rule_shop
      @ps_cart_rule_shop = PsCartRuleShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_cart_rule_shop_params
      params.fetch(:ps_cart_rule_shop, {})
    end
end
