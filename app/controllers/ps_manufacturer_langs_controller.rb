class PsManufacturerLangsController < ApplicationController
  before_action :set_ps_manufacturer_lang, only: [:show, :update, :destroy]

  # GET /ps_manufacturer_langs
  def index
    @ps_manufacturer_langs = PsManufacturerLang.all

    render json: @ps_manufacturer_langs
  end

  # GET /ps_manufacturer_langs/1
  def show
    render json: @ps_manufacturer_lang
  end

  # POST /ps_manufacturer_langs
  def create
    @ps_manufacturer_lang = PsManufacturerLang.new(ps_manufacturer_lang_params)

    if @ps_manufacturer_lang.save
      render json: @ps_manufacturer_lang, status: :created, location: @ps_manufacturer_lang
    else
      render json: @ps_manufacturer_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_manufacturer_langs/1
  def update
    if @ps_manufacturer_lang.update(ps_manufacturer_lang_params)
      render json: @ps_manufacturer_lang
    else
      render json: @ps_manufacturer_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_manufacturer_langs/1
  def destroy
    @ps_manufacturer_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_manufacturer_lang
      @ps_manufacturer_lang = PsManufacturerLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_manufacturer_lang_params
      params.fetch(:ps_manufacturer_lang, {})
    end
end
