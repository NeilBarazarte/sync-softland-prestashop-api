class PsZoneShopsController < ApplicationController
  before_action :set_ps_zone_shop, only: [:show, :update, :destroy]

  # GET /ps_zone_shops
  def index
    @ps_zone_shops = PsZoneShop.all

    render json: @ps_zone_shops
  end

  # GET /ps_zone_shops/1
  def show
    render json: @ps_zone_shop
  end

  # POST /ps_zone_shops
  def create
    @ps_zone_shop = PsZoneShop.new(ps_zone_shop_params)

    if @ps_zone_shop.save
      render json: @ps_zone_shop, status: :created, location: @ps_zone_shop
    else
      render json: @ps_zone_shop.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_zone_shops/1
  def update
    if @ps_zone_shop.update(ps_zone_shop_params)
      render json: @ps_zone_shop
    else
      render json: @ps_zone_shop.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_zone_shops/1
  def destroy
    @ps_zone_shop.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_zone_shop
      @ps_zone_shop = PsZoneShop.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_zone_shop_params
      params.fetch(:ps_zone_shop, {})
    end
end
