class PsProfilesController < ApplicationController
  before_action :set_ps_profile, only: [:show, :update, :destroy]

  # GET /ps_profiles
  def index
    @ps_profiles = PsProfile.all

    render json: @ps_profiles
  end

  # GET /ps_profiles/1
  def show
    render json: @ps_profile
  end

  # POST /ps_profiles
  def create
    @ps_profile = PsProfile.new(ps_profile_params)

    if @ps_profile.save
      render json: @ps_profile, status: :created, location: @ps_profile
    else
      render json: @ps_profile.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_profiles/1
  def update
    if @ps_profile.update(ps_profile_params)
      render json: @ps_profile
    else
      render json: @ps_profile.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_profiles/1
  def destroy
    @ps_profile.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_profile
      @ps_profile = PsProfile.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_profile_params
      params.fetch(:ps_profile, {})
    end
end
