class PsProductCarriersController < ApplicationController
  before_action :set_ps_product_carrier, only: [:show, :update, :destroy]

  # GET /ps_product_carriers
  def index
    @ps_product_carriers = PsProductCarrier.all

    render json: @ps_product_carriers
  end

  # GET /ps_product_carriers/1
  def show
    render json: @ps_product_carrier
  end

  # POST /ps_product_carriers
  def create
    @ps_product_carrier = PsProductCarrier.new(ps_product_carrier_params)

    if @ps_product_carrier.save
      render json: @ps_product_carrier, status: :created, location: @ps_product_carrier
    else
      render json: @ps_product_carrier.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_carriers/1
  def update
    if @ps_product_carrier.update(ps_product_carrier_params)
      render json: @ps_product_carrier
    else
      render json: @ps_product_carrier.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_carriers/1
  def destroy
    @ps_product_carrier.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_carrier
      @ps_product_carrier = PsProductCarrier.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_carrier_params
      params.fetch(:ps_product_carrier, {})
    end
end
