class PsProductCountryTaxesController < ApplicationController
  before_action :set_ps_product_country_tax, only: [:show, :update, :destroy]

  # GET /ps_product_country_taxes
  def index
    @ps_product_country_taxes = PsProductCountryTax.all

    render json: @ps_product_country_taxes
  end

  # GET /ps_product_country_taxes/1
  def show
    render json: @ps_product_country_tax
  end

  # POST /ps_product_country_taxes
  def create
    @ps_product_country_tax = PsProductCountryTax.new(ps_product_country_tax_params)

    if @ps_product_country_tax.save
      render json: @ps_product_country_tax, status: :created, location: @ps_product_country_tax
    else
      render json: @ps_product_country_tax.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_product_country_taxes/1
  def update
    if @ps_product_country_tax.update(ps_product_country_tax_params)
      render json: @ps_product_country_tax
    else
      render json: @ps_product_country_tax.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_product_country_taxes/1
  def destroy
    @ps_product_country_tax.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_product_country_tax
      @ps_product_country_tax = PsProductCountryTax.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_product_country_tax_params
      params.fetch(:ps_product_country_tax, {})
    end
end
