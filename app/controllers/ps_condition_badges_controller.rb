class PsConditionBadgesController < ApplicationController
  before_action :set_ps_condition_badge, only: [:show, :update, :destroy]

  # GET /ps_condition_badges
  def index
    @ps_condition_badges = PsConditionBadge.all

    render json: @ps_condition_badges
  end

  # GET /ps_condition_badges/1
  def show
    render json: @ps_condition_badge
  end

  # POST /ps_condition_badges
  def create
    @ps_condition_badge = PsConditionBadge.new(ps_condition_badge_params)

    if @ps_condition_badge.save
      render json: @ps_condition_badge, status: :created, location: @ps_condition_badge
    else
      render json: @ps_condition_badge.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_condition_badges/1
  def update
    if @ps_condition_badge.update(ps_condition_badge_params)
      render json: @ps_condition_badge
    else
      render json: @ps_condition_badge.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_condition_badges/1
  def destroy
    @ps_condition_badge.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_condition_badge
      @ps_condition_badge = PsConditionBadge.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_condition_badge_params
      params.fetch(:ps_condition_badge, {})
    end
end
