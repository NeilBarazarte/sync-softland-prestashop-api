class PsHomesliderSlidesController < ApplicationController
  before_action :set_ps_homeslider_slide, only: [:show, :update, :destroy]

  # GET /ps_homeslider_slides
  def index
    @ps_homeslider_slides = PsHomesliderSlide.all

    render json: @ps_homeslider_slides
  end

  # GET /ps_homeslider_slides/1
  def show
    render json: @ps_homeslider_slide
  end

  # POST /ps_homeslider_slides
  def create
    @ps_homeslider_slide = PsHomesliderSlide.new(ps_homeslider_slide_params)

    if @ps_homeslider_slide.save
      render json: @ps_homeslider_slide, status: :created, location: @ps_homeslider_slide
    else
      render json: @ps_homeslider_slide.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_homeslider_slides/1
  def update
    if @ps_homeslider_slide.update(ps_homeslider_slide_params)
      render json: @ps_homeslider_slide
    else
      render json: @ps_homeslider_slide.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_homeslider_slides/1
  def destroy
    @ps_homeslider_slide.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_homeslider_slide
      @ps_homeslider_slide = PsHomesliderSlide.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_homeslider_slide_params
      params.fetch(:ps_homeslider_slide, {})
    end
end
