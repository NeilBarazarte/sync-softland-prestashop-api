class PsCustomizationFieldsController < ApplicationController
  before_action :set_ps_customization_field, only: [:show, :update, :destroy]

  # GET /ps_customization_fields
  def index
    @ps_customization_fields = PsCustomizationField.all

    render json: @ps_customization_fields
  end

  # GET /ps_customization_fields/1
  def show
    render json: @ps_customization_field
  end

  # POST /ps_customization_fields
  def create
    @ps_customization_field = PsCustomizationField.new(ps_customization_field_params)

    if @ps_customization_field.save
      render json: @ps_customization_field, status: :created, location: @ps_customization_field
    else
      render json: @ps_customization_field.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_customization_fields/1
  def update
    if @ps_customization_field.update(ps_customization_field_params)
      render json: @ps_customization_field
    else
      render json: @ps_customization_field.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_customization_fields/1
  def destroy
    @ps_customization_field.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_customization_field
      @ps_customization_field = PsCustomizationField.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_customization_field_params
      params.fetch(:ps_customization_field, {})
    end
end
