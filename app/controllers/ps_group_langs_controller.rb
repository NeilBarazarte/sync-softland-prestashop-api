class PsGroupLangsController < ApplicationController
  before_action :set_ps_group_lang, only: [:show, :update, :destroy]

  # GET /ps_group_langs
  def index
    @ps_group_langs = PsGroupLang.all

    render json: @ps_group_langs
  end

  # GET /ps_group_langs/1
  def show
    render json: @ps_group_lang
  end

  # POST /ps_group_langs
  def create
    @ps_group_lang = PsGroupLang.new(ps_group_lang_params)

    if @ps_group_lang.save
      render json: @ps_group_lang, status: :created, location: @ps_group_lang
    else
      render json: @ps_group_lang.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_group_langs/1
  def update
    if @ps_group_lang.update(ps_group_lang_params)
      render json: @ps_group_lang
    else
      render json: @ps_group_lang.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_group_langs/1
  def destroy
    @ps_group_lang.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_group_lang
      @ps_group_lang = PsGroupLang.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_group_lang_params
      params.fetch(:ps_group_lang, {})
    end
end
