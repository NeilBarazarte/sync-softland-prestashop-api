class PsSuppliersController < ApplicationController
  before_action :set_ps_supplier, only: [:show, :update, :destroy]

  # GET /ps_suppliers
  def index
    @ps_suppliers = PsSupplier.all

    render json: @ps_suppliers
  end

  # GET /ps_suppliers/1
  def show
    render json: @ps_supplier
  end

  # POST /ps_suppliers
  def create
    @ps_supplier = PsSupplier.new(ps_supplier_params)

    if @ps_supplier.save
      render json: @ps_supplier, status: :created, location: @ps_supplier
    else
      render json: @ps_supplier.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ps_suppliers/1
  def update
    if @ps_supplier.update(ps_supplier_params)
      render json: @ps_supplier
    else
      render json: @ps_supplier.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ps_suppliers/1
  def destroy
    @ps_supplier.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ps_supplier
      @ps_supplier = PsSupplier.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ps_supplier_params
      params.fetch(:ps_supplier, {})
    end
end
