require 'test_helper'

class PsModuleAccessesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_module_access = ps_module_access(:one)
  end

  test "should get index" do
    get ps_module_accesses_url, as: :json
    assert_response :success
  end

  test "should create ps_module_access" do
    assert_difference('PsModuleAccess.count') do
      post ps_module_accesses_url, params: { ps_module_access: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_module_access" do
    get ps_module_access_url(@ps_module_access), as: :json
    assert_response :success
  end

  test "should update ps_module_access" do
    patch ps_module_access_url(@ps_module_access), params: { ps_module_access: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_module_access" do
    assert_difference('PsModuleAccess.count', -1) do
      delete ps_module_access_url(@ps_module_access), as: :json
    end

    assert_response 204
  end
end
