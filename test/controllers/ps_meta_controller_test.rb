require 'test_helper'

class PsMetaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_metum = ps_metum(:one)
  end

  test "should get index" do
    get ps_meta_url, as: :json
    assert_response :success
  end

  test "should create ps_metum" do
    assert_difference('PsMetum.count') do
      post ps_meta_url, params: { ps_metum: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_metum" do
    get ps_metum_url(@ps_metum), as: :json
    assert_response :success
  end

  test "should update ps_metum" do
    patch ps_metum_url(@ps_metum), params: { ps_metum: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_metum" do
    assert_difference('PsMetum.count', -1) do
      delete ps_metum_url(@ps_metum), as: :json
    end

    assert_response 204
  end
end
