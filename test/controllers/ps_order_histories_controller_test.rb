require 'test_helper'

class PsOrderHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_history = ps_order_history(:one)
  end

  test "should get index" do
    get ps_order_histories_url, as: :json
    assert_response :success
  end

  test "should create ps_order_history" do
    assert_difference('PsOrderHistory.count') do
      post ps_order_histories_url, params: { ps_order_history: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_history" do
    get ps_order_history_url(@ps_order_history), as: :json
    assert_response :success
  end

  test "should update ps_order_history" do
    patch ps_order_history_url(@ps_order_history), params: { ps_order_history: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_history" do
    assert_difference('PsOrderHistory.count', -1) do
      delete ps_order_history_url(@ps_order_history), as: :json
    end

    assert_response 204
  end
end
