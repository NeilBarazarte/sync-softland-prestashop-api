require 'test_helper'

class PsSpecificPriceRulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_specific_price_rule = ps_specific_price_rule(:one)
  end

  test "should get index" do
    get ps_specific_price_rules_url, as: :json
    assert_response :success
  end

  test "should create ps_specific_price_rule" do
    assert_difference('PsSpecificPriceRule.count') do
      post ps_specific_price_rules_url, params: { ps_specific_price_rule: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_specific_price_rule" do
    get ps_specific_price_rule_url(@ps_specific_price_rule), as: :json
    assert_response :success
  end

  test "should update ps_specific_price_rule" do
    patch ps_specific_price_rule_url(@ps_specific_price_rule), params: { ps_specific_price_rule: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_specific_price_rule" do
    assert_difference('PsSpecificPriceRule.count', -1) do
      delete ps_specific_price_rule_url(@ps_specific_price_rule), as: :json
    end

    assert_response 204
  end
end
