require 'test_helper'

class PsManufacturerShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_manufacturer_shop = ps_manufacturer_shop(:one)
  end

  test "should get index" do
    get ps_manufacturer_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_manufacturer_shop" do
    assert_difference('PsManufacturerShop.count') do
      post ps_manufacturer_shops_url, params: { ps_manufacturer_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_manufacturer_shop" do
    get ps_manufacturer_shop_url(@ps_manufacturer_shop), as: :json
    assert_response :success
  end

  test "should update ps_manufacturer_shop" do
    patch ps_manufacturer_shop_url(@ps_manufacturer_shop), params: { ps_manufacturer_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_manufacturer_shop" do
    assert_difference('PsManufacturerShop.count', -1) do
      delete ps_manufacturer_shop_url(@ps_manufacturer_shop), as: :json
    end

    assert_response 204
  end
end
