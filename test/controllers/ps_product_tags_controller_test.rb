require 'test_helper'

class PsProductTagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_tag = ps_product_tag(:one)
  end

  test "should get index" do
    get ps_product_tags_url, as: :json
    assert_response :success
  end

  test "should create ps_product_tag" do
    assert_difference('PsProductTag.count') do
      post ps_product_tags_url, params: { ps_product_tag: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_tag" do
    get ps_product_tag_url(@ps_product_tag), as: :json
    assert_response :success
  end

  test "should update ps_product_tag" do
    patch ps_product_tag_url(@ps_product_tag), params: { ps_product_tag: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_tag" do
    assert_difference('PsProductTag.count', -1) do
      delete ps_product_tag_url(@ps_product_tag), as: :json
    end

    assert_response 204
  end
end
