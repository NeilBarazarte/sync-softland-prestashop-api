require 'test_helper'

class PsCountryLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_country_lang = ps_country_lang(:one)
  end

  test "should get index" do
    get ps_country_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_country_lang" do
    assert_difference('PsCountryLang.count') do
      post ps_country_langs_url, params: { ps_country_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_country_lang" do
    get ps_country_lang_url(@ps_country_lang), as: :json
    assert_response :success
  end

  test "should update ps_country_lang" do
    patch ps_country_lang_url(@ps_country_lang), params: { ps_country_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_country_lang" do
    assert_difference('PsCountryLang.count', -1) do
      delete ps_country_lang_url(@ps_country_lang), as: :json
    end

    assert_response 204
  end
end
