require 'test_helper'

class PsEmployeeShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_employee_shop = ps_employee_shop(:one)
  end

  test "should get index" do
    get ps_employee_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_employee_shop" do
    assert_difference('PsEmployeeShop.count') do
      post ps_employee_shops_url, params: { ps_employee_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_employee_shop" do
    get ps_employee_shop_url(@ps_employee_shop), as: :json
    assert_response :success
  end

  test "should update ps_employee_shop" do
    patch ps_employee_shop_url(@ps_employee_shop), params: { ps_employee_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_employee_shop" do
    assert_difference('PsEmployeeShop.count', -1) do
      delete ps_employee_shop_url(@ps_employee_shop), as: :json
    end

    assert_response 204
  end
end
