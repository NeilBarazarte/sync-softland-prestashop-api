require 'test_helper'

class PsOrderSlipDetailTaxesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_slip_detail_tax = ps_order_slip_detail_tax(:one)
  end

  test "should get index" do
    get ps_order_slip_detail_taxes_url, as: :json
    assert_response :success
  end

  test "should create ps_order_slip_detail_tax" do
    assert_difference('PsOrderSlipDetailTax.count') do
      post ps_order_slip_detail_taxes_url, params: { ps_order_slip_detail_tax: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_slip_detail_tax" do
    get ps_order_slip_detail_tax_url(@ps_order_slip_detail_tax), as: :json
    assert_response :success
  end

  test "should update ps_order_slip_detail_tax" do
    patch ps_order_slip_detail_tax_url(@ps_order_slip_detail_tax), params: { ps_order_slip_detail_tax: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_slip_detail_tax" do
    assert_difference('PsOrderSlipDetailTax.count', -1) do
      delete ps_order_slip_detail_tax_url(@ps_order_slip_detail_tax), as: :json
    end

    assert_response 204
  end
end
