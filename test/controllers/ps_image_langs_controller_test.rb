require 'test_helper'

class PsImageLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_image_lang = ps_image_lang(:one)
  end

  test "should get index" do
    get ps_image_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_image_lang" do
    assert_difference('PsImageLang.count') do
      post ps_image_langs_url, params: { ps_image_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_image_lang" do
    get ps_image_lang_url(@ps_image_lang), as: :json
    assert_response :success
  end

  test "should update ps_image_lang" do
    patch ps_image_lang_url(@ps_image_lang), params: { ps_image_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_image_lang" do
    assert_difference('PsImageLang.count', -1) do
      delete ps_image_lang_url(@ps_image_lang), as: :json
    end

    assert_response 204
  end
end
