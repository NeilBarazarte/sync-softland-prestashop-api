require 'test_helper'

class PsSpecificPriceRuleConditionGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_specific_price_rule_condition_group = ps_specific_price_rule_condition_group(:one)
  end

  test "should get index" do
    get ps_specific_price_rule_condition_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_specific_price_rule_condition_group" do
    assert_difference('PsSpecificPriceRuleConditionGroup.count') do
      post ps_specific_price_rule_condition_groups_url, params: { ps_specific_price_rule_condition_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_specific_price_rule_condition_group" do
    get ps_specific_price_rule_condition_group_url(@ps_specific_price_rule_condition_group), as: :json
    assert_response :success
  end

  test "should update ps_specific_price_rule_condition_group" do
    patch ps_specific_price_rule_condition_group_url(@ps_specific_price_rule_condition_group), params: { ps_specific_price_rule_condition_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_specific_price_rule_condition_group" do
    assert_difference('PsSpecificPriceRuleConditionGroup.count', -1) do
      delete ps_specific_price_rule_condition_group_url(@ps_specific_price_rule_condition_group), as: :json
    end

    assert_response 204
  end
end
