require 'test_helper'

class PsCronjobsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cronjob = ps_cronjob(:one)
  end

  test "should get index" do
    get ps_cronjobs_url, as: :json
    assert_response :success
  end

  test "should create ps_cronjob" do
    assert_difference('PsCronjob.count') do
      post ps_cronjobs_url, params: { ps_cronjob: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cronjob" do
    get ps_cronjob_url(@ps_cronjob), as: :json
    assert_response :success
  end

  test "should update ps_cronjob" do
    patch ps_cronjob_url(@ps_cronjob), params: { ps_cronjob: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cronjob" do
    assert_difference('PsCronjob.count', -1) do
      delete ps_cronjob_url(@ps_cronjob), as: :json
    end

    assert_response 204
  end
end
