require 'test_helper'

class PsAddressFormatsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_address_format = ps_address_format(:one)
  end

  test "should get index" do
    get ps_address_formats_url, as: :json
    assert_response :success
  end

  test "should create ps_address_format" do
    assert_difference('PsAddressFormat.count') do
      post ps_address_formats_url, params: { ps_address_format: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_address_format" do
    get ps_address_format_url(@ps_address_format), as: :json
    assert_response :success
  end

  test "should update ps_address_format" do
    patch ps_address_format_url(@ps_address_format), params: { ps_address_format: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_address_format" do
    assert_difference('PsAddressFormat.count', -1) do
      delete ps_address_format_url(@ps_address_format), as: :json
    end

    assert_response 204
  end
end
