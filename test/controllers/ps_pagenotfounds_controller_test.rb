require 'test_helper'

class PsPagenotfoundsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_pagenotfound = ps_pagenotfound(:one)
  end

  test "should get index" do
    get ps_pagenotfounds_url, as: :json
    assert_response :success
  end

  test "should create ps_pagenotfound" do
    assert_difference('PsPagenotfound.count') do
      post ps_pagenotfounds_url, params: { ps_pagenotfound: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_pagenotfound" do
    get ps_pagenotfound_url(@ps_pagenotfound), as: :json
    assert_response :success
  end

  test "should update ps_pagenotfound" do
    patch ps_pagenotfound_url(@ps_pagenotfound), params: { ps_pagenotfound: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_pagenotfound" do
    assert_difference('PsPagenotfound.count', -1) do
      delete ps_pagenotfound_url(@ps_pagenotfound), as: :json
    end

    assert_response 204
  end
end
