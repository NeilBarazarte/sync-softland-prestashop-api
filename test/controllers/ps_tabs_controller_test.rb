require 'test_helper'

class PsTabsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tab = ps_tab(:one)
  end

  test "should get index" do
    get ps_tabs_url, as: :json
    assert_response :success
  end

  test "should create ps_tab" do
    assert_difference('PsTab.count') do
      post ps_tabs_url, params: { ps_tab: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tab" do
    get ps_tab_url(@ps_tab), as: :json
    assert_response :success
  end

  test "should update ps_tab" do
    patch ps_tab_url(@ps_tab), params: { ps_tab: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tab" do
    assert_difference('PsTab.count', -1) do
      delete ps_tab_url(@ps_tab), as: :json
    end

    assert_response 204
  end
end
