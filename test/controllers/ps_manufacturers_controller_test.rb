require 'test_helper'

class PsManufacturersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_manufacturer = ps_manufacturer(:one)
  end

  test "should get index" do
    get ps_manufacturers_url, as: :json
    assert_response :success
  end

  test "should create ps_manufacturer" do
    assert_difference('PsManufacturer.count') do
      post ps_manufacturers_url, params: { ps_manufacturer: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_manufacturer" do
    get ps_manufacturer_url(@ps_manufacturer), as: :json
    assert_response :success
  end

  test "should update ps_manufacturer" do
    patch ps_manufacturer_url(@ps_manufacturer), params: { ps_manufacturer: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_manufacturer" do
    assert_difference('PsManufacturer.count', -1) do
      delete ps_manufacturer_url(@ps_manufacturer), as: :json
    end

    assert_response 204
  end
end
