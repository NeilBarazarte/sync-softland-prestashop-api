require 'test_helper'

class PsShopUrlsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_shop_url = ps_shop_url(:one)
  end

  test "should get index" do
    get ps_shop_urls_url, as: :json
    assert_response :success
  end

  test "should create ps_shop_url" do
    assert_difference('PsShopUrl.count') do
      post ps_shop_urls_url, params: { ps_shop_url: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_shop_url" do
    get ps_shop_url_url(@ps_shop_url), as: :json
    assert_response :success
  end

  test "should update ps_shop_url" do
    patch ps_shop_url_url(@ps_shop_url), params: { ps_shop_url: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_shop_url" do
    assert_difference('PsShopUrl.count', -1) do
      delete ps_shop_url_url(@ps_shop_url), as: :json
    end

    assert_response 204
  end
end
