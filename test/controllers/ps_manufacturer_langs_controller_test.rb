require 'test_helper'

class PsManufacturerLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_manufacturer_lang = ps_manufacturer_lang(:one)
  end

  test "should get index" do
    get ps_manufacturer_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_manufacturer_lang" do
    assert_difference('PsManufacturerLang.count') do
      post ps_manufacturer_langs_url, params: { ps_manufacturer_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_manufacturer_lang" do
    get ps_manufacturer_lang_url(@ps_manufacturer_lang), as: :json
    assert_response :success
  end

  test "should update ps_manufacturer_lang" do
    patch ps_manufacturer_lang_url(@ps_manufacturer_lang), params: { ps_manufacturer_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_manufacturer_lang" do
    assert_difference('PsManufacturerLang.count', -1) do
      delete ps_manufacturer_lang_url(@ps_manufacturer_lang), as: :json
    end

    assert_response 204
  end
end
