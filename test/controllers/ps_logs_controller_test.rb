require 'test_helper'

class PsLogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_log = ps_log(:one)
  end

  test "should get index" do
    get ps_logs_url, as: :json
    assert_response :success
  end

  test "should create ps_log" do
    assert_difference('PsLog.count') do
      post ps_logs_url, params: { ps_log: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_log" do
    get ps_log_url(@ps_log), as: :json
    assert_response :success
  end

  test "should update ps_log" do
    patch ps_log_url(@ps_log), params: { ps_log: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_log" do
    assert_difference('PsLog.count', -1) do
      delete ps_log_url(@ps_log), as: :json
    end

    assert_response 204
  end
end
