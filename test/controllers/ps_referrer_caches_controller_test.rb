require 'test_helper'

class PsReferrerCachesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_referrer_cache = ps_referrer_cache(:one)
  end

  test "should get index" do
    get ps_referrer_caches_url, as: :json
    assert_response :success
  end

  test "should create ps_referrer_cache" do
    assert_difference('PsReferrerCache.count') do
      post ps_referrer_caches_url, params: { ps_referrer_cache: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_referrer_cache" do
    get ps_referrer_cache_url(@ps_referrer_cache), as: :json
    assert_response :success
  end

  test "should update ps_referrer_cache" do
    patch ps_referrer_cache_url(@ps_referrer_cache), params: { ps_referrer_cache: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_referrer_cache" do
    assert_difference('PsReferrerCache.count', -1) do
      delete ps_referrer_cache_url(@ps_referrer_cache), as: :json
    end

    assert_response 204
  end
end
