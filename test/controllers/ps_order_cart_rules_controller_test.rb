require 'test_helper'

class PsOrderCartRulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_cart_rule = ps_order_cart_rule(:one)
  end

  test "should get index" do
    get ps_order_cart_rules_url, as: :json
    assert_response :success
  end

  test "should create ps_order_cart_rule" do
    assert_difference('PsOrderCartRule.count') do
      post ps_order_cart_rules_url, params: { ps_order_cart_rule: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_cart_rule" do
    get ps_order_cart_rule_url(@ps_order_cart_rule), as: :json
    assert_response :success
  end

  test "should update ps_order_cart_rule" do
    patch ps_order_cart_rule_url(@ps_order_cart_rule), params: { ps_order_cart_rule: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_cart_rule" do
    assert_difference('PsOrderCartRule.count', -1) do
      delete ps_order_cart_rule_url(@ps_order_cart_rule), as: :json
    end

    assert_response 204
  end
end
