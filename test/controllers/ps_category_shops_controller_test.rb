require 'test_helper'

class PsCategoryShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_category_shop = ps_category_shop(:one)
  end

  test "should get index" do
    get ps_category_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_category_shop" do
    assert_difference('PsCategoryShop.count') do
      post ps_category_shops_url, params: { ps_category_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_category_shop" do
    get ps_category_shop_url(@ps_category_shop), as: :json
    assert_response :success
  end

  test "should update ps_category_shop" do
    patch ps_category_shop_url(@ps_category_shop), params: { ps_category_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_category_shop" do
    assert_difference('PsCategoryShop.count', -1) do
      delete ps_category_shop_url(@ps_category_shop), as: :json
    end

    assert_response 204
  end
end
