require 'test_helper'

class PsEmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_employee = ps_employee(:one)
  end

  test "should get index" do
    get ps_employees_url, as: :json
    assert_response :success
  end

  test "should create ps_employee" do
    assert_difference('PsEmployee.count') do
      post ps_employees_url, params: { ps_employee: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_employee" do
    get ps_employee_url(@ps_employee), as: :json
    assert_response :success
  end

  test "should update ps_employee" do
    patch ps_employee_url(@ps_employee), params: { ps_employee: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_employee" do
    assert_difference('PsEmployee.count', -1) do
      delete ps_employee_url(@ps_employee), as: :json
    end

    assert_response 204
  end
end
