require 'test_helper'

class PsCustomizedDataControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_customized_datum = ps_customized_datum(:one)
  end

  test "should get index" do
    get ps_customized_data_url, as: :json
    assert_response :success
  end

  test "should create ps_customized_datum" do
    assert_difference('PsCustomizedDatum.count') do
      post ps_customized_data_url, params: { ps_customized_datum: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_customized_datum" do
    get ps_customized_datum_url(@ps_customized_datum), as: :json
    assert_response :success
  end

  test "should update ps_customized_datum" do
    patch ps_customized_datum_url(@ps_customized_datum), params: { ps_customized_datum: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_customized_datum" do
    assert_difference('PsCustomizedDatum.count', -1) do
      delete ps_customized_datum_url(@ps_customized_datum), as: :json
    end

    assert_response 204
  end
end
