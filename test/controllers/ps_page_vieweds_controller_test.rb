require 'test_helper'

class PsPageViewedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_page_viewed = ps_page_viewed(:one)
  end

  test "should get index" do
    get ps_page_vieweds_url, as: :json
    assert_response :success
  end

  test "should create ps_page_viewed" do
    assert_difference('PsPageViewed.count') do
      post ps_page_vieweds_url, params: { ps_page_viewed: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_page_viewed" do
    get ps_page_viewed_url(@ps_page_viewed), as: :json
    assert_response :success
  end

  test "should update ps_page_viewed" do
    patch ps_page_viewed_url(@ps_page_viewed), params: { ps_page_viewed: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_page_viewed" do
    assert_difference('PsPageViewed.count', -1) do
      delete ps_page_viewed_url(@ps_page_viewed), as: :json
    end

    assert_response 204
  end
end
