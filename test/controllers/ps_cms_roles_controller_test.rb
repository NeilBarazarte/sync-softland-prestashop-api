require 'test_helper'

class PsCmsRolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cms_role = ps_cms_role(:one)
  end

  test "should get index" do
    get ps_cms_roles_url, as: :json
    assert_response :success
  end

  test "should create ps_cms_role" do
    assert_difference('PsCmsRole.count') do
      post ps_cms_roles_url, params: { ps_cms_role: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cms_role" do
    get ps_cms_role_url(@ps_cms_role), as: :json
    assert_response :success
  end

  test "should update ps_cms_role" do
    patch ps_cms_role_url(@ps_cms_role), params: { ps_cms_role: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cms_role" do
    assert_difference('PsCmsRole.count', -1) do
      delete ps_cms_role_url(@ps_cms_role), as: :json
    end

    assert_response 204
  end
end
