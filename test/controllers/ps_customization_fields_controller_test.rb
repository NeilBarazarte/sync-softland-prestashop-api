require 'test_helper'

class PsCustomizationFieldsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_customization_field = ps_customization_field(:one)
  end

  test "should get index" do
    get ps_customization_fields_url, as: :json
    assert_response :success
  end

  test "should create ps_customization_field" do
    assert_difference('PsCustomizationField.count') do
      post ps_customization_fields_url, params: { ps_customization_field: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_customization_field" do
    get ps_customization_field_url(@ps_customization_field), as: :json
    assert_response :success
  end

  test "should update ps_customization_field" do
    patch ps_customization_field_url(@ps_customization_field), params: { ps_customization_field: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_customization_field" do
    assert_difference('PsCustomizationField.count', -1) do
      delete ps_customization_field_url(@ps_customization_field), as: :json
    end

    assert_response 204
  end
end
