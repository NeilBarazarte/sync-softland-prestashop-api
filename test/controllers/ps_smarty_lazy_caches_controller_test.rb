require 'test_helper'

class PsSmartyLazyCachesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_smarty_lazy_cache = ps_smarty_lazy_cache(:one)
  end

  test "should get index" do
    get ps_smarty_lazy_caches_url, as: :json
    assert_response :success
  end

  test "should create ps_smarty_lazy_cache" do
    assert_difference('PsSmartyLazyCache.count') do
      post ps_smarty_lazy_caches_url, params: { ps_smarty_lazy_cache: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_smarty_lazy_cache" do
    get ps_smarty_lazy_cache_url(@ps_smarty_lazy_cache), as: :json
    assert_response :success
  end

  test "should update ps_smarty_lazy_cache" do
    patch ps_smarty_lazy_cache_url(@ps_smarty_lazy_cache), params: { ps_smarty_lazy_cache: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_smarty_lazy_cache" do
    assert_difference('PsSmartyLazyCache.count', -1) do
      delete ps_smarty_lazy_cache_url(@ps_smarty_lazy_cache), as: :json
    end

    assert_response 204
  end
end
