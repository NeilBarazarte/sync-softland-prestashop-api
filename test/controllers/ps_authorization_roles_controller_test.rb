require 'test_helper'

class PsAuthorizationRolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_authorization_role = ps_authorization_role(:one)
  end

  test "should get index" do
    get ps_authorization_roles_url, as: :json
    assert_response :success
  end

  test "should create ps_authorization_role" do
    assert_difference('PsAuthorizationRole.count') do
      post ps_authorization_roles_url, params: { ps_authorization_role: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_authorization_role" do
    get ps_authorization_role_url(@ps_authorization_role), as: :json
    assert_response :success
  end

  test "should update ps_authorization_role" do
    patch ps_authorization_role_url(@ps_authorization_role), params: { ps_authorization_role: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_authorization_role" do
    assert_difference('PsAuthorizationRole.count', -1) do
      delete ps_authorization_role_url(@ps_authorization_role), as: :json
    end

    assert_response 204
  end
end
