require 'test_helper'

class PsRangePricesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_range_price = ps_range_price(:one)
  end

  test "should get index" do
    get ps_range_prices_url, as: :json
    assert_response :success
  end

  test "should create ps_range_price" do
    assert_difference('PsRangePrice.count') do
      post ps_range_prices_url, params: { ps_range_price: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_range_price" do
    get ps_range_price_url(@ps_range_price), as: :json
    assert_response :success
  end

  test "should update ps_range_price" do
    patch ps_range_price_url(@ps_range_price), params: { ps_range_price: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_range_price" do
    assert_difference('PsRangePrice.count', -1) do
      delete ps_range_price_url(@ps_range_price), as: :json
    end

    assert_response 204
  end
end
