require 'test_helper'

class PsShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_shop = ps_shop(:one)
  end

  test "should get index" do
    get ps_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_shop" do
    assert_difference('PsShop.count') do
      post ps_shops_url, params: { ps_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_shop" do
    get ps_shop_url(@ps_shop), as: :json
    assert_response :success
  end

  test "should update ps_shop" do
    patch ps_shop_url(@ps_shop), params: { ps_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_shop" do
    assert_difference('PsShop.count', -1) do
      delete ps_shop_url(@ps_shop), as: :json
    end

    assert_response 204
  end
end
