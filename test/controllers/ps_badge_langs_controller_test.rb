require 'test_helper'

class PsBadgeLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_badge_lang = ps_badge_lang(:one)
  end

  test "should get index" do
    get ps_badge_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_badge_lang" do
    assert_difference('PsBadgeLang.count') do
      post ps_badge_langs_url, params: { ps_badge_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_badge_lang" do
    get ps_badge_lang_url(@ps_badge_lang), as: :json
    assert_response :success
  end

  test "should update ps_badge_lang" do
    patch ps_badge_lang_url(@ps_badge_lang), params: { ps_badge_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_badge_lang" do
    assert_difference('PsBadgeLang.count', -1) do
      delete ps_badge_lang_url(@ps_badge_lang), as: :json
    end

    assert_response 204
  end
end
