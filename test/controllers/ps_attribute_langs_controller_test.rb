require 'test_helper'

class PsAttributeLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_attribute_lang = ps_attribute_lang(:one)
  end

  test "should get index" do
    get ps_attribute_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_attribute_lang" do
    assert_difference('PsAttributeLang.count') do
      post ps_attribute_langs_url, params: { ps_attribute_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_attribute_lang" do
    get ps_attribute_lang_url(@ps_attribute_lang), as: :json
    assert_response :success
  end

  test "should update ps_attribute_lang" do
    patch ps_attribute_lang_url(@ps_attribute_lang), params: { ps_attribute_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_attribute_lang" do
    assert_difference('PsAttributeLang.count', -1) do
      delete ps_attribute_lang_url(@ps_attribute_lang), as: :json
    end

    assert_response 204
  end
end
