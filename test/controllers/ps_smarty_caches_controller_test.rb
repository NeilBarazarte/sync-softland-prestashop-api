require 'test_helper'

class PsSmartyCachesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_smarty_cache = ps_smarty_cache(:one)
  end

  test "should get index" do
    get ps_smarty_caches_url, as: :json
    assert_response :success
  end

  test "should create ps_smarty_cache" do
    assert_difference('PsSmartyCache.count') do
      post ps_smarty_caches_url, params: { ps_smarty_cache: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_smarty_cache" do
    get ps_smarty_cache_url(@ps_smarty_cache), as: :json
    assert_response :success
  end

  test "should update ps_smarty_cache" do
    patch ps_smarty_cache_url(@ps_smarty_cache), params: { ps_smarty_cache: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_smarty_cache" do
    assert_difference('PsSmartyCache.count', -1) do
      delete ps_smarty_cache_url(@ps_smarty_cache), as: :json
    end

    assert_response 204
  end
end
