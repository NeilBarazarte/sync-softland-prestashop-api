require 'test_helper'

class PsWebserviceAccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_webservice_account = ps_webservice_account(:one)
  end

  test "should get index" do
    get ps_webservice_accounts_url, as: :json
    assert_response :success
  end

  test "should create ps_webservice_account" do
    assert_difference('PsWebserviceAccount.count') do
      post ps_webservice_accounts_url, params: { ps_webservice_account: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_webservice_account" do
    get ps_webservice_account_url(@ps_webservice_account), as: :json
    assert_response :success
  end

  test "should update ps_webservice_account" do
    patch ps_webservice_account_url(@ps_webservice_account), params: { ps_webservice_account: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_webservice_account" do
    assert_difference('PsWebserviceAccount.count', -1) do
      delete ps_webservice_account_url(@ps_webservice_account), as: :json
    end

    assert_response 204
  end
end
