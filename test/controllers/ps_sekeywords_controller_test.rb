require 'test_helper'

class PsSekeywordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_sekeyword = ps_sekeyword(:one)
  end

  test "should get index" do
    get ps_sekeywords_url, as: :json
    assert_response :success
  end

  test "should create ps_sekeyword" do
    assert_difference('PsSekeyword.count') do
      post ps_sekeywords_url, params: { ps_sekeyword: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_sekeyword" do
    get ps_sekeyword_url(@ps_sekeyword), as: :json
    assert_response :success
  end

  test "should update ps_sekeyword" do
    patch ps_sekeyword_url(@ps_sekeyword), params: { ps_sekeyword: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_sekeyword" do
    assert_difference('PsSekeyword.count', -1) do
      delete ps_sekeyword_url(@ps_sekeyword), as: :json
    end

    assert_response 204
  end
end
