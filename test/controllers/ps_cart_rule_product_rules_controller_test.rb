require 'test_helper'

class PsCartRuleProductRulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_rule_product_rule = ps_cart_rule_product_rule(:one)
  end

  test "should get index" do
    get ps_cart_rule_product_rules_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_rule_product_rule" do
    assert_difference('PsCartRuleProductRule.count') do
      post ps_cart_rule_product_rules_url, params: { ps_cart_rule_product_rule: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_rule_product_rule" do
    get ps_cart_rule_product_rule_url(@ps_cart_rule_product_rule), as: :json
    assert_response :success
  end

  test "should update ps_cart_rule_product_rule" do
    patch ps_cart_rule_product_rule_url(@ps_cart_rule_product_rule), params: { ps_cart_rule_product_rule: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_rule_product_rule" do
    assert_difference('PsCartRuleProductRule.count', -1) do
      delete ps_cart_rule_product_rule_url(@ps_cart_rule_product_rule), as: :json
    end

    assert_response 204
  end
end
