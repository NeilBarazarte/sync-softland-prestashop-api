require 'test_helper'

class PsStatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_state = ps_state(:one)
  end

  test "should get index" do
    get ps_states_url, as: :json
    assert_response :success
  end

  test "should create ps_state" do
    assert_difference('PsState.count') do
      post ps_states_url, params: { ps_state: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_state" do
    get ps_state_url(@ps_state), as: :json
    assert_response :success
  end

  test "should update ps_state" do
    patch ps_state_url(@ps_state), params: { ps_state: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_state" do
    assert_difference('PsState.count', -1) do
      delete ps_state_url(@ps_state), as: :json
    end

    assert_response 204
  end
end
