require 'test_helper'

class PsSupplyOrderReceiptHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_supply_order_receipt_history = ps_supply_order_receipt_history(:one)
  end

  test "should get index" do
    get ps_supply_order_receipt_histories_url, as: :json
    assert_response :success
  end

  test "should create ps_supply_order_receipt_history" do
    assert_difference('PsSupplyOrderReceiptHistory.count') do
      post ps_supply_order_receipt_histories_url, params: { ps_supply_order_receipt_history: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_supply_order_receipt_history" do
    get ps_supply_order_receipt_history_url(@ps_supply_order_receipt_history), as: :json
    assert_response :success
  end

  test "should update ps_supply_order_receipt_history" do
    patch ps_supply_order_receipt_history_url(@ps_supply_order_receipt_history), params: { ps_supply_order_receipt_history: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_supply_order_receipt_history" do
    assert_difference('PsSupplyOrderReceiptHistory.count', -1) do
      delete ps_supply_order_receipt_history_url(@ps_supply_order_receipt_history), as: :json
    end

    assert_response 204
  end
end
