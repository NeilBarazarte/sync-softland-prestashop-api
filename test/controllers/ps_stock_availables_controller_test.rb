require 'test_helper'

class PsStockAvailablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_stock_available = ps_stock_available(:one)
  end

  test "should get index" do
    get ps_stock_availables_url, as: :json
    assert_response :success
  end

  test "should create ps_stock_available" do
    assert_difference('PsStockAvailable.count') do
      post ps_stock_availables_url, params: { ps_stock_available: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_stock_available" do
    get ps_stock_available_url(@ps_stock_available), as: :json
    assert_response :success
  end

  test "should update ps_stock_available" do
    patch ps_stock_available_url(@ps_stock_available), params: { ps_stock_available: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_stock_available" do
    assert_difference('PsStockAvailable.count', -1) do
      delete ps_stock_available_url(@ps_stock_available), as: :json
    end

    assert_response 204
  end
end
