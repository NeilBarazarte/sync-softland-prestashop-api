require 'test_helper'

class PsContactShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_contact_shop = ps_contact_shop(:one)
  end

  test "should get index" do
    get ps_contact_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_contact_shop" do
    assert_difference('PsContactShop.count') do
      post ps_contact_shops_url, params: { ps_contact_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_contact_shop" do
    get ps_contact_shop_url(@ps_contact_shop), as: :json
    assert_response :success
  end

  test "should update ps_contact_shop" do
    patch ps_contact_shop_url(@ps_contact_shop), params: { ps_contact_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_contact_shop" do
    assert_difference('PsContactShop.count', -1) do
      delete ps_contact_shop_url(@ps_contact_shop), as: :json
    end

    assert_response 204
  end
end
