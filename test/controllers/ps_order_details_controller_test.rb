require 'test_helper'

class PsOrderDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_detail = ps_order_detail(:one)
  end

  test "should get index" do
    get ps_order_details_url, as: :json
    assert_response :success
  end

  test "should create ps_order_detail" do
    assert_difference('PsOrderDetail.count') do
      post ps_order_details_url, params: { ps_order_detail: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_detail" do
    get ps_order_detail_url(@ps_order_detail), as: :json
    assert_response :success
  end

  test "should update ps_order_detail" do
    patch ps_order_detail_url(@ps_order_detail), params: { ps_order_detail: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_detail" do
    assert_difference('PsOrderDetail.count', -1) do
      delete ps_order_detail_url(@ps_order_detail), as: :json
    end

    assert_response 204
  end
end
