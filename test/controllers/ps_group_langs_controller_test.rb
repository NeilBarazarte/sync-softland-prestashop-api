require 'test_helper'

class PsGroupLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_group_lang = ps_group_lang(:one)
  end

  test "should get index" do
    get ps_group_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_group_lang" do
    assert_difference('PsGroupLang.count') do
      post ps_group_langs_url, params: { ps_group_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_group_lang" do
    get ps_group_lang_url(@ps_group_lang), as: :json
    assert_response :success
  end

  test "should update ps_group_lang" do
    patch ps_group_lang_url(@ps_group_lang), params: { ps_group_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_group_lang" do
    assert_difference('PsGroupLang.count', -1) do
      delete ps_group_lang_url(@ps_group_lang), as: :json
    end

    assert_response 204
  end
end
