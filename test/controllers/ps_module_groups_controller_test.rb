require 'test_helper'

class PsModuleGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_module_group = ps_module_group(:one)
  end

  test "should get index" do
    get ps_module_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_module_group" do
    assert_difference('PsModuleGroup.count') do
      post ps_module_groups_url, params: { ps_module_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_module_group" do
    get ps_module_group_url(@ps_module_group), as: :json
    assert_response :success
  end

  test "should update ps_module_group" do
    patch ps_module_group_url(@ps_module_group), params: { ps_module_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_module_group" do
    assert_difference('PsModuleGroup.count', -1) do
      delete ps_module_group_url(@ps_module_group), as: :json
    end

    assert_response 204
  end
end
