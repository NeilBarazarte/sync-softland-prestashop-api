require 'test_helper'

class PsOrderSlipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_slip = ps_order_slip(:one)
  end

  test "should get index" do
    get ps_order_slips_url, as: :json
    assert_response :success
  end

  test "should create ps_order_slip" do
    assert_difference('PsOrderSlip.count') do
      post ps_order_slips_url, params: { ps_order_slip: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_slip" do
    get ps_order_slip_url(@ps_order_slip), as: :json
    assert_response :success
  end

  test "should update ps_order_slip" do
    patch ps_order_slip_url(@ps_order_slip), params: { ps_order_slip: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_slip" do
    assert_difference('PsOrderSlip.count', -1) do
      delete ps_order_slip_url(@ps_order_slip), as: :json
    end

    assert_response 204
  end
end
