require 'test_helper'

class PsCustomerMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_customer_message = ps_customer_message(:one)
  end

  test "should get index" do
    get ps_customer_messages_url, as: :json
    assert_response :success
  end

  test "should create ps_customer_message" do
    assert_difference('PsCustomerMessage.count') do
      post ps_customer_messages_url, params: { ps_customer_message: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_customer_message" do
    get ps_customer_message_url(@ps_customer_message), as: :json
    assert_response :success
  end

  test "should update ps_customer_message" do
    patch ps_customer_message_url(@ps_customer_message), params: { ps_customer_message: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_customer_message" do
    assert_difference('PsCustomerMessage.count', -1) do
      delete ps_customer_message_url(@ps_customer_message), as: :json
    end

    assert_response 204
  end
end
