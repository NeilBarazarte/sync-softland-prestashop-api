require 'test_helper'

class PsContactsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_contact = ps_contact(:one)
  end

  test "should get index" do
    get ps_contacts_url, as: :json
    assert_response :success
  end

  test "should create ps_contact" do
    assert_difference('PsContact.count') do
      post ps_contacts_url, params: { ps_contact: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_contact" do
    get ps_contact_url(@ps_contact), as: :json
    assert_response :success
  end

  test "should update ps_contact" do
    patch ps_contact_url(@ps_contact), params: { ps_contact: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_contact" do
    assert_difference('PsContact.count', -1) do
      delete ps_contact_url(@ps_contact), as: :json
    end

    assert_response 204
  end
end
