require 'test_helper'

class PsCmsCategoryShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cms_category_shop = ps_cms_category_shop(:one)
  end

  test "should get index" do
    get ps_cms_category_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_cms_category_shop" do
    assert_difference('PsCmsCategoryShop.count') do
      post ps_cms_category_shops_url, params: { ps_cms_category_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cms_category_shop" do
    get ps_cms_category_shop_url(@ps_cms_category_shop), as: :json
    assert_response :success
  end

  test "should update ps_cms_category_shop" do
    patch ps_cms_category_shop_url(@ps_cms_category_shop), params: { ps_cms_category_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cms_category_shop" do
    assert_difference('PsCmsCategoryShop.count', -1) do
      delete ps_cms_category_shop_url(@ps_cms_category_shop), as: :json
    end

    assert_response 204
  end
end
