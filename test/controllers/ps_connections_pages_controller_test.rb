require 'test_helper'

class PsConnectionsPagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_connections_page = ps_connections_page(:one)
  end

  test "should get index" do
    get ps_connections_pages_url, as: :json
    assert_response :success
  end

  test "should create ps_connections_page" do
    assert_difference('PsConnectionsPage.count') do
      post ps_connections_pages_url, params: { ps_connections_page: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_connections_page" do
    get ps_connections_page_url(@ps_connections_page), as: :json
    assert_response :success
  end

  test "should update ps_connections_page" do
    patch ps_connections_page_url(@ps_connections_page), params: { ps_connections_page: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_connections_page" do
    assert_difference('PsConnectionsPage.count', -1) do
      delete ps_connections_page_url(@ps_connections_page), as: :json
    end

    assert_response 204
  end
end
