require 'test_helper'

class PsAttributeGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_attribute_group = ps_attribute_group(:one)
  end

  test "should get index" do
    get ps_attribute_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_attribute_group" do
    assert_difference('PsAttributeGroup.count') do
      post ps_attribute_groups_url, params: { ps_attribute_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_attribute_group" do
    get ps_attribute_group_url(@ps_attribute_group), as: :json
    assert_response :success
  end

  test "should update ps_attribute_group" do
    patch ps_attribute_group_url(@ps_attribute_group), params: { ps_attribute_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_attribute_group" do
    assert_difference('PsAttributeGroup.count', -1) do
      delete ps_attribute_group_url(@ps_attribute_group), as: :json
    end

    assert_response 204
  end
end
