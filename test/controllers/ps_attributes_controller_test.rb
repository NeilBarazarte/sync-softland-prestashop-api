require 'test_helper'

class PsAttributesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_attribute = ps_attribute(:one)
  end

  test "should get index" do
    get ps_attributes_url, as: :json
    assert_response :success
  end

  test "should create ps_attribute" do
    assert_difference('PsAttribute.count') do
      post ps_attributes_url, params: { ps_attribute: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_attribute" do
    get ps_attribute_url(@ps_attribute), as: :json
    assert_response :success
  end

  test "should update ps_attribute" do
    patch ps_attribute_url(@ps_attribute), params: { ps_attribute: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_attribute" do
    assert_difference('PsAttribute.count', -1) do
      delete ps_attribute_url(@ps_attribute), as: :json
    end

    assert_response 204
  end
end
