require 'test_helper'

class PsModuleShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_module_shop = ps_module_shop(:one)
  end

  test "should get index" do
    get ps_module_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_module_shop" do
    assert_difference('PsModuleShop.count') do
      post ps_module_shops_url, params: { ps_module_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_module_shop" do
    get ps_module_shop_url(@ps_module_shop), as: :json
    assert_response :success
  end

  test "should update ps_module_shop" do
    patch ps_module_shop_url(@ps_module_shop), params: { ps_module_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_module_shop" do
    assert_difference('PsModuleShop.count', -1) do
      delete ps_module_shop_url(@ps_module_shop), as: :json
    end

    assert_response 204
  end
end
