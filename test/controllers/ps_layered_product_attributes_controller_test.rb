require 'test_helper'

class PsLayeredProductAttributesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_layered_product_attribute = ps_layered_product_attribute(:one)
  end

  test "should get index" do
    get ps_layered_product_attributes_url, as: :json
    assert_response :success
  end

  test "should create ps_layered_product_attribute" do
    assert_difference('PsLayeredProductAttribute.count') do
      post ps_layered_product_attributes_url, params: { ps_layered_product_attribute: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_layered_product_attribute" do
    get ps_layered_product_attribute_url(@ps_layered_product_attribute), as: :json
    assert_response :success
  end

  test "should update ps_layered_product_attribute" do
    patch ps_layered_product_attribute_url(@ps_layered_product_attribute), params: { ps_layered_product_attribute: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_layered_product_attribute" do
    assert_difference('PsLayeredProductAttribute.count', -1) do
      delete ps_layered_product_attribute_url(@ps_layered_product_attribute), as: :json
    end

    assert_response 204
  end
end
