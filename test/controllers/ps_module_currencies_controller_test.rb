require 'test_helper'

class PsModuleCurrenciesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_module_currency = ps_module_currency(:one)
  end

  test "should get index" do
    get ps_module_currencies_url, as: :json
    assert_response :success
  end

  test "should create ps_module_currency" do
    assert_difference('PsModuleCurrency.count') do
      post ps_module_currencies_url, params: { ps_module_currency: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_module_currency" do
    get ps_module_currency_url(@ps_module_currency), as: :json
    assert_response :success
  end

  test "should update ps_module_currency" do
    patch ps_module_currency_url(@ps_module_currency), params: { ps_module_currency: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_module_currency" do
    assert_difference('PsModuleCurrency.count', -1) do
      delete ps_module_currency_url(@ps_module_currency), as: :json
    end

    assert_response 204
  end
end
