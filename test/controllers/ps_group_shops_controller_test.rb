require 'test_helper'

class PsGroupShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_group_shop = ps_group_shop(:one)
  end

  test "should get index" do
    get ps_group_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_group_shop" do
    assert_difference('PsGroupShop.count') do
      post ps_group_shops_url, params: { ps_group_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_group_shop" do
    get ps_group_shop_url(@ps_group_shop), as: :json
    assert_response :success
  end

  test "should update ps_group_shop" do
    patch ps_group_shop_url(@ps_group_shop), params: { ps_group_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_group_shop" do
    assert_difference('PsGroupShop.count', -1) do
      delete ps_group_shop_url(@ps_group_shop), as: :json
    end

    assert_response 204
  end
end
