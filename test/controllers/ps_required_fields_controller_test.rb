require 'test_helper'

class PsRequiredFieldsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_required_field = ps_required_field(:one)
  end

  test "should get index" do
    get ps_required_fields_url, as: :json
    assert_response :success
  end

  test "should create ps_required_field" do
    assert_difference('PsRequiredField.count') do
      post ps_required_fields_url, params: { ps_required_field: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_required_field" do
    get ps_required_field_url(@ps_required_field), as: :json
    assert_response :success
  end

  test "should update ps_required_field" do
    patch ps_required_field_url(@ps_required_field), params: { ps_required_field: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_required_field" do
    assert_difference('PsRequiredField.count', -1) do
      delete ps_required_field_url(@ps_required_field), as: :json
    end

    assert_response 204
  end
end
