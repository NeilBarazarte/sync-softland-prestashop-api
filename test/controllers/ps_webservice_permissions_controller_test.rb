require 'test_helper'

class PsWebservicePermissionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_webservice_permission = ps_webservice_permission(:one)
  end

  test "should get index" do
    get ps_webservice_permissions_url, as: :json
    assert_response :success
  end

  test "should create ps_webservice_permission" do
    assert_difference('PsWebservicePermission.count') do
      post ps_webservice_permissions_url, params: { ps_webservice_permission: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_webservice_permission" do
    get ps_webservice_permission_url(@ps_webservice_permission), as: :json
    assert_response :success
  end

  test "should update ps_webservice_permission" do
    patch ps_webservice_permission_url(@ps_webservice_permission), params: { ps_webservice_permission: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_webservice_permission" do
    assert_difference('PsWebservicePermission.count', -1) do
      delete ps_webservice_permission_url(@ps_webservice_permission), as: :json
    end

    assert_response 204
  end
end
