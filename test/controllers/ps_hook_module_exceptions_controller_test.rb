require 'test_helper'

class PsHookModuleExceptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_hook_module_exception = ps_hook_module_exception(:one)
  end

  test "should get index" do
    get ps_hook_module_exceptions_url, as: :json
    assert_response :success
  end

  test "should create ps_hook_module_exception" do
    assert_difference('PsHookModuleException.count') do
      post ps_hook_module_exceptions_url, params: { ps_hook_module_exception: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_hook_module_exception" do
    get ps_hook_module_exception_url(@ps_hook_module_exception), as: :json
    assert_response :success
  end

  test "should update ps_hook_module_exception" do
    patch ps_hook_module_exception_url(@ps_hook_module_exception), params: { ps_hook_module_exception: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_hook_module_exception" do
    assert_difference('PsHookModuleException.count', -1) do
      delete ps_hook_module_exception_url(@ps_hook_module_exception), as: :json
    end

    assert_response 204
  end
end
