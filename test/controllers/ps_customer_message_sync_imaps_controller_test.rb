require 'test_helper'

class PsCustomerMessageSyncImapsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_customer_message_sync_imap = ps_customer_message_sync_imap(:one)
  end

  test "should get index" do
    get ps_customer_message_sync_imaps_url, as: :json
    assert_response :success
  end

  test "should create ps_customer_message_sync_imap" do
    assert_difference('PsCustomerMessageSyncImap.count') do
      post ps_customer_message_sync_imaps_url, params: { ps_customer_message_sync_imap: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_customer_message_sync_imap" do
    get ps_customer_message_sync_imap_url(@ps_customer_message_sync_imap), as: :json
    assert_response :success
  end

  test "should update ps_customer_message_sync_imap" do
    patch ps_customer_message_sync_imap_url(@ps_customer_message_sync_imap), params: { ps_customer_message_sync_imap: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_customer_message_sync_imap" do
    assert_difference('PsCustomerMessageSyncImap.count', -1) do
      delete ps_customer_message_sync_imap_url(@ps_customer_message_sync_imap), as: :json
    end

    assert_response 204
  end
end
