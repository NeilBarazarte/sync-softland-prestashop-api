require 'test_helper'

class PsCategoryProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_category_product = ps_category_product(:one)
  end

  test "should get index" do
    get ps_category_products_url, as: :json
    assert_response :success
  end

  test "should create ps_category_product" do
    assert_difference('PsCategoryProduct.count') do
      post ps_category_products_url, params: { ps_category_product: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_category_product" do
    get ps_category_product_url(@ps_category_product), as: :json
    assert_response :success
  end

  test "should update ps_category_product" do
    patch ps_category_product_url(@ps_category_product), params: { ps_category_product: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_category_product" do
    assert_difference('PsCategoryProduct.count', -1) do
      delete ps_category_product_url(@ps_category_product), as: :json
    end

    assert_response 204
  end
end
