require 'test_helper'

class PsCartCartRulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_cart_rule = ps_cart_cart_rule(:one)
  end

  test "should get index" do
    get ps_cart_cart_rules_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_cart_rule" do
    assert_difference('PsCartCartRule.count') do
      post ps_cart_cart_rules_url, params: { ps_cart_cart_rule: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_cart_rule" do
    get ps_cart_cart_rule_url(@ps_cart_cart_rule), as: :json
    assert_response :success
  end

  test "should update ps_cart_cart_rule" do
    patch ps_cart_cart_rule_url(@ps_cart_cart_rule), params: { ps_cart_cart_rule: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_cart_rule" do
    assert_difference('PsCartCartRule.count', -1) do
      delete ps_cart_cart_rule_url(@ps_cart_cart_rule), as: :json
    end

    assert_response 204
  end
end
