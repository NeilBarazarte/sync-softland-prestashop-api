require 'test_helper'

class PsFeatureLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_feature_lang = ps_feature_lang(:one)
  end

  test "should get index" do
    get ps_feature_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_feature_lang" do
    assert_difference('PsFeatureLang.count') do
      post ps_feature_langs_url, params: { ps_feature_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_feature_lang" do
    get ps_feature_lang_url(@ps_feature_lang), as: :json
    assert_response :success
  end

  test "should update ps_feature_lang" do
    patch ps_feature_lang_url(@ps_feature_lang), params: { ps_feature_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_feature_lang" do
    assert_difference('PsFeatureLang.count', -1) do
      delete ps_feature_lang_url(@ps_feature_lang), as: :json
    end

    assert_response 204
  end
end
