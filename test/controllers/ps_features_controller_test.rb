require 'test_helper'

class PsFeaturesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_feature = ps_feature(:one)
  end

  test "should get index" do
    get ps_features_url, as: :json
    assert_response :success
  end

  test "should create ps_feature" do
    assert_difference('PsFeature.count') do
      post ps_features_url, params: { ps_feature: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_feature" do
    get ps_feature_url(@ps_feature), as: :json
    assert_response :success
  end

  test "should update ps_feature" do
    patch ps_feature_url(@ps_feature), params: { ps_feature: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_feature" do
    assert_difference('PsFeature.count', -1) do
      delete ps_feature_url(@ps_feature), as: :json
    end

    assert_response 204
  end
end
