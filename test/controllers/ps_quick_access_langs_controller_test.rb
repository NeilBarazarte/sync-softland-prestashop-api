require 'test_helper'

class PsQuickAccessLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_quick_access_lang = ps_quick_access_lang(:one)
  end

  test "should get index" do
    get ps_quick_access_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_quick_access_lang" do
    assert_difference('PsQuickAccessLang.count') do
      post ps_quick_access_langs_url, params: { ps_quick_access_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_quick_access_lang" do
    get ps_quick_access_lang_url(@ps_quick_access_lang), as: :json
    assert_response :success
  end

  test "should update ps_quick_access_lang" do
    patch ps_quick_access_lang_url(@ps_quick_access_lang), params: { ps_quick_access_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_quick_access_lang" do
    assert_difference('PsQuickAccessLang.count', -1) do
      delete ps_quick_access_lang_url(@ps_quick_access_lang), as: :json
    end

    assert_response 204
  end
end
