require 'test_helper'

class PsHomesliderSlidesLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_homeslider_slides_lang = ps_homeslider_slides_lang(:one)
  end

  test "should get index" do
    get ps_homeslider_slides_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_homeslider_slides_lang" do
    assert_difference('PsHomesliderSlidesLang.count') do
      post ps_homeslider_slides_langs_url, params: { ps_homeslider_slides_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_homeslider_slides_lang" do
    get ps_homeslider_slides_lang_url(@ps_homeslider_slides_lang), as: :json
    assert_response :success
  end

  test "should update ps_homeslider_slides_lang" do
    patch ps_homeslider_slides_lang_url(@ps_homeslider_slides_lang), params: { ps_homeslider_slides_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_homeslider_slides_lang" do
    assert_difference('PsHomesliderSlidesLang.count', -1) do
      delete ps_homeslider_slides_lang_url(@ps_homeslider_slides_lang), as: :json
    end

    assert_response 204
  end
end
