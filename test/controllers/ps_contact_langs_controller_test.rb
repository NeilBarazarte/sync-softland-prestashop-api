require 'test_helper'

class PsContactLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_contact_lang = ps_contact_lang(:one)
  end

  test "should get index" do
    get ps_contact_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_contact_lang" do
    assert_difference('PsContactLang.count') do
      post ps_contact_langs_url, params: { ps_contact_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_contact_lang" do
    get ps_contact_lang_url(@ps_contact_lang), as: :json
    assert_response :success
  end

  test "should update ps_contact_lang" do
    patch ps_contact_lang_url(@ps_contact_lang), params: { ps_contact_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_contact_lang" do
    assert_difference('PsContactLang.count', -1) do
      delete ps_contact_lang_url(@ps_contact_lang), as: :json
    end

    assert_response 204
  end
end
