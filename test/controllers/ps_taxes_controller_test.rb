require 'test_helper'

class PsTaxesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tax = ps_tax(:one)
  end

  test "should get index" do
    get ps_taxes_url, as: :json
    assert_response :success
  end

  test "should create ps_tax" do
    assert_difference('PsTax.count') do
      post ps_taxes_url, params: { ps_tax: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tax" do
    get ps_tax_url(@ps_tax), as: :json
    assert_response :success
  end

  test "should update ps_tax" do
    patch ps_tax_url(@ps_tax), params: { ps_tax: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tax" do
    assert_difference('PsTax.count', -1) do
      delete ps_tax_url(@ps_tax), as: :json
    end

    assert_response 204
  end
end
