require 'test_helper'

class PsHomesliderSlidesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_homeslider_slide = ps_homeslider_slide(:one)
  end

  test "should get index" do
    get ps_homeslider_slides_url, as: :json
    assert_response :success
  end

  test "should create ps_homeslider_slide" do
    assert_difference('PsHomesliderSlide.count') do
      post ps_homeslider_slides_url, params: { ps_homeslider_slide: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_homeslider_slide" do
    get ps_homeslider_slide_url(@ps_homeslider_slide), as: :json
    assert_response :success
  end

  test "should update ps_homeslider_slide" do
    patch ps_homeslider_slide_url(@ps_homeslider_slide), params: { ps_homeslider_slide: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_homeslider_slide" do
    assert_difference('PsHomesliderSlide.count', -1) do
      delete ps_homeslider_slide_url(@ps_homeslider_slide), as: :json
    end

    assert_response 204
  end
end
