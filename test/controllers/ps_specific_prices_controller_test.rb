require 'test_helper'

class PsSpecificPricesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_specific_price = ps_specific_price(:one)
  end

  test "should get index" do
    get ps_specific_prices_url, as: :json
    assert_response :success
  end

  test "should create ps_specific_price" do
    assert_difference('PsSpecificPrice.count') do
      post ps_specific_prices_url, params: { ps_specific_price: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_specific_price" do
    get ps_specific_price_url(@ps_specific_price), as: :json
    assert_response :success
  end

  test "should update ps_specific_price" do
    patch ps_specific_price_url(@ps_specific_price), params: { ps_specific_price: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_specific_price" do
    assert_difference('PsSpecificPrice.count', -1) do
      delete ps_specific_price_url(@ps_specific_price), as: :json
    end

    assert_response 204
  end
end
