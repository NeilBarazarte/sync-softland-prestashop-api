require 'test_helper'

class PsTabLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tab_lang = ps_tab_lang(:one)
  end

  test "should get index" do
    get ps_tab_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_tab_lang" do
    assert_difference('PsTabLang.count') do
      post ps_tab_langs_url, params: { ps_tab_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tab_lang" do
    get ps_tab_lang_url(@ps_tab_lang), as: :json
    assert_response :success
  end

  test "should update ps_tab_lang" do
    patch ps_tab_lang_url(@ps_tab_lang), params: { ps_tab_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tab_lang" do
    assert_difference('PsTabLang.count', -1) do
      delete ps_tab_lang_url(@ps_tab_lang), as: :json
    end

    assert_response 204
  end
end
