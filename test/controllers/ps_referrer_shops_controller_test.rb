require 'test_helper'

class PsReferrerShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_referrer_shop = ps_referrer_shop(:one)
  end

  test "should get index" do
    get ps_referrer_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_referrer_shop" do
    assert_difference('PsReferrerShop.count') do
      post ps_referrer_shops_url, params: { ps_referrer_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_referrer_shop" do
    get ps_referrer_shop_url(@ps_referrer_shop), as: :json
    assert_response :success
  end

  test "should update ps_referrer_shop" do
    patch ps_referrer_shop_url(@ps_referrer_shop), params: { ps_referrer_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_referrer_shop" do
    assert_difference('PsReferrerShop.count', -1) do
      delete ps_referrer_shop_url(@ps_referrer_shop), as: :json
    end

    assert_response 204
  end
end
