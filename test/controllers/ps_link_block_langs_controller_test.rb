require 'test_helper'

class PsLinkBlockLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_link_block_lang = ps_link_block_lang(:one)
  end

  test "should get index" do
    get ps_link_block_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_link_block_lang" do
    assert_difference('PsLinkBlockLang.count') do
      post ps_link_block_langs_url, params: { ps_link_block_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_link_block_lang" do
    get ps_link_block_lang_url(@ps_link_block_lang), as: :json
    assert_response :success
  end

  test "should update ps_link_block_lang" do
    patch ps_link_block_lang_url(@ps_link_block_lang), params: { ps_link_block_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_link_block_lang" do
    assert_difference('PsLinkBlockLang.count', -1) do
      delete ps_link_block_lang_url(@ps_link_block_lang), as: :json
    end

    assert_response 204
  end
end
