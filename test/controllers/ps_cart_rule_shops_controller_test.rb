require 'test_helper'

class PsCartRuleShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_rule_shop = ps_cart_rule_shop(:one)
  end

  test "should get index" do
    get ps_cart_rule_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_rule_shop" do
    assert_difference('PsCartRuleShop.count') do
      post ps_cart_rule_shops_url, params: { ps_cart_rule_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_rule_shop" do
    get ps_cart_rule_shop_url(@ps_cart_rule_shop), as: :json
    assert_response :success
  end

  test "should update ps_cart_rule_shop" do
    patch ps_cart_rule_shop_url(@ps_cart_rule_shop), params: { ps_cart_rule_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_rule_shop" do
    assert_difference('PsCartRuleShop.count', -1) do
      delete ps_cart_rule_shop_url(@ps_cart_rule_shop), as: :json
    end

    assert_response 204
  end
end
