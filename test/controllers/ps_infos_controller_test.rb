require 'test_helper'

class PsInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_info = ps_info(:one)
  end

  test "should get index" do
    get ps_infos_url, as: :json
    assert_response :success
  end

  test "should create ps_info" do
    assert_difference('PsInfo.count') do
      post ps_infos_url, params: { ps_info: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_info" do
    get ps_info_url(@ps_info), as: :json
    assert_response :success
  end

  test "should update ps_info" do
    patch ps_info_url(@ps_info), params: { ps_info: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_info" do
    assert_difference('PsInfo.count', -1) do
      delete ps_info_url(@ps_info), as: :json
    end

    assert_response 204
  end
end
