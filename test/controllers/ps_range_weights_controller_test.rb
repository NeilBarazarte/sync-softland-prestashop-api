require 'test_helper'

class PsRangeWeightsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_range_weight = ps_range_weight(:one)
  end

  test "should get index" do
    get ps_range_weights_url, as: :json
    assert_response :success
  end

  test "should create ps_range_weight" do
    assert_difference('PsRangeWeight.count') do
      post ps_range_weights_url, params: { ps_range_weight: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_range_weight" do
    get ps_range_weight_url(@ps_range_weight), as: :json
    assert_response :success
  end

  test "should update ps_range_weight" do
    patch ps_range_weight_url(@ps_range_weight), params: { ps_range_weight: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_range_weight" do
    assert_difference('PsRangeWeight.count', -1) do
      delete ps_range_weight_url(@ps_range_weight), as: :json
    end

    assert_response 204
  end
end
