require 'test_helper'

class PsHookModulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_hook_module = ps_hook_module(:one)
  end

  test "should get index" do
    get ps_hook_modules_url, as: :json
    assert_response :success
  end

  test "should create ps_hook_module" do
    assert_difference('PsHookModule.count') do
      post ps_hook_modules_url, params: { ps_hook_module: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_hook_module" do
    get ps_hook_module_url(@ps_hook_module), as: :json
    assert_response :success
  end

  test "should update ps_hook_module" do
    patch ps_hook_module_url(@ps_hook_module), params: { ps_hook_module: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_hook_module" do
    assert_difference('PsHookModule.count', -1) do
      delete ps_hook_module_url(@ps_hook_module), as: :json
    end

    assert_response 204
  end
end
