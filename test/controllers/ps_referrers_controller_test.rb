require 'test_helper'

class PsReferrersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_referrer = ps_referrer(:one)
  end

  test "should get index" do
    get ps_referrers_url, as: :json
    assert_response :success
  end

  test "should create ps_referrer" do
    assert_difference('PsReferrer.count') do
      post ps_referrers_url, params: { ps_referrer: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_referrer" do
    get ps_referrer_url(@ps_referrer), as: :json
    assert_response :success
  end

  test "should update ps_referrer" do
    patch ps_referrer_url(@ps_referrer), params: { ps_referrer: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_referrer" do
    assert_difference('PsReferrer.count', -1) do
      delete ps_referrer_url(@ps_referrer), as: :json
    end

    assert_response 204
  end
end
