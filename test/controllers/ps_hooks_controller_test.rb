require 'test_helper'

class PsHooksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_hook = ps_hook(:one)
  end

  test "should get index" do
    get ps_hooks_url, as: :json
    assert_response :success
  end

  test "should create ps_hook" do
    assert_difference('PsHook.count') do
      post ps_hooks_url, params: { ps_hook: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_hook" do
    get ps_hook_url(@ps_hook), as: :json
    assert_response :success
  end

  test "should update ps_hook" do
    patch ps_hook_url(@ps_hook), params: { ps_hook: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_hook" do
    assert_difference('PsHook.count', -1) do
      delete ps_hook_url(@ps_hook), as: :json
    end

    assert_response 204
  end
end
