require 'test_helper'

class PsMailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_mail = ps_mail(:one)
  end

  test "should get index" do
    get ps_mails_url, as: :json
    assert_response :success
  end

  test "should create ps_mail" do
    assert_difference('PsMail.count') do
      post ps_mails_url, params: { ps_mail: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_mail" do
    get ps_mail_url(@ps_mail), as: :json
    assert_response :success
  end

  test "should update ps_mail" do
    patch ps_mail_url(@ps_mail), params: { ps_mail: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_mail" do
    assert_difference('PsMail.count', -1) do
      delete ps_mail_url(@ps_mail), as: :json
    end

    assert_response 204
  end
end
