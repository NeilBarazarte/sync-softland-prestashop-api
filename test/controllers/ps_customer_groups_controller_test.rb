require 'test_helper'

class PsCustomerGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_customer_group = ps_customer_group(:one)
  end

  test "should get index" do
    get ps_customer_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_customer_group" do
    assert_difference('PsCustomerGroup.count') do
      post ps_customer_groups_url, params: { ps_customer_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_customer_group" do
    get ps_customer_group_url(@ps_customer_group), as: :json
    assert_response :success
  end

  test "should update ps_customer_group" do
    patch ps_customer_group_url(@ps_customer_group), params: { ps_customer_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_customer_group" do
    assert_difference('PsCustomerGroup.count', -1) do
      delete ps_customer_group_url(@ps_customer_group), as: :json
    end

    assert_response 204
  end
end
