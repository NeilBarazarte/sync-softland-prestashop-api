require 'test_helper'

class PsOperatingSystemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_operating_system = ps_operating_system(:one)
  end

  test "should get index" do
    get ps_operating_systems_url, as: :json
    assert_response :success
  end

  test "should create ps_operating_system" do
    assert_difference('PsOperatingSystem.count') do
      post ps_operating_systems_url, params: { ps_operating_system: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_operating_system" do
    get ps_operating_system_url(@ps_operating_system), as: :json
    assert_response :success
  end

  test "should update ps_operating_system" do
    patch ps_operating_system_url(@ps_operating_system), params: { ps_operating_system: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_operating_system" do
    assert_difference('PsOperatingSystem.count', -1) do
      delete ps_operating_system_url(@ps_operating_system), as: :json
    end

    assert_response 204
  end
end
