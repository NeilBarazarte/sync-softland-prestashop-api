require 'test_helper'

class PsShopGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_shop_group = ps_shop_group(:one)
  end

  test "should get index" do
    get ps_shop_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_shop_group" do
    assert_difference('PsShopGroup.count') do
      post ps_shop_groups_url, params: { ps_shop_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_shop_group" do
    get ps_shop_group_url(@ps_shop_group), as: :json
    assert_response :success
  end

  test "should update ps_shop_group" do
    patch ps_shop_group_url(@ps_shop_group), params: { ps_shop_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_shop_group" do
    assert_difference('PsShopGroup.count', -1) do
      delete ps_shop_group_url(@ps_shop_group), as: :json
    end

    assert_response 204
  end
end
