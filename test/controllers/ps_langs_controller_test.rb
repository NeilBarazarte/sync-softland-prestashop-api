require 'test_helper'

class PsLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_lang = ps_lang(:one)
  end

  test "should get index" do
    get ps_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_lang" do
    assert_difference('PsLang.count') do
      post ps_langs_url, params: { ps_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_lang" do
    get ps_lang_url(@ps_lang), as: :json
    assert_response :success
  end

  test "should update ps_lang" do
    patch ps_lang_url(@ps_lang), params: { ps_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_lang" do
    assert_difference('PsLang.count', -1) do
      delete ps_lang_url(@ps_lang), as: :json
    end

    assert_response 204
  end
end
