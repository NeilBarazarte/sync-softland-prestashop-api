require 'test_helper'

class PsCartRuleProductRuleValuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_rule_product_rule_value = ps_cart_rule_product_rule_value(:one)
  end

  test "should get index" do
    get ps_cart_rule_product_rule_values_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_rule_product_rule_value" do
    assert_difference('PsCartRuleProductRuleValue.count') do
      post ps_cart_rule_product_rule_values_url, params: { ps_cart_rule_product_rule_value: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_rule_product_rule_value" do
    get ps_cart_rule_product_rule_value_url(@ps_cart_rule_product_rule_value), as: :json
    assert_response :success
  end

  test "should update ps_cart_rule_product_rule_value" do
    patch ps_cart_rule_product_rule_value_url(@ps_cart_rule_product_rule_value), params: { ps_cart_rule_product_rule_value: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_rule_product_rule_value" do
    assert_difference('PsCartRuleProductRuleValue.count', -1) do
      delete ps_cart_rule_product_rule_value_url(@ps_cart_rule_product_rule_value), as: :json
    end

    assert_response 204
  end
end
