require 'test_helper'

class PsCustomizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_customization = ps_customization(:one)
  end

  test "should get index" do
    get ps_customizations_url, as: :json
    assert_response :success
  end

  test "should create ps_customization" do
    assert_difference('PsCustomization.count') do
      post ps_customizations_url, params: { ps_customization: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_customization" do
    get ps_customization_url(@ps_customization), as: :json
    assert_response :success
  end

  test "should update ps_customization" do
    patch ps_customization_url(@ps_customization), params: { ps_customization: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_customization" do
    assert_difference('PsCustomization.count', -1) do
      delete ps_customization_url(@ps_customization), as: :json
    end

    assert_response 204
  end
end
