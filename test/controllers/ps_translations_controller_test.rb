require 'test_helper'

class PsTranslationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_translation = ps_translation(:one)
  end

  test "should get index" do
    get ps_translations_url, as: :json
    assert_response :success
  end

  test "should create ps_translation" do
    assert_difference('PsTranslation.count') do
      post ps_translations_url, params: { ps_translation: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_translation" do
    get ps_translation_url(@ps_translation), as: :json
    assert_response :success
  end

  test "should update ps_translation" do
    patch ps_translation_url(@ps_translation), params: { ps_translation: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_translation" do
    assert_difference('PsTranslation.count', -1) do
      delete ps_translation_url(@ps_translation), as: :json
    end

    assert_response 204
  end
end
