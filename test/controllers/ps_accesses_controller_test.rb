require 'test_helper'

class PsAccessesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_access = ps_access(:one)
  end

  test "should get index" do
    get ps_accesses_url, as: :json
    assert_response :success
  end

  test "should create ps_access" do
    assert_difference('PsAccess.count') do
      post ps_accesses_url, params: { ps_access: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_access" do
    get ps_access_url(@ps_access), as: :json
    assert_response :success
  end

  test "should update ps_access" do
    patch ps_access_url(@ps_access), params: { ps_access: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_access" do
    assert_difference('PsAccess.count', -1) do
      delete ps_access_url(@ps_access), as: :json
    end

    assert_response 204
  end
end
