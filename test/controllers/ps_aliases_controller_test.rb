require 'test_helper'

class PsAliasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_alias = ps_alias(:one)
  end

  test "should get index" do
    get ps_aliases_url, as: :json
    assert_response :success
  end

  test "should create ps_alias" do
    assert_difference('PsAlias.count') do
      post ps_aliases_url, params: { ps_alias: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_alias" do
    get ps_alias_url(@ps_alias), as: :json
    assert_response :success
  end

  test "should update ps_alias" do
    patch ps_alias_url(@ps_alias), params: { ps_alias: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_alias" do
    assert_difference('PsAlias.count', -1) do
      delete ps_alias_url(@ps_alias), as: :json
    end

    assert_response 204
  end
end
