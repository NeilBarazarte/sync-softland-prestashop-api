require 'test_helper'

class PsCartRuleCountriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_rule_country = ps_cart_rule_country(:one)
  end

  test "should get index" do
    get ps_cart_rule_countries_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_rule_country" do
    assert_difference('PsCartRuleCountry.count') do
      post ps_cart_rule_countries_url, params: { ps_cart_rule_country: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_rule_country" do
    get ps_cart_rule_country_url(@ps_cart_rule_country), as: :json
    assert_response :success
  end

  test "should update ps_cart_rule_country" do
    patch ps_cart_rule_country_url(@ps_cart_rule_country), params: { ps_cart_rule_country: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_rule_country" do
    assert_difference('PsCartRuleCountry.count', -1) do
      delete ps_cart_rule_country_url(@ps_cart_rule_country), as: :json
    end

    assert_response 204
  end
end
