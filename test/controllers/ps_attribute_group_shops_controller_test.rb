require 'test_helper'

class PsAttributeGroupShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_attribute_group_shop = ps_attribute_group_shop(:one)
  end

  test "should get index" do
    get ps_attribute_group_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_attribute_group_shop" do
    assert_difference('PsAttributeGroupShop.count') do
      post ps_attribute_group_shops_url, params: { ps_attribute_group_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_attribute_group_shop" do
    get ps_attribute_group_shop_url(@ps_attribute_group_shop), as: :json
    assert_response :success
  end

  test "should update ps_attribute_group_shop" do
    patch ps_attribute_group_shop_url(@ps_attribute_group_shop), params: { ps_attribute_group_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_attribute_group_shop" do
    assert_difference('PsAttributeGroupShop.count', -1) do
      delete ps_attribute_group_shop_url(@ps_attribute_group_shop), as: :json
    end

    assert_response 204
  end
end
