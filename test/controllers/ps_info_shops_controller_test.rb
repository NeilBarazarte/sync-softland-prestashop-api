require 'test_helper'

class PsInfoShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_info_shop = ps_info_shop(:one)
  end

  test "should get index" do
    get ps_info_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_info_shop" do
    assert_difference('PsInfoShop.count') do
      post ps_info_shops_url, params: { ps_info_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_info_shop" do
    get ps_info_shop_url(@ps_info_shop), as: :json
    assert_response :success
  end

  test "should update ps_info_shop" do
    patch ps_info_shop_url(@ps_info_shop), params: { ps_info_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_info_shop" do
    assert_difference('PsInfoShop.count', -1) do
      delete ps_info_shop_url(@ps_info_shop), as: :json
    end

    assert_response 204
  end
end
