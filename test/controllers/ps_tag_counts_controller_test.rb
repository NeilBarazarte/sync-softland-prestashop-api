require 'test_helper'

class PsTagCountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tag_count = ps_tag_count(:one)
  end

  test "should get index" do
    get ps_tag_counts_url, as: :json
    assert_response :success
  end

  test "should create ps_tag_count" do
    assert_difference('PsTagCount.count') do
      post ps_tag_counts_url, params: { ps_tag_count: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tag_count" do
    get ps_tag_count_url(@ps_tag_count), as: :json
    assert_response :success
  end

  test "should update ps_tag_count" do
    patch ps_tag_count_url(@ps_tag_count), params: { ps_tag_count: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tag_count" do
    assert_difference('PsTagCount.count', -1) do
      delete ps_tag_count_url(@ps_tag_count), as: :json
    end

    assert_response 204
  end
end
