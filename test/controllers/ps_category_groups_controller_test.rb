require 'test_helper'

class PsCategoryGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_category_group = ps_category_group(:one)
  end

  test "should get index" do
    get ps_category_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_category_group" do
    assert_difference('PsCategoryGroup.count') do
      post ps_category_groups_url, params: { ps_category_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_category_group" do
    get ps_category_group_url(@ps_category_group), as: :json
    assert_response :success
  end

  test "should update ps_category_group" do
    patch ps_category_group_url(@ps_category_group), params: { ps_category_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_category_group" do
    assert_difference('PsCategoryGroup.count', -1) do
      delete ps_category_group_url(@ps_category_group), as: :json
    end

    assert_response 204
  end
end
