require 'test_helper'

class PsReassuranceLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_reassurance_lang = ps_reassurance_lang(:one)
  end

  test "should get index" do
    get ps_reassurance_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_reassurance_lang" do
    assert_difference('PsReassuranceLang.count') do
      post ps_reassurance_langs_url, params: { ps_reassurance_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_reassurance_lang" do
    get ps_reassurance_lang_url(@ps_reassurance_lang), as: :json
    assert_response :success
  end

  test "should update ps_reassurance_lang" do
    patch ps_reassurance_lang_url(@ps_reassurance_lang), params: { ps_reassurance_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_reassurance_lang" do
    assert_difference('PsReassuranceLang.count', -1) do
      delete ps_reassurance_lang_url(@ps_reassurance_lang), as: :json
    end

    assert_response 204
  end
end
