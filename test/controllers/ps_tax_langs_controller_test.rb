require 'test_helper'

class PsTaxLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tax_lang = ps_tax_lang(:one)
  end

  test "should get index" do
    get ps_tax_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_tax_lang" do
    assert_difference('PsTaxLang.count') do
      post ps_tax_langs_url, params: { ps_tax_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tax_lang" do
    get ps_tax_lang_url(@ps_tax_lang), as: :json
    assert_response :success
  end

  test "should update ps_tax_lang" do
    patch ps_tax_lang_url(@ps_tax_lang), params: { ps_tax_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tax_lang" do
    assert_difference('PsTaxLang.count', -1) do
      delete ps_tax_lang_url(@ps_tax_lang), as: :json
    end

    assert_response 204
  end
end
