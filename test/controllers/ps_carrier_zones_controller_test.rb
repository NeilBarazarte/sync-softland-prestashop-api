require 'test_helper'

class PsCarrierZonesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_carrier_zone = ps_carrier_zone(:one)
  end

  test "should get index" do
    get ps_carrier_zones_url, as: :json
    assert_response :success
  end

  test "should create ps_carrier_zone" do
    assert_difference('PsCarrierZone.count') do
      post ps_carrier_zones_url, params: { ps_carrier_zone: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_carrier_zone" do
    get ps_carrier_zone_url(@ps_carrier_zone), as: :json
    assert_response :success
  end

  test "should update ps_carrier_zone" do
    patch ps_carrier_zone_url(@ps_carrier_zone), params: { ps_carrier_zone: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_carrier_zone" do
    assert_difference('PsCarrierZone.count', -1) do
      delete ps_carrier_zone_url(@ps_carrier_zone), as: :json
    end

    assert_response 204
  end
end
