require 'test_helper'

class PsBadgesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_badge = ps_badge(:one)
  end

  test "should get index" do
    get ps_badges_url, as: :json
    assert_response :success
  end

  test "should create ps_badge" do
    assert_difference('PsBadge.count') do
      post ps_badges_url, params: { ps_badge: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_badge" do
    get ps_badge_url(@ps_badge), as: :json
    assert_response :success
  end

  test "should update ps_badge" do
    patch ps_badge_url(@ps_badge), params: { ps_badge: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_badge" do
    assert_difference('PsBadge.count', -1) do
      delete ps_badge_url(@ps_badge), as: :json
    end

    assert_response 204
  end
end
