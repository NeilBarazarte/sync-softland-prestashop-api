require 'test_helper'

class PsSupplierLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_supplier_lang = ps_supplier_lang(:one)
  end

  test "should get index" do
    get ps_supplier_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_supplier_lang" do
    assert_difference('PsSupplierLang.count') do
      post ps_supplier_langs_url, params: { ps_supplier_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_supplier_lang" do
    get ps_supplier_lang_url(@ps_supplier_lang), as: :json
    assert_response :success
  end

  test "should update ps_supplier_lang" do
    patch ps_supplier_lang_url(@ps_supplier_lang), params: { ps_supplier_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_supplier_lang" do
    assert_difference('PsSupplierLang.count', -1) do
      delete ps_supplier_lang_url(@ps_supplier_lang), as: :json
    end

    assert_response 204
  end
end
