require 'test_helper'

class PsQuickAccessesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_quick_access = ps_quick_access(:one)
  end

  test "should get index" do
    get ps_quick_accesses_url, as: :json
    assert_response :success
  end

  test "should create ps_quick_access" do
    assert_difference('PsQuickAccess.count') do
      post ps_quick_accesses_url, params: { ps_quick_access: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_quick_access" do
    get ps_quick_access_url(@ps_quick_access), as: :json
    assert_response :success
  end

  test "should update ps_quick_access" do
    patch ps_quick_access_url(@ps_quick_access), params: { ps_quick_access: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_quick_access" do
    assert_difference('PsQuickAccess.count', -1) do
      delete ps_quick_access_url(@ps_quick_access), as: :json
    end

    assert_response 204
  end
end
