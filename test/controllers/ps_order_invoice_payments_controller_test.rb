require 'test_helper'

class PsOrderInvoicePaymentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_invoice_payment = ps_order_invoice_payment(:one)
  end

  test "should get index" do
    get ps_order_invoice_payments_url, as: :json
    assert_response :success
  end

  test "should create ps_order_invoice_payment" do
    assert_difference('PsOrderInvoicePayment.count') do
      post ps_order_invoice_payments_url, params: { ps_order_invoice_payment: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_invoice_payment" do
    get ps_order_invoice_payment_url(@ps_order_invoice_payment), as: :json
    assert_response :success
  end

  test "should update ps_order_invoice_payment" do
    patch ps_order_invoice_payment_url(@ps_order_invoice_payment), params: { ps_order_invoice_payment: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_invoice_payment" do
    assert_difference('PsOrderInvoicePayment.count', -1) do
      delete ps_order_invoice_payment_url(@ps_order_invoice_payment), as: :json
    end

    assert_response 204
  end
end
