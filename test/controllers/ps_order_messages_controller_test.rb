require 'test_helper'

class PsOrderMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_message = ps_order_message(:one)
  end

  test "should get index" do
    get ps_order_messages_url, as: :json
    assert_response :success
  end

  test "should create ps_order_message" do
    assert_difference('PsOrderMessage.count') do
      post ps_order_messages_url, params: { ps_order_message: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_message" do
    get ps_order_message_url(@ps_order_message), as: :json
    assert_response :success
  end

  test "should update ps_order_message" do
    patch ps_order_message_url(@ps_order_message), params: { ps_order_message: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_message" do
    assert_difference('PsOrderMessage.count', -1) do
      delete ps_order_message_url(@ps_order_message), as: :json
    end

    assert_response 204
  end
end
