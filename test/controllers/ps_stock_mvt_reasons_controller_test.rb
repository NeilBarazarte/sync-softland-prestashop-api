require 'test_helper'

class PsStockMvtReasonsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_stock_mvt_reason = ps_stock_mvt_reason(:one)
  end

  test "should get index" do
    get ps_stock_mvt_reasons_url, as: :json
    assert_response :success
  end

  test "should create ps_stock_mvt_reason" do
    assert_difference('PsStockMvtReason.count') do
      post ps_stock_mvt_reasons_url, params: { ps_stock_mvt_reason: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_stock_mvt_reason" do
    get ps_stock_mvt_reason_url(@ps_stock_mvt_reason), as: :json
    assert_response :success
  end

  test "should update ps_stock_mvt_reason" do
    patch ps_stock_mvt_reason_url(@ps_stock_mvt_reason), params: { ps_stock_mvt_reason: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_stock_mvt_reason" do
    assert_difference('PsStockMvtReason.count', -1) do
      delete ps_stock_mvt_reason_url(@ps_stock_mvt_reason), as: :json
    end

    assert_response 204
  end
end
