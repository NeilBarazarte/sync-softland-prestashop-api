require 'test_helper'

class PsSuppliersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_supplier = ps_supplier(:one)
  end

  test "should get index" do
    get ps_suppliers_url, as: :json
    assert_response :success
  end

  test "should create ps_supplier" do
    assert_difference('PsSupplier.count') do
      post ps_suppliers_url, params: { ps_supplier: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_supplier" do
    get ps_supplier_url(@ps_supplier), as: :json
    assert_response :success
  end

  test "should update ps_supplier" do
    patch ps_supplier_url(@ps_supplier), params: { ps_supplier: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_supplier" do
    assert_difference('PsSupplier.count', -1) do
      delete ps_supplier_url(@ps_supplier), as: :json
    end

    assert_response 204
  end
end
