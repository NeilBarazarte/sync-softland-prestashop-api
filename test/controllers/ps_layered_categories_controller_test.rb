require 'test_helper'

class PsLayeredCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_layered_category = ps_layered_category(:one)
  end

  test "should get index" do
    get ps_layered_categories_url, as: :json
    assert_response :success
  end

  test "should create ps_layered_category" do
    assert_difference('PsLayeredCategory.count') do
      post ps_layered_categories_url, params: { ps_layered_category: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_layered_category" do
    get ps_layered_category_url(@ps_layered_category), as: :json
    assert_response :success
  end

  test "should update ps_layered_category" do
    patch ps_layered_category_url(@ps_layered_category), params: { ps_layered_category: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_layered_category" do
    assert_difference('PsLayeredCategory.count', -1) do
      delete ps_layered_category_url(@ps_layered_category), as: :json
    end

    assert_response 204
  end
end
