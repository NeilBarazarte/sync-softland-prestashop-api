require 'test_helper'

class PsProductSalesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_sale = ps_product_sale(:one)
  end

  test "should get index" do
    get ps_product_sales_url, as: :json
    assert_response :success
  end

  test "should create ps_product_sale" do
    assert_difference('PsProductSale.count') do
      post ps_product_sales_url, params: { ps_product_sale: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_sale" do
    get ps_product_sale_url(@ps_product_sale), as: :json
    assert_response :success
  end

  test "should update ps_product_sale" do
    patch ps_product_sale_url(@ps_product_sale), params: { ps_product_sale: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_sale" do
    assert_difference('PsProductSale.count', -1) do
      delete ps_product_sale_url(@ps_product_sale), as: :json
    end

    assert_response 204
  end
end
