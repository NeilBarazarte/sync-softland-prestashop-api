require 'test_helper'

class PsModuleHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_module_history = ps_module_history(:one)
  end

  test "should get index" do
    get ps_module_histories_url, as: :json
    assert_response :success
  end

  test "should create ps_module_history" do
    assert_difference('PsModuleHistory.count') do
      post ps_module_histories_url, params: { ps_module_history: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_module_history" do
    get ps_module_history_url(@ps_module_history), as: :json
    assert_response :success
  end

  test "should update ps_module_history" do
    patch ps_module_history_url(@ps_module_history), params: { ps_module_history: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_module_history" do
    assert_difference('PsModuleHistory.count', -1) do
      delete ps_module_history_url(@ps_module_history), as: :json
    end

    assert_response 204
  end
end
