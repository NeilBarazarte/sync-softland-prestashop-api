require 'test_helper'

class PsOrderInvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_invoice = ps_order_invoice(:one)
  end

  test "should get index" do
    get ps_order_invoices_url, as: :json
    assert_response :success
  end

  test "should create ps_order_invoice" do
    assert_difference('PsOrderInvoice.count') do
      post ps_order_invoices_url, params: { ps_order_invoice: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_invoice" do
    get ps_order_invoice_url(@ps_order_invoice), as: :json
    assert_response :success
  end

  test "should update ps_order_invoice" do
    patch ps_order_invoice_url(@ps_order_invoice), params: { ps_order_invoice: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_invoice" do
    assert_difference('PsOrderInvoice.count', -1) do
      delete ps_order_invoice_url(@ps_order_invoice), as: :json
    end

    assert_response 204
  end
end
