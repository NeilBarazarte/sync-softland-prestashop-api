require 'test_helper'

class PsHookAliasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_hook_alias = ps_hook_alias(:one)
  end

  test "should get index" do
    get ps_hook_aliases_url, as: :json
    assert_response :success
  end

  test "should create ps_hook_alias" do
    assert_difference('PsHookAlias.count') do
      post ps_hook_aliases_url, params: { ps_hook_alias: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_hook_alias" do
    get ps_hook_alias_url(@ps_hook_alias), as: :json
    assert_response :success
  end

  test "should update ps_hook_alias" do
    patch ps_hook_alias_url(@ps_hook_alias), params: { ps_hook_alias: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_hook_alias" do
    assert_difference('PsHookAlias.count', -1) do
      delete ps_hook_alias_url(@ps_hook_alias), as: :json
    end

    assert_response 204
  end
end
