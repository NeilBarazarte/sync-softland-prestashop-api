require 'test_helper'

class PsProductGroupReductionCachesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_group_reduction_cache = ps_product_group_reduction_cache(:one)
  end

  test "should get index" do
    get ps_product_group_reduction_caches_url, as: :json
    assert_response :success
  end

  test "should create ps_product_group_reduction_cache" do
    assert_difference('PsProductGroupReductionCache.count') do
      post ps_product_group_reduction_caches_url, params: { ps_product_group_reduction_cache: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_group_reduction_cache" do
    get ps_product_group_reduction_cache_url(@ps_product_group_reduction_cache), as: :json
    assert_response :success
  end

  test "should update ps_product_group_reduction_cache" do
    patch ps_product_group_reduction_cache_url(@ps_product_group_reduction_cache), params: { ps_product_group_reduction_cache: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_group_reduction_cache" do
    assert_difference('PsProductGroupReductionCache.count', -1) do
      delete ps_product_group_reduction_cache_url(@ps_product_group_reduction_cache), as: :json
    end

    assert_response 204
  end
end
