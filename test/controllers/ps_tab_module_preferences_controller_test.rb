require 'test_helper'

class PsTabModulePreferencesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tab_module_preference = ps_tab_module_preference(:one)
  end

  test "should get index" do
    get ps_tab_module_preferences_url, as: :json
    assert_response :success
  end

  test "should create ps_tab_module_preference" do
    assert_difference('PsTabModulePreference.count') do
      post ps_tab_module_preferences_url, params: { ps_tab_module_preference: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tab_module_preference" do
    get ps_tab_module_preference_url(@ps_tab_module_preference), as: :json
    assert_response :success
  end

  test "should update ps_tab_module_preference" do
    patch ps_tab_module_preference_url(@ps_tab_module_preference), params: { ps_tab_module_preference: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tab_module_preference" do
    assert_difference('PsTabModulePreference.count', -1) do
      delete ps_tab_module_preference_url(@ps_tab_module_preference), as: :json
    end

    assert_response 204
  end
end
