require 'test_helper'

class PsFeatureProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_feature_product = ps_feature_product(:one)
  end

  test "should get index" do
    get ps_feature_products_url, as: :json
    assert_response :success
  end

  test "should create ps_feature_product" do
    assert_difference('PsFeatureProduct.count') do
      post ps_feature_products_url, params: { ps_feature_product: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_feature_product" do
    get ps_feature_product_url(@ps_feature_product), as: :json
    assert_response :success
  end

  test "should update ps_feature_product" do
    patch ps_feature_product_url(@ps_feature_product), params: { ps_feature_product: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_feature_product" do
    assert_difference('PsFeatureProduct.count', -1) do
      delete ps_feature_product_url(@ps_feature_product), as: :json
    end

    assert_response 204
  end
end
