require 'test_helper'

class PsSupplyOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_supply_order = ps_supply_order(:one)
  end

  test "should get index" do
    get ps_supply_orders_url, as: :json
    assert_response :success
  end

  test "should create ps_supply_order" do
    assert_difference('PsSupplyOrder.count') do
      post ps_supply_orders_url, params: { ps_supply_order: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_supply_order" do
    get ps_supply_order_url(@ps_supply_order), as: :json
    assert_response :success
  end

  test "should update ps_supply_order" do
    patch ps_supply_order_url(@ps_supply_order), params: { ps_supply_order: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_supply_order" do
    assert_difference('PsSupplyOrder.count', -1) do
      delete ps_supply_order_url(@ps_supply_order), as: :json
    end

    assert_response 204
  end
end
