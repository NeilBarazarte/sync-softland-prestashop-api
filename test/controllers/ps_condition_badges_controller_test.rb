require 'test_helper'

class PsConditionBadgesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_condition_badge = ps_condition_badge(:one)
  end

  test "should get index" do
    get ps_condition_badges_url, as: :json
    assert_response :success
  end

  test "should create ps_condition_badge" do
    assert_difference('PsConditionBadge.count') do
      post ps_condition_badges_url, params: { ps_condition_badge: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_condition_badge" do
    get ps_condition_badge_url(@ps_condition_badge), as: :json
    assert_response :success
  end

  test "should update ps_condition_badge" do
    patch ps_condition_badge_url(@ps_condition_badge), params: { ps_condition_badge: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_condition_badge" do
    assert_difference('PsConditionBadge.count', -1) do
      delete ps_condition_badge_url(@ps_condition_badge), as: :json
    end

    assert_response 204
  end
end
