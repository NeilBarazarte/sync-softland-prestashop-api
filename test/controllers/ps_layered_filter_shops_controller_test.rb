require 'test_helper'

class PsLayeredFilterShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_layered_filter_shop = ps_layered_filter_shop(:one)
  end

  test "should get index" do
    get ps_layered_filter_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_layered_filter_shop" do
    assert_difference('PsLayeredFilterShop.count') do
      post ps_layered_filter_shops_url, params: { ps_layered_filter_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_layered_filter_shop" do
    get ps_layered_filter_shop_url(@ps_layered_filter_shop), as: :json
    assert_response :success
  end

  test "should update ps_layered_filter_shop" do
    patch ps_layered_filter_shop_url(@ps_layered_filter_shop), params: { ps_layered_filter_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_layered_filter_shop" do
    assert_difference('PsLayeredFilterShop.count', -1) do
      delete ps_layered_filter_shop_url(@ps_layered_filter_shop), as: :json
    end

    assert_response 204
  end
end
