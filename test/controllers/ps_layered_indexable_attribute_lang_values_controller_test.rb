require 'test_helper'

class PsLayeredIndexableAttributeLangValuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_layered_indexable_attribute_lang_value = ps_layered_indexable_attribute_lang_value(:one)
  end

  test "should get index" do
    get ps_layered_indexable_attribute_lang_values_url, as: :json
    assert_response :success
  end

  test "should create ps_layered_indexable_attribute_lang_value" do
    assert_difference('PsLayeredIndexableAttributeLangValue.count') do
      post ps_layered_indexable_attribute_lang_values_url, params: { ps_layered_indexable_attribute_lang_value: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_layered_indexable_attribute_lang_value" do
    get ps_layered_indexable_attribute_lang_value_url(@ps_layered_indexable_attribute_lang_value), as: :json
    assert_response :success
  end

  test "should update ps_layered_indexable_attribute_lang_value" do
    patch ps_layered_indexable_attribute_lang_value_url(@ps_layered_indexable_attribute_lang_value), params: { ps_layered_indexable_attribute_lang_value: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_layered_indexable_attribute_lang_value" do
    assert_difference('PsLayeredIndexableAttributeLangValue.count', -1) do
      delete ps_layered_indexable_attribute_lang_value_url(@ps_layered_indexable_attribute_lang_value), as: :json
    end

    assert_response 204
  end
end
