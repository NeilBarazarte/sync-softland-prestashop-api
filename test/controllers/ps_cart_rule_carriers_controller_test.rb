require 'test_helper'

class PsCartRuleCarriersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_rule_carrier = ps_cart_rule_carrier(:one)
  end

  test "should get index" do
    get ps_cart_rule_carriers_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_rule_carrier" do
    assert_difference('PsCartRuleCarrier.count') do
      post ps_cart_rule_carriers_url, params: { ps_cart_rule_carrier: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_rule_carrier" do
    get ps_cart_rule_carrier_url(@ps_cart_rule_carrier), as: :json
    assert_response :success
  end

  test "should update ps_cart_rule_carrier" do
    patch ps_cart_rule_carrier_url(@ps_cart_rule_carrier), params: { ps_cart_rule_carrier: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_rule_carrier" do
    assert_difference('PsCartRuleCarrier.count', -1) do
      delete ps_cart_rule_carrier_url(@ps_cart_rule_carrier), as: :json
    end

    assert_response 204
  end
end
