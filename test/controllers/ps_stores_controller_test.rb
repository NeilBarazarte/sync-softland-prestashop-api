require 'test_helper'

class PsStoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_store = ps_store(:one)
  end

  test "should get index" do
    get ps_stores_url, as: :json
    assert_response :success
  end

  test "should create ps_store" do
    assert_difference('PsStore.count') do
      post ps_stores_url, params: { ps_store: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_store" do
    get ps_store_url(@ps_store), as: :json
    assert_response :success
  end

  test "should update ps_store" do
    patch ps_store_url(@ps_store), params: { ps_store: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_store" do
    assert_difference('PsStore.count', -1) do
      delete ps_store_url(@ps_store), as: :json
    end

    assert_response 204
  end
end
