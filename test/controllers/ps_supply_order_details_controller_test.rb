require 'test_helper'

class PsSupplyOrderDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_supply_order_detail = ps_supply_order_detail(:one)
  end

  test "should get index" do
    get ps_supply_order_details_url, as: :json
    assert_response :success
  end

  test "should create ps_supply_order_detail" do
    assert_difference('PsSupplyOrderDetail.count') do
      post ps_supply_order_details_url, params: { ps_supply_order_detail: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_supply_order_detail" do
    get ps_supply_order_detail_url(@ps_supply_order_detail), as: :json
    assert_response :success
  end

  test "should update ps_supply_order_detail" do
    patch ps_supply_order_detail_url(@ps_supply_order_detail), params: { ps_supply_order_detail: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_supply_order_detail" do
    assert_difference('PsSupplyOrderDetail.count', -1) do
      delete ps_supply_order_detail_url(@ps_supply_order_detail), as: :json
    end

    assert_response 204
  end
end
