require 'test_helper'

class PsSearchEnginesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_search_engine = ps_search_engine(:one)
  end

  test "should get index" do
    get ps_search_engines_url, as: :json
    assert_response :success
  end

  test "should create ps_search_engine" do
    assert_difference('PsSearchEngine.count') do
      post ps_search_engines_url, params: { ps_search_engine: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_search_engine" do
    get ps_search_engine_url(@ps_search_engine), as: :json
    assert_response :success
  end

  test "should update ps_search_engine" do
    patch ps_search_engine_url(@ps_search_engine), params: { ps_search_engine: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_search_engine" do
    assert_difference('PsSearchEngine.count', -1) do
      delete ps_search_engine_url(@ps_search_engine), as: :json
    end

    assert_response 204
  end
end
