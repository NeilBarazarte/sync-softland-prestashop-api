require 'test_helper'

class PsStocksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_stock = ps_stock(:one)
  end

  test "should get index" do
    get ps_stocks_url, as: :json
    assert_response :success
  end

  test "should create ps_stock" do
    assert_difference('PsStock.count') do
      post ps_stocks_url, params: { ps_stock: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_stock" do
    get ps_stock_url(@ps_stock), as: :json
    assert_response :success
  end

  test "should update ps_stock" do
    patch ps_stock_url(@ps_stock), params: { ps_stock: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_stock" do
    assert_difference('PsStock.count', -1) do
      delete ps_stock_url(@ps_stock), as: :json
    end

    assert_response 204
  end
end
