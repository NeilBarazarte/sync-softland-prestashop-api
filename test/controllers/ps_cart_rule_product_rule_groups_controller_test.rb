require 'test_helper'

class PsCartRuleProductRuleGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_rule_product_rule_group = ps_cart_rule_product_rule_group(:one)
  end

  test "should get index" do
    get ps_cart_rule_product_rule_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_rule_product_rule_group" do
    assert_difference('PsCartRuleProductRuleGroup.count') do
      post ps_cart_rule_product_rule_groups_url, params: { ps_cart_rule_product_rule_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_rule_product_rule_group" do
    get ps_cart_rule_product_rule_group_url(@ps_cart_rule_product_rule_group), as: :json
    assert_response :success
  end

  test "should update ps_cart_rule_product_rule_group" do
    patch ps_cart_rule_product_rule_group_url(@ps_cart_rule_product_rule_group), params: { ps_cart_rule_product_rule_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_rule_product_rule_group" do
    assert_difference('PsCartRuleProductRuleGroup.count', -1) do
      delete ps_cart_rule_product_rule_group_url(@ps_cart_rule_product_rule_group), as: :json
    end

    assert_response 204
  end
end
