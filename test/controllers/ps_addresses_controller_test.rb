require 'test_helper'

class PsAddressesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_address = ps_address(:one)
  end

  test "should get index" do
    get ps_addresses_url, as: :json
    assert_response :success
  end

  test "should create ps_address" do
    assert_difference('PsAddress.count') do
      post ps_addresses_url, params: { ps_address: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_address" do
    get ps_address_url(@ps_address), as: :json
    assert_response :success
  end

  test "should update ps_address" do
    patch ps_address_url(@ps_address), params: { ps_address: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_address" do
    assert_difference('PsAddress.count', -1) do
      delete ps_address_url(@ps_address), as: :json
    end

    assert_response 204
  end
end
