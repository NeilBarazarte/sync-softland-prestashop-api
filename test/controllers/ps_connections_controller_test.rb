require 'test_helper'

class PsConnectionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_connection = ps_connection(:one)
  end

  test "should get index" do
    get ps_connections_url, as: :json
    assert_response :success
  end

  test "should create ps_connection" do
    assert_difference('PsConnection.count') do
      post ps_connections_url, params: { ps_connection: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_connection" do
    get ps_connection_url(@ps_connection), as: :json
    assert_response :success
  end

  test "should update ps_connection" do
    patch ps_connection_url(@ps_connection), params: { ps_connection: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_connection" do
    assert_difference('PsConnection.count', -1) do
      delete ps_connection_url(@ps_connection), as: :json
    end

    assert_response 204
  end
end
