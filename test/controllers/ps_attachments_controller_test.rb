require 'test_helper'

class PsAttachmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_attachment = ps_attachment(:one)
  end

  test "should get index" do
    get ps_attachments_url, as: :json
    assert_response :success
  end

  test "should create ps_attachment" do
    assert_difference('PsAttachment.count') do
      post ps_attachments_url, params: { ps_attachment: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_attachment" do
    get ps_attachment_url(@ps_attachment), as: :json
    assert_response :success
  end

  test "should update ps_attachment" do
    patch ps_attachment_url(@ps_attachment), params: { ps_attachment: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_attachment" do
    assert_difference('PsAttachment.count', -1) do
      delete ps_attachment_url(@ps_attachment), as: :json
    end

    assert_response 204
  end
end
