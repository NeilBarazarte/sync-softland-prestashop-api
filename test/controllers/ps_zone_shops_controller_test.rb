require 'test_helper'

class PsZoneShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_zone_shop = ps_zone_shop(:one)
  end

  test "should get index" do
    get ps_zone_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_zone_shop" do
    assert_difference('PsZoneShop.count') do
      post ps_zone_shops_url, params: { ps_zone_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_zone_shop" do
    get ps_zone_shop_url(@ps_zone_shop), as: :json
    assert_response :success
  end

  test "should update ps_zone_shop" do
    patch ps_zone_shop_url(@ps_zone_shop), params: { ps_zone_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_zone_shop" do
    assert_difference('PsZoneShop.count', -1) do
      delete ps_zone_shop_url(@ps_zone_shop), as: :json
    end

    assert_response 204
  end
end
