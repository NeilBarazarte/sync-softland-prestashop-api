require 'test_helper'

class PsRisksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_risk = ps_risk(:one)
  end

  test "should get index" do
    get ps_risks_url, as: :json
    assert_response :success
  end

  test "should create ps_risk" do
    assert_difference('PsRisk.count') do
      post ps_risks_url, params: { ps_risk: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_risk" do
    get ps_risk_url(@ps_risk), as: :json
    assert_response :success
  end

  test "should update ps_risk" do
    patch ps_risk_url(@ps_risk), params: { ps_risk: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_risk" do
    assert_difference('PsRisk.count', -1) do
      delete ps_risk_url(@ps_risk), as: :json
    end

    assert_response 204
  end
end
