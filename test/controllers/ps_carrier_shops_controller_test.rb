require 'test_helper'

class PsCarrierShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_carrier_shop = ps_carrier_shop(:one)
  end

  test "should get index" do
    get ps_carrier_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_carrier_shop" do
    assert_difference('PsCarrierShop.count') do
      post ps_carrier_shops_url, params: { ps_carrier_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_carrier_shop" do
    get ps_carrier_shop_url(@ps_carrier_shop), as: :json
    assert_response :success
  end

  test "should update ps_carrier_shop" do
    patch ps_carrier_shop_url(@ps_carrier_shop), params: { ps_carrier_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_carrier_shop" do
    assert_difference('PsCarrierShop.count', -1) do
      delete ps_carrier_shop_url(@ps_carrier_shop), as: :json
    end

    assert_response 204
  end
end
