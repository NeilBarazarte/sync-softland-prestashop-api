require 'test_helper'

class PsLayeredIndexableFeaturesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_layered_indexable_feature = ps_layered_indexable_feature(:one)
  end

  test "should get index" do
    get ps_layered_indexable_features_url, as: :json
    assert_response :success
  end

  test "should create ps_layered_indexable_feature" do
    assert_difference('PsLayeredIndexableFeature.count') do
      post ps_layered_indexable_features_url, params: { ps_layered_indexable_feature: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_layered_indexable_feature" do
    get ps_layered_indexable_feature_url(@ps_layered_indexable_feature), as: :json
    assert_response :success
  end

  test "should update ps_layered_indexable_feature" do
    patch ps_layered_indexable_feature_url(@ps_layered_indexable_feature), params: { ps_layered_indexable_feature: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_layered_indexable_feature" do
    assert_difference('PsLayeredIndexableFeature.count', -1) do
      delete ps_layered_indexable_feature_url(@ps_layered_indexable_feature), as: :json
    end

    assert_response 204
  end
end
