require 'test_helper'

class PsOrderReturnsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_return = ps_order_return(:one)
  end

  test "should get index" do
    get ps_order_returns_url, as: :json
    assert_response :success
  end

  test "should create ps_order_return" do
    assert_difference('PsOrderReturn.count') do
      post ps_order_returns_url, params: { ps_order_return: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_return" do
    get ps_order_return_url(@ps_order_return), as: :json
    assert_response :success
  end

  test "should update ps_order_return" do
    patch ps_order_return_url(@ps_order_return), params: { ps_order_return: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_return" do
    assert_difference('PsOrderReturn.count', -1) do
      delete ps_order_return_url(@ps_order_return), as: :json
    end

    assert_response 204
  end
end
