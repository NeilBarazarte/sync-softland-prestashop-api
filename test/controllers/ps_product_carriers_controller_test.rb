require 'test_helper'

class PsProductCarriersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_carrier = ps_product_carrier(:one)
  end

  test "should get index" do
    get ps_product_carriers_url, as: :json
    assert_response :success
  end

  test "should create ps_product_carrier" do
    assert_difference('PsProductCarrier.count') do
      post ps_product_carriers_url, params: { ps_product_carrier: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_carrier" do
    get ps_product_carrier_url(@ps_product_carrier), as: :json
    assert_response :success
  end

  test "should update ps_product_carrier" do
    patch ps_product_carrier_url(@ps_product_carrier), params: { ps_product_carrier: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_carrier" do
    assert_difference('PsProductCarrier.count', -1) do
      delete ps_product_carrier_url(@ps_product_carrier), as: :json
    end

    assert_response 204
  end
end
