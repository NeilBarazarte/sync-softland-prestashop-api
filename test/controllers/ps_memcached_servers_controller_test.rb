require 'test_helper'

class PsMemcachedServersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_memcached_server = ps_memcached_server(:one)
  end

  test "should get index" do
    get ps_memcached_servers_url, as: :json
    assert_response :success
  end

  test "should create ps_memcached_server" do
    assert_difference('PsMemcachedServer.count') do
      post ps_memcached_servers_url, params: { ps_memcached_server: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_memcached_server" do
    get ps_memcached_server_url(@ps_memcached_server), as: :json
    assert_response :success
  end

  test "should update ps_memcached_server" do
    patch ps_memcached_server_url(@ps_memcached_server), params: { ps_memcached_server: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_memcached_server" do
    assert_difference('PsMemcachedServer.count', -1) do
      delete ps_memcached_server_url(@ps_memcached_server), as: :json
    end

    assert_response 204
  end
end
