require 'test_helper'

class PsModulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_module = ps_module(:one)
  end

  test "should get index" do
    get ps_modules_url, as: :json
    assert_response :success
  end

  test "should create ps_module" do
    assert_difference('PsModule.count') do
      post ps_modules_url, params: { ps_module: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_module" do
    get ps_module_url(@ps_module), as: :json
    assert_response :success
  end

  test "should update ps_module" do
    patch ps_module_url(@ps_module), params: { ps_module: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_module" do
    assert_difference('PsModule.count', -1) do
      delete ps_module_url(@ps_module), as: :json
    end

    assert_response 204
  end
end
