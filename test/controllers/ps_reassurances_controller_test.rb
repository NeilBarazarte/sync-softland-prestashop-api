require 'test_helper'

class PsReassurancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_reassurance = ps_reassurance(:one)
  end

  test "should get index" do
    get ps_reassurances_url, as: :json
    assert_response :success
  end

  test "should create ps_reassurance" do
    assert_difference('PsReassurance.count') do
      post ps_reassurances_url, params: { ps_reassurance: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_reassurance" do
    get ps_reassurance_url(@ps_reassurance), as: :json
    assert_response :success
  end

  test "should update ps_reassurance" do
    patch ps_reassurance_url(@ps_reassurance), params: { ps_reassurance: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_reassurance" do
    assert_difference('PsReassurance.count', -1) do
      delete ps_reassurance_url(@ps_reassurance), as: :json
    end

    assert_response 204
  end
end
