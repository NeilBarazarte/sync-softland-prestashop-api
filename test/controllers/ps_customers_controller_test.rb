require 'test_helper'

class PsCustomersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_customer = ps_customer(:one)
  end

  test "should get index" do
    get ps_customers_url, as: :json
    assert_response :success
  end

  test "should create ps_customer" do
    assert_difference('PsCustomer.count') do
      post ps_customers_url, params: { ps_customer: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_customer" do
    get ps_customer_url(@ps_customer), as: :json
    assert_response :success
  end

  test "should update ps_customer" do
    patch ps_customer_url(@ps_customer), params: { ps_customer: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_customer" do
    assert_difference('PsCustomer.count', -1) do
      delete ps_customer_url(@ps_customer), as: :json
    end

    assert_response 204
  end
end
