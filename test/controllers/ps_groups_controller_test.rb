require 'test_helper'

class PsGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_group = ps_group(:one)
  end

  test "should get index" do
    get ps_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_group" do
    assert_difference('PsGroup.count') do
      post ps_groups_url, params: { ps_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_group" do
    get ps_group_url(@ps_group), as: :json
    assert_response :success
  end

  test "should update ps_group" do
    patch ps_group_url(@ps_group), params: { ps_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_group" do
    assert_difference('PsGroup.count', -1) do
      delete ps_group_url(@ps_group), as: :json
    end

    assert_response 204
  end
end
