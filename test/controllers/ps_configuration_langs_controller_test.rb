require 'test_helper'

class PsConfigurationLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_configuration_lang = ps_configuration_lang(:one)
  end

  test "should get index" do
    get ps_configuration_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_configuration_lang" do
    assert_difference('PsConfigurationLang.count') do
      post ps_configuration_langs_url, params: { ps_configuration_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_configuration_lang" do
    get ps_configuration_lang_url(@ps_configuration_lang), as: :json
    assert_response :success
  end

  test "should update ps_configuration_lang" do
    patch ps_configuration_lang_url(@ps_configuration_lang), params: { ps_configuration_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_configuration_lang" do
    assert_difference('PsConfigurationLang.count', -1) do
      delete ps_configuration_lang_url(@ps_configuration_lang), as: :json
    end

    assert_response 204
  end
end
