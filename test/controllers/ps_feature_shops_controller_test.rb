require 'test_helper'

class PsFeatureShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_feature_shop = ps_feature_shop(:one)
  end

  test "should get index" do
    get ps_feature_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_feature_shop" do
    assert_difference('PsFeatureShop.count') do
      post ps_feature_shops_url, params: { ps_feature_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_feature_shop" do
    get ps_feature_shop_url(@ps_feature_shop), as: :json
    assert_response :success
  end

  test "should update ps_feature_shop" do
    patch ps_feature_shop_url(@ps_feature_shop), params: { ps_feature_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_feature_shop" do
    assert_difference('PsFeatureShop.count', -1) do
      delete ps_feature_shop_url(@ps_feature_shop), as: :json
    end

    assert_response 204
  end
end
