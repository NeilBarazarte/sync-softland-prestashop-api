require 'test_helper'

class PsGendersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_gender = ps_gender(:one)
  end

  test "should get index" do
    get ps_genders_url, as: :json
    assert_response :success
  end

  test "should create ps_gender" do
    assert_difference('PsGender.count') do
      post ps_genders_url, params: { ps_gender: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_gender" do
    get ps_gender_url(@ps_gender), as: :json
    assert_response :success
  end

  test "should update ps_gender" do
    patch ps_gender_url(@ps_gender), params: { ps_gender: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_gender" do
    assert_difference('PsGender.count', -1) do
      delete ps_gender_url(@ps_gender), as: :json
    end

    assert_response 204
  end
end
