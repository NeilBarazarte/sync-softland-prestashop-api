require 'test_helper'

class PsOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order = ps_order(:one)
  end

  test "should get index" do
    get ps_orders_url, as: :json
    assert_response :success
  end

  test "should create ps_order" do
    assert_difference('PsOrder.count') do
      post ps_orders_url, params: { ps_order: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order" do
    get ps_order_url(@ps_order), as: :json
    assert_response :success
  end

  test "should update ps_order" do
    patch ps_order_url(@ps_order), params: { ps_order: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order" do
    assert_difference('PsOrder.count', -1) do
      delete ps_order_url(@ps_order), as: :json
    end

    assert_response 204
  end
end
