require 'test_helper'

class PsZonesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_zone = ps_zone(:one)
  end

  test "should get index" do
    get ps_zones_url, as: :json
    assert_response :success
  end

  test "should create ps_zone" do
    assert_difference('PsZone.count') do
      post ps_zones_url, params: { ps_zone: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_zone" do
    get ps_zone_url(@ps_zone), as: :json
    assert_response :success
  end

  test "should update ps_zone" do
    patch ps_zone_url(@ps_zone), params: { ps_zone: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_zone" do
    assert_difference('PsZone.count', -1) do
      delete ps_zone_url(@ps_zone), as: :json
    end

    assert_response 204
  end
end
