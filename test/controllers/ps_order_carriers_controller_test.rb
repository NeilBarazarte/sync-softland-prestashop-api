require 'test_helper'

class PsOrderCarriersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_carrier = ps_order_carrier(:one)
  end

  test "should get index" do
    get ps_order_carriers_url, as: :json
    assert_response :success
  end

  test "should create ps_order_carrier" do
    assert_difference('PsOrderCarrier.count') do
      post ps_order_carriers_url, params: { ps_order_carrier: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_carrier" do
    get ps_order_carrier_url(@ps_order_carrier), as: :json
    assert_response :success
  end

  test "should update ps_order_carrier" do
    patch ps_order_carrier_url(@ps_order_carrier), params: { ps_order_carrier: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_carrier" do
    assert_difference('PsOrderCarrier.count', -1) do
      delete ps_order_carrier_url(@ps_order_carrier), as: :json
    end

    assert_response 204
  end
end
