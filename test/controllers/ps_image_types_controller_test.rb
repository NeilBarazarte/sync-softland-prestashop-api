require 'test_helper'

class PsImageTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_image_type = ps_image_type(:one)
  end

  test "should get index" do
    get ps_image_types_url, as: :json
    assert_response :success
  end

  test "should create ps_image_type" do
    assert_difference('PsImageType.count') do
      post ps_image_types_url, params: { ps_image_type: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_image_type" do
    get ps_image_type_url(@ps_image_type), as: :json
    assert_response :success
  end

  test "should update ps_image_type" do
    patch ps_image_type_url(@ps_image_type), params: { ps_image_type: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_image_type" do
    assert_difference('PsImageType.count', -1) do
      delete ps_image_type_url(@ps_image_type), as: :json
    end

    assert_response 204
  end
end
