require 'test_helper'

class PsCustomerThreadsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_customer_thread = ps_customer_thread(:one)
  end

  test "should get index" do
    get ps_customer_threads_url, as: :json
    assert_response :success
  end

  test "should create ps_customer_thread" do
    assert_difference('PsCustomerThread.count') do
      post ps_customer_threads_url, params: { ps_customer_thread: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_customer_thread" do
    get ps_customer_thread_url(@ps_customer_thread), as: :json
    assert_response :success
  end

  test "should update ps_customer_thread" do
    patch ps_customer_thread_url(@ps_customer_thread), params: { ps_customer_thread: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_customer_thread" do
    assert_difference('PsCustomerThread.count', -1) do
      delete ps_customer_thread_url(@ps_customer_thread), as: :json
    end

    assert_response 204
  end
end
