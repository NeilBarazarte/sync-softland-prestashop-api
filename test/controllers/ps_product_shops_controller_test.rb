require 'test_helper'

class PsProductShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_shop = ps_product_shop(:one)
  end

  test "should get index" do
    get ps_product_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_product_shop" do
    assert_difference('PsProductShop.count') do
      post ps_product_shops_url, params: { ps_product_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_shop" do
    get ps_product_shop_url(@ps_product_shop), as: :json
    assert_response :success
  end

  test "should update ps_product_shop" do
    patch ps_product_shop_url(@ps_product_shop), params: { ps_product_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_shop" do
    assert_difference('PsProductShop.count', -1) do
      delete ps_product_shop_url(@ps_product_shop), as: :json
    end

    assert_response 204
  end
end
