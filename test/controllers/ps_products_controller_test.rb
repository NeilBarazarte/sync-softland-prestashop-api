require 'test_helper'

class PsProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product = ps_products(:one)
  end

  test "should get index" do
    get ps_products_url, as: :json
    assert_response :success
  end

  test "should create ps_product" do
    assert_difference('PsProduct.count') do
      post ps_products_url, params: { ps_product: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product" do
    get ps_product_url(@ps_product), as: :json
    assert_response :success
  end

  test "should update ps_product" do
    patch ps_product_url(@ps_product), params: { ps_product: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product" do
    assert_difference('PsProduct.count', -1) do
      delete ps_product_url(@ps_product), as: :json
    end

    assert_response 204
  end
end
