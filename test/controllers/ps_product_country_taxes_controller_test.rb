require 'test_helper'

class PsProductCountryTaxesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_country_tax = ps_product_country_tax(:one)
  end

  test "should get index" do
    get ps_product_country_taxes_url, as: :json
    assert_response :success
  end

  test "should create ps_product_country_tax" do
    assert_difference('PsProductCountryTax.count') do
      post ps_product_country_taxes_url, params: { ps_product_country_tax: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_country_tax" do
    get ps_product_country_tax_url(@ps_product_country_tax), as: :json
    assert_response :success
  end

  test "should update ps_product_country_tax" do
    patch ps_product_country_tax_url(@ps_product_country_tax), params: { ps_product_country_tax: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_country_tax" do
    assert_difference('PsProductCountryTax.count', -1) do
      delete ps_product_country_tax_url(@ps_product_country_tax), as: :json
    end

    assert_response 204
  end
end
