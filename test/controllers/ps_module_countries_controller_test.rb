require 'test_helper'

class PsModuleCountriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_module_country = ps_module_country(:one)
  end

  test "should get index" do
    get ps_module_countries_url, as: :json
    assert_response :success
  end

  test "should create ps_module_country" do
    assert_difference('PsModuleCountry.count') do
      post ps_module_countries_url, params: { ps_module_country: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_module_country" do
    get ps_module_country_url(@ps_module_country), as: :json
    assert_response :success
  end

  test "should update ps_module_country" do
    patch ps_module_country_url(@ps_module_country), params: { ps_module_country: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_module_country" do
    assert_difference('PsModuleCountry.count', -1) do
      delete ps_module_country_url(@ps_module_country), as: :json
    end

    assert_response 204
  end
end
