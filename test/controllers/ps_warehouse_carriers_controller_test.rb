require 'test_helper'

class PsWarehouseCarriersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_warehouse_carrier = ps_warehouse_carrier(:one)
  end

  test "should get index" do
    get ps_warehouse_carriers_url, as: :json
    assert_response :success
  end

  test "should create ps_warehouse_carrier" do
    assert_difference('PsWarehouseCarrier.count') do
      post ps_warehouse_carriers_url, params: { ps_warehouse_carrier: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_warehouse_carrier" do
    get ps_warehouse_carrier_url(@ps_warehouse_carrier), as: :json
    assert_response :success
  end

  test "should update ps_warehouse_carrier" do
    patch ps_warehouse_carrier_url(@ps_warehouse_carrier), params: { ps_warehouse_carrier: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_warehouse_carrier" do
    assert_difference('PsWarehouseCarrier.count', -1) do
      delete ps_warehouse_carrier_url(@ps_warehouse_carrier), as: :json
    end

    assert_response 204
  end
end
