require 'test_helper'

class PsLinksmenutopLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_linksmenutop_lang = ps_linksmenutop_lang(:one)
  end

  test "should get index" do
    get ps_linksmenutop_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_linksmenutop_lang" do
    assert_difference('PsLinksmenutopLang.count') do
      post ps_linksmenutop_langs_url, params: { ps_linksmenutop_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_linksmenutop_lang" do
    get ps_linksmenutop_lang_url(@ps_linksmenutop_lang), as: :json
    assert_response :success
  end

  test "should update ps_linksmenutop_lang" do
    patch ps_linksmenutop_lang_url(@ps_linksmenutop_lang), params: { ps_linksmenutop_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_linksmenutop_lang" do
    assert_difference('PsLinksmenutopLang.count', -1) do
      delete ps_linksmenutop_lang_url(@ps_linksmenutop_lang), as: :json
    end

    assert_response 204
  end
end
