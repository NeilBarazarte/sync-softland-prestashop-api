require 'test_helper'

class PsWarehouseProductLocationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_warehouse_product_location = ps_warehouse_product_location(:one)
  end

  test "should get index" do
    get ps_warehouse_product_locations_url, as: :json
    assert_response :success
  end

  test "should create ps_warehouse_product_location" do
    assert_difference('PsWarehouseProductLocation.count') do
      post ps_warehouse_product_locations_url, params: { ps_warehouse_product_location: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_warehouse_product_location" do
    get ps_warehouse_product_location_url(@ps_warehouse_product_location), as: :json
    assert_response :success
  end

  test "should update ps_warehouse_product_location" do
    patch ps_warehouse_product_location_url(@ps_warehouse_product_location), params: { ps_warehouse_product_location: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_warehouse_product_location" do
    assert_difference('PsWarehouseProductLocation.count', -1) do
      delete ps_warehouse_product_location_url(@ps_warehouse_product_location), as: :json
    end

    assert_response 204
  end
end
