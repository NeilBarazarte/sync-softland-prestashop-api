require 'test_helper'

class PsAttachmentLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_attachment_lang = ps_attachment_lang(:one)
  end

  test "should get index" do
    get ps_attachment_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_attachment_lang" do
    assert_difference('PsAttachmentLang.count') do
      post ps_attachment_langs_url, params: { ps_attachment_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_attachment_lang" do
    get ps_attachment_lang_url(@ps_attachment_lang), as: :json
    assert_response :success
  end

  test "should update ps_attachment_lang" do
    patch ps_attachment_lang_url(@ps_attachment_lang), params: { ps_attachment_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_attachment_lang" do
    assert_difference('PsAttachmentLang.count', -1) do
      delete ps_attachment_lang_url(@ps_attachment_lang), as: :json
    end

    assert_response 204
  end
end
