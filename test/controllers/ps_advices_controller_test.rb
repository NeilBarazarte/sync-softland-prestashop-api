require 'test_helper'

class PsAdvicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_advice = ps_advice(:one)
  end

  test "should get index" do
    get ps_advices_url, as: :json
    assert_response :success
  end

  test "should create ps_advice" do
    assert_difference('PsAdvice.count') do
      post ps_advices_url, params: { ps_advice: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_advice" do
    get ps_advice_url(@ps_advice), as: :json
    assert_response :success
  end

  test "should update ps_advice" do
    patch ps_advice_url(@ps_advice), params: { ps_advice: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_advice" do
    assert_difference('PsAdvice.count', -1) do
      delete ps_advice_url(@ps_advice), as: :json
    end

    assert_response 204
  end
end
