require 'test_helper'

class PsGuestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_guest = ps_guest(:one)
  end

  test "should get index" do
    get ps_guests_url, as: :json
    assert_response :success
  end

  test "should create ps_guest" do
    assert_difference('PsGuest.count') do
      post ps_guests_url, params: { ps_guest: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_guest" do
    get ps_guest_url(@ps_guest), as: :json
    assert_response :success
  end

  test "should update ps_guest" do
    patch ps_guest_url(@ps_guest), params: { ps_guest: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_guest" do
    assert_difference('PsGuest.count', -1) do
      delete ps_guest_url(@ps_guest), as: :json
    end

    assert_response 204
  end
end
