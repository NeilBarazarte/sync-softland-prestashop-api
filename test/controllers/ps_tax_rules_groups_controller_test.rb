require 'test_helper'

class PsTaxRulesGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tax_rules_group = ps_tax_rules_group(:one)
  end

  test "should get index" do
    get ps_tax_rules_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_tax_rules_group" do
    assert_difference('PsTaxRulesGroup.count') do
      post ps_tax_rules_groups_url, params: { ps_tax_rules_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tax_rules_group" do
    get ps_tax_rules_group_url(@ps_tax_rules_group), as: :json
    assert_response :success
  end

  test "should update ps_tax_rules_group" do
    patch ps_tax_rules_group_url(@ps_tax_rules_group), params: { ps_tax_rules_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tax_rules_group" do
    assert_difference('PsTaxRulesGroup.count', -1) do
      delete ps_tax_rules_group_url(@ps_tax_rules_group), as: :json
    end

    assert_response 204
  end
end
