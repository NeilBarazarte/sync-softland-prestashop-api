require 'test_helper'

class PsSpecificPriceRuleConditionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_specific_price_rule_condition = ps_specific_price_rule_condition(:one)
  end

  test "should get index" do
    get ps_specific_price_rule_conditions_url, as: :json
    assert_response :success
  end

  test "should create ps_specific_price_rule_condition" do
    assert_difference('PsSpecificPriceRuleCondition.count') do
      post ps_specific_price_rule_conditions_url, params: { ps_specific_price_rule_condition: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_specific_price_rule_condition" do
    get ps_specific_price_rule_condition_url(@ps_specific_price_rule_condition), as: :json
    assert_response :success
  end

  test "should update ps_specific_price_rule_condition" do
    patch ps_specific_price_rule_condition_url(@ps_specific_price_rule_condition), params: { ps_specific_price_rule_condition: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_specific_price_rule_condition" do
    assert_difference('PsSpecificPriceRuleCondition.count', -1) do
      delete ps_specific_price_rule_condition_url(@ps_specific_price_rule_condition), as: :json
    end

    assert_response 204
  end
end
