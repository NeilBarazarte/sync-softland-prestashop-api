require 'test_helper'

class PsDeliveriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_delivery = ps_delivery(:one)
  end

  test "should get index" do
    get ps_deliveries_url, as: :json
    assert_response :success
  end

  test "should create ps_delivery" do
    assert_difference('PsDelivery.count') do
      post ps_deliveries_url, params: { ps_delivery: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_delivery" do
    get ps_delivery_url(@ps_delivery), as: :json
    assert_response :success
  end

  test "should update ps_delivery" do
    patch ps_delivery_url(@ps_delivery), params: { ps_delivery: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_delivery" do
    assert_difference('PsDelivery.count', -1) do
      delete ps_delivery_url(@ps_delivery), as: :json
    end

    assert_response 204
  end
end
