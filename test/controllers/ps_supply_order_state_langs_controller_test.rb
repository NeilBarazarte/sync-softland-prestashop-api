require 'test_helper'

class PsSupplyOrderStateLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_supply_order_state_lang = ps_supply_order_state_lang(:one)
  end

  test "should get index" do
    get ps_supply_order_state_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_supply_order_state_lang" do
    assert_difference('PsSupplyOrderStateLang.count') do
      post ps_supply_order_state_langs_url, params: { ps_supply_order_state_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_supply_order_state_lang" do
    get ps_supply_order_state_lang_url(@ps_supply_order_state_lang), as: :json
    assert_response :success
  end

  test "should update ps_supply_order_state_lang" do
    patch ps_supply_order_state_lang_url(@ps_supply_order_state_lang), params: { ps_supply_order_state_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_supply_order_state_lang" do
    assert_difference('PsSupplyOrderStateLang.count', -1) do
      delete ps_supply_order_state_lang_url(@ps_supply_order_state_lang), as: :json
    end

    assert_response 204
  end
end
