require 'test_helper'

class PsLayeredPriceIndicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_layered_price_index = ps_layered_price_index(:one)
  end

  test "should get index" do
    get ps_layered_price_indices_url, as: :json
    assert_response :success
  end

  test "should create ps_layered_price_index" do
    assert_difference('PsLayeredPriceIndex.count') do
      post ps_layered_price_indices_url, params: { ps_layered_price_index: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_layered_price_index" do
    get ps_layered_price_index_url(@ps_layered_price_index), as: :json
    assert_response :success
  end

  test "should update ps_layered_price_index" do
    patch ps_layered_price_index_url(@ps_layered_price_index), params: { ps_layered_price_index: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_layered_price_index" do
    assert_difference('PsLayeredPriceIndex.count', -1) do
      delete ps_layered_price_index_url(@ps_layered_price_index), as: :json
    end

    assert_response 204
  end
end
