require 'test_helper'

class PsProductAttributeImagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_attribute_image = ps_product_attribute_image(:one)
  end

  test "should get index" do
    get ps_product_attribute_images_url, as: :json
    assert_response :success
  end

  test "should create ps_product_attribute_image" do
    assert_difference('PsProductAttributeImage.count') do
      post ps_product_attribute_images_url, params: { ps_product_attribute_image: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_attribute_image" do
    get ps_product_attribute_image_url(@ps_product_attribute_image), as: :json
    assert_response :success
  end

  test "should update ps_product_attribute_image" do
    patch ps_product_attribute_image_url(@ps_product_attribute_image), params: { ps_product_attribute_image: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_attribute_image" do
    assert_difference('PsProductAttributeImage.count', -1) do
      delete ps_product_attribute_image_url(@ps_product_attribute_image), as: :json
    end

    assert_response 204
  end
end
