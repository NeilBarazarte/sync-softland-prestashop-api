require 'test_helper'

class PsCartProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_product = ps_cart_product(:one)
  end

  test "should get index" do
    get ps_cart_products_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_product" do
    assert_difference('PsCartProduct.count') do
      post ps_cart_products_url, params: { ps_cart_product: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_product" do
    get ps_cart_product_url(@ps_cart_product), as: :json
    assert_response :success
  end

  test "should update ps_cart_product" do
    patch ps_cart_product_url(@ps_cart_product), params: { ps_cart_product: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_product" do
    assert_difference('PsCartProduct.count', -1) do
      delete ps_cart_product_url(@ps_cart_product), as: :json
    end

    assert_response 204
  end
end
