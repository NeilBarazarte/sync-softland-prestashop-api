require 'test_helper'

class PsProductAttachmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_attachment = ps_product_attachment(:one)
  end

  test "should get index" do
    get ps_product_attachments_url, as: :json
    assert_response :success
  end

  test "should create ps_product_attachment" do
    assert_difference('PsProductAttachment.count') do
      post ps_product_attachments_url, params: { ps_product_attachment: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_attachment" do
    get ps_product_attachment_url(@ps_product_attachment), as: :json
    assert_response :success
  end

  test "should update ps_product_attachment" do
    patch ps_product_attachment_url(@ps_product_attachment), params: { ps_product_attachment: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_attachment" do
    assert_difference('PsProductAttachment.count', -1) do
      delete ps_product_attachment_url(@ps_product_attachment), as: :json
    end

    assert_response 204
  end
end
