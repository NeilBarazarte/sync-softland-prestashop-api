require 'test_helper'

class PsOrderReturnDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_return_detail = ps_order_return_detail(:one)
  end

  test "should get index" do
    get ps_order_return_details_url, as: :json
    assert_response :success
  end

  test "should create ps_order_return_detail" do
    assert_difference('PsOrderReturnDetail.count') do
      post ps_order_return_details_url, params: { ps_order_return_detail: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_return_detail" do
    get ps_order_return_detail_url(@ps_order_return_detail), as: :json
    assert_response :success
  end

  test "should update ps_order_return_detail" do
    patch ps_order_return_detail_url(@ps_order_return_detail), params: { ps_order_return_detail: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_return_detail" do
    assert_difference('PsOrderReturnDetail.count', -1) do
      delete ps_order_return_detail_url(@ps_order_return_detail), as: :json
    end

    assert_response 204
  end
end
