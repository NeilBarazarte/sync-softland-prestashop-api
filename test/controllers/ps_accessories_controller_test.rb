require 'test_helper'

class PsAccessoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_accessory = ps_accessory(:one)
  end

  test "should get index" do
    get ps_accessories_url, as: :json
    assert_response :success
  end

  test "should create ps_accessory" do
    assert_difference('PsAccessory.count') do
      post ps_accessories_url, params: { ps_accessory: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_accessory" do
    get ps_accessory_url(@ps_accessory), as: :json
    assert_response :success
  end

  test "should update ps_accessory" do
    patch ps_accessory_url(@ps_accessory), params: { ps_accessory: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_accessory" do
    assert_difference('PsAccessory.count', -1) do
      delete ps_accessory_url(@ps_accessory), as: :json
    end

    assert_response 204
  end
end
