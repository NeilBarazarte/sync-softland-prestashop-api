require 'test_helper'

class PsTabAdvicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tab_advice = ps_tab_advice(:one)
  end

  test "should get index" do
    get ps_tab_advices_url, as: :json
    assert_response :success
  end

  test "should create ps_tab_advice" do
    assert_difference('PsTabAdvice.count') do
      post ps_tab_advices_url, params: { ps_tab_advice: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tab_advice" do
    get ps_tab_advice_url(@ps_tab_advice), as: :json
    assert_response :success
  end

  test "should update ps_tab_advice" do
    patch ps_tab_advice_url(@ps_tab_advice), params: { ps_tab_advice: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tab_advice" do
    assert_difference('PsTabAdvice.count', -1) do
      delete ps_tab_advice_url(@ps_tab_advice), as: :json
    end

    assert_response 204
  end
end
