require 'test_helper'

class PsGroupReductionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_group_reduction = ps_group_reduction(:one)
  end

  test "should get index" do
    get ps_group_reductions_url, as: :json
    assert_response :success
  end

  test "should create ps_group_reduction" do
    assert_difference('PsGroupReduction.count') do
      post ps_group_reductions_url, params: { ps_group_reduction: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_group_reduction" do
    get ps_group_reduction_url(@ps_group_reduction), as: :json
    assert_response :success
  end

  test "should update ps_group_reduction" do
    patch ps_group_reduction_url(@ps_group_reduction), params: { ps_group_reduction: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_group_reduction" do
    assert_difference('PsGroupReduction.count', -1) do
      delete ps_group_reduction_url(@ps_group_reduction), as: :json
    end

    assert_response 204
  end
end
