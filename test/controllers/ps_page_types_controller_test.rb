require 'test_helper'

class PsPageTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_page_type = ps_page_type(:one)
  end

  test "should get index" do
    get ps_page_types_url, as: :json
    assert_response :success
  end

  test "should create ps_page_type" do
    assert_difference('PsPageType.count') do
      post ps_page_types_url, params: { ps_page_type: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_page_type" do
    get ps_page_type_url(@ps_page_type), as: :json
    assert_response :success
  end

  test "should update ps_page_type" do
    patch ps_page_type_url(@ps_page_type), params: { ps_page_type: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_page_type" do
    assert_difference('PsPageType.count', -1) do
      delete ps_page_type_url(@ps_page_type), as: :json
    end

    assert_response 204
  end
end
