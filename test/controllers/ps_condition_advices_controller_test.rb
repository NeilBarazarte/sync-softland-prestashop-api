require 'test_helper'

class PsConditionAdvicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_condition_advice = ps_condition_advice(:one)
  end

  test "should get index" do
    get ps_condition_advices_url, as: :json
    assert_response :success
  end

  test "should create ps_condition_advice" do
    assert_difference('PsConditionAdvice.count') do
      post ps_condition_advices_url, params: { ps_condition_advice: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_condition_advice" do
    get ps_condition_advice_url(@ps_condition_advice), as: :json
    assert_response :success
  end

  test "should update ps_condition_advice" do
    patch ps_condition_advice_url(@ps_condition_advice), params: { ps_condition_advice: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_condition_advice" do
    assert_difference('PsConditionAdvice.count', -1) do
      delete ps_condition_advice_url(@ps_condition_advice), as: :json
    end

    assert_response 204
  end
end
