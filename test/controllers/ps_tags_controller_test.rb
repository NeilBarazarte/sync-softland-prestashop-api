require 'test_helper'

class PsTagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tag = ps_tag(:one)
  end

  test "should get index" do
    get ps_tags_url, as: :json
    assert_response :success
  end

  test "should create ps_tag" do
    assert_difference('PsTag.count') do
      post ps_tags_url, params: { ps_tag: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tag" do
    get ps_tag_url(@ps_tag), as: :json
    assert_response :success
  end

  test "should update ps_tag" do
    patch ps_tag_url(@ps_tag), params: { ps_tag: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tag" do
    assert_difference('PsTag.count', -1) do
      delete ps_tag_url(@ps_tag), as: :json
    end

    assert_response 204
  end
end
