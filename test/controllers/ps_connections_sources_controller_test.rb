require 'test_helper'

class PsConnectionsSourcesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_connections_source = ps_connections_source(:one)
  end

  test "should get index" do
    get ps_connections_sources_url, as: :json
    assert_response :success
  end

  test "should create ps_connections_source" do
    assert_difference('PsConnectionsSource.count') do
      post ps_connections_sources_url, params: { ps_connections_source: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_connections_source" do
    get ps_connections_source_url(@ps_connections_source), as: :json
    assert_response :success
  end

  test "should update ps_connections_source" do
    patch ps_connections_source_url(@ps_connections_source), params: { ps_connections_source: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_connections_source" do
    assert_difference('PsConnectionsSource.count', -1) do
      delete ps_connections_source_url(@ps_connections_source), as: :json
    end

    assert_response 204
  end
end
