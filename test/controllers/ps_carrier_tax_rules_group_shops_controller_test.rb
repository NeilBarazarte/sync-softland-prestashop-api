require 'test_helper'

class PsCarrierTaxRulesGroupShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_carrier_tax_rules_group_shop = ps_carrier_tax_rules_group_shop(:one)
  end

  test "should get index" do
    get ps_carrier_tax_rules_group_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_carrier_tax_rules_group_shop" do
    assert_difference('PsCarrierTaxRulesGroupShop.count') do
      post ps_carrier_tax_rules_group_shops_url, params: { ps_carrier_tax_rules_group_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_carrier_tax_rules_group_shop" do
    get ps_carrier_tax_rules_group_shop_url(@ps_carrier_tax_rules_group_shop), as: :json
    assert_response :success
  end

  test "should update ps_carrier_tax_rules_group_shop" do
    patch ps_carrier_tax_rules_group_shop_url(@ps_carrier_tax_rules_group_shop), params: { ps_carrier_tax_rules_group_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_carrier_tax_rules_group_shop" do
    assert_difference('PsCarrierTaxRulesGroupShop.count', -1) do
      delete ps_carrier_tax_rules_group_shop_url(@ps_carrier_tax_rules_group_shop), as: :json
    end

    assert_response 204
  end
end
