require 'test_helper'

class PsCountriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_country = ps_country(:one)
  end

  test "should get index" do
    get ps_countries_url, as: :json
    assert_response :success
  end

  test "should create ps_country" do
    assert_difference('PsCountry.count') do
      post ps_countries_url, params: { ps_country: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_country" do
    get ps_country_url(@ps_country), as: :json
    assert_response :success
  end

  test "should update ps_country" do
    patch ps_country_url(@ps_country), params: { ps_country: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_country" do
    assert_difference('PsCountry.count', -1) do
      delete ps_country_url(@ps_country), as: :json
    end

    assert_response 204
  end
end
