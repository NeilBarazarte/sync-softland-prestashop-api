require 'test_helper'

class PsLinkBlocksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_link_block = ps_link_block(:one)
  end

  test "should get index" do
    get ps_link_blocks_url, as: :json
    assert_response :success
  end

  test "should create ps_link_block" do
    assert_difference('PsLinkBlock.count') do
      post ps_link_blocks_url, params: { ps_link_block: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_link_block" do
    get ps_link_block_url(@ps_link_block), as: :json
    assert_response :success
  end

  test "should update ps_link_block" do
    patch ps_link_block_url(@ps_link_block), params: { ps_link_block: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_link_block" do
    assert_difference('PsLinkBlock.count', -1) do
      delete ps_link_block_url(@ps_link_block), as: :json
    end

    assert_response 204
  end
end
