require 'test_helper'

class PsCurrenciesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_currency = ps_currency(:one)
  end

  test "should get index" do
    get ps_currencies_url, as: :json
    assert_response :success
  end

  test "should create ps_currency" do
    assert_difference('PsCurrency.count') do
      post ps_currencies_url, params: { ps_currency: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_currency" do
    get ps_currency_url(@ps_currency), as: :json
    assert_response :success
  end

  test "should update ps_currency" do
    patch ps_currency_url(@ps_currency), params: { ps_currency: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_currency" do
    assert_difference('PsCurrency.count', -1) do
      delete ps_currency_url(@ps_currency), as: :json
    end

    assert_response 204
  end
end
