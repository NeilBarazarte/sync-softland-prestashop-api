require 'test_helper'

class PsProductDownloadsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_download = ps_product_download(:one)
  end

  test "should get index" do
    get ps_product_downloads_url, as: :json
    assert_response :success
  end

  test "should create ps_product_download" do
    assert_difference('PsProductDownload.count') do
      post ps_product_downloads_url, params: { ps_product_download: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_download" do
    get ps_product_download_url(@ps_product_download), as: :json
    assert_response :success
  end

  test "should update ps_product_download" do
    patch ps_product_download_url(@ps_product_download), params: { ps_product_download: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_download" do
    assert_difference('PsProductDownload.count', -1) do
      delete ps_product_download_url(@ps_product_download), as: :json
    end

    assert_response 204
  end
end
