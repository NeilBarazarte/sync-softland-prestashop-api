require 'test_helper'

class PsPagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_page = ps_page(:one)
  end

  test "should get index" do
    get ps_pages_url, as: :json
    assert_response :success
  end

  test "should create ps_page" do
    assert_difference('PsPage.count') do
      post ps_pages_url, params: { ps_page: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_page" do
    get ps_page_url(@ps_page), as: :json
    assert_response :success
  end

  test "should update ps_page" do
    patch ps_page_url(@ps_page), params: { ps_page: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_page" do
    assert_difference('PsPage.count', -1) do
      delete ps_page_url(@ps_page), as: :json
    end

    assert_response 204
  end
end
