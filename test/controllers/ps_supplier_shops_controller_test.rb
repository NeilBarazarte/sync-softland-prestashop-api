require 'test_helper'

class PsSupplierShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_supplier_shop = ps_supplier_shop(:one)
  end

  test "should get index" do
    get ps_supplier_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_supplier_shop" do
    assert_difference('PsSupplierShop.count') do
      post ps_supplier_shops_url, params: { ps_supplier_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_supplier_shop" do
    get ps_supplier_shop_url(@ps_supplier_shop), as: :json
    assert_response :success
  end

  test "should update ps_supplier_shop" do
    patch ps_supplier_shop_url(@ps_supplier_shop), params: { ps_supplier_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_supplier_shop" do
    assert_difference('PsSupplierShop.count', -1) do
      delete ps_supplier_shop_url(@ps_supplier_shop), as: :json
    end

    assert_response 204
  end
end
