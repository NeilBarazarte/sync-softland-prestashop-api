require 'test_helper'

class PsFeatureValuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_feature_value = ps_feature_value(:one)
  end

  test "should get index" do
    get ps_feature_values_url, as: :json
    assert_response :success
  end

  test "should create ps_feature_value" do
    assert_difference('PsFeatureValue.count') do
      post ps_feature_values_url, params: { ps_feature_value: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_feature_value" do
    get ps_feature_value_url(@ps_feature_value), as: :json
    assert_response :success
  end

  test "should update ps_feature_value" do
    patch ps_feature_value_url(@ps_feature_value), params: { ps_feature_value: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_feature_value" do
    assert_difference('PsFeatureValue.count', -1) do
      delete ps_feature_value_url(@ps_feature_value), as: :json
    end

    assert_response 204
  end
end
