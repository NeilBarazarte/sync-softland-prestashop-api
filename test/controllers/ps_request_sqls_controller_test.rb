require 'test_helper'

class PsRequestSqlsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_request_sql = ps_request_sql(:one)
  end

  test "should get index" do
    get ps_request_sqls_url, as: :json
    assert_response :success
  end

  test "should create ps_request_sql" do
    assert_difference('PsRequestSql.count') do
      post ps_request_sqls_url, params: { ps_request_sql: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_request_sql" do
    get ps_request_sql_url(@ps_request_sql), as: :json
    assert_response :success
  end

  test "should update ps_request_sql" do
    patch ps_request_sql_url(@ps_request_sql), params: { ps_request_sql: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_request_sql" do
    assert_difference('PsRequestSql.count', -1) do
      delete ps_request_sql_url(@ps_request_sql), as: :json
    end

    assert_response 204
  end
end
