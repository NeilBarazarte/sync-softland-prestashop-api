require 'test_helper'

class PsCurrencyShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_currency_shop = ps_currency_shop(:one)
  end

  test "should get index" do
    get ps_currency_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_currency_shop" do
    assert_difference('PsCurrencyShop.count') do
      post ps_currency_shops_url, params: { ps_currency_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_currency_shop" do
    get ps_currency_shop_url(@ps_currency_shop), as: :json
    assert_response :success
  end

  test "should update ps_currency_shop" do
    patch ps_currency_shop_url(@ps_currency_shop), params: { ps_currency_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_currency_shop" do
    assert_difference('PsCurrencyShop.count', -1) do
      delete ps_currency_shop_url(@ps_currency_shop), as: :json
    end

    assert_response 204
  end
end
