require 'test_helper'

class PsSpecificPricePrioritiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_specific_price_priority = ps_specific_price_priority(:one)
  end

  test "should get index" do
    get ps_specific_price_priorities_url, as: :json
    assert_response :success
  end

  test "should create ps_specific_price_priority" do
    assert_difference('PsSpecificPricePriority.count') do
      post ps_specific_price_priorities_url, params: { ps_specific_price_priority: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_specific_price_priority" do
    get ps_specific_price_priority_url(@ps_specific_price_priority), as: :json
    assert_response :success
  end

  test "should update ps_specific_price_priority" do
    patch ps_specific_price_priority_url(@ps_specific_price_priority), params: { ps_specific_price_priority: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_specific_price_priority" do
    assert_difference('PsSpecificPricePriority.count', -1) do
      delete ps_specific_price_priority_url(@ps_specific_price_priority), as: :json
    end

    assert_response 204
  end
end
