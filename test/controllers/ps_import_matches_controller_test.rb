require 'test_helper'

class PsImportMatchesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_import_match = ps_import_match(:one)
  end

  test "should get index" do
    get ps_import_matches_url, as: :json
    assert_response :success
  end

  test "should create ps_import_match" do
    assert_difference('PsImportMatch.count') do
      post ps_import_matches_url, params: { ps_import_match: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_import_match" do
    get ps_import_match_url(@ps_import_match), as: :json
    assert_response :success
  end

  test "should update ps_import_match" do
    patch ps_import_match_url(@ps_import_match), params: { ps_import_match: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_import_match" do
    assert_difference('PsImportMatch.count', -1) do
      delete ps_import_match_url(@ps_import_match), as: :json
    end

    assert_response 204
  end
end
