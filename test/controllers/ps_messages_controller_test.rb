require 'test_helper'

class PsMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_message = ps_message(:one)
  end

  test "should get index" do
    get ps_messages_url, as: :json
    assert_response :success
  end

  test "should create ps_message" do
    assert_difference('PsMessage.count') do
      post ps_messages_url, params: { ps_message: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_message" do
    get ps_message_url(@ps_message), as: :json
    assert_response :success
  end

  test "should update ps_message" do
    patch ps_message_url(@ps_message), params: { ps_message: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_message" do
    assert_difference('PsMessage.count', -1) do
      delete ps_message_url(@ps_message), as: :json
    end

    assert_response 204
  end
end
