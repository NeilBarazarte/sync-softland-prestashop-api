require 'test_helper'

class PsCountryShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_country_shop = ps_country_shop(:one)
  end

  test "should get index" do
    get ps_country_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_country_shop" do
    assert_difference('PsCountryShop.count') do
      post ps_country_shops_url, params: { ps_country_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_country_shop" do
    get ps_country_shop_url(@ps_country_shop), as: :json
    assert_response :success
  end

  test "should update ps_country_shop" do
    patch ps_country_shop_url(@ps_country_shop), params: { ps_country_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_country_shop" do
    assert_difference('PsCountryShop.count', -1) do
      delete ps_country_shop_url(@ps_country_shop), as: :json
    end

    assert_response 204
  end
end
