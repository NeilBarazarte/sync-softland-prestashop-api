require 'test_helper'

class PsInfoLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_info_lang = ps_info_lang(:one)
  end

  test "should get index" do
    get ps_info_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_info_lang" do
    assert_difference('PsInfoLang.count') do
      post ps_info_langs_url, params: { ps_info_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_info_lang" do
    get ps_info_lang_url(@ps_info_lang), as: :json
    assert_response :success
  end

  test "should update ps_info_lang" do
    patch ps_info_lang_url(@ps_info_lang), params: { ps_info_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_info_lang" do
    assert_difference('PsInfoLang.count', -1) do
      delete ps_info_lang_url(@ps_info_lang), as: :json
    end

    assert_response 204
  end
end
