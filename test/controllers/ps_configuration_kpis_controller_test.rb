require 'test_helper'

class PsConfigurationKpisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_configuration_kpi = ps_configuration_kpi(:one)
  end

  test "should get index" do
    get ps_configuration_kpis_url, as: :json
    assert_response :success
  end

  test "should create ps_configuration_kpi" do
    assert_difference('PsConfigurationKpi.count') do
      post ps_configuration_kpis_url, params: { ps_configuration_kpi: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_configuration_kpi" do
    get ps_configuration_kpi_url(@ps_configuration_kpi), as: :json
    assert_response :success
  end

  test "should update ps_configuration_kpi" do
    patch ps_configuration_kpi_url(@ps_configuration_kpi), params: { ps_configuration_kpi: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_configuration_kpi" do
    assert_difference('PsConfigurationKpi.count', -1) do
      delete ps_configuration_kpi_url(@ps_configuration_kpi), as: :json
    end

    assert_response 204
  end
end
