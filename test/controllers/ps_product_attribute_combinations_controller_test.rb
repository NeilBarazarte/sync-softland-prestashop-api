require 'test_helper'

class PsProductAttributeCombinationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_attribute_combination = ps_product_attribute_combination(:one)
  end

  test "should get index" do
    get ps_product_attribute_combinations_url, as: :json
    assert_response :success
  end

  test "should create ps_product_attribute_combination" do
    assert_difference('PsProductAttributeCombination.count') do
      post ps_product_attribute_combinations_url, params: { ps_product_attribute_combination: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_attribute_combination" do
    get ps_product_attribute_combination_url(@ps_product_attribute_combination), as: :json
    assert_response :success
  end

  test "should update ps_product_attribute_combination" do
    patch ps_product_attribute_combination_url(@ps_product_attribute_combination), params: { ps_product_attribute_combination: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_attribute_combination" do
    assert_difference('PsProductAttributeCombination.count', -1) do
      delete ps_product_attribute_combination_url(@ps_product_attribute_combination), as: :json
    end

    assert_response 204
  end
end
