require 'test_helper'

class PsAdminFiltersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_admin_filter = ps_admin_filter(:one)
  end

  test "should get index" do
    get ps_admin_filters_url, as: :json
    assert_response :success
  end

  test "should create ps_admin_filter" do
    assert_difference('PsAdminFilter.count') do
      post ps_admin_filters_url, params: { ps_admin_filter: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_admin_filter" do
    get ps_admin_filter_url(@ps_admin_filter), as: :json
    assert_response :success
  end

  test "should update ps_admin_filter" do
    patch ps_admin_filter_url(@ps_admin_filter), params: { ps_admin_filter: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_admin_filter" do
    assert_difference('PsAdminFilter.count', -1) do
      delete ps_admin_filter_url(@ps_admin_filter), as: :json
    end

    assert_response 204
  end
end
