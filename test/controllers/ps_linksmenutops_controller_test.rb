require 'test_helper'

class PsLinksmenutopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_linksmenutop = ps_linksmenutop(:one)
  end

  test "should get index" do
    get ps_linksmenutops_url, as: :json
    assert_response :success
  end

  test "should create ps_linksmenutop" do
    assert_difference('PsLinksmenutop.count') do
      post ps_linksmenutops_url, params: { ps_linksmenutop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_linksmenutop" do
    get ps_linksmenutop_url(@ps_linksmenutop), as: :json
    assert_response :success
  end

  test "should update ps_linksmenutop" do
    patch ps_linksmenutop_url(@ps_linksmenutop), params: { ps_linksmenutop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_linksmenutop" do
    assert_difference('PsLinksmenutop.count', -1) do
      delete ps_linksmenutop_url(@ps_linksmenutop), as: :json
    end

    assert_response 204
  end
end
