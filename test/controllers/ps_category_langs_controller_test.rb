require 'test_helper'

class PsCategoryLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_category_lang = ps_category_lang(:one)
  end

  test "should get index" do
    get ps_category_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_category_lang" do
    assert_difference('PsCategoryLang.count') do
      post ps_category_langs_url, params: { ps_category_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_category_lang" do
    get ps_category_lang_url(@ps_category_lang), as: :json
    assert_response :success
  end

  test "should update ps_category_lang" do
    patch ps_category_lang_url(@ps_category_lang), params: { ps_category_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_category_lang" do
    assert_difference('PsCategoryLang.count', -1) do
      delete ps_category_lang_url(@ps_category_lang), as: :json
    end

    assert_response 204
  end
end
