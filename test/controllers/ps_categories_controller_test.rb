require 'test_helper'

class PsCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_category = ps_category(:one)
  end

  test "should get index" do
    get ps_categories_url, as: :json
    assert_response :success
  end

  test "should create ps_category" do
    assert_difference('PsCategory.count') do
      post ps_categories_url, params: { ps_category: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_category" do
    get ps_category_url(@ps_category), as: :json
    assert_response :success
  end

  test "should update ps_category" do
    patch ps_category_url(@ps_category), params: { ps_category: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_category" do
    assert_difference('PsCategory.count', -1) do
      delete ps_category_url(@ps_category), as: :json
    end

    assert_response 204
  end
end
