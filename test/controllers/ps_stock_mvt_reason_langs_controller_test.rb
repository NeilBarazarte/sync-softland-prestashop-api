require 'test_helper'

class PsStockMvtReasonLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_stock_mvt_reason_lang = ps_stock_mvt_reason_lang(:one)
  end

  test "should get index" do
    get ps_stock_mvt_reason_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_stock_mvt_reason_lang" do
    assert_difference('PsStockMvtReasonLang.count') do
      post ps_stock_mvt_reason_langs_url, params: { ps_stock_mvt_reason_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_stock_mvt_reason_lang" do
    get ps_stock_mvt_reason_lang_url(@ps_stock_mvt_reason_lang), as: :json
    assert_response :success
  end

  test "should update ps_stock_mvt_reason_lang" do
    patch ps_stock_mvt_reason_lang_url(@ps_stock_mvt_reason_lang), params: { ps_stock_mvt_reason_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_stock_mvt_reason_lang" do
    assert_difference('PsStockMvtReasonLang.count', -1) do
      delete ps_stock_mvt_reason_lang_url(@ps_stock_mvt_reason_lang), as: :json
    end

    assert_response 204
  end
end
