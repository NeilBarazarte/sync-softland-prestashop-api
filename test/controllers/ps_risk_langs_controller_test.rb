require 'test_helper'

class PsRiskLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_risk_lang = ps_risk_lang(:one)
  end

  test "should get index" do
    get ps_risk_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_risk_lang" do
    assert_difference('PsRiskLang.count') do
      post ps_risk_langs_url, params: { ps_risk_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_risk_lang" do
    get ps_risk_lang_url(@ps_risk_lang), as: :json
    assert_response :success
  end

  test "should update ps_risk_lang" do
    patch ps_risk_lang_url(@ps_risk_lang), params: { ps_risk_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_risk_lang" do
    assert_difference('PsRiskLang.count', -1) do
      delete ps_risk_lang_url(@ps_risk_lang), as: :json
    end

    assert_response 204
  end
end
