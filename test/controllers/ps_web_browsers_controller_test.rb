require 'test_helper'

class PsWebBrowsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_web_browser = ps_web_browser(:one)
  end

  test "should get index" do
    get ps_web_browsers_url, as: :json
    assert_response :success
  end

  test "should create ps_web_browser" do
    assert_difference('PsWebBrowser.count') do
      post ps_web_browsers_url, params: { ps_web_browser: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_web_browser" do
    get ps_web_browser_url(@ps_web_browser), as: :json
    assert_response :success
  end

  test "should update ps_web_browser" do
    patch ps_web_browser_url(@ps_web_browser), params: { ps_web_browser: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_web_browser" do
    assert_difference('PsWebBrowser.count', -1) do
      delete ps_web_browser_url(@ps_web_browser), as: :json
    end

    assert_response 204
  end
end
