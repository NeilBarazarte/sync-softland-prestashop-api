require 'test_helper'

class PsCartsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart = ps_cart(:one)
  end

  test "should get index" do
    get ps_carts_url, as: :json
    assert_response :success
  end

  test "should create ps_cart" do
    assert_difference('PsCart.count') do
      post ps_carts_url, params: { ps_cart: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart" do
    get ps_cart_url(@ps_cart), as: :json
    assert_response :success
  end

  test "should update ps_cart" do
    patch ps_cart_url(@ps_cart), params: { ps_cart: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart" do
    assert_difference('PsCart.count', -1) do
      delete ps_cart_url(@ps_cart), as: :json
    end

    assert_response 204
  end
end
