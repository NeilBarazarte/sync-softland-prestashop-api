require 'test_helper'

class PsTimezonesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_timezone = ps_timezone(:one)
  end

  test "should get index" do
    get ps_timezones_url, as: :json
    assert_response :success
  end

  test "should create ps_timezone" do
    assert_difference('PsTimezone.count') do
      post ps_timezones_url, params: { ps_timezone: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_timezone" do
    get ps_timezone_url(@ps_timezone), as: :json
    assert_response :success
  end

  test "should update ps_timezone" do
    patch ps_timezone_url(@ps_timezone), params: { ps_timezone: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_timezone" do
    assert_difference('PsTimezone.count', -1) do
      delete ps_timezone_url(@ps_timezone), as: :json
    end

    assert_response 204
  end
end
