require 'test_helper'

class PsStoreShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_store_shop = ps_store_shop(:one)
  end

  test "should get index" do
    get ps_store_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_store_shop" do
    assert_difference('PsStoreShop.count') do
      post ps_store_shops_url, params: { ps_store_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_store_shop" do
    get ps_store_shop_url(@ps_store_shop), as: :json
    assert_response :success
  end

  test "should update ps_store_shop" do
    patch ps_store_shop_url(@ps_store_shop), params: { ps_store_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_store_shop" do
    assert_difference('PsStoreShop.count', -1) do
      delete ps_store_shop_url(@ps_store_shop), as: :json
    end

    assert_response 204
  end
end
