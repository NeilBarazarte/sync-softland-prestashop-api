require 'test_helper'

class PsModuleCarriersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_module_carrier = ps_module_carrier(:one)
  end

  test "should get index" do
    get ps_module_carriers_url, as: :json
    assert_response :success
  end

  test "should create ps_module_carrier" do
    assert_difference('PsModuleCarrier.count') do
      post ps_module_carriers_url, params: { ps_module_carrier: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_module_carrier" do
    get ps_module_carrier_url(@ps_module_carrier), as: :json
    assert_response :success
  end

  test "should update ps_module_carrier" do
    patch ps_module_carrier_url(@ps_module_carrier), params: { ps_module_carrier: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_module_carrier" do
    assert_difference('PsModuleCarrier.count', -1) do
      delete ps_module_carrier_url(@ps_module_carrier), as: :json
    end

    assert_response 204
  end
end
