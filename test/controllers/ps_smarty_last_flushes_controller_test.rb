require 'test_helper'

class PsSmartyLastFlushesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_smarty_last_flush = ps_smarty_last_flush(:one)
  end

  test "should get index" do
    get ps_smarty_last_flushes_url, as: :json
    assert_response :success
  end

  test "should create ps_smarty_last_flush" do
    assert_difference('PsSmartyLastFlush.count') do
      post ps_smarty_last_flushes_url, params: { ps_smarty_last_flush: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_smarty_last_flush" do
    get ps_smarty_last_flush_url(@ps_smarty_last_flush), as: :json
    assert_response :success
  end

  test "should update ps_smarty_last_flush" do
    patch ps_smarty_last_flush_url(@ps_smarty_last_flush), params: { ps_smarty_last_flush: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_smarty_last_flush" do
    assert_difference('PsSmartyLastFlush.count', -1) do
      delete ps_smarty_last_flush_url(@ps_smarty_last_flush), as: :json
    end

    assert_response 204
  end
end
