require 'test_helper'

class PsAdviceLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_advice_lang = ps_advice_lang(:one)
  end

  test "should get index" do
    get ps_advice_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_advice_lang" do
    assert_difference('PsAdviceLang.count') do
      post ps_advice_langs_url, params: { ps_advice_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_advice_lang" do
    get ps_advice_lang_url(@ps_advice_lang), as: :json
    assert_response :success
  end

  test "should update ps_advice_lang" do
    patch ps_advice_lang_url(@ps_advice_lang), params: { ps_advice_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_advice_lang" do
    assert_difference('PsAdviceLang.count', -1) do
      delete ps_advice_lang_url(@ps_advice_lang), as: :json
    end

    assert_response 204
  end
end
