require 'test_helper'

class PsEmailsubscriptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_emailsubscription = ps_emailsubscription(:one)
  end

  test "should get index" do
    get ps_emailsubscriptions_url, as: :json
    assert_response :success
  end

  test "should create ps_emailsubscription" do
    assert_difference('PsEmailsubscription.count') do
      post ps_emailsubscriptions_url, params: { ps_emailsubscription: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_emailsubscription" do
    get ps_emailsubscription_url(@ps_emailsubscription), as: :json
    assert_response :success
  end

  test "should update ps_emailsubscription" do
    patch ps_emailsubscription_url(@ps_emailsubscription), params: { ps_emailsubscription: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_emailsubscription" do
    assert_difference('PsEmailsubscription.count', -1) do
      delete ps_emailsubscription_url(@ps_emailsubscription), as: :json
    end

    assert_response 204
  end
end
