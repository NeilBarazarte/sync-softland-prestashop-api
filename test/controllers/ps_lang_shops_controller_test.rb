require 'test_helper'

class PsLangShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_lang_shop = ps_lang_shop(:one)
  end

  test "should get index" do
    get ps_lang_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_lang_shop" do
    assert_difference('PsLangShop.count') do
      post ps_lang_shops_url, params: { ps_lang_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_lang_shop" do
    get ps_lang_shop_url(@ps_lang_shop), as: :json
    assert_response :success
  end

  test "should update ps_lang_shop" do
    patch ps_lang_shop_url(@ps_lang_shop), params: { ps_lang_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_lang_shop" do
    assert_difference('PsLangShop.count', -1) do
      delete ps_lang_shop_url(@ps_lang_shop), as: :json
    end

    assert_response 204
  end
end
