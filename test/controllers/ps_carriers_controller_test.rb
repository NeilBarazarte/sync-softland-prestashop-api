require 'test_helper'

class PsCarriersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_carrier = ps_carrier(:one)
  end

  test "should get index" do
    get ps_carriers_url, as: :json
    assert_response :success
  end

  test "should create ps_carrier" do
    assert_difference('PsCarrier.count') do
      post ps_carriers_url, params: { ps_carrier: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_carrier" do
    get ps_carrier_url(@ps_carrier), as: :json
    assert_response :success
  end

  test "should update ps_carrier" do
    patch ps_carrier_url(@ps_carrier), params: { ps_carrier: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_carrier" do
    assert_difference('PsCarrier.count', -1) do
      delete ps_carrier_url(@ps_carrier), as: :json
    end

    assert_response 204
  end
end
