require 'test_helper'

class PsLayeredFiltersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_layered_filter = ps_layered_filter(:one)
  end

  test "should get index" do
    get ps_layered_filters_url, as: :json
    assert_response :success
  end

  test "should create ps_layered_filter" do
    assert_difference('PsLayeredFilter.count') do
      post ps_layered_filters_url, params: { ps_layered_filter: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_layered_filter" do
    get ps_layered_filter_url(@ps_layered_filter), as: :json
    assert_response :success
  end

  test "should update ps_layered_filter" do
    patch ps_layered_filter_url(@ps_layered_filter), params: { ps_layered_filter: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_layered_filter" do
    assert_difference('PsLayeredFilter.count', -1) do
      delete ps_layered_filter_url(@ps_layered_filter), as: :json
    end

    assert_response 204
  end
end
