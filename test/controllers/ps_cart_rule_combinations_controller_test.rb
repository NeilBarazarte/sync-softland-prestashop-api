require 'test_helper'

class PsCartRuleCombinationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_rule_combination = ps_cart_rule_combination(:one)
  end

  test "should get index" do
    get ps_cart_rule_combinations_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_rule_combination" do
    assert_difference('PsCartRuleCombination.count') do
      post ps_cart_rule_combinations_url, params: { ps_cart_rule_combination: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_rule_combination" do
    get ps_cart_rule_combination_url(@ps_cart_rule_combination), as: :json
    assert_response :success
  end

  test "should update ps_cart_rule_combination" do
    patch ps_cart_rule_combination_url(@ps_cart_rule_combination), params: { ps_cart_rule_combination: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_rule_combination" do
    assert_difference('PsCartRuleCombination.count', -1) do
      delete ps_cart_rule_combination_url(@ps_cart_rule_combination), as: :json
    end

    assert_response 204
  end
end
