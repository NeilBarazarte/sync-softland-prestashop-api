require 'test_helper'

class PsImagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_image = ps_image(:one)
  end

  test "should get index" do
    get ps_images_url, as: :json
    assert_response :success
  end

  test "should create ps_image" do
    assert_difference('PsImage.count') do
      post ps_images_url, params: { ps_image: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_image" do
    get ps_image_url(@ps_image), as: :json
    assert_response :success
  end

  test "should update ps_image" do
    patch ps_image_url(@ps_image), params: { ps_image: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_image" do
    assert_difference('PsImage.count', -1) do
      delete ps_image_url(@ps_image), as: :json
    end

    assert_response 204
  end
end
