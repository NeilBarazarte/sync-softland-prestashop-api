require 'test_helper'

class PsTaxRulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tax_rule = ps_tax_rule(:one)
  end

  test "should get index" do
    get ps_tax_rules_url, as: :json
    assert_response :success
  end

  test "should create ps_tax_rule" do
    assert_difference('PsTaxRule.count') do
      post ps_tax_rules_url, params: { ps_tax_rule: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tax_rule" do
    get ps_tax_rule_url(@ps_tax_rule), as: :json
    assert_response :success
  end

  test "should update ps_tax_rule" do
    patch ps_tax_rule_url(@ps_tax_rule), params: { ps_tax_rule: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tax_rule" do
    assert_difference('PsTaxRule.count', -1) do
      delete ps_tax_rule_url(@ps_tax_rule), as: :json
    end

    assert_response 204
  end
end
