require 'test_helper'

class PsGenderLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_gender_lang = ps_gender_lang(:one)
  end

  test "should get index" do
    get ps_gender_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_gender_lang" do
    assert_difference('PsGenderLang.count') do
      post ps_gender_langs_url, params: { ps_gender_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_gender_lang" do
    get ps_gender_lang_url(@ps_gender_lang), as: :json
    assert_response :success
  end

  test "should update ps_gender_lang" do
    patch ps_gender_lang_url(@ps_gender_lang), params: { ps_gender_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_gender_lang" do
    assert_difference('PsGenderLang.count', -1) do
      delete ps_gender_lang_url(@ps_gender_lang), as: :json
    end

    assert_response 204
  end
end
