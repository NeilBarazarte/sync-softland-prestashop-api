require 'test_helper'

class PsProductLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_lang = ps_product_lang(:one)
  end

  test "should get index" do
    get ps_product_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_product_lang" do
    assert_difference('PsProductLang.count') do
      post ps_product_langs_url, params: { ps_product_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_lang" do
    get ps_product_lang_url(@ps_product_lang), as: :json
    assert_response :success
  end

  test "should update ps_product_lang" do
    patch ps_product_lang_url(@ps_product_lang), params: { ps_product_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_lang" do
    assert_difference('PsProductLang.count', -1) do
      delete ps_product_lang_url(@ps_product_lang), as: :json
    end

    assert_response 204
  end
end
