require 'test_helper'

class PsCarrierLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_carrier_lang = ps_carrier_lang(:one)
  end

  test "should get index" do
    get ps_carrier_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_carrier_lang" do
    assert_difference('PsCarrierLang.count') do
      post ps_carrier_langs_url, params: { ps_carrier_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_carrier_lang" do
    get ps_carrier_lang_url(@ps_carrier_lang), as: :json
    assert_response :success
  end

  test "should update ps_carrier_lang" do
    patch ps_carrier_lang_url(@ps_carrier_lang), params: { ps_carrier_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_carrier_lang" do
    assert_difference('PsCarrierLang.count', -1) do
      delete ps_carrier_lang_url(@ps_carrier_lang), as: :json
    end

    assert_response 204
  end
end
