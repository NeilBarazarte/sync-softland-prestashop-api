require 'test_helper'

class PsOrderReturnStateLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_order_return_state_lang = ps_order_return_state_lang(:one)
  end

  test "should get index" do
    get ps_order_return_state_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_order_return_state_lang" do
    assert_difference('PsOrderReturnStateLang.count') do
      post ps_order_return_state_langs_url, params: { ps_order_return_state_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_order_return_state_lang" do
    get ps_order_return_state_lang_url(@ps_order_return_state_lang), as: :json
    assert_response :success
  end

  test "should update ps_order_return_state_lang" do
    patch ps_order_return_state_lang_url(@ps_order_return_state_lang), params: { ps_order_return_state_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_order_return_state_lang" do
    assert_difference('PsOrderReturnStateLang.count', -1) do
      delete ps_order_return_state_lang_url(@ps_order_return_state_lang), as: :json
    end

    assert_response 204
  end
end
