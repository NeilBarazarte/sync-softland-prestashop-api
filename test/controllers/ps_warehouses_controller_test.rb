require 'test_helper'

class PsWarehousesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_warehouse = ps_warehouse(:one)
  end

  test "should get index" do
    get ps_warehouses_url, as: :json
    assert_response :success
  end

  test "should create ps_warehouse" do
    assert_difference('PsWarehouse.count') do
      post ps_warehouses_url, params: { ps_warehouse: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_warehouse" do
    get ps_warehouse_url(@ps_warehouse), as: :json
    assert_response :success
  end

  test "should update ps_warehouse" do
    patch ps_warehouse_url(@ps_warehouse), params: { ps_warehouse: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_warehouse" do
    assert_difference('PsWarehouse.count', -1) do
      delete ps_warehouse_url(@ps_warehouse), as: :json
    end

    assert_response 204
  end
end
