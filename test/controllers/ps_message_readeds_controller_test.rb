require 'test_helper'

class PsMessageReadedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_message_readed = ps_message_readed(:one)
  end

  test "should get index" do
    get ps_message_readeds_url, as: :json
    assert_response :success
  end

  test "should create ps_message_readed" do
    assert_difference('PsMessageReaded.count') do
      post ps_message_readeds_url, params: { ps_message_readed: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_message_readed" do
    get ps_message_readed_url(@ps_message_readed), as: :json
    assert_response :success
  end

  test "should update ps_message_readed" do
    patch ps_message_readed_url(@ps_message_readed), params: { ps_message_readed: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_message_readed" do
    assert_difference('PsMessageReaded.count', -1) do
      delete ps_message_readed_url(@ps_message_readed), as: :json
    end

    assert_response 204
  end
end
