require 'test_helper'

class PsCmsCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cms_category = ps_cms_category(:one)
  end

  test "should get index" do
    get ps_cms_categories_url, as: :json
    assert_response :success
  end

  test "should create ps_cms_category" do
    assert_difference('PsCmsCategory.count') do
      post ps_cms_categories_url, params: { ps_cms_category: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cms_category" do
    get ps_cms_category_url(@ps_cms_category), as: :json
    assert_response :success
  end

  test "should update ps_cms_category" do
    patch ps_cms_category_url(@ps_cms_category), params: { ps_cms_category: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cms_category" do
    assert_difference('PsCmsCategory.count', -1) do
      delete ps_cms_category_url(@ps_cms_category), as: :json
    end

    assert_response 204
  end
end
