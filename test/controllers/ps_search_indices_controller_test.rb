require 'test_helper'

class PsSearchIndicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_search_index = ps_search_index(:one)
  end

  test "should get index" do
    get ps_search_indices_url, as: :json
    assert_response :success
  end

  test "should create ps_search_index" do
    assert_difference('PsSearchIndex.count') do
      post ps_search_indices_url, params: { ps_search_index: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_search_index" do
    get ps_search_index_url(@ps_search_index), as: :json
    assert_response :success
  end

  test "should update ps_search_index" do
    patch ps_search_index_url(@ps_search_index), params: { ps_search_index: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_search_index" do
    assert_difference('PsSearchIndex.count', -1) do
      delete ps_search_index_url(@ps_search_index), as: :json
    end

    assert_response 204
  end
end
