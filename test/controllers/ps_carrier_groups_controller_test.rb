require 'test_helper'

class PsCarrierGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_carrier_group = ps_carrier_group(:one)
  end

  test "should get index" do
    get ps_carrier_groups_url, as: :json
    assert_response :success
  end

  test "should create ps_carrier_group" do
    assert_difference('PsCarrierGroup.count') do
      post ps_carrier_groups_url, params: { ps_carrier_group: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_carrier_group" do
    get ps_carrier_group_url(@ps_carrier_group), as: :json
    assert_response :success
  end

  test "should update ps_carrier_group" do
    patch ps_carrier_group_url(@ps_carrier_group), params: { ps_carrier_group: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_carrier_group" do
    assert_difference('PsCarrierGroup.count', -1) do
      delete ps_carrier_group_url(@ps_carrier_group), as: :json
    end

    assert_response 204
  end
end
