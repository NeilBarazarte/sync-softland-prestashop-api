require 'test_helper'

class PsProfilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_profile = ps_profile(:one)
  end

  test "should get index" do
    get ps_profiles_url, as: :json
    assert_response :success
  end

  test "should create ps_profile" do
    assert_difference('PsProfile.count') do
      post ps_profiles_url, params: { ps_profile: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_profile" do
    get ps_profile_url(@ps_profile), as: :json
    assert_response :success
  end

  test "should update ps_profile" do
    patch ps_profile_url(@ps_profile), params: { ps_profile: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_profile" do
    assert_difference('PsProfile.count', -1) do
      delete ps_profile_url(@ps_profile), as: :json
    end

    assert_response 204
  end
end
