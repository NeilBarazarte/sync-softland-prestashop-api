require 'test_helper'

class PsSearchWordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_search_word = ps_search_word(:one)
  end

  test "should get index" do
    get ps_search_words_url, as: :json
    assert_response :success
  end

  test "should create ps_search_word" do
    assert_difference('PsSearchWord.count') do
      post ps_search_words_url, params: { ps_search_word: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_search_word" do
    get ps_search_word_url(@ps_search_word), as: :json
    assert_response :success
  end

  test "should update ps_search_word" do
    patch ps_search_word_url(@ps_search_word), params: { ps_search_word: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_search_word" do
    assert_difference('PsSearchWord.count', -1) do
      delete ps_search_word_url(@ps_search_word), as: :json
    end

    assert_response 204
  end
end
