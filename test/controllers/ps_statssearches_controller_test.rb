require 'test_helper'

class PsStatssearchesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_statssearch = ps_statssearch(:one)
  end

  test "should get index" do
    get ps_statssearches_url, as: :json
    assert_response :success
  end

  test "should create ps_statssearch" do
    assert_difference('PsStatssearch.count') do
      post ps_statssearches_url, params: { ps_statssearch: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_statssearch" do
    get ps_statssearch_url(@ps_statssearch), as: :json
    assert_response :success
  end

  test "should update ps_statssearch" do
    patch ps_statssearch_url(@ps_statssearch), params: { ps_statssearch: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_statssearch" do
    assert_difference('PsStatssearch.count', -1) do
      delete ps_statssearch_url(@ps_statssearch), as: :json
    end

    assert_response 204
  end
end
