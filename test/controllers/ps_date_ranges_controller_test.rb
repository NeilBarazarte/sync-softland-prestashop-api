require 'test_helper'

class PsDateRangesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_date_range = ps_date_range(:one)
  end

  test "should get index" do
    get ps_date_ranges_url, as: :json
    assert_response :success
  end

  test "should create ps_date_range" do
    assert_difference('PsDateRange.count') do
      post ps_date_ranges_url, params: { ps_date_range: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_date_range" do
    get ps_date_range_url(@ps_date_range), as: :json
    assert_response :success
  end

  test "should update ps_date_range" do
    patch ps_date_range_url(@ps_date_range), params: { ps_date_range: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_date_range" do
    assert_difference('PsDateRange.count', -1) do
      delete ps_date_range_url(@ps_date_range), as: :json
    end

    assert_response 204
  end
end
