require 'test_helper'

class PsStoreLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_store_lang = ps_store_lang(:one)
  end

  test "should get index" do
    get ps_store_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_store_lang" do
    assert_difference('PsStoreLang.count') do
      post ps_store_langs_url, params: { ps_store_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_store_lang" do
    get ps_store_lang_url(@ps_store_lang), as: :json
    assert_response :success
  end

  test "should update ps_store_lang" do
    patch ps_store_lang_url(@ps_store_lang), params: { ps_store_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_store_lang" do
    assert_difference('PsStoreLang.count', -1) do
      delete ps_store_lang_url(@ps_store_lang), as: :json
    end

    assert_response 204
  end
end
