require 'test_helper'

class PsCmsLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cms_lang = ps_cms_lang(:one)
  end

  test "should get index" do
    get ps_cms_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_cms_lang" do
    assert_difference('PsCmsLang.count') do
      post ps_cms_langs_url, params: { ps_cms_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cms_lang" do
    get ps_cms_lang_url(@ps_cms_lang), as: :json
    assert_response :success
  end

  test "should update ps_cms_lang" do
    patch ps_cms_lang_url(@ps_cms_lang), params: { ps_cms_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cms_lang" do
    assert_difference('PsCmsLang.count', -1) do
      delete ps_cms_lang_url(@ps_cms_lang), as: :json
    end

    assert_response 204
  end
end
