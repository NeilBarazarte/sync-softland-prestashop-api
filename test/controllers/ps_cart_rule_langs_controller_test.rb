require 'test_helper'

class PsCartRuleLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cart_rule_lang = ps_cart_rule_lang(:one)
  end

  test "should get index" do
    get ps_cart_rule_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_cart_rule_lang" do
    assert_difference('PsCartRuleLang.count') do
      post ps_cart_rule_langs_url, params: { ps_cart_rule_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cart_rule_lang" do
    get ps_cart_rule_lang_url(@ps_cart_rule_lang), as: :json
    assert_response :success
  end

  test "should update ps_cart_rule_lang" do
    patch ps_cart_rule_lang_url(@ps_cart_rule_lang), params: { ps_cart_rule_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cart_rule_lang" do
    assert_difference('PsCartRuleLang.count', -1) do
      delete ps_cart_rule_lang_url(@ps_cart_rule_lang), as: :json
    end

    assert_response 204
  end
end
