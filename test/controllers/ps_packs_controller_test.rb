require 'test_helper'

class PsPacksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_pack = ps_pack(:one)
  end

  test "should get index" do
    get ps_packs_url, as: :json
    assert_response :success
  end

  test "should create ps_pack" do
    assert_difference('PsPack.count') do
      post ps_packs_url, params: { ps_pack: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_pack" do
    get ps_pack_url(@ps_pack), as: :json
    assert_response :success
  end

  test "should update ps_pack" do
    patch ps_pack_url(@ps_pack), params: { ps_pack: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_pack" do
    assert_difference('PsPack.count', -1) do
      delete ps_pack_url(@ps_pack), as: :json
    end

    assert_response 204
  end
end
