require 'test_helper'

class PsCustomizationFieldLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_customization_field_lang = ps_customization_field_lang(:one)
  end

  test "should get index" do
    get ps_customization_field_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_customization_field_lang" do
    assert_difference('PsCustomizationFieldLang.count') do
      post ps_customization_field_langs_url, params: { ps_customization_field_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_customization_field_lang" do
    get ps_customization_field_lang_url(@ps_customization_field_lang), as: :json
    assert_response :success
  end

  test "should update ps_customization_field_lang" do
    patch ps_customization_field_lang_url(@ps_customization_field_lang), params: { ps_customization_field_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_customization_field_lang" do
    assert_difference('PsCustomizationFieldLang.count', -1) do
      delete ps_customization_field_lang_url(@ps_customization_field_lang), as: :json
    end

    assert_response 204
  end
end
