require 'test_helper'

class PsSupplyOrderStatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_supply_order_state = ps_supply_order_state(:one)
  end

  test "should get index" do
    get ps_supply_order_states_url, as: :json
    assert_response :success
  end

  test "should create ps_supply_order_state" do
    assert_difference('PsSupplyOrderState.count') do
      post ps_supply_order_states_url, params: { ps_supply_order_state: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_supply_order_state" do
    get ps_supply_order_state_url(@ps_supply_order_state), as: :json
    assert_response :success
  end

  test "should update ps_supply_order_state" do
    patch ps_supply_order_state_url(@ps_supply_order_state), params: { ps_supply_order_state: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_supply_order_state" do
    assert_difference('PsSupplyOrderState.count', -1) do
      delete ps_supply_order_state_url(@ps_supply_order_state), as: :json
    end

    assert_response 204
  end
end
