require 'test_helper'

class PsProductSuppliersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_supplier = ps_product_supplier(:one)
  end

  test "should get index" do
    get ps_product_suppliers_url, as: :json
    assert_response :success
  end

  test "should create ps_product_supplier" do
    assert_difference('PsProductSupplier.count') do
      post ps_product_suppliers_url, params: { ps_product_supplier: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_supplier" do
    get ps_product_supplier_url(@ps_product_supplier), as: :json
    assert_response :success
  end

  test "should update ps_product_supplier" do
    patch ps_product_supplier_url(@ps_product_supplier), params: { ps_product_supplier: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_supplier" do
    assert_difference('PsProductSupplier.count', -1) do
      delete ps_product_supplier_url(@ps_product_supplier), as: :json
    end

    assert_response 204
  end
end
