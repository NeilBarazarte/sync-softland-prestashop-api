require 'test_helper'

class PsCmsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_cm = ps_cm(:one)
  end

  test "should get index" do
    get ps_cms_url, as: :json
    assert_response :success
  end

  test "should create ps_cm" do
    assert_difference('PsCm.count') do
      post ps_cms_url, params: { ps_cm: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_cm" do
    get ps_cm_url(@ps_cm), as: :json
    assert_response :success
  end

  test "should update ps_cm" do
    patch ps_cm_url(@ps_cm), params: { ps_cm: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_cm" do
    assert_difference('PsCm.count', -1) do
      delete ps_cm_url(@ps_cm), as: :json
    end

    assert_response 204
  end
end
