require 'test_helper'

class PsHomeslidersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_homeslider = ps_homeslider(:one)
  end

  test "should get index" do
    get ps_homesliders_url, as: :json
    assert_response :success
  end

  test "should create ps_homeslider" do
    assert_difference('PsHomeslider.count') do
      post ps_homesliders_url, params: { ps_homeslider: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_homeslider" do
    get ps_homeslider_url(@ps_homeslider), as: :json
    assert_response :success
  end

  test "should update ps_homeslider" do
    patch ps_homeslider_url(@ps_homeslider), params: { ps_homeslider: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_homeslider" do
    assert_difference('PsHomeslider.count', -1) do
      delete ps_homeslider_url(@ps_homeslider), as: :json
    end

    assert_response 204
  end
end
