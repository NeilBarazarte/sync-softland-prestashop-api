require 'test_helper'

class PsStockMvtsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_stock_mvt = ps_stock_mvt(:one)
  end

  test "should get index" do
    get ps_stock_mvts_url, as: :json
    assert_response :success
  end

  test "should create ps_stock_mvt" do
    assert_difference('PsStockMvt.count') do
      post ps_stock_mvts_url, params: { ps_stock_mvt: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_stock_mvt" do
    get ps_stock_mvt_url(@ps_stock_mvt), as: :json
    assert_response :success
  end

  test "should update ps_stock_mvt" do
    patch ps_stock_mvt_url(@ps_stock_mvt), params: { ps_stock_mvt: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_stock_mvt" do
    assert_difference('PsStockMvt.count', -1) do
      delete ps_stock_mvt_url(@ps_stock_mvt), as: :json
    end

    assert_response 204
  end
end
