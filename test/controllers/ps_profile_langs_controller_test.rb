require 'test_helper'

class PsProfileLangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_profile_lang = ps_profile_lang(:one)
  end

  test "should get index" do
    get ps_profile_langs_url, as: :json
    assert_response :success
  end

  test "should create ps_profile_lang" do
    assert_difference('PsProfileLang.count') do
      post ps_profile_langs_url, params: { ps_profile_lang: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_profile_lang" do
    get ps_profile_lang_url(@ps_profile_lang), as: :json
    assert_response :success
  end

  test "should update ps_profile_lang" do
    patch ps_profile_lang_url(@ps_profile_lang), params: { ps_profile_lang: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_profile_lang" do
    assert_difference('PsProfileLang.count', -1) do
      delete ps_profile_lang_url(@ps_profile_lang), as: :json
    end

    assert_response 204
  end
end
