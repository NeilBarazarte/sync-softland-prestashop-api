require 'test_helper'

class PsTaxRulesGroupShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_tax_rules_group_shop = ps_tax_rules_group_shop(:one)
  end

  test "should get index" do
    get ps_tax_rules_group_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_tax_rules_group_shop" do
    assert_difference('PsTaxRulesGroupShop.count') do
      post ps_tax_rules_group_shops_url, params: { ps_tax_rules_group_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_tax_rules_group_shop" do
    get ps_tax_rules_group_shop_url(@ps_tax_rules_group_shop), as: :json
    assert_response :success
  end

  test "should update ps_tax_rules_group_shop" do
    patch ps_tax_rules_group_shop_url(@ps_tax_rules_group_shop), params: { ps_tax_rules_group_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_tax_rules_group_shop" do
    assert_difference('PsTaxRulesGroupShop.count', -1) do
      delete ps_tax_rules_group_shop_url(@ps_tax_rules_group_shop), as: :json
    end

    assert_response 204
  end
end
