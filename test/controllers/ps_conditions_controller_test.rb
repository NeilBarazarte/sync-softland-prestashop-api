require 'test_helper'

class PsConditionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_condition = ps_condition(:one)
  end

  test "should get index" do
    get ps_conditions_url, as: :json
    assert_response :success
  end

  test "should create ps_condition" do
    assert_difference('PsCondition.count') do
      post ps_conditions_url, params: { ps_condition: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_condition" do
    get ps_condition_url(@ps_condition), as: :json
    assert_response :success
  end

  test "should update ps_condition" do
    patch ps_condition_url(@ps_condition), params: { ps_condition: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_condition" do
    assert_difference('PsCondition.count', -1) do
      delete ps_condition_url(@ps_condition), as: :json
    end

    assert_response 204
  end
end
