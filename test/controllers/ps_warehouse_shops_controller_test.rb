require 'test_helper'

class PsWarehouseShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_warehouse_shop = ps_warehouse_shop(:one)
  end

  test "should get index" do
    get ps_warehouse_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_warehouse_shop" do
    assert_difference('PsWarehouseShop.count') do
      post ps_warehouse_shops_url, params: { ps_warehouse_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_warehouse_shop" do
    get ps_warehouse_shop_url(@ps_warehouse_shop), as: :json
    assert_response :success
  end

  test "should update ps_warehouse_shop" do
    patch ps_warehouse_shop_url(@ps_warehouse_shop), params: { ps_warehouse_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_warehouse_shop" do
    assert_difference('PsWarehouseShop.count', -1) do
      delete ps_warehouse_shop_url(@ps_warehouse_shop), as: :json
    end

    assert_response 204
  end
end
