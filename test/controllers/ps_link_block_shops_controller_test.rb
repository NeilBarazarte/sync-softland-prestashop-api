require 'test_helper'

class PsLinkBlockShopsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_link_block_shop = ps_link_block_shop(:one)
  end

  test "should get index" do
    get ps_link_block_shops_url, as: :json
    assert_response :success
  end

  test "should create ps_link_block_shop" do
    assert_difference('PsLinkBlockShop.count') do
      post ps_link_block_shops_url, params: { ps_link_block_shop: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_link_block_shop" do
    get ps_link_block_shop_url(@ps_link_block_shop), as: :json
    assert_response :success
  end

  test "should update ps_link_block_shop" do
    patch ps_link_block_shop_url(@ps_link_block_shop), params: { ps_link_block_shop: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_link_block_shop" do
    assert_difference('PsLinkBlockShop.count', -1) do
      delete ps_link_block_shop_url(@ps_link_block_shop), as: :json
    end

    assert_response 204
  end
end
