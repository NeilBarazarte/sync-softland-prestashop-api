require 'test_helper'

class PsConfigurationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_configuration = ps_configuration(:one)
  end

  test "should get index" do
    get ps_configurations_url, as: :json
    assert_response :success
  end

  test "should create ps_configuration" do
    assert_difference('PsConfiguration.count') do
      post ps_configurations_url, params: { ps_configuration: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_configuration" do
    get ps_configuration_url(@ps_configuration), as: :json
    assert_response :success
  end

  test "should update ps_configuration" do
    patch ps_configuration_url(@ps_configuration), params: { ps_configuration: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_configuration" do
    assert_difference('PsConfiguration.count', -1) do
      delete ps_configuration_url(@ps_configuration), as: :json
    end

    assert_response 204
  end
end
