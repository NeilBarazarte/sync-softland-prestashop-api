require 'test_helper'

class PsProductAttributesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ps_product_attribute = ps_product_attribute(:one)
  end

  test "should get index" do
    get ps_product_attributes_url, as: :json
    assert_response :success
  end

  test "should create ps_product_attribute" do
    assert_difference('PsProductAttribute.count') do
      post ps_product_attributes_url, params: { ps_product_attribute: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ps_product_attribute" do
    get ps_product_attribute_url(@ps_product_attribute), as: :json
    assert_response :success
  end

  test "should update ps_product_attribute" do
    patch ps_product_attribute_url(@ps_product_attribute), params: { ps_product_attribute: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ps_product_attribute" do
    assert_difference('PsProductAttribute.count', -1) do
      delete ps_product_attribute_url(@ps_product_attribute), as: :json
    end

    assert_response 204
  end
end
