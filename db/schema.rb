# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "ps_access", primary_key: ["id_profile", "id_authorization_role"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_profile", null: false, unsigned: true
    t.integer "id_authorization_role", null: false, unsigned: true
  end

  create_table "ps_accessory", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product_1", null: false, unsigned: true
    t.integer "id_product_2", null: false, unsigned: true
    t.index ["id_product_1", "id_product_2"], name: "accessory_product"
  end

  create_table "ps_address", primary_key: "id_address", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_country", null: false, unsigned: true
    t.integer "id_state", unsigned: true
    t.integer "id_customer", default: 0, null: false, unsigned: true
    t.integer "id_manufacturer", default: 0, null: false, unsigned: true
    t.integer "id_supplier", default: 0, null: false, unsigned: true
    t.integer "id_warehouse", default: 0, null: false, unsigned: true
    t.string "alias", limit: 32, null: false
    t.string "company"
    t.string "lastname", null: false
    t.string "firstname", null: false
    t.string "address1", limit: 128, null: false
    t.string "address2", limit: 128
    t.string "postcode", limit: 12
    t.string "city", limit: 64, null: false
    t.text "other"
    t.string "phone", limit: 32
    t.string "phone_mobile", limit: 32
    t.string "vat_number", limit: 32
    t.string "dni", limit: 16
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.boolean "active", default: true, null: false, unsigned: true
    t.boolean "deleted", default: false, null: false, unsigned: true
    t.index ["id_country"], name: "id_country"
    t.index ["id_customer"], name: "address_customer"
    t.index ["id_manufacturer"], name: "id_manufacturer"
    t.index ["id_state"], name: "id_state"
    t.index ["id_supplier"], name: "id_supplier"
    t.index ["id_warehouse"], name: "id_warehouse"
  end

  create_table "ps_address_format", primary_key: "id_country", id: :integer, unsigned: true, default: nil, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "format", default: "", null: false
  end

  create_table "ps_admin_filter", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "employee", null: false
    t.integer "shop", null: false
    t.string "controller", limit: 60, null: false
    t.string "action", limit: 100, null: false
    t.text "filter", limit: 4294967295, null: false
    t.index ["employee", "shop", "controller", "action"], name: "admin_filter_search_idx", unique: true
  end

  create_table "ps_advice", primary_key: "id_advice", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_ps_advice", null: false
    t.integer "id_tab", null: false
    t.text "ids_tab"
    t.boolean "validated", default: false, null: false, unsigned: true
    t.boolean "hide", default: false, null: false
    t.string "location", limit: 6, null: false
    t.string "selector"
    t.integer "start_day", default: 0, null: false
    t.integer "stop_day", default: 0, null: false
    t.integer "weight", default: 1
  end

  create_table "ps_advice_lang", primary_key: ["id_advice", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_advice", null: false
    t.integer "id_lang", null: false
    t.text "html"
  end

  create_table "ps_alias", primary_key: "id_alias", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "alias", null: false
    t.string "search", null: false
    t.boolean "active", default: true, null: false
    t.index ["alias"], name: "alias", unique: true
  end

  create_table "ps_attachment", primary_key: "id_attachment", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "file", limit: 40, null: false
    t.string "file_name", limit: 128, null: false
    t.bigint "file_size", default: 0, null: false, unsigned: true
    t.string "mime", limit: 128, null: false
  end

  create_table "ps_attachment_lang", primary_key: ["id_attachment", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_attachment", null: false, unsigned: true, auto_increment: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 32
    t.text "description"
  end

  create_table "ps_attribute", primary_key: "id_attribute", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_attribute_group", null: false
    t.string "color", limit: 32, null: false
    t.integer "position", null: false
    t.index ["id_attribute_group"], name: "attribute_group"
  end

  create_table "ps_attribute_group", primary_key: "id_attribute_group", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.boolean "is_color_group", null: false
    t.string "group_type", null: false
    t.integer "position", null: false
  end

  create_table "ps_attribute_group_lang", primary_key: ["id_attribute_group", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_attribute_group", null: false
    t.integer "id_lang", null: false
    t.string "name", limit: 128, null: false
    t.string "public_name", limit: 64, null: false
    t.index ["id_attribute_group"], name: "IDX_4653726C67A664FB"
  end

  create_table "ps_attribute_group_shop", primary_key: ["id_attribute_group", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_attribute_group", null: false
    t.integer "id_shop", null: false
    t.index ["id_attribute_group"], name: "IDX_DB30BAAC67A664FB"
    t.index ["id_shop"], name: "IDX_DB30BAAC274A50A0"
  end

  create_table "ps_attribute_impact", primary_key: "id_attribute_impact", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_attribute", null: false, unsigned: true
    t.decimal "weight", precision: 20, scale: 6, null: false
    t.decimal "price", precision: 17, scale: 2, null: false
    t.index ["id_product", "id_attribute"], name: "id_product", unique: true
  end

  create_table "ps_attribute_lang", primary_key: ["id_attribute", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_attribute", null: false
    t.integer "id_lang", null: false
    t.string "name", limit: 128, null: false
    t.index ["id_attribute"], name: "IDX_3ABE46A77A4F53DC"
  end

  create_table "ps_attribute_shop", primary_key: ["id_attribute", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_attribute", null: false
    t.integer "id_shop", null: false
    t.index ["id_attribute"], name: "IDX_A7DD8E677A4F53DC"
    t.index ["id_shop"], name: "IDX_A7DD8E67274A50A0"
  end

  create_table "ps_authorization_role", primary_key: "id_authorization_role", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "slug", null: false
    t.index ["slug"], name: "slug", unique: true
  end

  create_table "ps_badge", primary_key: "id_badge", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_ps_badge", null: false
    t.string "type", limit: 32, null: false
    t.integer "id_group", null: false
    t.integer "group_position", null: false
    t.integer "scoring", null: false
    t.integer "awb", default: 0
    t.boolean "validated", default: false, null: false, unsigned: true
  end

  create_table "ps_badge_lang", primary_key: ["id_badge", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_badge", null: false
    t.integer "id_lang", null: false
    t.string "name", limit: 64
    t.string "description"
    t.string "group_name"
  end

  create_table "ps_carrier", primary_key: "id_carrier", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_reference", null: false, unsigned: true
    t.integer "id_tax_rules_group", default: 0, unsigned: true
    t.string "name", limit: 64, null: false
    t.string "url"
    t.boolean "active", default: false, null: false, unsigned: true
    t.boolean "deleted", default: false, null: false, unsigned: true
    t.boolean "shipping_handling", default: true, null: false, unsigned: true
    t.boolean "range_behavior", default: false, null: false, unsigned: true
    t.boolean "is_module", default: false, null: false, unsigned: true
    t.boolean "is_free", default: false, null: false, unsigned: true
    t.boolean "shipping_external", default: false, null: false, unsigned: true
    t.boolean "need_range", default: false, null: false, unsigned: true
    t.string "external_module_name", limit: 64
    t.integer "shipping_method", default: 0, null: false
    t.integer "position", default: 0, null: false, unsigned: true
    t.integer "max_width", default: 0
    t.integer "max_height", default: 0
    t.integer "max_depth", default: 0
    t.decimal "max_weight", precision: 20, scale: 6, default: "0.0"
    t.integer "grade", default: 0
    t.index ["deleted", "active"], name: "deleted"
    t.index ["id_reference", "deleted", "active"], name: "reference"
    t.index ["id_tax_rules_group"], name: "id_tax_rules_group"
  end

  create_table "ps_carrier_group", primary_key: ["id_carrier", "id_group"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_carrier", null: false, unsigned: true
    t.integer "id_group", null: false, unsigned: true
  end

  create_table "ps_carrier_lang", primary_key: ["id_lang", "id_shop", "id_carrier"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_carrier", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "delay", limit: 512
  end

  create_table "ps_carrier_shop", primary_key: ["id_carrier", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_carrier", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_carrier_tax_rules_group_shop", primary_key: ["id_carrier", "id_tax_rules_group", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_carrier", null: false, unsigned: true
    t.integer "id_tax_rules_group", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
  end

  create_table "ps_carrier_zone", primary_key: ["id_carrier", "id_zone"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_carrier", null: false, unsigned: true
    t.integer "id_zone", null: false, unsigned: true
  end

  create_table "ps_cart", primary_key: "id_cart", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop_group", default: 1, null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_carrier", null: false, unsigned: true
    t.text "delivery_option", null: false
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_address_delivery", null: false, unsigned: true
    t.integer "id_address_invoice", null: false, unsigned: true
    t.integer "id_currency", null: false, unsigned: true
    t.integer "id_customer", null: false, unsigned: true
    t.integer "id_guest", null: false, unsigned: true
    t.string "secure_key", limit: 32, default: "-1", null: false
    t.boolean "recyclable", default: true, null: false, unsigned: true
    t.boolean "gift", default: false, null: false, unsigned: true
    t.text "gift_message"
    t.boolean "mobile_theme", default: false, null: false
    t.boolean "allow_seperated_package", default: false, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.text "checkout_session_data", limit: 16777215
    t.index ["id_address_delivery"], name: "id_address_delivery"
    t.index ["id_address_invoice"], name: "id_address_invoice"
    t.index ["id_carrier"], name: "id_carrier"
    t.index ["id_currency"], name: "id_currency"
    t.index ["id_customer"], name: "cart_customer"
    t.index ["id_guest"], name: "id_guest"
    t.index ["id_lang"], name: "id_lang"
    t.index ["id_shop", "date_add"], name: "id_shop"
    t.index ["id_shop", "date_upd"], name: "id_shop_2"
    t.index ["id_shop_group"], name: "id_shop_group"
  end

  create_table "ps_cart_cart_rule", primary_key: ["id_cart", "id_cart_rule"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart", null: false, unsigned: true
    t.integer "id_cart_rule", null: false, unsigned: true
    t.index ["id_cart_rule"], name: "id_cart_rule"
  end

  create_table "ps_cart_product", primary_key: ["id_cart", "id_product", "id_product_attribute", "id_customization", "id_address_delivery"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart", null: false, unsigned: true
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_address_delivery", default: 0, null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_product_attribute", default: 0, null: false, unsigned: true
    t.integer "id_customization", default: 0, null: false, unsigned: true
    t.integer "quantity", default: 0, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.index ["id_cart", "date_add", "id_product", "id_product_attribute"], name: "id_cart_order"
    t.index ["id_product_attribute"], name: "id_product_attribute"
  end

  create_table "ps_cart_rule", primary_key: "id_cart_rule", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_customer", default: 0, null: false, unsigned: true
    t.datetime "date_from", null: false
    t.datetime "date_to", null: false
    t.text "description"
    t.integer "quantity", default: 0, null: false, unsigned: true
    t.integer "quantity_per_user", default: 0, null: false, unsigned: true
    t.integer "priority", default: 1, null: false, unsigned: true
    t.boolean "partial_use", default: false, null: false, unsigned: true
    t.string "code", limit: 254, null: false
    t.decimal "minimum_amount", precision: 17, scale: 2, default: "0.0", null: false
    t.boolean "minimum_amount_tax", default: false, null: false
    t.integer "minimum_amount_currency", default: 0, null: false, unsigned: true
    t.boolean "minimum_amount_shipping", default: false, null: false
    t.boolean "country_restriction", default: false, null: false, unsigned: true
    t.boolean "carrier_restriction", default: false, null: false, unsigned: true
    t.boolean "group_restriction", default: false, null: false, unsigned: true
    t.boolean "cart_rule_restriction", default: false, null: false, unsigned: true
    t.boolean "product_restriction", default: false, null: false, unsigned: true
    t.boolean "shop_restriction", default: false, null: false, unsigned: true
    t.boolean "free_shipping", default: false, null: false
    t.decimal "reduction_percent", precision: 5, scale: 2, default: "0.0", null: false
    t.decimal "reduction_amount", precision: 17, scale: 2, default: "0.0", null: false
    t.boolean "reduction_tax", default: false, null: false, unsigned: true
    t.integer "reduction_currency", default: 0, null: false, unsigned: true
    t.integer "reduction_product", default: 0, null: false
    t.boolean "reduction_exclude_special", default: false, null: false, unsigned: true
    t.integer "gift_product", default: 0, null: false, unsigned: true
    t.integer "gift_product_attribute", default: 0, null: false, unsigned: true
    t.boolean "highlight", default: false, null: false, unsigned: true
    t.boolean "active", default: false, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.index ["date_from"], name: "date_from"
    t.index ["date_to"], name: "date_to"
    t.index ["group_restriction", "active", "date_to"], name: "group_restriction"
    t.index ["group_restriction", "active", "highlight", "date_to"], name: "group_restriction_2"
    t.index ["id_customer", "active", "date_to"], name: "id_customer"
    t.index ["id_customer", "active", "highlight", "date_to"], name: "id_customer_2"
  end

  create_table "ps_cart_rule_carrier", primary_key: ["id_cart_rule", "id_carrier"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart_rule", null: false, unsigned: true
    t.integer "id_carrier", null: false, unsigned: true
  end

  create_table "ps_cart_rule_combination", primary_key: ["id_cart_rule_1", "id_cart_rule_2"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart_rule_1", null: false, unsigned: true
    t.integer "id_cart_rule_2", null: false, unsigned: true
    t.index ["id_cart_rule_1"], name: "id_cart_rule_1"
    t.index ["id_cart_rule_2"], name: "id_cart_rule_2"
  end

  create_table "ps_cart_rule_country", primary_key: ["id_cart_rule", "id_country"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart_rule", null: false, unsigned: true
    t.integer "id_country", null: false, unsigned: true
  end

  create_table "ps_cart_rule_group", primary_key: ["id_cart_rule", "id_group"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart_rule", null: false, unsigned: true
    t.integer "id_group", null: false, unsigned: true
  end

  create_table "ps_cart_rule_lang", primary_key: ["id_cart_rule", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart_rule", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 254, null: false
  end

  create_table "ps_cart_rule_product_rule", primary_key: "id_product_rule", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product_rule_group", null: false, unsigned: true
    t.string "type", limit: 13, null: false
  end

  create_table "ps_cart_rule_product_rule_group", primary_key: "id_product_rule_group", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart_rule", null: false, unsigned: true
    t.integer "quantity", default: 1, null: false, unsigned: true
  end

  create_table "ps_cart_rule_product_rule_value", primary_key: ["id_product_rule", "id_item"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product_rule", null: false, unsigned: true
    t.integer "id_item", null: false, unsigned: true
  end

  create_table "ps_cart_rule_shop", primary_key: ["id_cart_rule", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart_rule", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
  end

  create_table "ps_category", primary_key: "id_category", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_parent", null: false, unsigned: true
    t.integer "id_shop_default", default: 1, null: false, unsigned: true
    t.integer "level_depth", limit: 1, default: 0, null: false, unsigned: true
    t.integer "nleft", default: 0, null: false, unsigned: true
    t.integer "nright", default: 0, null: false, unsigned: true
    t.boolean "active", default: false, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.integer "position", default: 0, null: false, unsigned: true
    t.boolean "is_root_category", default: false, null: false
    t.index ["active", "nleft"], name: "activenleft"
    t.index ["active", "nright"], name: "activenright"
    t.index ["id_parent"], name: "category_parent"
    t.index ["level_depth"], name: "level_depth"
    t.index ["nleft", "nright", "active"], name: "nleftrightactive"
    t.index ["nright"], name: "nright"
  end

  create_table "ps_category_group", primary_key: ["id_category", "id_group"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_category", null: false, unsigned: true
    t.integer "id_group", null: false, unsigned: true
    t.index ["id_category"], name: "id_category"
    t.index ["id_group"], name: "id_group"
  end

  create_table "ps_category_lang", primary_key: ["id_category", "id_shop", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_category", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 128, null: false
    t.text "description"
    t.string "link_rewrite", limit: 128, null: false
    t.string "meta_title", limit: 128
    t.string "meta_keywords"
    t.string "meta_description"
    t.index ["name"], name: "category_name"
  end

  create_table "ps_category_product", primary_key: ["id_category", "id_product"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_category", null: false, unsigned: true
    t.integer "id_product", null: false, unsigned: true
    t.integer "position", default: 0, null: false, unsigned: true
    t.index ["id_category", "position"], name: "id_category"
    t.index ["id_product"], name: "id_product"
  end

  create_table "ps_category_shop", primary_key: ["id_category", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_category", null: false
    t.integer "id_shop", null: false
    t.integer "position", default: 0, null: false, unsigned: true
  end

  create_table "ps_cms", primary_key: "id_cms", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cms_category", null: false, unsigned: true
    t.integer "position", default: 0, null: false, unsigned: true
    t.boolean "active", default: false, null: false, unsigned: true
    t.boolean "indexation", default: true, null: false, unsigned: true
  end

  create_table "ps_cms_category", primary_key: "id_cms_category", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_parent", null: false, unsigned: true
    t.integer "level_depth", limit: 1, default: 0, null: false, unsigned: true
    t.boolean "active", default: false, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.integer "position", default: 0, null: false, unsigned: true
    t.index ["id_parent"], name: "category_parent"
  end

  create_table "ps_cms_category_lang", primary_key: ["id_cms_category", "id_shop", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cms_category", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.string "name", limit: 128, null: false
    t.text "description"
    t.string "link_rewrite", limit: 128, null: false
    t.string "meta_title", limit: 128
    t.string "meta_keywords"
    t.string "meta_description"
    t.index ["name"], name: "category_name"
  end

  create_table "ps_cms_category_shop", primary_key: ["id_cms_category", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cms_category", null: false, unsigned: true, auto_increment: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_cms_lang", primary_key: ["id_cms", "id_shop", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cms", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.string "meta_title", limit: 128, null: false
    t.string "meta_description"
    t.string "meta_keywords"
    t.text "content", limit: 4294967295
    t.string "link_rewrite", limit: 128, null: false
  end

  create_table "ps_cms_role", primary_key: ["id_cms_role", "id_cms"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cms_role", null: false, unsigned: true, auto_increment: true
    t.string "name", limit: 50, null: false
    t.integer "id_cms", null: false, unsigned: true
    t.index ["name"], name: "name", unique: true
  end

  create_table "ps_cms_role_lang", primary_key: ["id_cms_role", "id_lang", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cms_role", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.string "name", limit: 128
  end

  create_table "ps_cms_shop", primary_key: ["id_cms", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cms", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_condition", primary_key: ["id_condition", "id_ps_condition"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_condition", null: false, auto_increment: true
    t.integer "id_ps_condition", null: false
    t.string "type", limit: 13, null: false
    t.text "request"
    t.string "operator", limit: 32
    t.string "value", limit: 64
    t.string "result", limit: 64
    t.string "calculation_type", limit: 4
    t.string "calculation_detail", limit: 64
    t.boolean "validated", default: false, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
  end

  create_table "ps_condition_advice", primary_key: ["id_condition", "id_advice"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_condition", null: false
    t.integer "id_advice", null: false
    t.boolean "display", default: false, null: false, unsigned: true
  end

  create_table "ps_condition_badge", primary_key: ["id_condition", "id_badge"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_condition", null: false
    t.integer "id_badge", null: false
  end

  create_table "ps_configuration", primary_key: "id_configuration", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop_group", unsigned: true
    t.integer "id_shop", unsigned: true
    t.string "name", limit: 254, null: false
    t.text "value"
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.index ["id_shop"], name: "id_shop"
    t.index ["id_shop_group"], name: "id_shop_group"
    t.index ["name"], name: "name"
  end

  create_table "ps_configuration_kpi", primary_key: "id_configuration_kpi", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop_group", unsigned: true
    t.integer "id_shop", unsigned: true
    t.string "name", limit: 64, null: false
    t.text "value"
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.index ["id_shop"], name: "id_shop"
    t.index ["id_shop_group"], name: "id_shop_group"
    t.index ["name"], name: "name"
  end

  create_table "ps_configuration_kpi_lang", primary_key: ["id_configuration_kpi", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_configuration_kpi", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.text "value"
    t.datetime "date_upd"
  end

  create_table "ps_configuration_lang", primary_key: ["id_configuration", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_configuration", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.text "value"
    t.datetime "date_upd"
  end

  create_table "ps_connections", primary_key: "id_connections", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop_group", default: 1, null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_guest", null: false, unsigned: true
    t.integer "id_page", null: false, unsigned: true
    t.bigint "ip_address"
    t.datetime "date_add", null: false
    t.string "http_referer"
    t.index ["date_add"], name: "date_add"
    t.index ["id_guest"], name: "id_guest"
    t.index ["id_page"], name: "id_page"
  end

  create_table "ps_connections_page", primary_key: ["id_connections", "id_page", "time_start"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_connections", null: false, unsigned: true
    t.integer "id_page", null: false, unsigned: true
    t.datetime "time_start", null: false
    t.datetime "time_end"
  end

  create_table "ps_connections_source", primary_key: "id_connections_source", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_connections", null: false, unsigned: true
    t.string "http_referer"
    t.string "request_uri"
    t.string "keywords"
    t.datetime "date_add", null: false
    t.index ["date_add"], name: "orderby"
    t.index ["http_referer"], name: "http_referer"
    t.index ["id_connections"], name: "connections"
    t.index ["request_uri"], name: "request_uri"
  end

  create_table "ps_contact", primary_key: "id_contact", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", limit: 128, null: false
    t.boolean "customer_service", default: false, null: false
    t.integer "position", limit: 1, default: 0, null: false, unsigned: true
  end

  create_table "ps_contact_lang", primary_key: ["id_contact", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_contact", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 32, null: false
    t.text "description"
  end

  create_table "ps_contact_shop", primary_key: ["id_contact", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_contact", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_country", primary_key: "id_country", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_zone", null: false, unsigned: true
    t.integer "id_currency", default: 0, null: false, unsigned: true
    t.string "iso_code", limit: 3, null: false
    t.integer "call_prefix", default: 0, null: false
    t.boolean "active", default: false, null: false, unsigned: true
    t.boolean "contains_states", default: false, null: false
    t.boolean "need_identification_number", default: false, null: false
    t.boolean "need_zip_code", default: true, null: false
    t.string "zip_code_format", limit: 12, default: "", null: false
    t.boolean "display_tax_label", null: false
    t.index ["id_zone"], name: "country_"
    t.index ["iso_code"], name: "country_iso_code"
  end

  create_table "ps_country_lang", primary_key: ["id_country", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_country", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 64, null: false
  end

  create_table "ps_country_shop", primary_key: ["id_country", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_country", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_cronjobs", primary_key: "id_cronjob", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_module"
    t.text "description"
    t.text "task"
    t.integer "hour", default: -1
    t.integer "day", default: -1
    t.integer "month", default: -1
    t.integer "day_of_week", default: -1
    t.datetime "updated_at"
    t.boolean "one_shot", default: false, null: false
    t.boolean "active", default: false
    t.integer "id_shop", default: 0
    t.integer "id_shop_group", default: 0
    t.index ["id_module"], name: "id_module"
  end

  create_table "ps_currency", primary_key: "id_currency", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.string "iso_code", limit: 3, default: "0", null: false
    t.decimal "conversion_rate", precision: 13, scale: 6, null: false
    t.boolean "deleted", default: false, null: false, unsigned: true
    t.boolean "active", default: true, null: false, unsigned: true
  end

  create_table "ps_currency_shop", primary_key: ["id_currency", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_currency", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.decimal "conversion_rate", precision: 13, scale: 6, null: false
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_customer", primary_key: "id_customer", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop_group", default: 1, null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_gender", null: false, unsigned: true
    t.integer "id_default_group", default: 1, null: false, unsigned: true
    t.integer "id_lang", unsigned: true
    t.integer "id_risk", default: 1, null: false, unsigned: true
    t.string "company", limit: 64
    t.string "siret", limit: 14
    t.string "ape", limit: 5
    t.string "firstname", null: false
    t.string "lastname", null: false
    t.string "email", limit: 128, null: false
    t.string "passwd", limit: 60, null: false
    t.timestamp "last_passwd_gen", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.date "birthday"
    t.boolean "newsletter", default: false, null: false, unsigned: true
    t.string "ip_registration_newsletter", limit: 15
    t.datetime "newsletter_date_add"
    t.boolean "optin", default: false, null: false, unsigned: true
    t.string "website", limit: 128
    t.decimal "outstanding_allow_amount", precision: 20, scale: 6, default: "0.0", null: false
    t.boolean "show_public_prices", default: false, null: false, unsigned: true
    t.integer "max_payment_days", default: 60, null: false, unsigned: true
    t.string "secure_key", limit: 32, default: "-1", null: false
    t.text "note"
    t.boolean "active", default: false, null: false, unsigned: true
    t.boolean "is_guest", default: false, null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.string "reset_password_token", limit: 40
    t.datetime "reset_password_validity"
    t.index ["email", "passwd"], name: "customer_login"
    t.index ["email"], name: "customer_email"
    t.index ["id_customer", "passwd"], name: "id_customer_passwd"
    t.index ["id_gender"], name: "id_gender"
    t.index ["id_shop", "date_add"], name: "id_shop"
    t.index ["id_shop_group"], name: "id_shop_group"
  end

  create_table "ps_customer_group", primary_key: ["id_customer", "id_group"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_customer", null: false, unsigned: true
    t.integer "id_group", null: false, unsigned: true
    t.index ["id_customer"], name: "id_customer"
    t.index ["id_group"], name: "customer_login"
  end

  create_table "ps_customer_message", primary_key: "id_customer_message", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_customer_thread"
    t.integer "id_employee", unsigned: true
    t.text "message", limit: 16777215, null: false
    t.string "file_name", limit: 18
    t.string "ip_address", limit: 16
    t.string "user_agent", limit: 128
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.integer "private", limit: 1, default: 0, null: false
    t.boolean "read", default: false, null: false
    t.index ["id_customer_thread"], name: "id_customer_thread"
    t.index ["id_employee"], name: "id_employee"
  end

  create_table "ps_customer_message_sync_imap", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.binary "md5_header", limit: 32, null: false
    t.index ["md5_header"], name: "md5_header_index", length: 4
  end

  create_table "ps_customer_thread", primary_key: "id_customer_thread", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_contact", null: false, unsigned: true
    t.integer "id_customer", unsigned: true
    t.integer "id_order", unsigned: true
    t.integer "id_product", unsigned: true
    t.string "status", limit: 8, default: "open", null: false
    t.string "email", limit: 128, null: false
    t.string "token", limit: 12
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.index ["id_contact"], name: "id_contact"
    t.index ["id_customer"], name: "id_customer"
    t.index ["id_lang"], name: "id_lang"
    t.index ["id_order"], name: "id_order"
    t.index ["id_product"], name: "id_product"
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_customization", primary_key: ["id_customization", "id_cart", "id_product", "id_address_delivery"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_customization", null: false, unsigned: true, auto_increment: true
    t.integer "id_product_attribute", default: 0, null: false, unsigned: true
    t.integer "id_address_delivery", default: 0, null: false, unsigned: true
    t.integer "id_cart", null: false, unsigned: true
    t.integer "id_product", null: false
    t.integer "quantity", null: false
    t.integer "quantity_refunded", default: 0, null: false
    t.integer "quantity_returned", default: 0, null: false
    t.boolean "in_cart", default: false, null: false, unsigned: true
    t.index ["id_cart", "id_product", "id_product_attribute"], name: "id_cart_product"
    t.index ["id_product_attribute"], name: "id_product_attribute"
  end

  create_table "ps_customization_field", primary_key: "id_customization_field", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.boolean "type", null: false
    t.boolean "required", null: false
    t.boolean "is_module", default: false, null: false
    t.boolean "is_deleted", default: false, null: false
    t.index ["id_product"], name: "id_product"
  end

  create_table "ps_customization_field_lang", primary_key: ["id_customization_field", "id_lang", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_customization_field", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.string "name", null: false
  end

  create_table "ps_customized_data", primary_key: ["id_customization", "type", "index"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_customization", null: false, unsigned: true
    t.boolean "type", null: false
    t.integer "index", null: false
    t.string "value", null: false
    t.integer "id_module", default: 0, null: false
    t.decimal "price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "weight", precision: 20, scale: 6, default: "0.0", null: false
  end

  create_table "ps_date_range", primary_key: "id_date_range", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.datetime "time_start", null: false
    t.datetime "time_end", null: false
  end

  create_table "ps_delivery", primary_key: "id_delivery", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", unsigned: true
    t.integer "id_shop_group", unsigned: true
    t.integer "id_carrier", null: false, unsigned: true
    t.integer "id_range_price", unsigned: true
    t.integer "id_range_weight", unsigned: true
    t.integer "id_zone", null: false, unsigned: true
    t.decimal "price", precision: 20, scale: 6, null: false
    t.index ["id_carrier", "id_zone"], name: "id_carrier"
    t.index ["id_range_price"], name: "id_range_price"
    t.index ["id_range_weight"], name: "id_range_weight"
    t.index ["id_zone"], name: "id_zone"
  end

  create_table "ps_emailsubscription", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_shop_group", default: 1, null: false, unsigned: true
    t.string "email", null: false
    t.datetime "newsletter_date_add"
    t.string "ip_registration_newsletter", limit: 15, null: false
    t.string "http_referer"
    t.boolean "active", default: false, null: false
  end

  create_table "ps_employee", primary_key: "id_employee", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_profile", null: false, unsigned: true
    t.integer "id_lang", default: 0, null: false, unsigned: true
    t.string "lastname", limit: 32, null: false
    t.string "firstname", limit: 32, null: false
    t.string "email", limit: 128, null: false
    t.string "passwd", limit: 60, null: false
    t.timestamp "last_passwd_gen", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.date "stats_date_from"
    t.date "stats_date_to"
    t.date "stats_compare_from"
    t.date "stats_compare_to"
    t.integer "stats_compare_option", default: 1, null: false, unsigned: true
    t.string "preselect_date_range", limit: 32
    t.string "bo_color", limit: 32
    t.string "bo_theme", limit: 32
    t.string "bo_css", limit: 64
    t.integer "default_tab", default: 0, null: false, unsigned: true
    t.integer "bo_width", default: 0, null: false, unsigned: true
    t.boolean "bo_menu", default: true, null: false
    t.boolean "active", default: false, null: false, unsigned: true
    t.boolean "optin", default: true, null: false, unsigned: true
    t.integer "id_last_order", default: 0, null: false, unsigned: true
    t.integer "id_last_customer_message", default: 0, null: false, unsigned: true
    t.integer "id_last_customer", default: 0, null: false, unsigned: true
    t.date "last_connection_date"
    t.string "reset_password_token", limit: 40
    t.datetime "reset_password_validity"
    t.index ["email", "passwd"], name: "employee_login"
    t.index ["id_employee", "passwd"], name: "id_employee_passwd"
    t.index ["id_profile"], name: "id_profile"
  end

  create_table "ps_employee_shop", primary_key: ["id_employee", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_employee", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_feature", primary_key: "id_feature", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "position", default: 0, null: false, unsigned: true
  end

  create_table "ps_feature_lang", primary_key: ["id_feature", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_feature", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 128
    t.index ["id_lang", "name"], name: "id_lang"
  end

  create_table "ps_feature_product", primary_key: ["id_feature", "id_product", "id_feature_value"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_feature", null: false, unsigned: true
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_feature_value", null: false, unsigned: true
    t.index ["id_feature_value"], name: "id_feature_value"
    t.index ["id_product"], name: "id_product"
  end

  create_table "ps_feature_shop", primary_key: ["id_feature", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_feature", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_feature_value", primary_key: "id_feature_value", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_feature", null: false, unsigned: true
    t.integer "custom", limit: 1, unsigned: true
    t.index ["id_feature"], name: "feature"
  end

  create_table "ps_feature_value_lang", primary_key: ["id_feature_value", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_feature_value", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "value"
  end

  create_table "ps_gender", primary_key: "id_gender", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "type", null: false
  end

  create_table "ps_gender_lang", primary_key: ["id_gender", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_gender", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 20, null: false
    t.index ["id_gender"], name: "id_gender"
  end

  create_table "ps_group", primary_key: "id_group", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "reduction", precision: 17, scale: 2, default: "0.0", null: false
    t.integer "price_display_method", limit: 1, default: 0, null: false
    t.boolean "show_prices", default: true, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
  end

  create_table "ps_group_lang", primary_key: ["id_group", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_group", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 32, null: false
  end

  create_table "ps_group_reduction", primary_key: "id_group_reduction", id: :integer, limit: 3, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_group", null: false, unsigned: true
    t.integer "id_category", null: false, unsigned: true
    t.decimal "reduction", precision: 4, scale: 3, null: false
    t.index ["id_group", "id_category"], name: "id_group", unique: true
  end

  create_table "ps_group_shop", primary_key: ["id_group", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_group", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_guest", primary_key: "id_guest", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_operating_system", unsigned: true
    t.integer "id_web_browser", unsigned: true
    t.integer "id_customer", unsigned: true
    t.boolean "javascript", default: false
    t.integer "screen_resolution_x", limit: 2, unsigned: true
    t.integer "screen_resolution_y", limit: 2, unsigned: true
    t.integer "screen_color", limit: 1, unsigned: true
    t.boolean "sun_java"
    t.boolean "adobe_flash"
    t.boolean "adobe_director"
    t.boolean "apple_quicktime"
    t.boolean "real_player"
    t.boolean "windows_media"
    t.string "accept_language", limit: 8
    t.boolean "mobile_theme", default: false, null: false
    t.index ["id_customer"], name: "id_customer"
    t.index ["id_operating_system"], name: "id_operating_system"
    t.index ["id_web_browser"], name: "id_web_browser"
  end

  create_table "ps_homeslider", primary_key: ["id_homeslider_slides", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_homeslider_slides", null: false, unsigned: true, auto_increment: true
    t.integer "id_shop", null: false, unsigned: true
  end

  create_table "ps_homeslider_slides", primary_key: "id_homeslider_slides", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "position", default: 0, null: false, unsigned: true
    t.boolean "active", default: false, null: false, unsigned: true
  end

  create_table "ps_homeslider_slides_lang", primary_key: ["id_homeslider_slides", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_homeslider_slides", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "title", null: false
    t.text "description", null: false
    t.string "legend", null: false
    t.string "url", null: false
    t.string "image", null: false
  end

  create_table "ps_hook", primary_key: "id_hook", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.string "title", limit: 64, null: false
    t.text "description"
    t.boolean "position", default: true, null: false
    t.index ["name"], name: "hook_name", unique: true
  end

  create_table "ps_hook_alias", primary_key: "id_hook_alias", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "alias", limit: 64, null: false
    t.string "name", limit: 64, null: false
    t.index ["alias"], name: "alias", unique: true
  end

  create_table "ps_hook_module", primary_key: ["id_module", "id_hook", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_module", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_hook", null: false, unsigned: true
    t.integer "position", limit: 1, null: false, unsigned: true
    t.index ["id_hook"], name: "id_hook"
    t.index ["id_module"], name: "id_module"
    t.index ["id_shop", "position"], name: "position"
  end

  create_table "ps_hook_module_exceptions", primary_key: "id_hook_module_exceptions", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_module", null: false, unsigned: true
    t.integer "id_hook", null: false, unsigned: true
    t.string "file_name"
    t.index ["id_hook"], name: "id_hook"
    t.index ["id_module"], name: "id_module"
  end

  create_table "ps_image", primary_key: "id_image", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "position", limit: 2, default: 0, null: false, unsigned: true
    t.boolean "cover", unsigned: true
    t.index ["id_image", "id_product", "cover"], name: "idx_product_image", unique: true
    t.index ["id_product", "cover"], name: "id_product_cover", unique: true
    t.index ["id_product"], name: "image_product"
  end

  create_table "ps_image_lang", primary_key: ["id_image", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_image", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "legend", limit: 128
    t.index ["id_image"], name: "id_image"
  end

  create_table "ps_image_shop", primary_key: ["id_image", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_image", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.boolean "cover", unsigned: true
    t.index ["id_product", "id_shop", "cover"], name: "id_product", unique: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_image_type", primary_key: "id_image_type", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.integer "width", null: false, unsigned: true
    t.integer "height", null: false, unsigned: true
    t.boolean "products", default: true, null: false
    t.boolean "categories", default: true, null: false
    t.boolean "manufacturers", default: true, null: false
    t.boolean "suppliers", default: true, null: false
    t.boolean "stores", default: true, null: false
    t.index ["name"], name: "image_type_name"
  end

  create_table "ps_import_match", primary_key: "id_import_match", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 32, null: false
    t.text "match", null: false
    t.integer "skip", null: false
  end

  create_table "ps_info", primary_key: "id_info", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
  end

  create_table "ps_info_lang", primary_key: ["id_info", "id_lang", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_info", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.text "text", null: false
  end

  create_table "ps_info_shop", primary_key: ["id_info", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_info", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
  end

  create_table "ps_lang", primary_key: "id_lang", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "name", limit: 32, null: false
    t.boolean "active", null: false
    t.string "iso_code", limit: 2, null: false
    t.string "language_code", limit: 5, null: false
    t.string "locale", limit: 5, null: false
    t.string "date_format_lite", limit: 32, null: false
    t.string "date_format_full", limit: 32, null: false
    t.boolean "is_rtl", null: false
  end

  create_table "ps_lang_shop", primary_key: ["id_lang", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_lang", null: false
    t.integer "id_shop", null: false
    t.index ["id_lang"], name: "IDX_2F43BFC7BA299860"
    t.index ["id_shop"], name: "IDX_2F43BFC7274A50A0"
  end

  create_table "ps_layered_category", primary_key: "id_layered_category", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "id_shop", null: false, unsigned: true
    t.integer "id_category", null: false, unsigned: true
    t.integer "id_value", default: 0, unsigned: true
    t.string "type", limit: 18, null: false
    t.integer "position", null: false, unsigned: true
    t.integer "filter_type", default: 0, null: false, unsigned: true
    t.integer "filter_show_limit", default: 0, null: false, unsigned: true
    t.index ["id_category", "type"], name: "id_category"
  end

  create_table "ps_layered_filter", primary_key: "id_layered_filter", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.text "filters", limit: 4294967295
    t.integer "n_categories", null: false, unsigned: true
    t.datetime "date_add", null: false
  end

  create_table "ps_layered_filter_shop", primary_key: ["id_layered_filter", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_layered_filter", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_layered_indexable_attribute_group", primary_key: "id_attribute_group", id: :integer, default: nil, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "indexable", default: false, null: false
  end

  create_table "ps_layered_indexable_attribute_group_lang_value", primary_key: ["id_attribute_group", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_attribute_group", null: false
    t.integer "id_lang", null: false
    t.string "url_name", limit: 128
    t.string "meta_title", limit: 128
  end

  create_table "ps_layered_indexable_attribute_lang_value", primary_key: ["id_attribute", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_attribute", null: false
    t.integer "id_lang", null: false
    t.string "url_name", limit: 128
    t.string "meta_title", limit: 128
  end

  create_table "ps_layered_indexable_feature", primary_key: "id_feature", id: :integer, default: nil, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "indexable", default: false, null: false
  end

  create_table "ps_layered_indexable_feature_lang_value", primary_key: ["id_feature", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_feature", null: false
    t.integer "id_lang", null: false
    t.string "url_name", limit: 128, null: false
    t.string "meta_title", limit: 128
  end

  create_table "ps_layered_indexable_feature_value_lang_value", primary_key: ["id_feature_value", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_feature_value", null: false
    t.integer "id_lang", null: false
    t.string "url_name", limit: 128
    t.string "meta_title", limit: 128
  end

  create_table "ps_layered_price_index", primary_key: ["id_product", "id_currency", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false
    t.integer "id_currency", null: false
    t.integer "id_shop", null: false
    t.integer "price_min", null: false
    t.integer "price_max", null: false
    t.index ["id_currency"], name: "id_currency"
    t.index ["price_max"], name: "price_max"
    t.index ["price_min"], name: "price_min"
  end

  create_table "ps_layered_product_attribute", primary_key: ["id_attribute", "id_product", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_attribute", null: false, unsigned: true
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_attribute_group", default: 0, null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.index ["id_attribute_group", "id_attribute", "id_product", "id_shop"], name: "id_attribute_group", unique: true
  end

  create_table "ps_link_block", primary_key: "id_link_block", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_hook", unsigned: true
    t.integer "position", default: 0, null: false, unsigned: true
    t.text "content"
  end

  create_table "ps_link_block_lang", primary_key: ["id_link_block", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_link_block", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 40, default: "", null: false
    t.text "custom_content"
  end

  create_table "ps_link_block_shop", primary_key: ["id_link_block", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_link_block", null: false, unsigned: true, auto_increment: true
    t.integer "id_shop", null: false, unsigned: true
  end

  create_table "ps_linksmenutop", primary_key: "id_linksmenutop", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", null: false, unsigned: true
    t.boolean "new_window", null: false
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_linksmenutop_lang", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_linksmenutop", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.string "label", limit: 128, null: false
    t.string "link", limit: 128, null: false
    t.index ["id_linksmenutop", "id_lang", "id_shop"], name: "id_linksmenutop"
  end

  create_table "ps_log", primary_key: "id_log", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "severity", null: false
    t.integer "error_code"
    t.text "message", null: false
    t.string "object_type", limit: 32
    t.integer "object_id", unsigned: true
    t.integer "id_employee", unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
  end

  create_table "ps_mail", primary_key: "id_mail", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "recipient", limit: 126, null: false
    t.string "template", limit: 62, null: false
    t.string "subject", limit: 254, null: false
    t.integer "id_lang", null: false, unsigned: true
    t.timestamp "date_add", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["recipient"], name: "recipient", length: 10
  end

  create_table "ps_manufacturer", primary_key: "id_manufacturer", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.boolean "active", default: false, null: false
  end

  create_table "ps_manufacturer_lang", primary_key: ["id_manufacturer", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_manufacturer", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.text "description"
    t.text "short_description"
    t.string "meta_title", limit: 128
    t.string "meta_keywords"
    t.string "meta_description"
  end

  create_table "ps_manufacturer_shop", primary_key: ["id_manufacturer", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_manufacturer", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_memcached_servers", primary_key: "id_memcached_server", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "ip", limit: 254, null: false
    t.integer "port", null: false, unsigned: true
    t.integer "weight", null: false, unsigned: true
  end

  create_table "ps_message", primary_key: "id_message", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_cart", unsigned: true
    t.integer "id_customer", null: false, unsigned: true
    t.integer "id_employee", unsigned: true
    t.integer "id_order", null: false, unsigned: true
    t.text "message", null: false
    t.boolean "private", default: true, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.index ["id_cart"], name: "id_cart"
    t.index ["id_customer"], name: "id_customer"
    t.index ["id_employee"], name: "id_employee"
    t.index ["id_order"], name: "message_order"
  end

  create_table "ps_message_readed", primary_key: ["id_message", "id_employee"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_message", null: false, unsigned: true
    t.integer "id_employee", null: false, unsigned: true
    t.datetime "date_add", null: false
  end

  create_table "ps_meta", primary_key: "id_meta", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "page", limit: 64, null: false
    t.boolean "configurable", default: true, null: false, unsigned: true
    t.index ["page"], name: "page", unique: true
  end

  create_table "ps_meta_lang", primary_key: ["id_meta", "id_shop", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_meta", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "title", limit: 128
    t.string "description"
    t.string "keywords"
    t.string "url_rewrite", limit: 254, null: false
    t.index ["id_lang"], name: "id_lang"
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_module", primary_key: "id_module", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.boolean "active", default: false, null: false, unsigned: true
    t.string "version", limit: 8, null: false
    t.index ["name"], name: "name"
    t.index ["name"], name: "name_UNIQUE", unique: true
  end

  create_table "ps_module_access", primary_key: ["id_profile", "id_authorization_role"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_profile", null: false, unsigned: true
    t.integer "id_authorization_role", null: false, unsigned: true
  end

  create_table "ps_module_carrier", primary_key: ["id_module", "id_shop", "id_reference"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_module", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_reference", null: false
  end

  create_table "ps_module_country", primary_key: ["id_module", "id_shop", "id_country"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_module", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_country", null: false, unsigned: true
  end

  create_table "ps_module_currency", primary_key: ["id_module", "id_shop", "id_currency"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_module", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_currency", null: false
    t.index ["id_module"], name: "id_module"
  end

  create_table "ps_module_group", primary_key: ["id_module", "id_shop", "id_group"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_module", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_group", null: false, unsigned: true
  end

  create_table "ps_module_history", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_employee", null: false
    t.integer "id_module", null: false
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
  end

  create_table "ps_module_preference", primary_key: "id_module_preference", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_employee", null: false
    t.string "module", null: false
    t.boolean "interest"
    t.boolean "favorite"
    t.index ["id_employee", "module"], name: "employee_module", unique: true
  end

  create_table "ps_module_shop", primary_key: ["id_module", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_module", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.boolean "enable_device", default: true, null: false
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_operating_system", primary_key: "id_operating_system", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64
  end

  create_table "ps_order_carrier", primary_key: "id_order_carrier", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order", null: false, unsigned: true
    t.integer "id_carrier", null: false, unsigned: true
    t.integer "id_order_invoice", unsigned: true
    t.decimal "weight", precision: 20, scale: 6
    t.decimal "shipping_cost_tax_excl", precision: 20, scale: 6
    t.decimal "shipping_cost_tax_incl", precision: 20, scale: 6
    t.string "tracking_number", limit: 64
    t.datetime "date_add", null: false
    t.index ["id_carrier"], name: "id_carrier"
    t.index ["id_order"], name: "id_order"
    t.index ["id_order_invoice"], name: "id_order_invoice"
  end

  create_table "ps_order_cart_rule", primary_key: "id_order_cart_rule", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order", null: false, unsigned: true
    t.integer "id_cart_rule", null: false, unsigned: true
    t.integer "id_order_invoice", default: 0, unsigned: true
    t.string "name", limit: 254, null: false
    t.decimal "value", precision: 17, scale: 2, default: "0.0", null: false
    t.decimal "value_tax_excl", precision: 17, scale: 2, default: "0.0", null: false
    t.boolean "free_shipping", default: false, null: false
    t.index ["id_cart_rule"], name: "id_cart_rule"
    t.index ["id_order"], name: "id_order"
  end

  create_table "ps_order_detail", primary_key: "id_order_detail", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order", null: false, unsigned: true
    t.integer "id_order_invoice"
    t.integer "id_warehouse", default: 0, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.integer "product_id", null: false, unsigned: true
    t.integer "product_attribute_id", unsigned: true
    t.integer "id_customization", default: 0, unsigned: true
    t.string "product_name", null: false
    t.integer "product_quantity", default: 0, null: false, unsigned: true
    t.integer "product_quantity_in_stock", default: 0, null: false
    t.integer "product_quantity_refunded", default: 0, null: false, unsigned: true
    t.integer "product_quantity_return", default: 0, null: false, unsigned: true
    t.integer "product_quantity_reinjected", default: 0, null: false, unsigned: true
    t.decimal "product_price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "reduction_percent", precision: 10, scale: 2, default: "0.0", null: false
    t.decimal "reduction_amount", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "reduction_amount_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "reduction_amount_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "group_reduction", precision: 10, scale: 2, default: "0.0", null: false
    t.decimal "product_quantity_discount", precision: 20, scale: 6, default: "0.0", null: false
    t.string "product_ean13", limit: 13
    t.string "product_isbn", limit: 32
    t.string "product_upc", limit: 12
    t.string "product_reference", limit: 32
    t.string "product_supplier_reference", limit: 32
    t.decimal "product_weight", precision: 20, scale: 6, null: false
    t.integer "id_tax_rules_group", default: 0, unsigned: true
    t.boolean "tax_computation_method", default: false, null: false, unsigned: true
    t.string "tax_name", limit: 16, null: false
    t.decimal "tax_rate", precision: 10, scale: 3, default: "0.0", null: false
    t.decimal "ecotax", precision: 21, scale: 6, default: "0.0", null: false
    t.decimal "ecotax_tax_rate", precision: 5, scale: 3, default: "0.0", null: false
    t.boolean "discount_quantity_applied", default: false, null: false
    t.string "download_hash"
    t.integer "download_nb", default: 0, unsigned: true
    t.datetime "download_deadline"
    t.decimal "total_price_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_price_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "unit_price_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "unit_price_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_shipping_price_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_shipping_price_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "purchase_supplier_price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "original_product_price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "original_wholesale_price", precision: 20, scale: 6, default: "0.0", null: false
    t.index ["id_order", "id_order_detail"], name: "id_order_id_order_detail"
    t.index ["id_order"], name: "order_detail_order"
    t.index ["id_tax_rules_group"], name: "id_tax_rules_group"
    t.index ["product_attribute_id"], name: "product_attribute_id"
    t.index ["product_id", "product_attribute_id"], name: "product_id"
  end

  create_table "ps_order_detail_tax", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order_detail", null: false
    t.integer "id_tax", null: false
    t.decimal "unit_amount", precision: 16, scale: 6, default: "0.0", null: false
    t.decimal "total_amount", precision: 16, scale: 6, default: "0.0", null: false
    t.index ["id_order_detail"], name: "id_order_detail"
    t.index ["id_tax"], name: "id_tax"
  end

  create_table "ps_order_history", primary_key: "id_order_history", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_employee", null: false, unsigned: true
    t.integer "id_order", null: false, unsigned: true
    t.integer "id_order_state", null: false, unsigned: true
    t.datetime "date_add", null: false
    t.index ["id_employee"], name: "id_employee"
    t.index ["id_order"], name: "order_history_order"
    t.index ["id_order_state"], name: "id_order_state"
  end

  create_table "ps_order_invoice", primary_key: "id_order_invoice", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order", null: false
    t.integer "number", null: false
    t.integer "delivery_number", null: false
    t.datetime "delivery_date"
    t.decimal "total_discount_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_discount_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_paid_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_paid_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_products", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_products_wt", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_shipping_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_shipping_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.integer "shipping_tax_computation_method", null: false, unsigned: true
    t.decimal "total_wrapping_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_wrapping_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.text "shop_address"
    t.text "note"
    t.datetime "date_add", null: false
    t.index ["id_order"], name: "id_order"
  end

  create_table "ps_order_invoice_payment", primary_key: ["id_order_invoice", "id_order_payment"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order_invoice", null: false, unsigned: true
    t.integer "id_order_payment", null: false, unsigned: true
    t.integer "id_order", null: false, unsigned: true
    t.index ["id_order"], name: "id_order"
    t.index ["id_order_payment"], name: "order_payment"
  end

  create_table "ps_order_invoice_tax", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order_invoice", null: false
    t.string "type", limit: 15, null: false
    t.integer "id_tax", null: false
    t.decimal "amount", precision: 10, scale: 6, default: "0.0", null: false
    t.index ["id_tax"], name: "id_tax"
  end

  create_table "ps_order_message", primary_key: "id_order_message", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.datetime "date_add", null: false
  end

  create_table "ps_order_message_lang", primary_key: ["id_order_message", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order_message", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 128, null: false
    t.text "message", null: false
  end

  create_table "ps_order_payment", primary_key: "id_order_payment", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "order_reference", limit: 9
    t.integer "id_currency", null: false, unsigned: true
    t.decimal "amount", precision: 10, scale: 2, null: false
    t.string "payment_method", null: false
    t.decimal "conversion_rate", precision: 13, scale: 6, default: "1.0", null: false
    t.string "transaction_id", limit: 254
    t.string "card_number", limit: 254
    t.string "card_brand", limit: 254
    t.string "card_expiration", limit: 7
    t.string "card_holder", limit: 254
    t.datetime "date_add", null: false
    t.index ["order_reference"], name: "order_reference"
  end

  create_table "ps_order_return", primary_key: "id_order_return", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_customer", null: false, unsigned: true
    t.integer "id_order", null: false, unsigned: true
    t.boolean "state", default: true, null: false, unsigned: true
    t.text "question", null: false
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.index ["id_customer"], name: "order_return_customer"
    t.index ["id_order"], name: "id_order"
  end

  create_table "ps_order_return_detail", primary_key: ["id_order_return", "id_order_detail", "id_customization"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order_return", null: false, unsigned: true
    t.integer "id_order_detail", null: false, unsigned: true
    t.integer "id_customization", default: 0, null: false, unsigned: true
    t.integer "product_quantity", default: 0, null: false, unsigned: true
  end

  create_table "ps_order_return_state", primary_key: "id_order_return_state", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "color", limit: 32
  end

  create_table "ps_order_return_state_lang", primary_key: ["id_order_return_state", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order_return_state", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 64, null: false
  end

  create_table "ps_order_slip", primary_key: "id_order_slip", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "conversion_rate", precision: 13, scale: 6, default: "1.0", null: false
    t.integer "id_customer", null: false, unsigned: true
    t.integer "id_order", null: false, unsigned: true
    t.decimal "total_products_tax_excl", precision: 20, scale: 6
    t.decimal "total_products_tax_incl", precision: 20, scale: 6
    t.decimal "total_shipping_tax_excl", precision: 20, scale: 6
    t.decimal "total_shipping_tax_incl", precision: 20, scale: 6
    t.integer "shipping_cost", limit: 1, default: 0, null: false, unsigned: true
    t.decimal "amount", precision: 10, scale: 2, null: false
    t.decimal "shipping_cost_amount", precision: 10, scale: 2, null: false
    t.boolean "partial", null: false
    t.boolean "order_slip_type", default: false, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.index ["id_customer"], name: "order_slip_customer"
    t.index ["id_order"], name: "id_order"
  end

  create_table "ps_order_slip_detail", primary_key: ["id_order_slip", "id_order_detail"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order_slip", null: false, unsigned: true
    t.integer "id_order_detail", null: false, unsigned: true
    t.integer "product_quantity", default: 0, null: false, unsigned: true
    t.decimal "unit_price_tax_excl", precision: 20, scale: 6
    t.decimal "unit_price_tax_incl", precision: 20, scale: 6
    t.decimal "total_price_tax_excl", precision: 20, scale: 6
    t.decimal "total_price_tax_incl", precision: 20, scale: 6
    t.decimal "amount_tax_excl", precision: 20, scale: 6
    t.decimal "amount_tax_incl", precision: 20, scale: 6
  end

  create_table "ps_order_slip_detail_tax", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order_slip_detail", null: false, unsigned: true
    t.integer "id_tax", null: false, unsigned: true
    t.decimal "unit_amount", precision: 16, scale: 6, default: "0.0", null: false
    t.decimal "total_amount", precision: 16, scale: 6, default: "0.0", null: false
    t.index ["id_order_slip_detail"], name: "id_order_slip_detail"
    t.index ["id_tax"], name: "id_tax"
  end

  create_table "ps_order_state", primary_key: "id_order_state", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "invoice", default: false, unsigned: true
    t.boolean "send_email", default: false, null: false, unsigned: true
    t.string "module_name"
    t.string "color", limit: 32
    t.boolean "unremovable", null: false, unsigned: true
    t.boolean "hidden", default: false, null: false, unsigned: true
    t.boolean "logable", default: false, null: false
    t.boolean "delivery", default: false, null: false, unsigned: true
    t.boolean "shipped", default: false, null: false, unsigned: true
    t.boolean "paid", default: false, null: false, unsigned: true
    t.boolean "pdf_invoice", default: false, null: false, unsigned: true
    t.boolean "pdf_delivery", default: false, null: false, unsigned: true
    t.boolean "deleted", default: false, null: false, unsigned: true
    t.index ["module_name"], name: "module_name"
  end

  create_table "ps_order_state_lang", primary_key: ["id_order_state", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_order_state", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 64, null: false
    t.string "template", limit: 64, null: false
  end

  create_table "ps_orders", primary_key: "id_order", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "reference", limit: 9
    t.integer "id_shop_group", default: 1, null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_carrier", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_customer", null: false, unsigned: true
    t.integer "id_cart", null: false, unsigned: true
    t.integer "id_currency", null: false, unsigned: true
    t.integer "id_address_delivery", null: false, unsigned: true
    t.integer "id_address_invoice", null: false, unsigned: true
    t.integer "current_state", null: false, unsigned: true
    t.string "secure_key", limit: 32, default: "-1", null: false
    t.string "payment", null: false
    t.decimal "conversion_rate", precision: 13, scale: 6, default: "1.0", null: false
    t.string "module"
    t.boolean "recyclable", default: false, null: false, unsigned: true
    t.boolean "gift", default: false, null: false, unsigned: true
    t.text "gift_message"
    t.boolean "mobile_theme", default: false, null: false
    t.string "shipping_number", limit: 64
    t.decimal "total_discounts", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_discounts_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_discounts_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_paid", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_paid_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_paid_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_paid_real", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_products", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_products_wt", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_shipping", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_shipping_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_shipping_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "carrier_tax_rate", precision: 10, scale: 3, default: "0.0", null: false
    t.decimal "total_wrapping", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_wrapping_tax_incl", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "total_wrapping_tax_excl", precision: 20, scale: 6, default: "0.0", null: false
    t.boolean "round_mode", default: true, null: false
    t.boolean "round_type", default: true, null: false
    t.integer "invoice_number", default: 0, null: false, unsigned: true
    t.integer "delivery_number", default: 0, null: false, unsigned: true
    t.datetime "invoice_date", null: false
    t.datetime "delivery_date", null: false
    t.integer "valid", default: 0, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.index ["current_state"], name: "current_state"
    t.index ["date_add"], name: "date_add"
    t.index ["id_address_delivery"], name: "id_address_delivery"
    t.index ["id_address_invoice"], name: "id_address_invoice"
    t.index ["id_carrier"], name: "id_carrier"
    t.index ["id_cart"], name: "id_cart"
    t.index ["id_currency"], name: "id_currency"
    t.index ["id_customer"], name: "id_customer"
    t.index ["id_lang"], name: "id_lang"
    t.index ["id_shop"], name: "id_shop"
    t.index ["id_shop_group"], name: "id_shop_group"
    t.index ["invoice_number"], name: "invoice_number"
    t.index ["reference"], name: "reference"
  end

  create_table "ps_pack", primary_key: ["id_product_pack", "id_product_item", "id_product_attribute_item"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product_pack", null: false, unsigned: true
    t.integer "id_product_item", null: false, unsigned: true
    t.integer "id_product_attribute_item", null: false, unsigned: true
    t.integer "quantity", default: 1, null: false, unsigned: true
    t.index ["id_product_item", "id_product_attribute_item"], name: "product_item"
  end

  create_table "ps_page", primary_key: "id_page", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_page_type", null: false, unsigned: true
    t.integer "id_object", unsigned: true
    t.index ["id_object"], name: "id_object"
    t.index ["id_page_type"], name: "id_page_type"
  end

  create_table "ps_page_type", primary_key: "id_page_type", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.index ["name"], name: "name"
  end

  create_table "ps_page_viewed", primary_key: ["id_page", "id_date_range", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_page", null: false, unsigned: true
    t.integer "id_shop_group", default: 1, null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_date_range", null: false, unsigned: true
    t.integer "counter", null: false, unsigned: true
  end

  create_table "ps_pagenotfound", primary_key: "id_pagenotfound", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_shop_group", default: 1, null: false, unsigned: true
    t.string "request_uri", limit: 256, null: false
    t.string "http_referer", limit: 256, null: false
    t.datetime "date_add", null: false
    t.index ["date_add"], name: "date_add"
  end

  create_table "ps_product", primary_key: "id_product", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_supplier", unsigned: true
    t.integer "id_manufacturer", unsigned: true
    t.integer "id_category_default", unsigned: true
    t.integer "id_shop_default", default: 1, null: false, unsigned: true
    t.integer "id_tax_rules_group", null: false, unsigned: true
    t.boolean "on_sale", default: false, null: false, unsigned: true
    t.boolean "online_only", default: false, null: false, unsigned: true
    t.string "ean13", limit: 13
    t.string "isbn", limit: 32
    t.string "upc", limit: 12
    t.decimal "ecotax", precision: 17, scale: 6, default: "0.0", null: false
    t.integer "quantity", default: 0, null: false
    t.integer "minimal_quantity", default: 1, null: false, unsigned: true
    t.integer "low_stock_threshold"
    t.boolean "low_stock_alert", default: false, null: false
    t.decimal "price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "wholesale_price", precision: 20, scale: 6, default: "0.0", null: false
    t.string "unity"
    t.decimal "unit_price_ratio", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "additional_shipping_cost", precision: 20, scale: 2, default: "0.0", null: false
    t.string "reference", limit: 32
    t.string "supplier_reference", limit: 32
    t.string "location", limit: 64
    t.decimal "width", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "height", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "depth", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "weight", precision: 20, scale: 6, default: "0.0", null: false
    t.integer "out_of_stock", default: 2, null: false, unsigned: true
    t.boolean "additional_delivery_times", default: true, null: false, unsigned: true
    t.boolean "quantity_discount", default: false
    t.integer "customizable", limit: 1, default: 0, null: false
    t.integer "uploadable_files", limit: 1, default: 0, null: false
    t.integer "text_fields", limit: 1, default: 0, null: false
    t.boolean "active", default: false, null: false, unsigned: true
    t.string "redirect_type", limit: 12, default: "", null: false
    t.integer "id_type_redirected", default: 0, null: false, unsigned: true
    t.boolean "available_for_order", default: true, null: false
    t.date "available_date"
    t.boolean "show_condition", default: false, null: false
    t.string "condition", limit: 11, default: "new", null: false
    t.boolean "show_price", default: true, null: false
    t.boolean "indexed", default: false, null: false
    t.string "visibility", limit: 7, default: "both", null: false
    t.boolean "cache_is_pack", default: false, null: false
    t.boolean "cache_has_attachments", default: false, null: false
    t.boolean "is_virtual", default: false, null: false
    t.integer "cache_default_attribute", unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.boolean "advanced_stock_management", default: false, null: false
    t.integer "pack_stock_type", default: 3, null: false, unsigned: true
    t.integer "state", default: 1, null: false, unsigned: true
    t.index ["date_add"], name: "date_add"
    t.index ["id_category_default"], name: "id_category_default"
    t.index ["id_manufacturer", "id_product"], name: "product_manufacturer"
    t.index ["id_supplier"], name: "product_supplier"
    t.index ["indexed"], name: "indexed"
    t.index ["state", "date_upd"], name: "state"
  end

  create_table "ps_product_attachment", primary_key: ["id_product", "id_attachment"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_attachment", null: false, unsigned: true
  end

  create_table "ps_product_attribute", primary_key: "id_product_attribute", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.string "reference", limit: 32
    t.string "supplier_reference", limit: 32
    t.string "location", limit: 64
    t.string "ean13", limit: 13
    t.string "isbn", limit: 32
    t.string "upc", limit: 12
    t.decimal "wholesale_price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "ecotax", precision: 17, scale: 6, default: "0.0", null: false
    t.integer "quantity", default: 0, null: false
    t.decimal "weight", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "unit_price_impact", precision: 20, scale: 6, default: "0.0", null: false
    t.boolean "default_on", unsigned: true
    t.integer "minimal_quantity", default: 1, null: false, unsigned: true
    t.integer "low_stock_threshold"
    t.boolean "low_stock_alert", default: false, null: false
    t.date "available_date"
    t.index ["id_product", "default_on"], name: "product_default", unique: true
    t.index ["id_product"], name: "product_attribute_product"
    t.index ["id_product_attribute", "id_product"], name: "id_product_id_product_attribute"
    t.index ["reference"], name: "reference"
    t.index ["supplier_reference"], name: "supplier_reference"
  end

  create_table "ps_product_attribute_combination", primary_key: ["id_attribute", "id_product_attribute"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_attribute", null: false, unsigned: true
    t.integer "id_product_attribute", null: false, unsigned: true
    t.index ["id_product_attribute"], name: "id_product_attribute"
  end

  create_table "ps_product_attribute_image", primary_key: ["id_product_attribute", "id_image"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product_attribute", null: false, unsigned: true
    t.integer "id_image", null: false, unsigned: true
    t.index ["id_image"], name: "id_image"
  end

  create_table "ps_product_attribute_shop", primary_key: ["id_product_attribute", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_product_attribute", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.decimal "wholesale_price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "ecotax", precision: 17, scale: 6, default: "0.0", null: false
    t.decimal "weight", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "unit_price_impact", precision: 20, scale: 6, default: "0.0", null: false
    t.boolean "default_on", unsigned: true
    t.integer "minimal_quantity", default: 1, null: false, unsigned: true
    t.integer "low_stock_threshold"
    t.boolean "low_stock_alert", default: false, null: false
    t.date "available_date"
    t.index ["id_product", "id_shop", "default_on"], name: "id_product", unique: true
  end

  create_table "ps_product_carrier", primary_key: ["id_product", "id_carrier_reference", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_carrier_reference", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
  end

  create_table "ps_product_country_tax", primary_key: ["id_product", "id_country"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false
    t.integer "id_country", null: false
    t.integer "id_tax", null: false
  end

  create_table "ps_product_download", primary_key: "id_product_download", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.string "display_filename"
    t.string "filename"
    t.datetime "date_add", null: false
    t.datetime "date_expiration"
    t.integer "nb_days_accessible", unsigned: true
    t.integer "nb_downloadable", default: 1, unsigned: true
    t.boolean "active", default: true, null: false, unsigned: true
    t.boolean "is_shareable", default: false, null: false, unsigned: true
    t.index ["id_product", "active"], name: "product_active"
    t.index ["id_product"], name: "id_product", unique: true
  end

  create_table "ps_product_group_reduction_cache", primary_key: ["id_product", "id_group"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_group", null: false, unsigned: true
    t.decimal "reduction", precision: 4, scale: 3, null: false
  end

  create_table "ps_product_lang", primary_key: ["id_product", "id_shop", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.text "description"
    t.text "description_short"
    t.string "link_rewrite", limit: 128, null: false
    t.string "meta_description"
    t.string "meta_keywords"
    t.string "meta_title", limit: 128
    t.string "name", limit: 128, null: false
    t.string "available_now"
    t.string "available_later"
    t.string "delivery_in_stock"
    t.string "delivery_out_stock"
    t.index ["id_lang"], name: "id_lang"
    t.index ["name"], name: "name"
  end

  create_table "ps_product_sale", primary_key: "id_product", id: :integer, unsigned: true, default: nil, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "quantity", default: 0, null: false, unsigned: true
    t.integer "sale_nbr", default: 0, null: false, unsigned: true
    t.date "date_upd"
    t.index ["quantity"], name: "quantity"
  end

  create_table "ps_product_shop", primary_key: ["id_product", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.integer "id_category_default", unsigned: true
    t.integer "id_tax_rules_group", null: false, unsigned: true
    t.boolean "on_sale", default: false, null: false, unsigned: true
    t.boolean "online_only", default: false, null: false, unsigned: true
    t.decimal "ecotax", precision: 17, scale: 6, default: "0.0", null: false
    t.integer "minimal_quantity", default: 1, null: false, unsigned: true
    t.integer "low_stock_threshold"
    t.boolean "low_stock_alert", default: false, null: false
    t.decimal "price", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "wholesale_price", precision: 20, scale: 6, default: "0.0", null: false
    t.string "unity"
    t.decimal "unit_price_ratio", precision: 20, scale: 6, default: "0.0", null: false
    t.decimal "additional_shipping_cost", precision: 20, scale: 2, default: "0.0", null: false
    t.integer "customizable", limit: 1, default: 0, null: false
    t.integer "uploadable_files", limit: 1, default: 0, null: false
    t.integer "text_fields", limit: 1, default: 0, null: false
    t.boolean "active", default: false, null: false, unsigned: true
    t.string "redirect_type", limit: 12, default: "", null: false
    t.integer "id_type_redirected", default: 0, null: false, unsigned: true
    t.boolean "available_for_order", default: true, null: false
    t.date "available_date"
    t.boolean "show_condition", default: true, null: false
    t.string "condition", limit: 11, default: "new", null: false
    t.boolean "show_price", default: true, null: false
    t.boolean "indexed", default: false, null: false
    t.string "visibility", limit: 7, default: "both", null: false
    t.integer "cache_default_attribute", unsigned: true
    t.boolean "advanced_stock_management", default: false, null: false
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.integer "pack_stock_type", default: 3, null: false, unsigned: true
    t.index ["date_add", "active", "visibility"], name: "date_add"
    t.index ["id_category_default"], name: "id_category_default"
    t.index ["indexed", "active", "id_product"], name: "indexed"
  end

  create_table "ps_product_supplier", primary_key: "id_product_supplier", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_product_attribute", default: 0, null: false, unsigned: true
    t.integer "id_supplier", null: false, unsigned: true
    t.string "product_supplier_reference", limit: 32
    t.decimal "product_supplier_price_te", precision: 20, scale: 6, default: "0.0", null: false
    t.integer "id_currency", null: false, unsigned: true
    t.index ["id_product", "id_product_attribute", "id_supplier"], name: "id_product", unique: true
    t.index ["id_supplier", "id_product"], name: "id_supplier"
  end

  create_table "ps_product_tag", primary_key: ["id_product", "id_tag"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_tag", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.index ["id_lang", "id_tag"], name: "id_lang"
    t.index ["id_tag"], name: "id_tag"
  end

  create_table "ps_profile", primary_key: "id_profile", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
  end

  create_table "ps_profile_lang", primary_key: ["id_profile", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_profile", null: false, unsigned: true
    t.string "name", limit: 128, null: false
  end

  create_table "ps_quick_access", primary_key: "id_quick_access", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "new_window", default: false, null: false
    t.string "link", null: false
  end

  create_table "ps_quick_access_lang", primary_key: ["id_quick_access", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_quick_access", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 32, null: false
  end

  create_table "ps_range_price", primary_key: "id_range_price", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_carrier", null: false, unsigned: true
    t.decimal "delimiter1", precision: 20, scale: 6, null: false
    t.decimal "delimiter2", precision: 20, scale: 6, null: false
    t.index ["id_carrier", "delimiter1", "delimiter2"], name: "id_carrier", unique: true
  end

  create_table "ps_range_weight", primary_key: "id_range_weight", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_carrier", null: false, unsigned: true
    t.decimal "delimiter1", precision: 20, scale: 6, null: false
    t.decimal "delimiter2", precision: 20, scale: 6, null: false
    t.index ["id_carrier", "delimiter1", "delimiter2"], name: "id_carrier", unique: true
  end

  create_table "ps_reassurance", primary_key: "id_reassurance", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", null: false, unsigned: true
    t.string "file_name", limit: 100, null: false
  end

  create_table "ps_reassurance_lang", primary_key: ["id_reassurance", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_reassurance", null: false, unsigned: true, auto_increment: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "text", limit: 300, null: false
  end

  create_table "ps_referrer", primary_key: "id_referrer", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.string "passwd", limit: 32
    t.string "http_referer_regexp", limit: 64
    t.string "http_referer_like", limit: 64
    t.string "request_uri_regexp", limit: 64
    t.string "request_uri_like", limit: 64
    t.string "http_referer_regexp_not", limit: 64
    t.string "http_referer_like_not", limit: 64
    t.string "request_uri_regexp_not", limit: 64
    t.string "request_uri_like_not", limit: 64
    t.decimal "base_fee", precision: 5, scale: 2, default: "0.0", null: false
    t.decimal "percent_fee", precision: 5, scale: 2, default: "0.0", null: false
    t.decimal "click_fee", precision: 5, scale: 2, default: "0.0", null: false
    t.datetime "date_add", null: false
  end

  create_table "ps_referrer_cache", primary_key: ["id_connections_source", "id_referrer"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_connections_source", null: false, unsigned: true
    t.integer "id_referrer", null: false, unsigned: true
  end

  create_table "ps_referrer_shop", primary_key: ["id_referrer", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_referrer", null: false, unsigned: true, auto_increment: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "cache_visitors"
    t.integer "cache_visits"
    t.integer "cache_pages"
    t.integer "cache_registrations"
    t.integer "cache_orders"
    t.decimal "cache_sales", precision: 17, scale: 2
    t.decimal "cache_reg_rate", precision: 5, scale: 4
    t.decimal "cache_order_rate", precision: 5, scale: 4
  end

  create_table "ps_request_sql", primary_key: "id_request_sql", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 200, null: false
    t.text "sql", null: false
  end

  create_table "ps_required_field", primary_key: "id_required_field", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "object_name", limit: 32, null: false
    t.string "field_name", limit: 32, null: false
    t.index ["object_name"], name: "object_name"
  end

  create_table "ps_risk", primary_key: "id_risk", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "percent", limit: 1, null: false
    t.string "color", limit: 32
  end

  create_table "ps_risk_lang", primary_key: ["id_risk", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_risk", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 20, null: false
    t.index ["id_risk"], name: "id_risk"
  end

  create_table "ps_search_engine", primary_key: "id_search_engine", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "server", limit: 64, null: false
    t.string "getvar", limit: 16, null: false
  end

  create_table "ps_search_index", primary_key: ["id_word", "id_product"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_word", null: false, unsigned: true
    t.integer "weight", limit: 2, default: 1, null: false, unsigned: true
    t.index ["id_product", "weight"], name: "id_product"
  end

  create_table "ps_search_word", primary_key: "id_word", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "word", limit: 15, null: false
    t.index ["id_lang", "id_shop", "word"], name: "id_lang", unique: true
  end

  create_table "ps_sekeyword", primary_key: "id_sekeyword", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_shop_group", default: 1, null: false, unsigned: true
    t.string "keyword", limit: 256, null: false
    t.datetime "date_add", null: false
  end

  create_table "ps_shop", primary_key: "id_shop", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_shop_group", null: false
    t.string "name", limit: 64, null: false
    t.integer "id_category", null: false
    t.string "theme_name", null: false
    t.boolean "active", null: false
    t.boolean "deleted", null: false
    t.index ["id_shop_group"], name: "IDX_CBDFBB9EF5C9E40"
  end

  create_table "ps_shop_group", primary_key: "id_shop_group", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.boolean "share_customer", null: false
    t.boolean "share_order", null: false
    t.boolean "share_stock", null: false
    t.boolean "active", null: false
    t.boolean "deleted", null: false
  end

  create_table "ps_shop_url", primary_key: "id_shop_url", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", null: false, unsigned: true
    t.string "domain", limit: 150, null: false
    t.string "domain_ssl", limit: 150, null: false
    t.string "physical_uri", limit: 64, null: false
    t.string "virtual_uri", limit: 64, null: false
    t.boolean "main", null: false
    t.boolean "active", null: false
    t.index ["domain", "physical_uri", "virtual_uri"], name: "full_shop_url", unique: true
    t.index ["domain_ssl", "physical_uri", "virtual_uri"], name: "full_shop_url_ssl", unique: true
    t.index ["id_shop", "main"], name: "id_shop"
  end

  create_table "ps_smarty_cache", primary_key: "id_smarty_cache", id: :string, limit: 40, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 40, null: false
    t.string "cache_id", limit: 254
    t.timestamp "modified", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.text "content", limit: 4294967295, null: false
    t.index ["cache_id"], name: "cache_id"
    t.index ["modified"], name: "modified"
    t.index ["name"], name: "name"
  end

  create_table "ps_smarty_last_flush", primary_key: "type", id: :string, limit: 8, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.datetime "last_flush", null: false
  end

  create_table "ps_smarty_lazy_cache", primary_key: ["template_hash", "cache_id", "compile_id"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "template_hash", limit: 32, default: "", null: false
    t.string "cache_id", default: "", null: false
    t.string "compile_id", limit: 32, default: "", null: false
    t.string "filepath", default: "", null: false
    t.datetime "last_update", null: false
  end

  create_table "ps_specific_price", primary_key: "id_specific_price", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_specific_price_rule", null: false, unsigned: true
    t.integer "id_cart", null: false, unsigned: true
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_shop_group", null: false, unsigned: true
    t.integer "id_currency", null: false, unsigned: true
    t.integer "id_country", null: false, unsigned: true
    t.integer "id_group", null: false, unsigned: true
    t.integer "id_customer", null: false, unsigned: true
    t.integer "id_product_attribute", null: false, unsigned: true
    t.decimal "price", precision: 20, scale: 6, null: false
    t.integer "from_quantity", limit: 3, null: false, unsigned: true
    t.decimal "reduction", precision: 20, scale: 6, null: false
    t.boolean "reduction_tax", default: true, null: false
    t.string "reduction_type", limit: 10, null: false
    t.datetime "from", null: false
    t.datetime "to", null: false
    t.index ["from"], name: "from"
    t.index ["from_quantity"], name: "from_quantity"
    t.index ["id_cart"], name: "id_cart"
    t.index ["id_customer"], name: "id_customer"
    t.index ["id_product", "id_product_attribute", "id_customer", "id_cart", "from", "to", "id_shop", "id_shop_group", "id_currency", "id_country", "id_group", "from_quantity", "id_specific_price_rule"], name: "id_product_2", unique: true
    t.index ["id_product", "id_shop", "id_currency", "id_country", "id_group", "id_customer", "from_quantity", "from", "to"], name: "id_product"
    t.index ["id_product_attribute"], name: "id_product_attribute"
    t.index ["id_shop"], name: "id_shop"
    t.index ["id_specific_price_rule"], name: "id_specific_price_rule"
    t.index ["to"], name: "to"
  end

  create_table "ps_specific_price_priority", primary_key: ["id_specific_price_priority", "id_product"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_specific_price_priority", null: false, auto_increment: true
    t.integer "id_product", null: false
    t.string "priority", limit: 80, null: false
    t.index ["id_product"], name: "id_product", unique: true
  end

  create_table "ps_specific_price_rule", primary_key: "id_specific_price_rule", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_currency", null: false, unsigned: true
    t.integer "id_country", null: false, unsigned: true
    t.integer "id_group", null: false, unsigned: true
    t.integer "from_quantity", limit: 3, null: false, unsigned: true
    t.decimal "price", precision: 20, scale: 6
    t.decimal "reduction", precision: 20, scale: 6, null: false
    t.boolean "reduction_tax", default: true, null: false
    t.string "reduction_type", limit: 10, null: false
    t.datetime "from", null: false
    t.datetime "to", null: false
    t.index ["id_shop", "id_currency", "id_country", "id_group", "from_quantity", "from", "to"], name: "id_product"
  end

  create_table "ps_specific_price_rule_condition", primary_key: "id_specific_price_rule_condition", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_specific_price_rule_condition_group", null: false, unsigned: true
    t.string "type", null: false
    t.string "value", null: false
    t.index ["id_specific_price_rule_condition_group"], name: "id_specific_price_rule_condition_group"
  end

  create_table "ps_specific_price_rule_condition_group", primary_key: ["id_specific_price_rule_condition_group", "id_specific_price_rule"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_specific_price_rule_condition_group", null: false, unsigned: true, auto_increment: true
    t.integer "id_specific_price_rule", null: false, unsigned: true
  end

  create_table "ps_state", primary_key: "id_state", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_country", null: false, unsigned: true
    t.integer "id_zone", null: false, unsigned: true
    t.string "name", limit: 64, null: false
    t.string "iso_code", limit: 7, null: false
    t.integer "tax_behavior", limit: 2, default: 0, null: false
    t.boolean "active", default: false, null: false
    t.index ["id_country"], name: "id_country"
    t.index ["id_zone"], name: "id_zone"
    t.index ["name"], name: "name"
  end

  create_table "ps_statssearch", primary_key: "id_statssearch", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", default: 1, null: false, unsigned: true
    t.integer "id_shop_group", default: 1, null: false, unsigned: true
    t.string "keywords", null: false
    t.integer "results", default: 0, null: false
    t.datetime "date_add", null: false
  end

  create_table "ps_stock", primary_key: "id_stock", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_warehouse", null: false, unsigned: true
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_product_attribute", null: false, unsigned: true
    t.string "reference", limit: 32, null: false
    t.string "ean13", limit: 13
    t.string "isbn", limit: 32
    t.string "upc", limit: 12
    t.integer "physical_quantity", null: false, unsigned: true
    t.integer "usable_quantity", null: false, unsigned: true
    t.decimal "price_te", precision: 20, scale: 6, default: "0.0"
    t.index ["id_product"], name: "id_product"
    t.index ["id_product_attribute"], name: "id_product_attribute"
    t.index ["id_warehouse"], name: "id_warehouse"
  end

  create_table "ps_stock_available", primary_key: "id_stock_available", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_product_attribute", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.integer "id_shop_group", null: false, unsigned: true
    t.integer "quantity", default: 0, null: false
    t.integer "physical_quantity", default: 0, null: false
    t.integer "reserved_quantity", default: 0, null: false
    t.boolean "depends_on_stock", default: false, null: false, unsigned: true
    t.boolean "out_of_stock", default: false, null: false, unsigned: true
    t.index ["id_product", "id_product_attribute", "id_shop", "id_shop_group"], name: "product_sqlstock", unique: true
    t.index ["id_product"], name: "id_product"
    t.index ["id_product_attribute"], name: "id_product_attribute"
    t.index ["id_shop"], name: "id_shop"
    t.index ["id_shop_group"], name: "id_shop_group"
  end

  create_table "ps_stock_mvt", primary_key: "id_stock_mvt", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_stock", null: false
    t.integer "id_order"
    t.integer "id_supply_order"
    t.integer "id_stock_mvt_reason", null: false
    t.integer "id_employee", null: false
    t.string "employee_lastname", limit: 32
    t.string "employee_firstname", limit: 32
    t.integer "physical_quantity", null: false
    t.datetime "date_add", null: false
    t.integer "sign", limit: 2, default: 1, null: false
    t.decimal "price_te", precision: 20, scale: 6, default: "0.0"
    t.decimal "last_wa", precision: 20, scale: 6, default: "0.0"
    t.decimal "current_wa", precision: 20, scale: 6, default: "0.0"
    t.bigint "referer"
    t.index ["id_stock"], name: "id_stock"
    t.index ["id_stock_mvt_reason"], name: "id_stock_mvt_reason"
  end

  create_table "ps_stock_mvt_reason", primary_key: "id_stock_mvt_reason", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "sign", default: true, null: false
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.boolean "deleted", default: false, null: false, unsigned: true
  end

  create_table "ps_stock_mvt_reason_lang", primary_key: ["id_stock_mvt_reason", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_stock_mvt_reason", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", null: false
  end

  create_table "ps_store", primary_key: "id_store", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_country", null: false, unsigned: true
    t.integer "id_state", unsigned: true
    t.string "city", limit: 64, null: false
    t.string "postcode", limit: 12, null: false
    t.decimal "latitude", precision: 13, scale: 8
    t.decimal "longitude", precision: 13, scale: 8
    t.string "phone", limit: 16
    t.string "fax", limit: 16
    t.string "email", limit: 128
    t.boolean "active", default: false, null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
  end

  create_table "ps_store_lang", primary_key: ["id_store", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_store", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", null: false
    t.string "address1", null: false
    t.string "address2"
    t.text "hours"
    t.text "note"
  end

  create_table "ps_store_shop", primary_key: ["id_store", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_store", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_supplier", primary_key: "id_supplier", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.boolean "active", default: false, null: false
  end

  create_table "ps_supplier_lang", primary_key: ["id_supplier", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_supplier", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.text "description"
    t.string "meta_title", limit: 128
    t.string "meta_keywords"
    t.string "meta_description"
  end

  create_table "ps_supplier_shop", primary_key: ["id_supplier", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_supplier", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_supply_order", primary_key: "id_supply_order", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_supplier", null: false, unsigned: true
    t.string "supplier_name", limit: 64, null: false
    t.integer "id_lang", null: false, unsigned: true
    t.integer "id_warehouse", null: false, unsigned: true
    t.integer "id_supply_order_state", null: false, unsigned: true
    t.integer "id_currency", null: false, unsigned: true
    t.integer "id_ref_currency", null: false, unsigned: true
    t.string "reference", limit: 64, null: false
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
    t.datetime "date_delivery_expected"
    t.decimal "total_te", precision: 20, scale: 6, default: "0.0"
    t.decimal "total_with_discount_te", precision: 20, scale: 6, default: "0.0"
    t.decimal "total_tax", precision: 20, scale: 6, default: "0.0"
    t.decimal "total_ti", precision: 20, scale: 6, default: "0.0"
    t.decimal "discount_rate", precision: 20, scale: 6, default: "0.0"
    t.decimal "discount_value_te", precision: 20, scale: 6, default: "0.0"
    t.boolean "is_template", default: false
    t.index ["id_supplier"], name: "id_supplier"
    t.index ["id_warehouse"], name: "id_warehouse"
    t.index ["reference"], name: "reference"
  end

  create_table "ps_supply_order_detail", primary_key: "id_supply_order_detail", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_supply_order", null: false, unsigned: true
    t.integer "id_currency", null: false, unsigned: true
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_product_attribute", null: false, unsigned: true
    t.string "reference", limit: 32, null: false
    t.string "supplier_reference", limit: 32, null: false
    t.string "name", limit: 128, null: false
    t.string "ean13", limit: 13
    t.string "isbn", limit: 32
    t.string "upc", limit: 12
    t.decimal "exchange_rate", precision: 20, scale: 6, default: "0.0"
    t.decimal "unit_price_te", precision: 20, scale: 6, default: "0.0"
    t.integer "quantity_expected", null: false, unsigned: true
    t.integer "quantity_received", null: false, unsigned: true
    t.decimal "price_te", precision: 20, scale: 6, default: "0.0"
    t.decimal "discount_rate", precision: 20, scale: 6, default: "0.0"
    t.decimal "discount_value_te", precision: 20, scale: 6, default: "0.0"
    t.decimal "price_with_discount_te", precision: 20, scale: 6, default: "0.0"
    t.decimal "tax_rate", precision: 20, scale: 6, default: "0.0"
    t.decimal "tax_value", precision: 20, scale: 6, default: "0.0"
    t.decimal "price_ti", precision: 20, scale: 6, default: "0.0"
    t.decimal "tax_value_with_order_discount", precision: 20, scale: 6, default: "0.0"
    t.decimal "price_with_order_discount_te", precision: 20, scale: 6, default: "0.0"
    t.index ["id_product", "id_product_attribute"], name: "id_product_product_attribute"
    t.index ["id_product_attribute"], name: "id_product_attribute"
    t.index ["id_supply_order", "id_product"], name: "id_supply_order"
  end

  create_table "ps_supply_order_history", primary_key: "id_supply_order_history", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_supply_order", null: false, unsigned: true
    t.integer "id_employee", null: false, unsigned: true
    t.string "employee_lastname", limit: 32, default: ""
    t.string "employee_firstname", limit: 32, default: ""
    t.integer "id_state", null: false, unsigned: true
    t.datetime "date_add", null: false
    t.index ["id_employee"], name: "id_employee"
    t.index ["id_state"], name: "id_state"
    t.index ["id_supply_order"], name: "id_supply_order"
  end

  create_table "ps_supply_order_receipt_history", primary_key: "id_supply_order_receipt_history", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_supply_order_detail", null: false, unsigned: true
    t.integer "id_employee", null: false, unsigned: true
    t.string "employee_lastname", limit: 32, default: ""
    t.string "employee_firstname", limit: 32, default: ""
    t.integer "id_supply_order_state", null: false, unsigned: true
    t.integer "quantity", null: false, unsigned: true
    t.datetime "date_add", null: false
    t.index ["id_supply_order_detail"], name: "id_supply_order_detail"
    t.index ["id_supply_order_state"], name: "id_supply_order_state"
  end

  create_table "ps_supply_order_state", primary_key: "id_supply_order_state", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "delivery_note", default: false, null: false
    t.boolean "editable", default: false, null: false
    t.boolean "receipt_state", default: false, null: false
    t.boolean "pending_receipt", default: false, null: false
    t.boolean "enclosed", default: false, null: false
    t.string "color", limit: 32
  end

  create_table "ps_supply_order_state_lang", primary_key: ["id_supply_order_state", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_supply_order_state", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 128
  end

  create_table "ps_tab", primary_key: "id_tab", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_parent", null: false
    t.integer "position", null: false
    t.string "module", limit: 64
    t.string "class_name", limit: 64
    t.boolean "active", null: false
    t.boolean "hide_host_mode", null: false
    t.string "icon", limit: 32
  end

  create_table "ps_tab_advice", primary_key: ["id_tab", "id_advice"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_tab", null: false
    t.integer "id_advice", null: false
  end

  create_table "ps_tab_lang", primary_key: ["id_tab", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_tab", null: false
    t.integer "id_lang", null: false
    t.string "name", limit: 128, null: false
    t.index ["id_tab"], name: "IDX_CFD9262DED47AB56"
  end

  create_table "ps_tab_module_preference", primary_key: "id_tab_module_preference", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_employee", null: false
    t.integer "id_tab", null: false
    t.string "module", null: false
    t.index ["id_employee", "id_tab", "module"], name: "employee_module", unique: true
  end

  create_table "ps_tag", primary_key: "id_tag", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 32, null: false
    t.index ["id_lang"], name: "id_lang"
    t.index ["name"], name: "tag_name"
  end

  create_table "ps_tag_count", primary_key: ["id_group", "id_tag"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_group", default: 0, null: false, unsigned: true
    t.integer "id_tag", default: 0, null: false, unsigned: true
    t.integer "id_lang", default: 0, null: false, unsigned: true
    t.integer "id_shop", default: 0, null: false, unsigned: true
    t.integer "counter", default: 0, null: false, unsigned: true
    t.index ["id_group", "id_lang", "id_shop", "counter"], name: "id_group"
  end

  create_table "ps_tax", primary_key: "id_tax", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "rate", precision: 10, scale: 3, null: false
    t.boolean "active", default: true, null: false, unsigned: true
    t.boolean "deleted", default: false, null: false, unsigned: true
  end

  create_table "ps_tax_lang", primary_key: ["id_tax", "id_lang"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_tax", null: false, unsigned: true
    t.integer "id_lang", null: false, unsigned: true
    t.string "name", limit: 32, null: false
  end

  create_table "ps_tax_rule", primary_key: "id_tax_rule", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_tax_rules_group", null: false
    t.integer "id_country", null: false
    t.integer "id_state", null: false
    t.string "zipcode_from", limit: 12, null: false
    t.string "zipcode_to", limit: 12, null: false
    t.integer "id_tax", null: false
    t.integer "behavior", null: false
    t.string "description", limit: 100, null: false
    t.index ["id_tax"], name: "id_tax"
    t.index ["id_tax_rules_group", "id_country", "id_state", "zipcode_from"], name: "category_getproducts"
    t.index ["id_tax_rules_group"], name: "id_tax_rules_group"
  end

  create_table "ps_tax_rules_group", primary_key: "id_tax_rules_group", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "active", null: false
    t.boolean "deleted", null: false, unsigned: true
    t.datetime "date_add", null: false
    t.datetime "date_upd", null: false
  end

  create_table "ps_tax_rules_group_shop", primary_key: ["id_tax_rules_group", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_tax_rules_group", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_timezone", primary_key: "id_timezone", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 32, null: false
  end

  create_table "ps_translation", primary_key: "id_translation", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "id_lang", null: false
    t.text "key", null: false
    t.text "translation", null: false
    t.string "domain", limit: 80, null: false
    t.string "theme", limit: 32
    t.index ["domain"], name: "key"
    t.index ["id_lang"], name: "IDX_ADEBEB36BA299860"
  end

  create_table "ps_warehouse", primary_key: "id_warehouse", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_currency", null: false, unsigned: true
    t.integer "id_address", null: false, unsigned: true
    t.integer "id_employee", null: false, unsigned: true
    t.string "reference", limit: 32
    t.string "name", limit: 45, null: false
    t.string "management_type", limit: 4, default: "WA", null: false
    t.boolean "deleted", default: false, null: false, unsigned: true
  end

  create_table "ps_warehouse_carrier", primary_key: ["id_warehouse", "id_carrier"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_carrier", null: false, unsigned: true
    t.integer "id_warehouse", null: false, unsigned: true
    t.index ["id_carrier"], name: "id_carrier"
    t.index ["id_warehouse"], name: "id_warehouse"
  end

  create_table "ps_warehouse_product_location", primary_key: "id_warehouse_product_location", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_product", null: false, unsigned: true
    t.integer "id_product_attribute", null: false, unsigned: true
    t.integer "id_warehouse", null: false, unsigned: true
    t.string "location", limit: 64
    t.index ["id_product", "id_product_attribute", "id_warehouse"], name: "id_product", unique: true
  end

  create_table "ps_warehouse_shop", primary_key: ["id_warehouse", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_shop", null: false, unsigned: true
    t.integer "id_warehouse", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
    t.index ["id_warehouse"], name: "id_warehouse"
  end

  create_table "ps_web_browser", primary_key: "id_web_browser", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64
  end

  create_table "ps_webservice_account", primary_key: "id_webservice_account", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "key", limit: 32, null: false
    t.text "description"
    t.string "class_name", limit: 50, default: "WebserviceRequest", null: false
    t.integer "is_module", limit: 1, default: 0, null: false
    t.string "module_name", limit: 50
    t.integer "active", limit: 1, null: false
    t.index ["key"], name: "key"
  end

  create_table "ps_webservice_account_shop", primary_key: ["id_webservice_account", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_webservice_account", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

  create_table "ps_webservice_permission", primary_key: "id_webservice_permission", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "resource", limit: 50, null: false
    t.string "method", limit: 6, null: false
    t.integer "id_webservice_account", null: false
    t.index ["id_webservice_account"], name: "id_webservice_account"
    t.index ["method"], name: "method"
    t.index ["resource", "method", "id_webservice_account"], name: "resource_2", unique: true
    t.index ["resource"], name: "resource"
  end

  create_table "ps_zone", primary_key: "id_zone", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.boolean "active", default: false, null: false, unsigned: true
  end

  create_table "ps_zone_shop", primary_key: ["id_zone", "id_shop"], options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_zone", null: false, unsigned: true
    t.integer "id_shop", null: false, unsigned: true
    t.index ["id_shop"], name: "id_shop"
  end

end
